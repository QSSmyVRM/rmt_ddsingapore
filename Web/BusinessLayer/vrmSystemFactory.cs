//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End ZD 100886
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.ComponentModel;
using System.Xml;
using System.Data;  
using NHibernate;
using NHibernate.Criterion;
using System.Linq;
using System.Xml.Linq;

using log4net;

using myVRM.DataLayer;
using ns_SqlHelper; //ZD 100526

namespace myVRM.BusinessLayer
{
    public class vrmSystemFactory
    {
        private SystemDAO m_systemDAO;

        private static log4net.ILog m_log;
        private string m_configPath;

        private ISystemDAO m_IsystemDAO;
        private ISysMailDAO m_ISysMailDAO; //Organization Module
        private ILDAPConfigDAO m_ILDAPConfigDAO; //FB 2027 - SetSuperAdmin
        private ISysTimeZonePrefDAO m_ISysTimeZonePrefDAO;
        private orgDAO m_orgDAO;
        private IOrgDAO m_IOrgDAO;
        private IOrgSettingsDAO m_IOrgSettingsDAO;
        private static double deActTime = 120 * 60; //Activation module
        private imageDAO m_imageDAO;    //System Logo...
        private IImageDAO m_IImageDAO;
        private vrmFactory m_vrmFactory;//System Logo...
        private imageFactory m_imageFactory; //FB 2136
        private myVRMException myvrmEx; //FB 2027
        private emailFactory m_vrmEmailFact; //FB 2363
        private ReportFactory m_vrmReportFact; //FB 2363
        private myVRMSearch m_Search; //FB 3069
        //FB 2027 Start
        private userDAO m_userDAO;
        private IUserDao m_IuserDao;
		public IvrmErrorLogDAO m_IvrmErrLog; 
        private errorlogDAO m_errorDAO;

        private IExternalSchedulingDAO m_ESDAO; //FB 2363
        private IESMailUsrRptSettingsDAO m_ESMailUsrRptSetDAO; //FB 2363
        private IsysWhygoSettings m_WhygoSettingsDAO; //FB 2392
        private ns_SqlHelper.SqlHelper m_sysLayer = null; //FB 2594
        private OrganizationFactory m_OrgFactory; //FB 2594
        private IOrgSettingsDAO m_IOrgSettingDAO;// FB 2719 Theme
		private IDefaultLicenseDAO m_IDefaultLicense; //FB 2659
        private UtilFactory m_utilFactory; //ZD 100288

        private static int m_PAGE_SIZE = 20;
		//FB 2027 End
        internal int remRooms = 0;
        internal int remNonVideoRooms = 0;
        internal int remVideoRooms = 0;
        internal int remUsers = 0;
        internal int remMCU = 0;
        internal int remEndpoints = 0;
        internal int remFacilities = 0;
        internal int remCatering = 0;
        internal int remHouseKeeping = 0;
        internal int remAPI = 0;
        internal int remExchangeUsers = 0;
        internal int remDominoUsers = 0;
        internal int remMobileUsers = 0, remWebexusr = 0;//ZD 100221 //FB 1979
        //internal int remPC = 0;//FB 2347 //FB 2693
        internal int remExtRoom = 0;//FB 2426
        internal int remEnchancedMCU = 0;//FB 2486
        internal int remVMRRooms = 0;//FB 2586
        internal int EnablePublicRooms = 0; //FB 2594
		internal int EnableCloud = 0;//FB 2599
        internal int remBJNUsers = 0; //ZD 104021
        internal int remPCUsers, remJabber, remLync, remVidtel = 0; //FB 2693
		internal int remVCHotRooms = 0;//FB 2694
        internal int remROHotRooms = 0;//FB 2694
        internal int remAdvancedReport = 0;//FB 2593
        internal int remiControlRooms = 0;//ZD 101098
        internal int isLDAP = 0;//ZD 101443
        internal int EnableBJNUsr = 0;//ZD 104225
        private SqlHelper sqlCon = null; //ZD 100526

        #region overloaded constructor
        public vrmSystemFactory(ref vrmDataObject obj)
        {
            try
            {
                m_log = obj.log;
                m_configPath = obj.ConfigPath;
                m_systemDAO = new SystemDAO(m_configPath, m_log);
                m_IsystemDAO = m_systemDAO.GetSystemDao();
                m_ISysTimeZonePrefDAO = m_systemDAO.GetSysTimeZonePrefDao();
                m_orgDAO = new orgDAO(m_configPath, m_log); //organization module
                
                m_IOrgDAO = m_orgDAO.GetOrgDao();
                m_IOrgSettingsDAO = m_orgDAO.GetOrgSettingsDao();
                m_ISysMailDAO = m_systemDAO.GetSysMailDao();  //Organization Module
                m_ILDAPConfigDAO = m_orgDAO.GetLDAPConfigDao(); // FB 2027 //ZD 101443
                m_imageDAO = new imageDAO(m_configPath, m_log); //System Logo...
                m_IImageDAO = m_imageDAO.GetImageDao();
				//FB 2027 Start
                m_userDAO = new userDAO(obj.ConfigPath, obj.log); //FB 2027
                m_IuserDao = m_userDAO.GetUserDao();
				m_errorDAO = new errorlogDAO(m_configPath, m_log);
                m_IvrmErrLog = m_errorDAO.GetErrorLogDAO();
                //FB 2027 End

                m_ESDAO = m_systemDAO.GetExternalSchedulingDAO(); // FB 2363
                m_ESMailUsrRptSetDAO = m_systemDAO.GetESMailUsrRptSettingsDAO(); // FB 2363
                m_WhygoSettingsDAO = m_systemDAO.GetsysWhygoSettingsDAO();//FB 2392
                m_sysLayer = new ns_SqlHelper.SqlHelper(m_configPath);
                m_IOrgSettingDAO = m_orgDAO.GetOrgSettingsDao();// FB 2719 Theme
				m_IDefaultLicense = m_systemDAO.GetDefaultLicenseDao(); //FB 2659
                m_Search = new myVRMSearch(obj.ConfigPath, obj.log); //FB 3069
                m_utilFactory = new UtilFactory(ref obj);//ZD 100288
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region Get System License Remaining Details
        /// <summary>
        /// Get System License Remaining Details
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        internal bool GetRemainingLicense()
        {
            int assignedRooms = 0;
            int assignedNVRooms = 0;
            int assignedVRooms = 0;
            int assignedUsers = 0;
            int assignedMCU = 0;
            int assignedEndPoints = 0;
            int assFacilities = 0;
            int assCatering = 0;
            int assHouseKeeping = 0;
            int assAPIs = 0;
            int assExchangeUsers = 0;
            int assDominoUsers = 0;
            int assMobileUsers = 0, assWebexUsers = 0;//ZD 100221 //FB 1979
            int assBJNUsers = 0; //ZD 104021
            //int assPC = 0;//FB 2347 //FB 2693
            int assignedExtRooms = 0;//FB 2426
            int assignedMCUEnchanced = 0;
            int assignedVMRRooms = 0;//FB 2586
            int assPCUsers = 0, assJabber = 0, assLync = 0; //FB 2693
			int assignedVCHotRooms = 0;//FB 2694
            int assignedROHotRooms = 0;//FB 2694
            int assAdvreport = 0;//FB 2593
            int assiControlRooms = 0;//ZD 101098
            try
            {
                remRooms = sysSettings.RoomLimit;
                remNonVideoRooms = sysSettings.MaxNVidRooms;
                remVideoRooms = sysSettings.MaxVidRooms;
                remUsers = sysSettings.UserLimit;
                remMCU = sysSettings.MCULimit;
                remEndpoints = sysSettings.MaxEndPts;
                remFacilities = sysSettings.MaxFacilities;
                remCatering = sysSettings.MaxCatering;
                remHouseKeeping = sysSettings.MaxHousekeeping;
                remAPI = sysSettings.MaxAPIs;
                remExchangeUsers = sysSettings.MaxExcUsrs;
                remDominoUsers = sysSettings.MaxDomUsrs;
                remMobileUsers = sysSettings.MaxMobUsrs; //FB 1979
                remWebexusr = sysSettings.MaxWebexUsr; //ZD 100221

                //ZD 104021 
                if(sysSettings.MaxBlueJeansUsers > 0) //if unlimited BJN user license
                    remBJNUsers = sysSettings.MaxBlueJeansUsers; 

                //remPC = sysSettings.MaxPCModule; //FB 2347 //FB 2693
                remExtRoom = sysSettings.ExtRoomLimit; //FB 2426
                remEnchancedMCU = sysSettings.MCUEnchancedLimit;//FB 2486
                remVMRRooms = sysSettings.MaxVMRooms;//FB 2586
                //FB 2693 Starts
                remPCUsers = sysSettings.MaxPCUsers;
                remJabber = sysSettings.MaxJabber;
                remLync = sysSettings.MaxLync;
                //remVidtel = sysSettings.MaxVidtel; //ZD 102004
                //FB 2693 Ends
                remAdvancedReport = sysSettings.MaxAdvancedReport;//FB 2593
                remiControlRooms = sysSettings.MaxiControlRooms;//ZD 101098
                isLDAP = sysSettings.IsLDAP;//ZD 101443
                
                List<int> orgIDs = new List<int>();
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("deleted", 0));

                List<vrmOrganization> OrgList = m_IOrgDAO.GetByCriteria(criterionList);
                if (OrgList != null)
                {
                    foreach (vrmOrganization vrmOrg in OrgList)
                    {
                        orgIDs.Add(vrmOrg.orgId);
                    }
                }

                criterionList = null;
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.In("OrgId", orgIDs));
                List<OrgData> orgDts = m_IOrgSettingsDAO.GetByCriteria(criterionList);

                if (orgDts != null)
                {
                    foreach (OrgData orgdt in orgDts)
                    {
                        assignedRooms = assignedRooms + orgdt.RoomLimit;
                        assignedExtRooms = assignedExtRooms + orgdt.GuestRoomLimit;//FB 2426
                        assignedNVRooms = assignedNVRooms + orgdt.MaxNonVideoRooms;
                        assignedVRooms = assignedVRooms + orgdt.MaxVideoRooms;
                        assignedUsers = assignedUsers + orgdt.UserLimit;
                        assExchangeUsers = assExchangeUsers + orgdt.ExchangeUserLimit;
                        assDominoUsers = assDominoUsers + orgdt.DominoUserLimit;
                        assMobileUsers = assMobileUsers + orgdt.MobileUserLimit; //FB 1979
                        assWebexUsers = assWebexUsers + orgdt.WebexUserLimit; //ZD 100221
                        assBJNUsers = assBJNUsers + orgdt.BlueJeansUserLimit; //ZD 104021
                        assignedMCU = assignedMCU + orgdt.MCULimit;
                        assignedEndPoints = assignedEndPoints + orgdt.MaxEndpoints;
                        assignedMCUEnchanced = assignedMCUEnchanced + orgdt.MCUEnchancedLimit;//FB 2486
                        assignedVMRRooms = assignedVMRRooms + orgdt.MaxVMRRooms;//FB 2586
                        assPCUsers = assPCUsers + orgdt.PCUserLimit; //FB 2693
						assignedVCHotRooms = assignedVCHotRooms + orgdt.MaxVCHotdesking;//FB 2694
                        assignedROHotRooms = assignedROHotRooms + orgdt.MaxROHotdesking;//FB 2694
                        assiControlRooms = assiControlRooms + orgdt.MaxiControlRooms;//ZD 101098

                        if (orgdt.EnableFacilities == 1)
                            assFacilities += 1;

                        if (orgdt.EnableCatering == 1)
                            assCatering += 1;

                        if (orgdt.EnableHousekeeping == 1)
                            assHouseKeeping += 1;

                        if (orgdt.EnableAPI == 1)
                            assAPIs += 1;
                        
                        if (orgdt.EnableLync == 1)
                            assLync += 1;
                        //ZD 102004
                        if (orgdt.EnableJabber == 1)
                            assJabber += 1;
                        //FB 2693 Ends

                        if(orgdt.EnableAdvancedReport == 1)//FB 2593
                            assAdvreport += 1;
                    }
                }
                remRooms = remRooms - assignedRooms;
                remVideoRooms = remVideoRooms - assignedVRooms;
                remNonVideoRooms = remNonVideoRooms - assignedNVRooms;
                remUsers = remUsers - assignedUsers;
                remExchangeUsers = remExchangeUsers - assExchangeUsers;
                remDominoUsers = remDominoUsers - assDominoUsers;
                remMobileUsers = remMobileUsers - assMobileUsers; //FB 1979
                remWebexusr = remWebexusr - assWebexUsers; //ZD 100221
                //ZD 104021
                if (sysSettings.MaxBlueJeansUsers > 0) //if not unlimited
                    remBJNUsers = remBJNUsers - assBJNUsers; 

                remMCU = remMCU - assignedMCU;
                remEndpoints = remEndpoints - assignedEndPoints;
                remFacilities = remFacilities - assFacilities;
                remCatering = remCatering - assCatering;
                remHouseKeeping = remHouseKeeping - assHouseKeeping;
                remAPI = remAPI - assAPIs;
                //remPC = remPC - assPC; //FB 2347 //FB 2693
                remExtRoom = remExtRoom - assignedExtRooms;//FB 2426
                remEnchancedMCU = remEnchancedMCU - assignedMCUEnchanced;//FB 2486
                remVMRRooms = remVMRRooms - assignedVMRRooms;//FB 2586
                //FB 2693 Starts
                remPCUsers = remPCUsers - assPCUsers;
                remJabber = remJabber - assJabber;
                remLync = remLync - assLync;
                //remVidtel = remVidtel - assVidtel; //ZD 102004
                //FB 2693 Ends
				remVCHotRooms = remVCHotRooms - assignedVCHotRooms; //FB 2694
                remROHotRooms = remROHotRooms - assignedROHotRooms; //FB 2694
                remAdvancedReport = remAdvancedReport - assAdvreport;//FB 2593
                remiControlRooms = remiControlRooms - assiControlRooms;//ZD 101098
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion
        
        #region Get System License Details
        /// <summary>
        /// Get System License Details
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetLicenseDetails(ref vrmDataObject obj)
        {
            int assignedRooms = 0;
            int assignedUsers = 0;
            int assignedMCU = 0;
            int assignedNVRooms = 0;
            int assignedVRooms = 0;
            int assignedEndPoints = 0;
            int assFacilities = 0;
            int assCatering = 0;
            int assHouseKeeping = 0;
            int assAPIs = 0;
            int assExchangeUsers = 0;
            int assDominoUsers = 0;
            int assMobileUsers = 0; //FB 1979
            int assWebexUsers = 0; //ZD 100221
            int assBJNUsers = 0; //ZD 104021
            //int assPCs =0; //FB 2347 //FB 2693
            int assignedExtRooms = 0;//FB 2426
			int assignedMCUEnchanced = 0;//FB 2486
			int assignedVMRRooms = 0;//FB 2586
            int assPCUsers = 0, assJabber = 0, assLync = 0, assVidtel = 0; //FB 2693
			int assignedVCHotRooms = 0;//FB 2694
            int assignedROHotRooms = 0;//FB 2694
            int assAdvreport = 0;//FB 2593
            int assiControlRooms = 0;//ZD 101098
            try
            {
                remRooms = sysSettings.RoomLimit;
                remUsers = sysSettings.UserLimit;
                remExchangeUsers = sysSettings.MaxExcUsrs;
                remDominoUsers = sysSettings.MaxDomUsrs;
                remMobileUsers = sysSettings.MaxMobUsrs; //FB 1979
                remWebexusr = sysSettings.MaxWebexUsr; //ZD 100221
                //ZD 104021
                if(sysSettings.MaxBlueJeansUsers >= 0)
                    remBJNUsers = sysSettings.MaxBlueJeansUsers;

                remMCU = sysSettings.MCULimit;
                remNonVideoRooms = sysSettings.MaxNVidRooms;
                remVideoRooms = sysSettings.MaxVidRooms;
                remEndpoints = sysSettings.MaxEndPts;
                remFacilities = sysSettings.MaxFacilities;
                remCatering = sysSettings.MaxCatering;
                remHouseKeeping = sysSettings.MaxHousekeeping;
                remAPI = sysSettings.MaxAPIs;
                //remPC = sysSettings.MaxPCModule; //FB 2347 //FB 2693
                remExtRoom = sysSettings.ExtRoomLimit;//FB 2426
				remEnchancedMCU = sysSettings.MCUEnchancedLimit;//FB 2486
                remVMRRooms = sysSettings.MaxVMRooms;//FB 2586
                //FB 2693 Starts
                remPCUsers = sysSettings.MaxPCUsers;
                remJabber = sysSettings.MaxJabber;
                remLync = sysSettings.MaxLync;
                //remVidtel = sysSettings.MaxVidtel; //ZD 102004
                //FB 2693 Ends
				remVCHotRooms = sysSettings.MaxVCHotdesking; //FB 2694
                remROHotRooms = sysSettings.MaxROHotdesking; //FB 2694
                remAdvancedReport = sysSettings.MaxAdvancedReport;//FB 2593
                remiControlRooms = sysSettings.MaxiControlRooms;//ZD 101098
                
                List<int> orgIDs = new List<int>();
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("deleted", 0));

                List<vrmOrganization> OrgList = m_IOrgDAO.GetByCriteria(criterionList);
                if (OrgList != null)
                {
                    foreach (vrmOrganization vrmOrg in OrgList)
                    {
                        orgIDs.Add(vrmOrg.orgId);
                    }
                }

                criterionList = null;
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.In("OrgId", orgIDs));
                List<OrgData> orgDts = m_IOrgSettingsDAO.GetByCriteria(criterionList);

                if (orgDts != null)
                {
                    foreach (OrgData orgdt in orgDts)
                    {
                        assignedRooms = assignedRooms + orgdt.RoomLimit;
                        assignedUsers = assignedUsers + orgdt.UserLimit;
                        assExchangeUsers = assExchangeUsers + orgdt.ExchangeUserLimit;
                        assDominoUsers = assDominoUsers + orgdt.DominoUserLimit;
                        assMobileUsers = assMobileUsers + orgdt.MobileUserLimit; //FB 1979
                        assWebexUsers = assWebexUsers + orgdt.WebexUserLimit; //ZD 100221
                        if (sysSettings.MaxBlueJeansUsers >= 0)
                            assBJNUsers = assBJNUsers + orgdt.BlueJeansUserLimit; //ZD 104021

                        assignedMCU = assignedMCU + orgdt.MCULimit;
                        assignedExtRooms = assignedExtRooms + orgdt.GuestRoomLimit;//FB 2426
                        assignedNVRooms = assignedNVRooms + orgdt.MaxNonVideoRooms;
                        assignedVRooms = assignedVRooms + orgdt.MaxVideoRooms;
                        assignedEndPoints = assignedEndPoints + orgdt.MaxEndpoints;
                        assignedMCUEnchanced = assignedMCUEnchanced + orgdt.MCUEnchancedLimit;//FB 2486
                        assignedVMRRooms = assignedVMRRooms + orgdt.MaxVMRRooms;//FB 2586
                        assPCUsers = assPCUsers + orgdt.PCUserLimit; //FB 2693
						assignedVCHotRooms = assignedVCHotRooms + orgdt.MaxVCHotdesking;//FB 2694
                        assignedROHotRooms = assignedROHotRooms + orgdt.MaxROHotdesking;//FB 2694
                        assiControlRooms = assiControlRooms + orgdt.MaxiControlRooms;//ZD 101098

                        if (orgdt.EnableFacilities == 1)
                            assFacilities += 1;

                        if (orgdt.EnableCatering == 1)
                            assCatering += 1;

                        if (orgdt.EnableHousekeeping == 1)
                            assHouseKeeping += 1;

                        if (orgdt.EnableAPI == 1)
                            assAPIs += 1;

                        //if (orgdt.EnablePCModule == 1) //FB 2347 //FB 2693
                        //    assPCs += 1;

                        //FB 2693 Starts
                        if (orgdt.EnableLync == 1)
                            assLync += 1;
                        //ZD 102004
                        //if (orgdt.EnableVidtel == 1)
                        //    assVidtel += 1;

                        if (orgdt.EnableJabber == 1)
                            assJabber += 1;
                        //FB 2693 Ends

                        if (orgdt.EnableAdvancedReport == 1)//FB 2593
                            assAdvreport += 1;

                    }
                }
                remRooms = remRooms - assignedRooms;
                remVideoRooms = remVideoRooms - assignedVRooms;
                remNonVideoRooms = remNonVideoRooms - assignedNVRooms;
                remUsers = remUsers - assignedUsers;
                remExchangeUsers = remExchangeUsers - assExchangeUsers;
                remDominoUsers = remDominoUsers - assDominoUsers;
                remMobileUsers = remMobileUsers - assMobileUsers; //FB 1979
                remWebexusr = remWebexusr - assWebexUsers; //ZD 100221
                if (sysSettings.MaxBlueJeansUsers >= 0)
                    remBJNUsers = remBJNUsers - assBJNUsers; //ZD 104021
                remMCU = remMCU - assignedMCU;
                remEndpoints = remEndpoints - assignedEndPoints;
                remFacilities = remFacilities - assFacilities;
                remCatering = remCatering - assCatering;
                remHouseKeeping = remHouseKeeping - assHouseKeeping;
                remAPI = remAPI - assAPIs;
                //remPC = remPC - assPCs; //FB 2347 //FB 2693
                remExtRoom = remExtRoom - assignedExtRooms;//FB 2426
                remEnchancedMCU = remEnchancedMCU - assignedMCUEnchanced;//FB 2486
                remVMRRooms = remVMRRooms - assignedVMRRooms;//FB 2586
                //FB 2693 Starts
                remPCUsers = remPCUsers - assPCUsers;
                remJabber = remJabber - assJabber;
                remLync = remLync - assLync;
                //remVidtel = remVidtel - assVidtel; //ZD 102004
                //FB 2693 Ends
				remVCHotRooms = remVCHotRooms - assignedVCHotRooms;//FB 2694
                remROHotRooms = remROHotRooms - assignedROHotRooms;//FB 2694
                remAdvancedReport = remAdvancedReport - assAdvreport;//FB 2593
                remiControlRooms = remiControlRooms - assiControlRooms;//ZD 101098
                
                obj.outXml = "<GetLicenseDetails>";
                obj.outXml += "<MaxOrganizations>" + sysSettings.MaxOrganizations + "</MaxOrganizations>"; //FB 1639
                obj.outXml += "<Cloud>" + sysSettings.Cloud + "</Cloud>";//FB 2262 - J //FB 2599
                obj.outXml += "<EnablePublicRooms>" + sysSettings.EnablePublicRooms + "</EnablePublicRooms>"; //FB 2594
                obj.outXml += "<MaxRoomLimit>" + sysSettings.RoomLimit + "</MaxRoomLimit>";
                obj.outXml += "<MaxVideoRooms>" + sysSettings.MaxVidRooms + "</MaxVideoRooms>";
                obj.outXml += "<MaxNonVideoRooms>" + sysSettings.MaxNVidRooms + "</MaxNonVideoRooms>";
                obj.outXml += "<MaxVMRRooms>" + sysSettings.MaxVMRooms + "</MaxVMRRooms>";//FB 2586
                obj.outXml += "<MaxiControlRooms>" + sysSettings.MaxiControlRooms + "</MaxiControlRooms>";//ZD 101098
                obj.outXml += "<MaxGuestRooms>" + sysSettings.ExtRoomLimit + "</MaxGuestRooms>";//FB 2426
                obj.outXml += "<MaxMCULimit>" + sysSettings.MCULimit + "</MaxMCULimit>";

                //FB 2694 Starts
                obj.outXml += "<MaxROHotdesking>" + sysSettings.MaxROHotdesking + "</MaxROHotdesking>";
                obj.outXml += "<MaxVCHotdesking>" + sysSettings.MaxVCHotdesking + "</MaxVCHotdesking>";
                //FB 2694 Ends

                obj.outXml += "<MaxMCUEnchancedLimit>" + sysSettings.MCUEnchancedLimit + "</MaxMCUEnchancedLimit>";//FB 2486
                obj.outXml += "<MaxEndpoints>" + sysSettings.MaxEndPts + "</MaxEndpoints>";
                obj.outXml += "<MaxUserLimit>" + sysSettings.UserLimit + "</MaxUserLimit>";
                obj.outXml += "<MaxExchangeUsers>" + sysSettings.MaxExcUsrs + "</MaxExchangeUsers>";
                obj.outXml += "<MaxDominoUsers>" + sysSettings.MaxDomUsrs + "</MaxDominoUsers>";
                obj.outXml += "<MaxMobileUsers>" + sysSettings.MaxMobUsrs + "</MaxMobileUsers>"; //FB 1979
                obj.outXml += "<MaxWebexUsers>" + sysSettings.MaxWebexUsr + "</MaxWebexUsers>"; //ZD 100221
                obj.outXml += "<MaxBlueJeansUsers>" + sysSettings.MaxBlueJeansUsers + "</MaxBlueJeansUsers>"; //ZD 104021
                obj.outXml += "<MaxFacilities>" + sysSettings.MaxFacilities + "</MaxFacilities>";
                obj.outXml += "<MaxCatering>" + sysSettings.MaxCatering + "</MaxCatering>";
                obj.outXml += "<MaxHousekeeping>" + sysSettings.MaxHousekeeping + "</MaxHousekeeping>";
                obj.outXml += "<MaxAPIs>" + sysSettings.MaxAPIs + "</MaxAPIs>";
                //obj.outXml += "<MaxPCModule>" + sysSettings.MaxPCModule + "</MaxPCModule>"; //FB 2347 //FB 2693
                //obj.outXml += "<MaxGroupsPerAccount>" + sysSettings.MaxGroups + "</MaxGroupsPerAccount>"; - Depricated as per new license
                //obj.outXml += "<MaxTemplatesPerAccount>" + sysSettings.MaxTemplates + "</MaxTemplatesPerAccount>";
                obj.outXml += "<MaxGuestsPerAccount>" + sysSettings.MaxGuests + "</MaxGuestsPerAccount>";
                //FB 2693 Starts
                obj.outXml += "<MaxPCUsers>" + sysSettings.MaxPCUsers + "</MaxPCUsers>";
                obj.outXml += "<MaxBlueJeans></MaxBlueJeans>"; //ZD 104021
                obj.outXml += "<MaxJabber>" + sysSettings.MaxJabber + "</MaxJabber>";
                obj.outXml += "<MaxLync>" + sysSettings.MaxLync + "</MaxLync>";
                obj.outXml += "<MaxVidtel>" + sysSettings.MaxVidtel + "</MaxVidtel>";
                //FB 2693 Ends
                obj.outXml += "<MaxAdvancedReport>" + sysSettings.MaxAdvancedReport + "</MaxAdvancedReport>";//FB 2593
                obj.outXml += "<RemainingRooms>" + remRooms + "</RemainingRooms>";
                obj.outXml += "<RemVideoRooms>" + remVideoRooms + "</RemVideoRooms>";
                obj.outXml += "<RemNonVideoRooms>" + remNonVideoRooms + "</RemNonVideoRooms>";
                obj.outXml += "<RemVMRRooms>" + remVMRRooms + "</RemVMRRooms>";//FB 2586
                obj.outXml += "<RemiControlRooms>" + remiControlRooms + "</RemiControlRooms>";//ZD 101098
                obj.outXml += "<RemainingMCUs>" + remMCU + "</RemainingMCUs>";
                obj.outXml += "<RemainingEnchancedMCUs>" + remEnchancedMCU + "</RemainingEnchancedMCUs>";//FB 2486
                obj.outXml += "<RemainingUsers>" + remUsers + "</RemainingUsers>";
                obj.outXml += "<RemExchangeUsers>" + remExchangeUsers + "</RemExchangeUsers>";
                obj.outXml += "<RemDominoUsers>" + remDominoUsers + "</RemDominoUsers>";
                obj.outXml += "<RemMobileUsers>" + remMobileUsers + "</RemMobileUsers>"; //FB 1979
                obj.outXml += "<RemWebexUsers>" + remWebexusr + "</RemWebexUsers>"; //ZD 100221
                obj.outXml += "<RemBlueJeans>" + remBJNUsers + "</RemBlueJeans>"; //ZD 104021
                obj.outXml += "<RemainingEndPoints>" + remEndpoints + "</RemainingEndPoints>";
                obj.outXml += "<RemFacilities>" + remFacilities + "</RemFacilities>";
                obj.outXml += "<RemCatering>" + remCatering + "</RemCatering>";
                obj.outXml += "<RemHouseKeeping>" + remHouseKeeping + "</RemHouseKeeping>";
                obj.outXml += "<RemAPI>" + remAPI + "</RemAPI>";
                //obj.outXml += "<RemPC>" + remPC + "</RemPC>"; //FB 2347 //FB 2693
                obj.outXml += "<RemainingGuestRooms>" + remExtRoom + "</RemainingGuestRooms>";//FB 2426
                //FB 2693 Starts
                obj.outXml += "<RemPCUsers>" + remPCUsers + "</RemPCUsers>";
                obj.outXml += "<RemJabber>" + remJabber + "</RemJabber>";
                obj.outXml += "<RemLync>" + remLync + "</RemLync>"; 
                obj.outXml += "<RemVidtel>" + remVidtel + "</RemVidtel>";
                //FB 2693 Ends
                obj.outXml += "<RemVCHotRooms>" + remVCHotRooms + "</RemVCHotRooms>";//FB 2694
                obj.outXml += "<RemROHotRooms>" + remROHotRooms + "</RemROHotRooms>";//FB 2694
                obj.outXml += "<RemAdvancedReport>" + remAdvancedReport + "</RemAdvancedReport>";//FB 2593
                obj.outXml += "</GetLicenseDetails>";
                
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                return false;
            }
        }
        #endregion

        #region GetActivation
        /// <summary>
        /// GetActivation
        /// Method added for Activation module
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetActivation(ref vrmDataObject obj)
        {
            bool bRet = true;
            String macID = "";
            String cpuID = "";
            DateTime actDate = DateTime.Now;
            DateTime serverdt = DateTime.Now;
            DateTime defltDate = new DateTime(1990, 01, 01);
            String isLocked = "0";
            Int32 isDemo = -1;
            String activated = "Demo";
            DateTime deactDate = DateTime.MinValue;
            Boolean isCpuID = false;
            Boolean isMacID = false;

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("System/CPUID");
                if (node != null)
                    cpuID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("System/MACID");
                if (node != null)
                    macID = node.InnerXml.Trim();


                sysSettings.Init(m_configPath);


                List<sysData> sysDatas = new List<sysData>();

                sysDatas = m_IsystemDAO.GetAll();

                isDemo = sysSettings.IsDemo;


                if (sysSettings.CPUID.Count <= 0 && sysSettings.MACID.Count <= 0)
                {
                    if (sysSettings.IsDemo == 1)
                    {
                        sysDatas[0].IsDemo = 0;
                        sysDatas[0].ActivatedDate = defltDate;
                        m_IsystemDAO.SaveOrUpdate(sysDatas[0]);
                        isDemo = 0;
                    }
                }

                foreach (String s in cpuID.Split(','))
                {
                    if (sysSettings.CPUID.Contains("")) //FB 2471
                    {
                        isCpuID = false;
                        break;
                    }
                    if (sysSettings.CPUID.Contains(s))
                    {
                        isCpuID = true;
                        break;
                    }
                }

                foreach (String s in macID.Split(','))
                {
                    if (sysSettings.MACID.Contains(""))//FB 2471
                    {
                        isMacID = false;
                        break;
                    }
                    if (sysSettings.MACID.Contains(s))
                    {
                        isMacID = true;
                        break;
                    }
                }



                if (isDemo == 0)
                {
                    if (!isCpuID || !isMacID)
                    {
                        activated = "0";

                        if (sysSettings.ActivatedDate == defltDate)
                        {
                            sysDatas[0].ActivatedDate = actDate;
                            m_IsystemDAO.SaveOrUpdate(sysDatas[0]);
                        }
                        else
                            actDate = sysSettings.ActivatedDate;

                        TimeSpan ts = serverdt.Subtract(actDate);

                        deactDate = actDate.AddMinutes(deActTime);

                        if (ts.TotalMinutes >= deActTime)
                            isLocked = "1";

                    }
                    else
                    {
                        activated = "1";
                        sysDatas[0].ActivatedDate = defltDate;
                        m_IsystemDAO.SaveOrUpdate(sysDatas[0]);
                    }

                }
                else if(isDemo == 1 && sysSettings.ActivatedDate == defltDate)//FB 2441
                    activated = "1";


                obj.outXml = "<System><Locked>" + isLocked + "</Locked><Activated>" + activated + "</Activated><DeactivateDate>" + deactDate.ToString() + "</DeactivateDate></System>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region Command to Get Email Last Run datetime - Added for DashBoard
        public bool GetLastMailRunDate(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetMailRunDate/UserID");
                string userID = node.InnerXml.Trim();

                //ZD 104183 Starts
                sysMailData sysMailData = new sysMailData();
                sysMailData = m_ISysMailDAO.GetById(1);

                obj.outXml = "<GetMailRunDate>";
                if (sysMailData.LastRunDateTime == DateTime.MinValue)
                    obj.outXml += "<LastRunDate></LastRunDate>";
                else
                    obj.outXml += "<LastRunDate>" + sysMailData.LastRunDateTime.ToString("g") + "</LastRunDate>"; //ZD 104183 Ends
                obj.outXml += "</GetMailRunDate>";
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                //obj.outXml = myVRMException.toXml(ex.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }
            return bRet;

        }

        #endregion

        //Site Logo starts here...
        #region SetSiteImage
       
        public bool SetImagekey(ref vrmDataObject obj)
        {
            int SiteLogoId = 0;
            int StdBannerId = 0;
            int HighBannerId = 0;
            int LoginBackgroundId = 0; // FB 2719
            int LoginBackgroundNormalId = 0; //ZD 103581
            byte[] imgData = null;
            byte[] bannerData = null;
            byte[] HbannerData = null;
            byte[] LoginBackgroundData = null; // FB 2719
            byte[] LoginBackgroundNormalData = null; //ZD 103581
            XmlNode node;
            string Image = "";
            string xCompanymessage = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                obj.outXml = "";
                m_vrmFactory = new vrmFactory(ref obj);
                m_imageFactory = new imageFactory(ref obj); //FB 2136
                node = xd.SelectSingleNode("//SetImagekey/Image");

                if(node!=null)
                {
                    Image = node.InnerText.Trim();
                    if (Image != "")
                    {
                        imgData = m_imageFactory.ConvertBase64ToByteArray(Image); //FB 2136
                    }
                }
                node = xd.SelectSingleNode("//SetImagekey/Companymessage");
                if (node != null)
                {
                    xCompanymessage = node.InnerText;
                }
                node = xd.SelectSingleNode("//SetImagekey/StdBanner");
                if (node != null)
                {
                    Image = node.InnerText.Trim();
                    if (Image != "")
                    {
                        bannerData = m_imageFactory.ConvertBase64ToByteArray(Image);  //FB 2136
                    }
                }
                node = xd.SelectSingleNode("//SetImagekey/HighBanner");

                if (node != null)
                {
                    Image = node.InnerText.Trim();
                    if (Image != "")
                    {
                        HbannerData = m_imageFactory.ConvertBase64ToByteArray(Image);  //FB 2136
                    }
                }

                // FB 2719 Starts
                node = xd.SelectSingleNode("//SetImagekey/LoginBackground");

                if (node != null)
                {
                    Image = node.InnerText.Trim();
                    if (Image != "")
                    {
                        LoginBackgroundData = m_imageFactory.ConvertBase64ToByteArray(Image);  //FB 2136
                    }
                }
                // FB 2719 Ends
                
                //ZD 103581 - Start
                node = xd.SelectSingleNode("//SetImagekey/LoginBackgroundNormal");
                if (node != null)
                {
                    Image = node.InnerText.Trim();
                    if (Image != "")
                    {
                        LoginBackgroundNormalData = m_imageFactory.ConvertBase64ToByteArray(Image);
                    }
                }
                //ZD 103581 - End
      
                List<sysData> sysDatas = m_IsystemDAO.GetAll();
                myvrmEx = null; //FB 2027
                if (sysDatas == null)
                {
                    myvrmEx = new myVRMException(200);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("AttributeType", 15)); //Site Logo...

                List<ICriterion> StdcriterionList = new List<ICriterion>();
                StdcriterionList.Add(Expression.Eq("AttributeType", 20)); //Std Banner..

                List<ICriterion> HighcriterionList = new List<ICriterion>();
                HighcriterionList.Add(Expression.Eq("AttributeType", 21)); //High Banner..
				
				//ZD 100996
                List<ICriterion> LoginbgcriterionList = new List<ICriterion>();
                LoginbgcriterionList.Add(Expression.Eq("AttributeType", 23)); //Login background.. 

                //ZD 103581
                List<ICriterion> LoginbgNormalcriterionList = new List<ICriterion>();
                LoginbgNormalcriterionList.Add(Expression.Eq("AttributeType", 31)); //Login background Normal.. 

                List<vrmImage> siteLogos = m_IImageDAO.GetByCriteria(criterionList);
                List<vrmImage> stdsiteLogos = m_IImageDAO.GetByCriteria(StdcriterionList);
                List<vrmImage> HighsiteLogos = m_IImageDAO.GetByCriteria(HighcriterionList);
                List<vrmImage> Loginbg = m_IImageDAO.GetByCriteria(LoginbgcriterionList);//ZD 100996
                List<vrmImage> LoginbgNormal = m_IImageDAO.GetByCriteria(LoginbgNormalcriterionList);//ZD 103581

                foreach (vrmImage siteObj in siteLogos)
                {
                    m_IImageDAO.Delete(siteObj);
                }
                       
                foreach (vrmImage siteObj in stdsiteLogos)
                {
                    m_IImageDAO.Delete(siteObj);
                }
                        
                foreach (vrmImage siteObj in HighsiteLogos)
                {
                    m_IImageDAO.Delete(siteObj);
                }
				//ZD 100996
                foreach (vrmImage siteObj in Loginbg)
                {
                    m_IImageDAO.Delete(siteObj);
                }

                //ZD 103581
                foreach (vrmImage siteObj in LoginbgNormal)
                {
                    m_IImageDAO.Delete(siteObj);
                }
                vrmImage imgObj = new vrmImage();
                if (imgData != null)
                {
                    imgObj.AttributeType = 15; //Site logo
                    imgObj.AttributeImage = imgData;
                    imgObj.OrgId = 11; // FB 2719
                    m_vrmFactory.SetImage(imgObj, ref SiteLogoId);
                }
                
                imgObj = new vrmImage();
                if (bannerData != null)
                {
                    imgObj.AttributeType = 20;
                    imgObj.AttributeImage = bannerData;
                    imgObj.OrgId = 11; // FB 2719
                    m_vrmFactory.SetImage(imgObj, ref StdBannerId);
                }
                
                imgObj = new vrmImage();
                if (HbannerData != null)
                {
                   imgObj.AttributeType = 21;
                   imgObj.AttributeImage = HbannerData;
                   imgObj.OrgId = 11; // FB 2719
                   m_vrmFactory.SetImage(imgObj, ref HighBannerId);
                }

                // FB 2719
                imgObj = new vrmImage();
                if (LoginBackgroundData != null)
                {
                    imgObj.AttributeType = 23;
                    imgObj.AttributeImage = LoginBackgroundData;
                    imgObj.OrgId = 11;
                    m_vrmFactory.SetImage(imgObj, ref LoginBackgroundId);
                }
                // FB 2719

                //ZD 103581 - Start
                imgObj = new vrmImage();
                if (LoginBackgroundNormalData != null)
                {
                    imgObj.AttributeType = 31;
                    imgObj.AttributeImage = LoginBackgroundNormalData;
                    imgObj.OrgId = 11;
                    m_vrmFactory.SetImage(imgObj, ref LoginBackgroundNormalId);
                }
                //ZD 103581 - End

                sysDatas[0].Companymessage = xCompanymessage;
                sysDatas[0].SiteLogoId = SiteLogoId;
                sysDatas[0].StdBannerId = StdBannerId;
                sysDatas[0].HighBannerId = HighBannerId;
                sysDatas[0].LoginBackgroundId = LoginBackgroundId; // FB 2719
                sysDatas[0].LoginBackgroundNormalId = LoginBackgroundNormalId; //ZD 103581
                m_IsystemDAO.Update(sysDatas[0]);

                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
        }
        #endregion
        
        #region GetSiteImage
        /// <summary>
        /// GetSiteImage
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetSiteImage(ref vrmDataObject obj)
        {
            m_vrmFactory = new vrmFactory(ref obj);
            string imageData = "";
            string StdBannerdata = "";
            string HighBannerdata = "";
            string LoginBackgroundData = ""; // FB 2719
            string LoginBackgroundNormalData = ""; // ZD 103581

            int ImageId = 0;
            int StdBannerId = 0;
            int HighBannerId = 0;
            int LoginBackgroundId = 0; // FB 2719
            int LoginBackgroundNormalId = 0;//ZD 103581
            string xCompanymessage = "";
            int ViewPublicConf = 1;//FB 2858
            int ReqUsrAcc = 0;//ZD 101846
            sysMailData sysMailDt = null;//FB 1943
            String mailExtnsn = "";//FB 1943
            try
            {
                m_IOrgSettingDAO.clearFetch();// FB 2719 Theme
                List<sysData> sysDatas = m_IsystemDAO.GetAll();

                if (sysDatas == null)
                {
                    myvrmEx = new myVRMException(200); //FB 2027
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                
                ImageId = sysDatas[0].SiteLogoId;
                //Banner Starts..
                StdBannerId = sysDatas[0].StdBannerId;
                HighBannerId = sysDatas[0].HighBannerId;
                imageData = m_vrmFactory.GetImage(ImageId);
                StdBannerdata = m_vrmFactory.GetImage(StdBannerId);
                HighBannerdata = m_vrmFactory.GetImage(HighBannerId);
                LoginBackgroundId = sysDatas[0].LoginBackgroundId;//ZD 100996
                LoginBackgroundData = m_vrmFactory.GetImage(LoginBackgroundId); // FB 2719
                LoginBackgroundNormalId = sysDatas[0].LoginBackgroundNormalId;//ZD 103581
                LoginBackgroundNormalData = m_vrmFactory.GetImage(LoginBackgroundNormalId); //ZD 103581
                //Banner Ends..
                xCompanymessage = sysDatas[0].Companymessage;
                ViewPublicConf = sysDatas[0].ViewPublicConf;//FB 2858
                ReqUsrAcc = sysDatas[0].ReqUsrAcc;//ZD 101846

                StringBuilder siteDt = new StringBuilder(); //FB 1820 start

                siteDt.Append("<GetSiteImage>");
                if(ImageId > 0)
                    siteDt.Append("<ImageId>" + ImageId + "</ImageId>");
                else
                    siteDt.Append("<ImageId></ImageId>");

                if (xCompanymessage != "")
                    siteDt.Append("<Companymessage>" + xCompanymessage + "</Companymessage>");
                else
                    siteDt.Append("<Companymessage></Companymessage>");

                if (imageData != "")
                {
                    siteDt.Append("<Image>" + imageData + "</Image>");
                }
                else
                    siteDt.Append("<Image></Image>");

                if (StdBannerdata != "")
                {
                    siteDt.Append("<StdBanner>" + StdBannerdata + "</StdBanner>");
                }
                else
                    siteDt.Append("<StdBanner></StdBanner>");

                if(HighBannerdata != "")
                {
                siteDt.Append("<HighBanner>" + HighBannerdata + "</HighBanner>");

                }
                else
                siteDt.Append("<HighBanner></HighBanner>");

                // FB 2719 Starts
                if (LoginBackgroundData != "")
                {
                    siteDt.Append("<LoginBackground>" + LoginBackgroundData + "</LoginBackground>");

                }
                else
                    siteDt.Append("<LoginBackground></LoginBackground>");
                // FB 2719 Ends

                //ZD 103581 - Start
                if (LoginBackgroundNormalData != "")
                    siteDt.Append("<LoginBackgroundNormal>" + LoginBackgroundNormalData + "</LoginBackgroundNormal>");
                else
                    siteDt.Append("<LoginBackgroundNormal></LoginBackgroundNormal>");
                //ZD 103581 - End

                //FB 1943
                sysMailDt = m_ISysMailDAO.GetById(1);

                if (sysMailDt == null)
                {
                    myvrmEx = new myVRMException(200); //FB 2027
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                // FB 3055 Starts
                if (sysDatas[0].roomCascadingControl.Equals(0)) // ZD 100263
                {
                    if (sysMailDt.CompanyMail.IndexOf("@") > 0)
                        if (sysMailDt.CompanyMail.Split('@').Length > 0)
                            mailExtnsn = sysMailDt.CompanyMail.Split('@')[1];
                }
                else
                    mailExtnsn = "";
                // FB 3055 Ends

                siteDt.Append("<CompanyMailExtension>" + mailExtnsn + "</CompanyMailExtension>");
                //FB 1943

                siteDt.Append("<ViewPublicConf>" + ViewPublicConf + "</ViewPublicConf>");//FB 2858
                siteDt.Append("<ReqUsrAcc>" + ReqUsrAcc + "</ReqUsrAcc>");//ZD 101846

                //ZD 101525
                string enableSSO = "";
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgId", 11));
                List<vrmLDAPConfig> ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(criterionList,true);
                if (ldapSettingsList != null && ldapSettingsList.Count > 0)
                    enableSSO = ldapSettingsList[0].EnableSSOMode.ToString();
                siteDt.Append("<EnableSSOMode>" + enableSSO + "</EnableSSOMode>");


                // FB 2719 Theme Starts
                OrgData orgInfo = new OrgData();
                orgInfo = m_IOrgSettingDAO.GetByOrgId(11);
                siteDt.Append("<Theme>" + orgInfo.ThemeType + "</Theme>");
                // FB 2719 Theme Ends
                siteDt.Append("<MemcacheEnabled>" + sysDatas[0].MemcacheEnabled + "</MemcacheEnabled>"); //ZD 103496

                siteDt.Append("</GetSiteImage>");
                obj.outXml = siteDt.ToString(); //FB 1820 End
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
        
        }

        
        #endregion
        
        #region Delete Site Image
        public bool DeleteSiteImage(ref vrmDataObject obj)
        {
            try
            {
                m_vrmFactory = new vrmFactory(ref obj);
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int imageId = 0;
                node = xd.SelectSingleNode("//DeleteImage/ImageId");
                imageId = int.Parse(node.InnerText);
                bool DeleteImage = m_vrmFactory.DeleteImage(imageId);
                if (DeleteImage)
                {
                    obj.outXml = "<success>1</success>";
                }
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
        }
        #endregion

        //Site Logo ends here...
        
        //Method added for Mail Data - Org Rss
        
        #region GetSysMailData
        /// <summary>
        ///  GetSysMailData
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetSysMailData(ref vrmDataObject obj)
        {
            sysMailData sysMailDt = null;
            try
            {
                sysMailDt = m_ISysMailDAO.GetById(1);

                if (sysMailDt == null)
                {
                    myvrmEx = new myVRMException(200); //FB 2027
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                obj.outXml = "<GetSysMailData>";

                obj.outXml += "<WebSiteURL>" + sysMailDt.websiteURL + "</WebSiteURL>";
                obj.outXml += "<CompanyMail>" + sysMailDt.CompanyMail + "</CompanyMail>";
                obj.outXml += "<ConTimeOut>" + sysMailDt.ConTimeOut.ToString() + "</ConTimeOut>";
                obj.outXml += "<displayname>" + sysMailDt.displayname + "</displayname>";
                obj.outXml += "<IsRemoteServer>" + sysMailDt.IsRemoteServer.ToString() + "</IsRemoteServer>";
                obj.outXml += "<ServerAddress>" + sysMailDt.ServerAddress + "</ServerAddress>";
                obj.outXml += "<Login>" + sysMailDt.Login + "</Login>";
                obj.outXml += "<password>" + sysMailDt.password + "</password>";
                obj.outXml += "<portNo>" + sysMailDt.portNo.ToString() + "</portNo>";
                obj.outXml += "<messagetemplate>" + sysMailDt.messagetemplate + "</messagetemplate>";
                obj.outXml += "<id>" + sysMailDt.id.ToString() + "</id>";
                obj.outXml += "<LastRunDateTime>" + sysMailDt.LastRunDateTime.ToString() + "</LastRunDateTime>";
                
                obj.outXml += "</GetSysMailData>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
        }
        #endregion

        //FB 2027 start
        #region Set SuperAdmin
        /// <summary>
        /// Set System License Details
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetSuperAdmin(ref vrmDataObject obj)
        {
            XmlDocument xd = new XmlDocument();
            try
            {
                xd.LoadXml(obj.inXml);
                XmlNode node;

                int userId = 0;
                node = xd.SelectSingleNode("//superAdmin/userID");

                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/userID not found.");

                Int32.TryParse(node.InnerXml.Trim(), out userId);
                
                //Login user information
                vrmUser myVrmUser = new userDAO(m_configPath, m_log).GetUserDao().GetByUserId(userId);
                if (myVrmUser == null)
                    throw new myVRMException("Error in fetching login user information.");

                int systimezoneid = 26;
                node = xd.SelectSingleNode("//superAdmin/preference/SystemTimeZoneID");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/preference/SystemTimeZoneID not found.");
                
                Int32.TryParse(node.InnerXml.Trim(), out systimezoneid);
                if (systimezoneid <= 0)
                    systimezoneid = 26; //EST

                node = xd.SelectSingleNode("//superAdmin/preference/securityKey");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/preference/securityKey not found.");
                string licenseTxt = node.InnerXml.Trim();

                int launchBuffer = 0;
                node = xd.SelectSingleNode("//superAdmin/preference/launchBuffer");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/preference/launchBuffer not found.");
                Int32.TryParse(node.InnerXml.Trim(), out launchBuffer);

                //FB 2437 - Starts
                int enableLaunchBufferP2P = 0;
                node = xd.SelectSingleNode("//superAdmin/preference/EnableLaunchBufferP2P");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/preference/EnableLaunchBufferP2P not found.");
                Int32.TryParse(node.InnerXml.Trim(), out enableLaunchBufferP2P);
                //FB 2437 - End

                //FB 2678 - Start
                int IndividualOrgExpiry = 0;
                node = xd.SelectSingleNode("//superAdmin/preference/IndividualOrgExpiry");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/preference/IndividualOrgExpiry not found.");
                Int32.TryParse(node.InnerXml.Trim(), out IndividualOrgExpiry);

                //103496 start
                int MemcacheEnabled = 0;
                node = xd.SelectSingleNode("//superAdmin/preference/MemcacheEnabled");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/preference/MemcacheEnabled not found.");
                int.TryParse(node.InnerXml.Trim(), out MemcacheEnabled);
                //103496 end

                // ZD 101011 Starts
                int sessionTimeout = 0;
                node = xd.SelectSingleNode("//superAdmin/preference/SessionTimeout");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/preference/SessionTimeout not found.");
                Int32.TryParse(node.InnerXml.Trim(), out sessionTimeout);
                // ZD 101011 Ends

                //FB 2678 - End
				 //FB 2670 start
                int Conciergesupport = 0;
                node = xd.SelectSingleNode("//superAdmin/preference/ConciergeSupport");
                if (node == null)
                    throw new myVRMException("Input Xml parse error.Element:superAdmin/preference/ConciergeSupport not found");
                int.TryParse(node.InnerText.Trim(), out Conciergesupport);
                //FB 2670 End

                //FB 2858 - Start
                int ViewPublicConf = 0;
                node = xd.SelectSingleNode("//superAdmin/preference/ViewPublicConf");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out ViewPublicConf);

                //FB 2858 - End

                //ZD 101846 - Start
                int ReqUsrAcc = 0;
                node = xd.SelectSingleNode("//superAdmin/preference/ReqUsrAcc");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out ReqUsrAcc);
                //ZD 101846 - End
                


                //FB 2501 starts
                int Startmode = 0;
                node = xd.SelectSingleNode("//superAdmin/preference/StartMode");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/preference/SelectMode not found.");
                Int32.TryParse(node.InnerXml.Trim(), out Startmode);
                //FB 2501 ends

                //FB 2659 Starts
                int enableCloudInstallation = 0;
                node = xd.SelectSingleNode("//superAdmin/preference/EnableCloudInstallation");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out enableCloudInstallation);
                //FB 2659 Ends

                //MailServer Details
                node = xd.SelectSingleNode("//superAdmin/preference/emailSystem/companyEmail");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/preference/emailSystem/companyEmail not found.");
                string companyEmail = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//superAdmin/preference/emailSystem/displayName");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/preference/emailSystem/displayName not found.");
                string displayName = node.InnerXml.Trim();
                
                node = xd.SelectSingleNode("//superAdmin/preference/emailSystem/messageTemplate");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/preference/emailSystem/messageTemplate not found.");
                string messageTemplate = node.InnerXml.Trim();

                int remoteServer = 0;
                node = xd.SelectSingleNode("//superAdmin/preference/emailSystem/remoteServer");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/preference/emailSystem/remoteServer not found.");
                
                int.TryParse(node.InnerXml.Trim(), out remoteServer);
                if (remoteServer < 0)
                    remoteServer = 0;

                node = xd.SelectSingleNode("//superAdmin/preference/emailSystem/websiteURL");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/preference/emailSystem/websiteURL not found.");
                string websiteURL = node.InnerXml.Trim();

                string serverAddress="", accountLogin ="", accountPwd = "";
                int portNo = 0, connectionTimeout = 0, retrycount = 0, ServerTimeout = 0;//ZD 100317
                if (remoteServer == 1)
                {
                    node = xd.SelectSingleNode("//superAdmin/preference/emailSystem/serverAddress");
                    if (node == null)
                        throw new myVRMException("Input xml parse error.Element:superAdmin/preference/emailSystem/serverAddress not found.");
                    serverAddress = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//superAdmin/preference/emailSystem/accountLogin");
                    if (node == null)
                        throw new myVRMException("Input xml parse error.Element:superAdmin/preference/emailSystem/accountLogin not found.");
                    accountLogin = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//superAdmin/preference/emailSystem/accountPwd");
                    if (node == null)
                        throw new myVRMException("Input xml parse error.Element:superAdmin/preference/emailSystem/accountPwd not found.");
                    accountPwd = node.InnerXml.Trim();

                    portNo = 0;
                    node = xd.SelectSingleNode("//superAdmin/preference/emailSystem/portNo");
                    if (node == null)
                        throw new myVRMException("Input xml parse error.Element:superAdmin/preference/emailSystem/portNo not found.");
                    int.TryParse(node.InnerXml.Trim(), out portNo);
                    
                    connectionTimeout = 0;
                    node = xd.SelectSingleNode("//superAdmin/preference/emailSystem/connectionTimeout");
                    if (node == null)
                        throw new myVRMException("Input xml parse error.Element:superAdmin/preference/emailSystem/connectionTimeout not found.");
                    int.TryParse(node.InnerXml.Trim(), out connectionTimeout);

                    //FB 2552 Start
                    retrycount = 1;
                    node = xd.SelectSingleNode("//superAdmin/preference/emailSystem/retrycount");
                    if (node != null)
                        int.TryParse(node.InnerXml.Trim(), out retrycount);
                    //FB 2552 End
                    
                    //ZD 100317 Starts
                    ServerTimeout = 10;
                    node = xd.SelectSingleNode("//superAdmin/preference/emailSystem/serverTimeout");
                    if (node != null)
                        int.TryParse(node.InnerXml.Trim(), out ServerTimeout);
                    //ZD 100317 Ends
                }

                if (serverAddress == "")
                    serverAddress = "127.0.0.1";
                if (portNo <= 0)
                    portNo = 25;
                if (connectionTimeout <= 0)
                    connectionTimeout = 10;

                //Mail Exchange Details - Not Used
                /*
                node = xd.SelectSingleNode("//superAdmin/preference/exchange/exchangeURL");
                string exchangeURL = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//superAdmin/preference/exchange/exchangeDomain");
                string exchangeDomain = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//superAdmin/preference/exchange/exchangeLogin");
                string exchangeLogin = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//superAdmin/preference/exchange/exchangePwd");
                string exchangePwd = node.InnerXml.Trim();
                */
                //lincense check - 407
                
                //Parse LDAP Settings inXML
                node = xd.SelectSingleNode("//superAdmin/preference/LDAP/serverAddress");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/userID not found.");
                string lserverAddress = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//superAdmin/preference/LDAP/loginName");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/userID not found.");
                string loginName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//superAdmin/preference/LDAP/loginPassword");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/userID not found.");
                string loginPassword = node.InnerXml.Trim();

                int lportNo = 389; //LDAP default port no
                node = xd.SelectSingleNode("//superAdmin/preference/LDAP/portNo");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/userID not found.");
                int.TryParse(node.InnerXml.Trim(), out lportNo);
                if (lportNo <= 0)
                    lportNo = 389;

                int lconnectionTimeout = 20; //default 20 seconds
                node = xd.SelectSingleNode("//superAdmin/preference/LDAP/connectionTimeout");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/userID not found.");
                int.TryParse(node.InnerXml.Trim(), out lconnectionTimeout);
                if (lconnectionTimeout <= 0)
                    lconnectionTimeout = 20;

                //Need to verify by gowniyan start
                node = xd.SelectSingleNode("//superAdmin/preference/LDAP/scheduler/Time");
                DateTime schedulerTime;
                DateTime.TryParse(node.InnerXml.Trim(), out schedulerTime);
                //timeZone.userPreferedTime(myVrmUser.TimeZone, ref schedulerTime); FB 2462
                //Need to verify by gowniyan end

                node = xd.SelectSingleNode("//superAdmin/preference/LDAP/scheduler/Days");
                string schedulerDays = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//superAdmin/preference/LDAP/loginkey");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/userID not found.");
                string loginkey = node.InnerXml.Trim();

                //node = xd.SelectSingleNode("//superAdmin/preference/LDAP/synctime");
                //string synctime = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//superAdmin/preference/LDAP/searchfilter");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/userID not found.");
                string searchfilter = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//superAdmin/preference/LDAP/LDAPPrefix");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/userID not found.");
                string LDAPPrefix = node.InnerXml.Trim();

                //FB 2993 LDAP START
                int AuthType = 0; // 0- None, 1- Signing,2- Sealing,3- Secure
                node = xd.SelectSingleNode("//superAdmin/preference/LDAP/AuthenticationType");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/userID not found.");
                int.TryParse(node.InnerXml.Trim(), out AuthType);
              
                //FB 2993 LDAP END

                //ZD 101443-LDAP Starts
                int EnableDomain = 0;
                node = xd.SelectSingleNode("//superAdmin/preference/LDAP/EnableDomain");
                if(node != null)
                    int.TryParse(node.InnerText.Trim(), out EnableDomain);

                int TenantOption = 0;
                node = xd.SelectSingleNode("//superAdmin/preference/LDAP/TenantOption");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out TenantOption);

                if (EnableDomain == 0)
                    TenantOption = -1;

                int EnableSSOMode = 0;
                node = xd.SelectSingleNode("//superAdmin/preference/LDAP/EnableSSOMode");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out EnableSSOMode);
                //ZD 101443-LDAP End

                //FB 2501 EM7 Starts
                node = xd.SelectSingleNode("//superAdmin/preference/EM7Connectivity/EM7URI");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/EM7URI not found.");
                string EM7URI = node.InnerXml.Trim();
                               
                node = xd.SelectSingleNode("//superAdmin/preference/EM7Connectivity/EM7Username");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/EM7Username not found.");
                string EM7Username = node.InnerXml.Trim();

                
                int EM7Port = 389;
                node = xd.SelectSingleNode("//superAdmin/preference/EM7Connectivity/EM7Port");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/EM7Port not found.");
                int.TryParse(node.InnerXml.Trim(), out EM7Port);
                //FB 2501 EM7 Ends

                //ZD 100526
                int ConfIDStartValue = 0;
                node = xd.SelectSingleNode("//superAdmin/preference/ConfIDStartValue");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out ConfIDStartValue);

                int isConfStartUpdate = 0;
                node = xd.SelectSingleNode("//superAdmin/preference/isConfStartUpdate");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out isConfStartUpdate);
                
                int errNo = 0;
                if (ConfIDStartValue > 0 && isConfStartUpdate == 1)
                {
                    if(!SetConfIDStartValue(ConfIDStartValue,ref obj,ref errNo))
                    {
                        myvrmEx = new myVRMException(errNo);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }

                //ZD 101837
                int isConfRecurLimit = 0;
                node = xd.SelectSingleNode("//superAdmin/preference/ConfRecurLimit");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out isConfRecurLimit);

                if (!(isConfRecurLimit >= 2 && isConfRecurLimit <= 250))
                {
                    myvrmEx = new myVRMException(743);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                //Validate License
                errNo = 0;
                string Datechange = ""; //FB 2678
                if (!ValidateLicense(licenseTxt, ref errNo ,ref Datechange))
                {
                    myvrmEx = new myVRMException(errNo);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                //Update Site information
                sysData siteInfo = m_IsystemDAO.GetByAdminId(11); //Default vrmAdministrator id
                if (siteInfo == null)
                    throw new myVRMException("Error in fetching sysdata for updation");

                DateTime nowdate = DateTime.Now;
                //Change to gmt based on system timezone
                timeZone.changeToGMTTime(systimezoneid, ref nowdate); //Need to verify by gowniyan

                node = xd.SelectSingleNode("//superAdmin/preference/EM7Connectivity/EM7Password");
                if (node != null)
                    siteInfo.EM7Password = node.InnerXml.Trim();

                //ZD 100152 Starts
                node = xd.SelectSingleNode("//preference/GoogleConfig/GoogleClientID");
                if (node != null)
                    siteInfo.GoogleClientID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//preference/GoogleConfig/GoogleSecretID");
                if (node != null)
                     siteInfo.GoogleSecretID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//preference/GoogleConfig/GoogleAPIKey");
                if (node != null)
                    siteInfo.GoogleAPIKey = node.InnerXml.Trim();

                int PushtoGoogle = 0;
                node = xd.SelectSingleNode("//preference/GoogleConfig//PushtoGoogle");
                if (node == null)
                    throw new myVRMException("Input xml parse error.Element:superAdmin/PushtoGoogle not found.");
                int.TryParse(node.InnerXml.Trim(), out PushtoGoogle);

                siteInfo.PushtoGoogle = PushtoGoogle; 
                //ZD 100152 Ends

                siteInfo.license = licenseTxt;
                siteInfo.TimeZone = systimezoneid;
                siteInfo.LaunchBuffer = launchBuffer;
                siteInfo.EnableLaunchBufferP2P = enableLaunchBufferP2P;//FB 2437
                siteInfo.IndividualOrgExpiry = IndividualOrgExpiry;//FB 2678
                siteInfo.MemcacheEnabled = MemcacheEnabled; //103496
                siteInfo.StartMode = Startmode; // FB 2501
				siteInfo.ViewPublicConf = ViewPublicConf;//FB 2858
                siteInfo.ConciergeSupport = Conciergesupport;//FB 2670
				siteInfo.EnableCloudInstallation = enableCloudInstallation; //FB 2659
                siteInfo.ConfIDStartValue = ConfIDStartValue;//ZD 100526
                //FB 2501 EM7 Starts
                siteInfo.EM7URI = EM7URI;
                siteInfo.EM7Username = EM7Username;
                siteInfo.EM7Port = EM7Port;
                //FB 2501 EM7 Ends
                siteInfo.LastModifiedUser = userId;
                siteInfo.LastModified = nowdate;
                siteInfo.SessionTimeout = sessionTimeout; // ZD 101011
                siteInfo.ConfRecurLimit = isConfRecurLimit;//ZD 101837
                siteInfo.ReqUsrAcc = ReqUsrAcc;//ZD 101846
                //update site images
                SetSiteImages(ref obj, ref siteInfo);

                //FB 2678 Start

                string Oldexpirydate = sysSettings.ExpiryDate.ToString("MM/dd/yyyy").Trim();
                if (sysSettings.IndividualOrgExpiry != IndividualOrgExpiry || Oldexpirydate != Datechange)
                    SetOrgExpiryLicense(ref IndividualOrgExpiry, Datechange);
                
                //FB 2678 End
                m_IsystemDAO.Update(siteInfo);

                //Update Mailserver Details
                try
                {
                    sysMailData sysMailDt = m_ISysMailDAO.GetById(1);
                    sysMailDt.CompanyMail = companyEmail;
                    sysMailDt.IsRemoteServer = remoteServer;
                    sysMailDt.ServerAddress = serverAddress;
                    sysMailDt.Login = accountLogin;
                    sysMailDt.password = accountPwd; //FB 1070
                    sysMailDt.portNo = portNo;
                    sysMailDt.ConTimeOut = connectionTimeout;
                    sysMailDt.displayname = displayName;
                    sysMailDt.messagetemplate = messageTemplate;
                    sysMailDt.websiteURL = websiteURL;
                    sysMailDt.RetryCount = retrycount;
                    sysMailDt.ServerTimeout = ServerTimeout; //ZD 100317
                    m_ISysMailDAO.Update(sysMailDt);
                }
                catch (Exception e)
                { throw new myVRMException(e.Message); }

                //Update LDAP Configurations
				//ZD 101443 Starts	
                vrmLDAPConfig ldapConfig = null;
                List<ICriterion> 
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgId", 11));
                List<vrmLDAPConfig> ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(criterionList);
                if (ldapSettingsList != null && ldapSettingsList.Count > 0)
                    ldapConfig = ldapSettingsList[0];

                if (ldapConfig == null)
                    ldapConfig = new vrmLDAPConfig();

                //vrmLDAPConfig ldapConfig = m_ILDAPConfigDAO.GetById(1);
                //if (ldapConfig == null)
                //    throw new myVRMException("Fetching LDAP configuration for updation failed"); 
				//ZD 101443 End
                cryptography.Crypto crypto = new cryptography.Crypto();
                ldapConfig.serveraddress = lserverAddress;
                ldapConfig.login = loginName;
                //ldapConfig.password = crypto.encrypt(loginPassword);
                ldapConfig.password = loginPassword; //FB 3054
                ldapConfig.port = lportNo;
                ldapConfig.timeout = lconnectionTimeout;
                ldapConfig.LoginKey = loginkey;
                //ldapConfig.SyncTime = synctime;
                ldapConfig.SyncTime = schedulerTime;
                ldapConfig.domainPrefix = LDAPPrefix;
                ldapConfig.SearchFilter = searchfilter;
                ldapConfig.scheduleTime = schedulerTime;
                ldapConfig.scheduleDays = schedulerDays;
                ldapConfig.AuthType = AuthType;//FB 2993 LDAP
                //ZD 101443-LDAP Starts
                ldapConfig.EnableDomain = EnableDomain;
                ldapConfig.TenantOption = TenantOption;
                ldapConfig.EnableSSOMode = EnableSSOMode;
                ldapConfig.OrgId = 11;
                m_ILDAPConfigDAO.SaveOrUpdate(ldapConfig);
                //ZD 101443-LDAP End

                //FB 2363 - Start
                List<ICriterion> criteria = new List<ICriterion>();

                node = xd.SelectSingleNode("//superAdmin/preference/external");
                if (node != null)
                {
                    criteria.Add(Expression.Eq("UId", 1));
                    IList<sysExternalScheduling> sysESList = m_ESDAO.GetByCriteria(criteria);
                    sysExternalScheduling sysES = new sysExternalScheduling();

                    Boolean isNew = true;
                    if (sysESList == null || sysESList.Count == 0)
                        isNew = true;
                    else
                    {
                        isNew = false;
                        sysES = sysESList[0];
                    }

                    node = xd.SelectSingleNode("//superAdmin/preference/external/PartnerName");
                    if (node != null)
                        sysES.PartnerName = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//superAdmin/preference/external/PartnerEmail");
                    if (node != null)
                        sysES.PartnerEmail = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//superAdmin/preference/external/PartnerURL");
                    if (node != null)
                        sysES.PartnerURL = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//superAdmin/preference/external/UserName");
                    if (node != null)
                        sysES.UserName = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//superAdmin/preference/external/Password");
                    if (node != null)
                        sysES.Password = node.InnerXml.Trim(); //FB 3054

                    Int32 timeoutValue = 0;//FB 2363
                    node = xd.SelectSingleNode("//superAdmin/preference/external/TimeoutValue"); 
                    if (node != null)
                        Int32.TryParse(node.InnerXml.Trim(), out timeoutValue);
                    sysES.TimeoutValue = timeoutValue;

                    if (isNew == false)
                        m_ESDAO.Update(sysES);
                    else
                        m_ESDAO.Save(sysES);

                    #region ES Mail User Report Setting 
                    ESMailUsrRptSettings ESMailUsrRptSet = new ESMailUsrRptSettings();//shan
                    criteria = new List<ICriterion>();
                    int DeliveryType =1,FrequencyType =1,FrequencyCount =1,Sent =0;
                    DateTime Starttime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1); //FB 2563

                    criteria.Add(Expression.Eq("UId", 1));
                    IList<ESMailUsrRptSettings> ESMailUsrRptSets = m_ESMailUsrRptSetDAO.GetByCriteria(criteria);

                    node = xd.SelectSingleNode("//superAdmin/preference/external/MailusrRptSetting/CustomerName");
                    if (node != null)
                        ESMailUsrRptSet.CustomerName = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//superAdmin/preference/external/MailusrRptSetting/DeliveryType");
                    if (node != null)
                        Int32.TryParse(node.InnerText.Trim(), out DeliveryType);

                    node = xd.SelectSingleNode("//superAdmin/preference/external/MailusrRptSetting/RptDestination");
                    if (node != null)
                        ESMailUsrRptSet.RptDestination = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//superAdmin/preference/external/MailusrRptSetting/StartTime");
                    if (node != null)
                       if (node.InnerXml.Trim() != "")
                        DateTime.TryParse(node.InnerXml.Trim(), out Starttime);

                    node = xd.SelectSingleNode("//superAdmin/preference/external/MailusrRptSetting/FrequencyType");
                    if (node != null)
                        Int32.TryParse(node.InnerText.Trim(), out FrequencyType);

                    node = xd.SelectSingleNode("//superAdmin/preference/external/MailusrRptSetting/FrequencyCount");
                    if (node != null)
                        Int32.TryParse(node.InnerText.Trim(), out FrequencyCount);

                    node = xd.SelectSingleNode("//superAdmin/preference/external/MailusrRptSetting/Sent");
                    if (node != null)
                        Int32.TryParse(node.InnerText.Trim(), out Sent);

                    ESMailUsrRptSet.DeliveryType = DeliveryType;
                    ESMailUsrRptSet.StartTime = Starttime;
                    ESMailUsrRptSet.FrequencyType = FrequencyType;
                    ESMailUsrRptSet.FrequencyCount = FrequencyCount;
                    ESMailUsrRptSet.Sent = Sent;
                    ESMailUsrRptSet.Type = "S";

                    if (ESMailUsrRptSets == null || ESMailUsrRptSets.Count == 0)
                        m_ESMailUsrRptSetDAO.Save(ESMailUsrRptSet);
                    else
                    {
                        ESMailUsrRptSet.UId = ESMailUsrRptSets[0].UId;
                        m_ESMailUsrRptSetDAO.Update(ESMailUsrRptSet);
                    }
                    #endregion
                }
                //FB 2363 - End

                //FB 2594 Starts
                OrgData baseOrgData = null;
                int defaultOrgID = 11;
                
                baseOrgData = m_IOrgSettingsDAO.GetByOrgId(defaultOrgID);
                if (baseOrgData != null)
                {
                    if (EnablePublicRooms == 1)
                    {
                        baseOrgData.EnablePublicRoomService = 1;
                        errNo = 0;
                        m_OrgFactory = new OrganizationFactory(ref obj);
                        if (!m_OrgFactory.PublicRoomSystemApprovers(ref errNo))
                        {
                            myvrmEx = new myVRMException(errNo);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    else
                    {
                        baseOrgData.EnablePublicRoomService = 0;
                    }

                    if (EnableCloud == 1)
                        baseOrgData.EnableCloud = 1;
                    else
                        baseOrgData.EnableCloud = 0;

                    m_IOrgSettingsDAO.SaveOrUpdate(baseOrgData);
                }
                //ZD 100166 Starts
                List<OrgData> AllOrgData = null;
                if (enableCloudInstallation == 1)
                {
                    AllOrgData = new List<OrgData>();
                    AllOrgData = m_IOrgSettingsDAO.GetAll();
                    AllOrgData = AllOrgData.Select((data, i) => { data.EnableSmartP2P = 0; data.recurEnabled = 0; data.SpecialRecur = 0;  return data; }).ToList(); //ZD 100518 //ZD 102626\
                    //data.EnableBufferZone = "0"; data.SetupTime = 0; data.TearDownTime = 0; data.McuSetupTime = 0; data.MCUTeardonwnTime = 0; data.MCUSetupDisplay = 0; data.MCUTearDisplay = 0;
                    m_IOrgSettingsDAO.SaveOrUpdateList(AllOrgData);
                }
                //ZD 100166 End
                //ZD 100753 Satrts
                if (EnableCloud == 1)
                {
                    AllOrgData = new List<OrgData>();
                    AllOrgData = m_IOrgSettingsDAO.GetAll();
                    AllOrgData = AllOrgData.Select((data, i) => { data.ShowHideVMR = 1; data.EnableExternalVMR = 1; return data; }).ToList();

                    m_IOrgSettingsDAO.SaveOrUpdateList(AllOrgData);
                }
                //ZD 100753 Ends
                //ZD 101443 Starts
                if (isLDAP == 1)
                {
                    AllOrgData = new List<OrgData>();
                    AllOrgData = m_IOrgSettingsDAO.GetAll();
                    AllOrgData = AllOrgData.Select((data, i) => { data.UserLimit = 0; return data; }).ToList();

                    m_IOrgSettingsDAO.SaveOrUpdateList(AllOrgData);
                }
                //ZD 101443 End
                //ZD 104116 - Start
                if (EnableBJNUsr == -2)//ZD 104225
                {
                    AllOrgData = new List<OrgData>();
                    AllOrgData = m_IOrgSettingsDAO.GetAll();
                    AllOrgData = AllOrgData.Select((data, i) => { data.EnableBJNIntegration = 1; data.EnableBlueJeans = 1; return data; }).ToList();

                    m_IOrgSettingsDAO.SaveOrUpdateList(AllOrgData);
                }
                //ZD 104116 - End
                String WhyGoURL = "", WhyGoUserName = "", WhyGoAdminEmail="";//ZD 100694
                node = xd.SelectSingleNode("//superAdmin/preference/WhyGo/WhyGoURL");
                if (node != null)
                    WhyGoURL = node.InnerText.Trim();
                if (WhyGoURL != "")
                {
                    criteria = new List<ICriterion>();
                    criteria.Add(Expression.Eq("UId", 1));
                    IList<sysWhygoSettings> sysWhygoList = m_WhygoSettingsDAO.GetByCriteria(criteria);
                    sysWhygoSettings sysWhygo = new sysWhygoSettings();
                    if (sysWhygoList != null && sysWhygoList.Count > 0)
                        sysWhygo = sysWhygoList[0];
                    //FB 2392 - Starts
                    node = xd.SelectSingleNode("//superAdmin/preference/WhyGo/WhyGoUserName");
                    if (node != null)
                        WhyGoUserName = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//superAdmin/preference/WhyGo/WhyGoPassword");
                    if (node != null)
                        sysWhygo.WhyGoPassword = node.InnerText.Trim();
                    //FB 2392 - End
                   //ZD 100694 start
                    node = xd.SelectSingleNode("//superAdmin/preference/WhyGo/WhyGoAdminEmail");
                    if (node != null)
                        WhyGoAdminEmail = node.InnerText.Trim();
                    //ZD 100694 End
                    sysWhygo.WhyGoURL = WhyGoURL;
                    sysWhygo.WhyGoUserName = WhyGoUserName;
                    sysWhygo.ModifiedDate = nowdate;
                    sysWhygo.WhyGoAdminEmail = WhyGoAdminEmail;//ZD 100694
                    m_WhygoSettingsDAO.SaveOrUpdate(sysWhygo);
 
                }
                //FB 2594 Ends
                sysSettings.Init(m_configPath,true); //ZD 104095

                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (myVRMException me)
            {
                m_log.Error("SETSuperAdmin: ", me);
                obj.outXml = "";
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("SETSuperAdmin: ", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region ValidateLicense
        /// <summary>
        /// ValidateLicense
        /// </summary>
        /// <param name="licesneKey"></param>
        /// <returns></returns>
        internal bool ValidateLicense(string licesneKey, ref int errNo,ref string Datechange) //FB 2678
        {
            int RoomLimit, MCULimit, UserLimit, MCUEnchancedLimit = 0;//FB 2486
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                cryptography.Crypto crypto = new cryptography.Crypto();
                string templicense = crypto.decrypt(licesneKey);

                xd.LoadXml(templicense);
                XmlNode node;

                DateTime ExpiryDate;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Site/ExpirationDate");
                DateTime.TryParse(node.InnerXml.Trim(), out ExpiryDate);
                Datechange = ExpiryDate.ToString("MM/dd/yyyy").Trim(); //FB 2678
               

                int MaxOrganizations = 1; //Default single organization
                node = xd.SelectSingleNode("//myVRMSiteLicense/Site/MaxOrganizations");
                int.TryParse(node.InnerXml.Trim(), out MaxOrganizations);
                if (MaxOrganizations <= 0)
                    MaxOrganizations = 1;


                //FB 2599 Start
                EnableCloud = 0;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Site/Cloud");
                int.TryParse(node.InnerXml.Trim(), out EnableCloud);
                if (EnableCloud == 1 && MaxOrganizations > 1)
                {
                    errNo = 637;
                    return false;
                }
                else if (EnableCloud > 1)
                {
                    errNo = 638;
                    return false;
                }
                //FB 2599 End

                //FB 2594 Starts
                int LicenseErr = 0;
                EnablePublicRooms = 0;  
                node = xd.SelectSingleNode("//myVRMSiteLicense/Site/PublicRoomService");
                Int32.TryParse(node.InnerXml.Trim(), out EnablePublicRooms);

                if (sysSettings.EnablePublicRooms == 1 && EnablePublicRooms == 0)
                {
                    if (!PublicRoomLicenseCheck(1, ref LicenseErr))
                    {
                        errNo = LicenseErr;
                        return false;
                    }
                }
                else if (EnablePublicRooms > 1)
                {
                    errNo = 639;
                    return false;
                }
                //FB 2594 Ends

                int nonVideoRooms = 0;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxNonVideoRooms");
                int.TryParse(node.InnerXml.Trim(), out nonVideoRooms);
                if (nonVideoRooms < 0)
                    nonVideoRooms = 0;

                int videoRooms = 0;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxVideoRooms");
                int.TryParse(node.InnerXml.Trim(), out videoRooms);
                if (videoRooms < 0)
                    videoRooms = 0;

                int VMRRooms = 0;//FB 2586
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxVMRRooms");
                int.TryParse(node.InnerXml.Trim(), out VMRRooms);
                if (VMRRooms < 0)
                    VMRRooms = 0;

                //ZD 101098 START
                int iControlRooms = 0;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxiControlRooms");
                int.TryParse(node.InnerXml.Trim(), out iControlRooms);
                if (iControlRooms < 0)
                    iControlRooms = 0;
                //ZD 101098 END

                //FB 2694 Starts
                int vcHotRooms = 0;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxVCHotdesking");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out vcHotRooms);
                if (vcHotRooms < 0)
                    vcHotRooms = 0;

                int roHotRooms = 0;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxROHotdesking");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out roHotRooms);
                if (roHotRooms < 0)
                    roHotRooms = 0;

                RoomLimit = nonVideoRooms + videoRooms + VMRRooms + vcHotRooms + roHotRooms + iControlRooms ;//FB 2586 //ZD 101098
                //FB 2694 End

                //FB 2426 Start
                int ExternalRooms = 0;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Rooms/MaxGuestRooms");
                int.TryParse(node.InnerXml.Trim(), out ExternalRooms);
                if (ExternalRooms < 0)
                    ExternalRooms = 0;
                //FB 2426 End
                MCULimit = 0;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Hardware/MaxStandardMCUs");//FB 2486
                int.TryParse(node.InnerXml.Trim(), out MCULimit);
                if (MCULimit < 0)
                    MCULimit = 0;

                //FB 2486
                MCUEnchancedLimit = 0;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Hardware/MaxEnhancedMCUs");
                int.TryParse(node.InnerXml.Trim(), out MCUEnchancedLimit);
                if (MCUEnchancedLimit < 0)
                    MCUEnchancedLimit = 0;

                int maxEndPoints = 0;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Hardware/MaxEndpoints");
                int.TryParse(node.InnerXml.Trim(), out maxEndPoints);
                if (maxEndPoints < 0)
                    maxEndPoints = 0;

                UserLimit = 0;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxTotalUsers");
                if (node != null)//ZD 101443
                    int.TryParse(node.InnerXml.Trim(), out UserLimit);
                if (UserLimit < 0)
                    UserLimit = 0;

                //ZD 101443-LDAP Starts
                int ldapEnabled = 0;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Site/IsLDAP");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out ldapEnabled);
                if (ldapEnabled < 0)
                    ldapEnabled = 0;
                //ZD 101443-LDAP End

                //int maxCDRs = 0;
                //node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Hardware/MaxCDRs");
                //int.TryParse(node.InnerXml.Trim(), out maxCDRs);
                //if (maxCDRs < 0)
                //    maxCDRs = 0;

                //FB 2531 Issue in setting License - START

                int maxGuest = 0;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxGuestsPerUser");
                int.TryParse(node.InnerXml.Trim(), out maxGuest);
                if (maxGuest <= 0)
                    maxGuest = 50; //default

                int maxexusr = 0;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxExchangeUsers");
                int.TryParse(node.InnerXml.Trim(), out maxexusr);


                int maxDominousr = 0;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxDominoUsers");
                int.TryParse(node.InnerXml.Trim(), out maxDominousr);
                if (maxDominousr < 0)
                    maxDominousr = 0;

                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxMobileUsers"); //FB 1979
                int maxMobileusr = 0;
                int.TryParse(node.InnerXml.Trim(), out maxMobileusr);
                if (maxMobileusr < 0)
                    maxMobileusr = 0;

                int maxPCusr = 0;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxPCUsers"); //FB 2693
                int.TryParse(node.InnerXml.Trim(), out maxPCusr);
                if (maxPCusr < 0)
                    maxPCusr = 0;

                //ZD 100221 Start
                int maxWebexusr = 0;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxWebexUsers");
                int.TryParse(node.InnerXml.Trim(), out maxWebexusr);
                if (maxWebexusr < 0)
                    maxWebexusr = 0;
                //ZD 100221 End

                //ZD 104021 start
                int maxBlueJeanUsr;
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Users/MaxBlueJeansUsers");
                if (node.InnerXml.Trim().ToUpper() == "UL")
                    maxBlueJeanUsr = -2; //Referring Unlimited license as -2
                else
                {
                    Int32.TryParse(node.InnerXml.Trim(), out maxBlueJeanUsr);
                }
                
                //ZD 104021 end

                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxFacilitiesModules");
                int maxav = 0;
                int.TryParse(node.InnerXml.Trim(), out maxav);
                if (maxav < 0)
                    maxav = 0;

                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxAPIModules");
                int maxapi = 0;
                int.TryParse(node.InnerXml.Trim(), out maxapi);
                if (maxapi < 0)
                    maxapi = 0;

                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxCateringModules");
                int maxcat = 0;
                int.TryParse(node.InnerXml.Trim(), out maxcat);
                if (maxcat < 0)
                    maxcat = 0;

                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxHousekeepingModules");
                int maxhkg = 0;
                int.TryParse(node.InnerXml.Trim(), out maxhkg);
                if (maxhkg < 0)
                    maxhkg = 0;
                //FB 2693 Starts
                int maxJabber = 0, maxLync = 0, maxVidtel = 0; //ZD 103550
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxJabber");
                int.TryParse(node.InnerXml.Trim(), out maxJabber);
                if (maxJabber < 0)
                    maxJabber = 0;

                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxLync");
                int.TryParse(node.InnerXml.Trim(), out maxLync);
                if (maxLync < 0)
                    maxLync = 0;

                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxVidtel");
                if(node != null) //ZD 102004
                    int.TryParse(node.InnerXml.Trim(), out maxVidtel);
                if (maxVidtel < 0)
                    maxVidtel = 0;

                //FB 2693 Ends

                //FB 2593 Start
                node = xd.SelectSingleNode("//myVRMSiteLicense/Organizations/Modules/MaxAdvancedReport");
                int maxAdvreport = 0;
				if(node != null)
                	int.TryParse(node.InnerXml.Trim(), out maxAdvreport);
                if (maxAdvreport < 0)
                    maxAdvreport = 0;
                //FB 2593 End

                DateTime nowDate = DateTime.Now;//Validate Expiration Date
                if (nowDate >= ExpiryDate)
                {
                    errNo = 515;
                    return false;
                }

                //Validate organization license befoere updation of new key
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("deleted", 0));

                List<vrmOrganization> vrmOrg = m_IOrgDAO.GetByCriteria(criterionList);
                if (vrmOrg == null)
                    throw new myVRMException("Fetch error in organization DAO");

                if (vrmOrg.Count > MaxOrganizations)
                {
                    errNo = 511;
                    return false;
                }

                int totNvrmCount = 0, totVrmCount = 0, totvmrCount = 0, totSmcuCount = 0, totEmcuCount = 0, totEndpointCount = 0, totUserCount = 0;
                int totGuestrmCount = 0, totGuestUserCount = 0, totOutlookUserCount = 0, totMobileUserCount = 0, totNotesUserCount = 0;
                int totCatModuleCount = 0, totFacModuleCount = 0, totHKModuleCount = 0, totAPIModuleCount = 0, totPCModuleCount = 0;
                int totPCUserCount = 0, totBJNUserCount = 0, totJabberCount = 0, totLyncCount = 0, totVidtelCount = 0; //FB 2693
				int totVCHotRoomCount = 0, totROHotRoomCount = 0; //FB 2694
                int totAdvancedreport = 0;//FB 2593
                int totiControlCount = 0; //ZD 101098

                List<int> orgIDs = new List<int>();
                List<vrmOrganization> OrgList = m_IOrgDAO.GetByCriteria(criterionList);
                if (OrgList != null)
                {
                    foreach (vrmOrganization vrmOrg1 in OrgList)
                    {
                        orgIDs.Add(vrmOrg1.orgId);
                    }
                }

                criterionList = null;
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.In("OrgId", orgIDs));

                List<OrgData> orgDts = m_IOrgSettingsDAO.GetByCriteria(criterionList);
                if (orgDts != null)
                {
                    foreach (OrgData orgdt in orgDts)
                    {
                        totNvrmCount = totNvrmCount + orgdt.MaxNonVideoRooms;
                        totVrmCount = totVrmCount + orgdt.MaxVideoRooms;
                        totSmcuCount = totSmcuCount + orgdt.MCULimit;
                        totEmcuCount = totEmcuCount + orgdt.MCUEnchancedLimit;
                        totEndpointCount = totEndpointCount + orgdt.MaxEndpoints;
                        totUserCount = totUserCount + orgdt.UserLimit;
                        totGuestrmCount = totGuestrmCount + orgdt.GuestRoomLimit;
                        totGuestUserCount = totGuestUserCount + orgdt.GuestRoomPerUser;
                        totOutlookUserCount = totOutlookUserCount + orgdt.ExchangeUserLimit;
                        totMobileUserCount = totMobileUserCount + orgdt.MobileUserLimit;
                        totNotesUserCount = totNotesUserCount + orgdt.DominoUserLimit;
                        totBJNUserCount = totBJNUserCount + orgdt.BlueJeansUserLimit;//ZD 104021
                        totCatModuleCount = totCatModuleCount + orgdt.EnableCatering;
                        totFacModuleCount = totFacModuleCount + orgdt.EnableFacilities;
                        totHKModuleCount = totHKModuleCount + orgdt.EnableHousekeeping;
                        totAPIModuleCount = totAPIModuleCount + orgdt.EnableAPI;
                        //totPCModuleCount = totPCModuleCount + orgdt.EnablePCModule; //FB 2693
                        totvmrCount = totvmrCount + orgdt.MaxVMRRooms;//FB 2586
                        //FB 2693 Starts
                        totPCUserCount = totPCUserCount + orgdt.PCUserLimit;
                       
                        totJabberCount = totJabberCount + orgdt.EnableJabber;
                        totLyncCount = totLyncCount + orgdt.EnableLync;
                        //totVidtelCount = totVidtelCount + orgdt.EnableVidtel; //ZD 102004
                        //FB 2693 Ends
                        totVCHotRoomCount = totVCHotRoomCount + orgdt.MaxVCHotdesking;//FB 2694
                        totROHotRoomCount = totROHotRoomCount + orgdt.MaxROHotdesking;
                        totAdvancedreport = totAdvancedreport + orgdt.EnableAdvancedReport;//FB 2593
                        totiControlCount = totiControlCount + orgdt.MaxiControlRooms;//ZD 101098
                    }
                }
                if (totVrmCount > videoRooms) //Validate video room count
                {
                    errNo = 653;
                    return false;
                }
                if (totNvrmCount > nonVideoRooms)//Validate non-video room count
                {
                    errNo = 650;
                    return false;
                }
                if (totvmrCount > VMRRooms)//FB 2586 Validate vmr room count
                {
                    errNo = 658;
                    return false;
                }

                //ZD 101098 START
                if (totiControlCount > iControlRooms)//Validate iControl room count
                {
                    errNo = 900;
                    return false;
                }
                //ZD 101098 END

                if (ldapEnabled == 0 && totUserCount > UserLimit)//Validate users limit //ZD 101443
                {
                    errNo = 651;
                    return false;
                }
                if (MCUEnchancedLimit > MCULimit)//FB 2486 License Validate for Enhanced limit exceeds Standard MCU
                {
                    errNo = 625;
                    return false;
                }
                if (totSmcuCount > MCULimit)//Validate Standard MCU limit
                {
                    errNo = 652;
                    return false;
                }
                if (totEmcuCount > MCUEnchancedLimit)//Validate Enhanced MCU limit
                {
                    errNo = 659;
                    return false;
                }
                if (totEndpointCount > maxEndPoints)//Validate endpoints limit 
                {
                    errNo = 661;
                    return false;
                }
                if (totGuestrmCount > ExternalRooms)//Validate Guest Room Limit
                {
                    errNo = 654;
                    return false;
                }
                //ZD 100634 start
                //ZD 101443
                if (ldapEnabled == 0 && (maxGuest > UserLimit || maxexusr > UserLimit || maxDominousr > UserLimit || maxMobileusr > UserLimit || maxPCusr > UserLimit || maxWebexusr > UserLimit || maxBlueJeanUsr > UserLimit)) //ZD 104021
                {
                    errNo = 716;
                    return false;
                }
                //ZD 100634 End
                /*if (totGuestUserCount > maxGuest)//Validate Guest Room per user 
                {
                    errNo = 634;
                    return false;
                }*/
                if (totOutlookUserCount > maxexusr)//Validate Outlook user
                {
                    errNo = 662;
                    return false;
                }
                if (totNotesUserCount > maxDominousr)//Validate Domino user (Notes User)
                {
                    errNo = 663;
                    return false;
                }
                if (totMobileUserCount > maxMobileusr)//Validate Mobile user 
                {
                    errNo = 664;
                    return false;
                }
                //ZD 104021
                if (maxBlueJeanUsr >=0 && totBJNUserCount > maxBlueJeanUsr)//Validate Blue Jeans User
                {
                    errNo = 677;
                    return false;
                }
                if (totFacModuleCount > maxav)//Validate Facilities Module
                {
                    errNo = 645;
                    return false;
                }
                if (totCatModuleCount > maxcat)//Validate Catering Module
                {
                    errNo = 647;
                    return false;
                }
                if (totHKModuleCount > maxhkg)//Validate House Keeping Module
                {
                    errNo = 646;
                    return false;
                }
                if (totAPIModuleCount > maxapi)//Validate API Module
                {
                    errNo = 648;
                    return false;
                }

                //FB 2693 Starts
                //if (totPCModuleCount > maxpc)//Validate PC Module
                //{
                //    errNo = 649;
                //    return false;
                //}
                //FB 2531 Issue in setting License - END

                if (totPCUserCount > maxPCusr)
                {
                    errNo = 683;
                    return false;
                }
                //ZD 104021 - Start
                //if (maxBlueJean != "UL")
                //{
                //    if (totBJCount > maxBJ)
                //    {
                //        errNo = 677;
                //        return false;
                //    }
                //}
                //ZD 104021 - End
                if (totJabberCount > maxJabber)
                {
                    errNo = 678;
                    return false;
                }
                if (totLyncCount > maxLync)
                {
                    errNo = 679;
                    return false;
                }
                //ZD 102004
                //if (totVidtelCount > maxVidtel)
                //{
                //    errNo = 680;
                //    return false;
                //}

                //FB 2693 Ends
				//FB 2694
                if (totVCHotRoomCount > vcHotRooms) //Validate VC HotDEsking room count
                {
                    errNo = 688;
                    return false;
                }

                if (totROHotRoomCount > roHotRooms) //Validate RO HotDEsking room count
                {
                    errNo = 689;
                    return false;
                }

                if (totAdvancedreport > maxAdvreport)//FB 2593 Validate Advanced Report Module Count
                {
                    errNo = 700;
                    return false;
                }

                isLDAP = ldapEnabled;//ZD 101443
                EnableBJNUsr = maxBlueJeanUsr;//ZD 104225

            }

            catch (Exception e)
            {
                m_log.Error("ValidateLicense: ", e);
                errNo = 407;
                return false;
            }
            return true;
        }
        #endregion

        #region SetSiteImages
        //New method added during FB 2027 - SetSuperAdmin
        private bool SetSiteImages(ref vrmDataObject obj, ref sysData siteInfo)
        {
            int SiteLogoId = 0;
            int StdBannerId = 0;
            int HighBannerId = 0;
            int LoginBackgroundId = 0; // FB 2719
            int LoginBackgroundNormalId = 0; //ZD 103581
            byte[] imgData = null;
            byte[] bannerData = null;
            byte[] HbannerData = null;
            byte[] LoginBackgroundData = null; // FB 2719
            byte[] LoginBackgroundNormalData = null; //ZD 103581

            XmlNode node;
            string Image = "";
            string xCompanymessage = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                m_vrmFactory = new vrmFactory(ref obj);
                m_imageFactory = new imageFactory(ref obj); //FB 2136
                node = xd.SelectSingleNode("//superAdmin/preference/SetImagekey/Image");

                if (node != null)
                {
                    Image = node.InnerText.Trim();
                    if (Image != "")
                    {
                        imgData = m_imageFactory.ConvertBase64ToByteArray(Image);  //FB 2136
                    }
                }
                node = xd.SelectSingleNode("//superAdmin/preference/SetImagekey/Companymessage");
                if (node != null)
                {
                    xCompanymessage = node.InnerText;
                }
                node = xd.SelectSingleNode("//superAdmin/preference/SetImagekey/StdBanner");
                if (node != null)
                {
                    Image = node.InnerText.Trim();
                    if (Image != "")
                    {
                        bannerData = m_imageFactory.ConvertBase64ToByteArray(Image);  //FB 2136
                    }
                }
                node = xd.SelectSingleNode("//superAdmin/preference/SetImagekey/HighBanner");

                if (node != null)
                {
                    Image = node.InnerText.Trim();
                    if (Image != "")
                    {
                        HbannerData = m_imageFactory.ConvertBase64ToByteArray(Image);  //FB 2136
                    }
                }

                // FB 2719 Starts
                node = xd.SelectSingleNode("//superAdmin/preference/SetImagekey/LoginBackground");

                if (node != null)
                {
                    Image = node.InnerText.Trim();
                    if (Image != "")
                    {
                        LoginBackgroundData = m_imageFactory.ConvertBase64ToByteArray(Image);  //FB 2136
                    }
                }
                // FB 2719 Ends

                //ZD 103581 - Start
                node = xd.SelectSingleNode("//superAdmin/preference/SetImagekey/LoginBackgroundNormal");
                if (node != null)
                {
                    Image = node.InnerText.Trim();
                    if (Image != "")
                    {
                        LoginBackgroundNormalData = m_imageFactory.ConvertBase64ToByteArray(Image);
                    }
                }
                //ZD 103581 - End

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("AttributeType", 15)); //Site Logo...

                List<ICriterion> StdcriterionList = new List<ICriterion>();
                StdcriterionList.Add(Expression.Eq("AttributeType", 20)); //Std Banner..

                List<ICriterion> HighcriterionList = new List<ICriterion>();
                HighcriterionList.Add(Expression.Eq("AttributeType", 21)); //High Banner..
				
				//ZD 100996
                List<ICriterion> LobbybgcriterionList = new List<ICriterion>();
                LobbybgcriterionList.Add(Expression.Eq("AttributeType", 23)); //Login background Wide

                //ZD 103581
                List<ICriterion> LobbybgNormalcriterionList = new List<ICriterion>();
                LobbybgNormalcriterionList.Add(Expression.Eq("AttributeType", 31)); //Login background Normal

                List<vrmImage> siteLogos = m_IImageDAO.GetByCriteria(criterionList);
                List<vrmImage> stdsiteLogos = m_IImageDAO.GetByCriteria(StdcriterionList);
                List<vrmImage> HighsiteLogos = m_IImageDAO.GetByCriteria(HighcriterionList);
                List<vrmImage> Lobbybg = m_IImageDAO.GetByCriteria(LobbybgcriterionList);//ZD 100996
                List<vrmImage> LobbybgNormal = m_IImageDAO.GetByCriteria(LobbybgNormalcriterionList);//ZD 103581



                foreach (vrmImage siteObj in siteLogos)
                {
                    m_IImageDAO.Delete(siteObj);
                }
                foreach (vrmImage siteObj in stdsiteLogos)
                {
                    m_IImageDAO.Delete(siteObj);
                }
                foreach (vrmImage siteObj in HighsiteLogos)
                {
                    m_IImageDAO.Delete(siteObj);
                }
                foreach (vrmImage siteObj in Lobbybg)//ZD 100996
                {
                    m_IImageDAO.Delete(siteObj);
                }
                foreach (vrmImage siteObj in LobbybgNormal)//ZD 103581
                {
                    m_IImageDAO.Delete(siteObj);
                }
                vrmImage imgObj = new vrmImage();
                if (imgData != null)
                {
                    imgObj.AttributeType = 15; //Site logo
                    imgObj.AttributeImage = imgData;
                    imgObj.OrgId = 11; // FB 2719
                    m_vrmFactory.SetImage(imgObj, ref SiteLogoId);
                }

                imgObj = new vrmImage();
                if (bannerData != null)
                {
                    imgObj.AttributeType = 20;
                    imgObj.AttributeImage = bannerData;
                    imgObj.OrgId = 11; // FB 2719
                    m_vrmFactory.SetImage(imgObj, ref StdBannerId);
                }

                imgObj = new vrmImage();
                if (HbannerData != null)
                {
                    imgObj.AttributeType = 21;
                    imgObj.AttributeImage = HbannerData;
                    imgObj.OrgId = 11; // FB 2719
                    m_vrmFactory.SetImage(imgObj, ref HighBannerId);
                }

                // FB 2719 Starts
                imgObj = new vrmImage();
                if (LoginBackgroundData != null)
                {
                    imgObj.AttributeType = 23;
                    imgObj.AttributeImage = LoginBackgroundData;
                    imgObj.OrgId = 11;
                    m_vrmFactory.SetImage(imgObj, ref LoginBackgroundId);
                }
                // FB 2719 Ends

                // ZD 103581 - Start
                imgObj = new vrmImage();
                if (LoginBackgroundNormalData != null)
                {
                    imgObj.AttributeType = 31;
                    imgObj.AttributeImage = LoginBackgroundNormalData;
                    imgObj.OrgId = 11;
                    m_vrmFactory.SetImage(imgObj, ref LoginBackgroundNormalId);
                }
                // ZD 103581 - End
                
                siteInfo.Companymessage = xCompanymessage;
                siteInfo.SiteLogoId = SiteLogoId;
                siteInfo.StdBannerId = StdBannerId;
                siteInfo.HighBannerId = HighBannerId;
                siteInfo.LoginBackgroundId = LoginBackgroundId; // FB 2719
                siteInfo.LoginBackgroundNormalId = LoginBackgroundNormalId; // FB 2719 // ZD 103581
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
        }
        #endregion

        #region GetSystemDateTime
        /// <summary>
        /// Get System Date & TimeZone Details
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetSystemDateTime(ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            StringBuilder outXML = new StringBuilder();
            vrmUser user = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;

                node = xd.SelectSingleNode("login/userID");
                int userid = 0;
                Int32.TryParse(node.InnerXml.Trim(), out userid);
                if (userid < 0)
                {
                    myvrmEx = new myVRMException(201);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                Int32 tzone = 0;
                DateTime tdayDate = DateTime.Now;

                if (userid == 0)
                {
                    IList list = m_IsystemDAO.GetObjectByCriteria(criterionList);

                    foreach (sysData vrmsys in list)
                    {
                        tzone = vrmsys.TimeZone;
                        timeZone.changeToGMTTime(sysSettings.TimeZone, ref tdayDate);
                        timeZone.userPreferedTime(tzone, ref tdayDate);
                    }
                }
                else
                {
                    user = new vrmUser();
                    user = m_IuserDao.GetByUserId(userid);
                    tzone = user.TimeZone;                   
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref tdayDate);
                    timeZone.userPreferedTime(tzone, ref tdayDate);
                }

                outXML.Append("<systemDateTime>");
                if (tzone > 0)
                {
                    outXML.Append("<systemDate>" + tdayDate.ToShortDateString() + "</systemDate>");
                    outXML.Append("<systemTime>" + tdayDate.ToShortTimeString() + "</systemTime>");
                    outXML.Append("<systemTimeZone>" + tzone.ToString() + "</systemTimeZone>");
                }
                else
                {
                    myvrmEx = new myVRMException(200);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                OrganizationFactory m_OrgFactory = new OrganizationFactory(m_configPath, m_log);
                StringBuilder timezoneXml = new StringBuilder();

                if (m_OrgFactory.timeZonesToXML(ref timezoneXml))
                    outXML.Append(timezoneXml);

                outXML.Append("</systemDateTime>");

                obj.outXml = outXML.ToString();

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }

        #endregion

        #region AppendEmailDetails

        public void AppendEmailDetails(ref StringBuilder outXml)
        {
            try
            {
                List<OrgData> AllOrgData = new List<OrgData>(); //FB 2993
                sysMailData sysMailData = new sysMailData();
                sysMailData = m_ISysMailDAO.GetById(1);

                outXml.Append("<emailSystem>");
                outXml.Append("     <companyEmail>" + sysMailData.CompanyMail + "</companyEmail>");
                outXml.Append("     <remoteServer>" + sysMailData.IsRemoteServer.ToString() + "</remoteServer>");
                outXml.Append("     <serverAddress>" + sysMailData.ServerAddress.Trim() + "</serverAddress>");
                outXml.Append("     <accountLogin>" + sysMailData.Login + "</accountLogin>");
                outXml.Append("     <accountPwd>" + sysMailData.password + "</accountPwd>");
                outXml.Append("     <portNo>" + sysMailData.portNo.ToString() + "</portNo>");
                outXml.Append("     <connectionTimeout>" + sysMailData.ConTimeOut.ToString() + "</connectionTimeout>");
                outXml.Append("     <websiteURL>" + sysMailData.websiteURL + "</websiteURL>");
                outXml.Append("</emailSystem>");
                //FB 2501 Starts
                outXml.Append("<ServerOptions>");
                outXml.Append("<StartMode>" + sysSettings.StartMode + "</StartMode>");
                outXml.Append("<SiteCongSupport>" + sysSettings.ConciergeSupport + "</SiteCongSupport>");
				outXml.Append("<ViewPublicConf>" + sysSettings.ViewPublicConf + "</ViewPublicConf>");//FB 2858
				outXml.Append("<EnableCloudInstallation>" + sysSettings.EnableCloudInstallation + "</EnableCloudInstallation>"); //FB 2659
                outXml.Append("<roomCascadingControl>" + sysSettings.roomCascadingControl + "</roomCascadingControl>");//ZD 100263
                outXml.Append("<ConfIDStartValue>" + sysSettings.ConfIDStartValue + "</ConfIDStartValue>");//ZD 100526
                outXml.Append("<EnableNetworkFeatures>" + sysSettings.EnableNetworkFeatures + "</EnableNetworkFeatures>");//FB 2993 start
                outXml.Append("<SessionTimeout>" + sysSettings.SessionTimeout + "</SessionTimeout>"); // ZD 101011
                outXml.Append("<ConfRecurLimit>" + sysSettings.ConfRecurLimit + "</ConfRecurLimit>");//ZD 101837
                outXml.Append("<ReqUsrAcc>" + sysSettings.ReqUsrAcc + "</ReqUsrAcc>");//ZD 101846
                //103496
                outXml.Append("<MemcacheEnabled>" + sysSettings.MemcacheEnabled + "</MemcacheEnabled>");
                //FB 2993 Starts
                if (sysSettings.EnableNetworkFeatures == 0)
                {
                    AllOrgData = m_IOrgSettingsDAO.GetAll();
                    AllOrgData = AllOrgData.Select((data, i) => { data.NetworkSwitching = 1; data.SecureSwitch = 0; return data; }).ToList();
                    m_IOrgSettingsDAO.SaveOrUpdateList(AllOrgData);
                } //FB 2993 End
                outXml.Append("</ServerOptions>");
                //FB 2501 Ends
                //FB 2678 Start
                outXml.Append("<OrganizationOptions>");
                outXml.Append("<IndividualOrgExpiry>" + sysSettings.IndividualOrgExpiry + "</IndividualOrgExpiry>");
                outXml.Append("</OrganizationOptions>");
                //FB 2678 End
            }
            catch (Exception ex)
            {
                m_log.Error("AppendEmailDetails" + ex.Message);
            }
        }

        #endregion

        #region SearchLog
        /// <summary>
        /// SearchLog
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SearchLog(ref vrmDataObject obj)
        {
            bool bRet = true;
            string severity = "", file = "", function = "", message = "", pageno = "", modulename = "";
            int pno = 0, err = 0, line = 0, numrecs = 0, moduleerrcode = 0;
            long ttlRecords = 0;
            DateTime from = DateTime.Now, to = DateTime.Now;
            StringBuilder outxml = new StringBuilder();
            IList errlog = new ArrayList();
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                ArrayList strsList = new ArrayList();
                List<int> sever = new List<int>();
                XmlNode node;

                node = xd.SelectSingleNode("//searchLog/from");
                if (node != null)
                    DateTime.TryParse(node.InnerText.Trim(), out from);
                criterionList.Add(Expression.Ge("timelogged", from));


                node = xd.SelectSingleNode("//searchLog/to");
                if (node != null)
                    DateTime.TryParse(node.InnerText.Trim(), out to);
                criterionList.Add(Expression.Le("timelogged", to));

                node = xd.SelectSingleNode("//searchLog/module/moduleName");
                if (node != null)
                    modulename = node.InnerXml.Trim();
                if (modulename != "")
                    criterionList.Add(Expression.Like("modulename", modulename));

                node = xd.SelectSingleNode("//searchLog/module/moduleErrorCode");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out moduleerrcode);
                if (moduleerrcode != 0)
                    criterionList.Add(Expression.Like("moduleerrcode", moduleerrcode));


                node = xd.SelectSingleNode("//searchLog/severity");
                if (node != null)
                    severity = node.InnerXml.Trim();
                if (severity != "")
                    if (severity.IndexOf(",") >= 0)
                    {
                        string[] strs = severity.Split(',');
                        foreach (string s in strs)
                            strsList.Add(s);
                        criterionList.Add(Expression.In("severity", strsList));
                    }
                    else
                    {
                        err = Int32.Parse(severity);
                        sever.Add(err);
                        criterionList.Add(Expression.In("severity", sever));
                    }

                node = xd.SelectSingleNode("//searchLog/file");
                if (node != null)
                    file = node.InnerXml.Trim();
                if (file != "")
                    criterionList.Add(Expression.Like("fil", file));

                node = xd.SelectSingleNode("//searchLog/function");
                if (node != null)
                    function = node.InnerXml.Trim();
                if (function != "")
                    criterionList.Add(Expression.Like("function", function));

                node = xd.SelectSingleNode("//searchLog/line");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out line);
                if (line != 0)
                    criterionList.Add(Expression.Like("line", line));

                node = xd.SelectSingleNode("//searchLog/message");
                if (node != null)
                    message = node.InnerXml.Trim();
                if (message != "")
                    criterionList.Add(Expression.Like("mess", message));

                node = xd.SelectSingleNode("//searchLog/pageNo");
                if (node != null)
                    pageno = node.InnerXml.Trim();
                if (pageno != "")
                    pno = Int32.Parse(pageno);
                if (pno == 0)
                    pno = 1;
                outxml.Append("<searchLog>");

                IList errlist = new List<vrmErrorLog>();
                m_IvrmErrLog.addProjection(Projections.RowCount());
                IList list = m_IvrmErrLog.GetObjectByCriteria(new List<ICriterion>());
                list = m_IvrmErrLog.GetObjectByCriteria(criterionList);
                Int32.TryParse((list[0].ToString()), out numrecs);
                m_IvrmErrLog.clearProjection();

                m_IvrmErrLog.pageSize(m_PAGE_SIZE);
                m_IvrmErrLog.pageNo(pno);
                ttlRecords = m_IvrmErrLog.CountByCriteria(criterionList);

                errlist = m_IvrmErrLog.GetByCriteria(criterionList);

                int ttlPages = (int)(ttlRecords / m_PAGE_SIZE);

                if (ttlRecords % m_PAGE_SIZE > 0)
                    ttlPages++;

                vrmErrorLog errl = null;
                for (int k = 0; k < errlist.Count; k++)
                {
                    errl = (vrmErrorLog)errlist[k];
                    outxml.Append("<log>");
                    outxml.Append("<logID>" + errl.logid + "</logID>");
                    outxml.Append("<moduleName>" + errl.modulename + "</moduleName>");
                    outxml.Append("<moduleErrorCode>" + errl.moduleerrcode + "</moduleErrorCode>");
                    outxml.Append("<severity>" + errl.severity + "</severity>");
                    outxml.Append("<file>" + errl.fil + "</file>");
                    outxml.Append("<function></function>");
                    outxml.Append("<line>" + errl.line + "</line>");
                    outxml.Append("<timestamp>" + errl.timelogged + "</timestamp>");
                    outxml.Append("<message>" + errl.mess.Replace('<', '(').Replace('>', ')').Replace('/', '|') + "</message>");
                    outxml.Append("</log>");
                }
                outxml.Append("<pageNo>" + pno + "</pageNo>");
                outxml.Append("<totalPages>" + ttlPages.ToString() + "</totalPages>");
                outxml.Append("</searchLog>");
                obj.outXml = outxml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region Get SuperAdmin
        /// <summary>
        /// Get System License Details
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetSuperAdmin(ref vrmDataObject obj)
        {
            sysMailData sysMailDt = null;
            vrmLDAPConfig ldapSettings = null;
            cryptography.Crypto crypto = null;
            string dformat = "MM/dd/yyyy";
            vrmUser myVrmUser = null;
            String whygoURL = "", whygoPassword = "", whygoUser = "", whyGoAdminEmail="";//FB 2392 //ZD 100694
            List<ICriterion> criteriaWhygo = null;//FB 2392
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string userId = node.InnerXml.Trim();

                if (userId != "")
                    myVrmUser = new userDAO(obj.ConfigPath, obj.log).GetUserDao().GetByUserId(Int32.Parse(userId));

                if (myVrmUser != null)
                    dformat = myVrmUser.DateFormat;

                StringBuilder siteDt = new StringBuilder();
                siteDt.Append("<preference>");
                siteDt.Append("<securityKey>" + sysSettings.license + "</securityKey>");
                siteDt.Append("<SystemTimeZoneID>" + sysSettings.TimeZone + "</SystemTimeZoneID>");
                siteDt.Append("<launchBuffer>" + sysSettings.LaunchBuffer + "</launchBuffer>");
                siteDt.Append("<StartMode>"+sysSettings.StartMode+"</StartMode>");  //FB 2051
                siteDt.Append("<EnablePublicRoom>" + sysSettings.EnablePublicRooms + "</EnablePublicRoom>");  //FB 2594
                siteDt.Append("<EnableLaunchBufferP2P>" + sysSettings.EnableLaunchBufferP2P + "</EnableLaunchBufferP2P>");//FB 2437
                siteDt.Append("<IndividualOrgExpiry>" + sysSettings.IndividualOrgExpiry + "</IndividualOrgExpiry>");//FB 2678
                siteDt.Append("<MemcacheEnabled>" + sysSettings.MemcacheEnabled + "</MemcacheEnabled>"); //ZD 103496
                siteDt.Append("<SessionTimeout>" + sysSettings.SessionTimeout + "</SessionTimeout>"); // ZD 101011
                siteDt.Append("<EnableCloudInstallation>" + sysSettings.EnableCloudInstallation + "</EnableCloudInstallation>");//FB 2659
                siteDt.Append("<EnableNetworkFeatures>" + sysSettings.EnableNetworkFeatures + "</EnableNetworkFeatures>");//ZD 100116
                siteDt.Append("<roomCascadingControl>" + sysSettings.roomCascadingControl + "</roomCascadingControl>");//ZD 100263
                siteDt.Append("<ConfIDStartValue>" + sysSettings.ConfIDStartValue + "</ConfIDStartValue>");//ZD 100526     
                siteDt.Append("<ConfRecurLimit>" + sysSettings.ConfRecurLimit + "</ConfRecurLimit>");//ZD 101837

                //FB 2594 Starts
                String PublicRoomService = "No";
                if (sysSettings.EnablePublicRooms == 1)
                    PublicRoomService = "Yes";
                
                //ZD 101443 START
                string userLimit = sysSettings.UserLimit.ToString();
                if (sysSettings.IsLDAP == 1)
                    userLimit = "AD";

                //ZD 104021
                string BlueJeanUserLimit = sysSettings.MaxBlueJeansUsers.ToString();
                if (sysSettings.MaxBlueJeansUsers == -2)
                    BlueJeanUserLimit = "UL";
                
                siteDt.Append("<licenseDetail> " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Expiration Date") + ": " + sysSettings.ExpiryDate.ToString(dformat) + "; " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Organization Limit") + ": " + sysSettings.MaxOrganizations.ToString() + "; " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Public Room Service") + ": " + m_utilFactory.GetTranslatedText(myVrmUser.Language,PublicRoomService) + "; " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Vidyo") + " : " + sysSettings.Cloud.ToString() + //FB 2834 //ZD 101714
                    "; " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Room Limit") + ": " + sysSettings.RoomLimit.ToString() + " (" + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Max Non Video Rooms") + ": " + sysSettings.MaxNVidRooms.ToString() + "; " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Max Video Rooms") + ": " + sysSettings.MaxVidRooms.ToString() + ", " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Max Virtual Meeting Room") + ": " + sysSettings.MaxVMRooms.ToString() + ",  " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Max iControl Room") + ": " + sysSettings.MaxiControlRooms.ToString() + ") " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Max RO Hotdesking Rooms") + ":" + sysSettings.MaxROHotdesking + "; " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Max VC Hotdesking Rooms") + ":" + sysSettings.MaxVCHotdesking + "; " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Guest Room Limit") + ": " + sysSettings.ExtRoomLimit.ToString() + "; " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Standard MCU Limit") + ": "//FB 2694 //ZD 100806
                    + sysSettings.MCULimit.ToString() + "; " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Enhanced MCU Limit") + ": " + sysSettings.MCUEnchancedLimit.ToString() + "; " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "End Point Limit") + ": " + sysSettings.MaxEndPts.ToString() + "; " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "User Limit") + " : " + userLimit + "(" + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Max Guests Per User") + ": " + sysSettings.MaxGstPerUsrs.ToString() + ", " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Max Exchange User") + ": " +
                    sysSettings.MaxExcUsrs.ToString() + ", " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Max Domino User") + ": " + sysSettings.MaxDomUsrs.ToString() + ", " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Max WebEx User") + ": " + sysSettings.MaxWebexUsr.ToString() + "," + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Max Mobile User") + ": " + sysSettings.MaxMobUsrs.ToString() + ", " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Max Blue Jeans User") + ": " + BlueJeanUserLimit + ", " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Max Desktop Video User") + ": " + sysSettings.MaxPCUsers.ToString() + ") " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Catering Module") + ": " + sysSettings.MaxCatering.ToString() + "; " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "HouseKeeping Module") + ": " + sysSettings.MaxHousekeeping.ToString() + "; " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Facilities Module") + ": " + //ZD 100221 100807 //ZD 104021
                    //sysSettings.MaxFacilities.ToString() + "; "+ m_utilFactory.GetTranslatedText(myVrmUser.Language, "API Module") + ": " + sysSettings.MaxAPIs.ToString() + "; Blue Jeans: " + sysSettings.MaxBlueJeans.ToString() + "; Jabber: " + sysSettings.MaxJabber.ToString() + "; Lync: " + sysSettings.MaxLync.ToString() + "; Vidtel: " + sysSettings.MaxVidtel.ToString() + ";" + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Adv Rep Mod") + ": " + sysSettings.MaxAdvancedReport.ToString() + ";" + "</licenseDetail>");//FB 2347 FB 2426 //FB 2486 //FB 2594 //FB 2586 //FB 2693 //FB 2593 //ZD 101098
                    sysSettings.MaxFacilities.ToString() + "; " + m_utilFactory.GetTranslatedText(myVrmUser.Language, "API Module") + ": " + sysSettings.MaxAPIs.ToString() + "; Jabber: " + sysSettings.MaxJabber.ToString() + "; Lync: " + sysSettings.MaxLync.ToString() + ";" + m_utilFactory.GetTranslatedText(myVrmUser.Language, "Adv Rep Mod") + ": " + sysSettings.MaxAdvancedReport.ToString() + ";" + "</licenseDetail>");//FB 2347 FB 2426 //FB 2486 //FB 2594 //FB 2586 //FB 2693 //FB 2593 //ZD 101098 //ZD 102004 //ZD 104021
                //ZD 101443 END

                siteDt.Append("<foodSecurityKey>" + sysSettings.foodsecuritykey + "</foodSecurityKey>");
                siteDt.Append("<resourceSecurityKey>" + sysSettings.resourcesecuritykey + "</resourceSecurityKey>");
                siteDt.Append("<emailSystem>");

                try
                {
                    sysMailDt = m_ISysMailDAO.GetById(1);
                    if (sysMailDt != null)
                    {
                        siteDt.Append("<companyEmail>" + sysMailDt.CompanyMail + "</companyEmail>");
                        siteDt.Append("<remoteServer>" + sysMailDt.IsRemoteServer + "</remoteServer>");
                        siteDt.Append("<serverAddress>" + sysMailDt.ServerAddress + "</serverAddress>");
                        siteDt.Append("<accountLogin>" + sysMailDt.Login + "</accountLogin>");
                        siteDt.Append("<accountPwd>" + sysMailDt.password + "</accountPwd>"); //FB 1070
                        siteDt.Append("<portNo>" + sysMailDt.portNo + "</portNo>");
                        siteDt.Append("<connectionTimeout>" + sysMailDt.ConTimeOut + "</connectionTimeout>");
                        siteDt.Append("<displayName>" + sysMailDt.displayname + "</displayName>");
                        siteDt.Append("<messageTemplate>" + sysMailDt.messagetemplate + "</messageTemplate>");
                        siteDt.Append("<websiteURL>" + sysMailDt.websiteURL + "</websiteURL>");
                        siteDt.Append("<retrycount>" + sysMailDt.RetryCount + "</retrycount>");//FB 2552
                        siteDt.Append("<serverTimeout>" + sysMailDt.ServerTimeout + "</serverTimeout>");//ZD 100317
                    }
                }
                catch (Exception)
                { }

                siteDt.Append("</emailSystem>");
                siteDt.Append("<LDAP>");
				//ZD 101443 Starts
                //ldapSettings = new vrmLDAPConfig();
                //ldapSettings = m_orgDAO.GetLDAPConfigDao().GetLDAPConfigDetails(m_configPath);
                List<ICriterion> criterionList = criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgId", 11));
                List<vrmLDAPConfig> ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(criterionList);
                if (ldapSettingsList != null && ldapSettingsList.Count > 0)
                    ldapSettings = ldapSettingsList[0];
                //Empty DB issue FB 2164 - Start
				//ZD 101443 End
                if (ldapSettings == null || ldapSettings.serveraddress == null)
                {
                    siteDt.Append("<serverAddress></serverAddress>");
                    siteDt.Append("<loginName></loginName>");
                    siteDt.Append("<loginPassword></loginPassword>");
                    siteDt.Append("<portNo></portNo>");
                    siteDt.Append("<connectionTimeout></connectionTimeout>");
                    siteDt.Append("<loginkey></loginkey>");
                    siteDt.Append("<synctime></synctime>");
                    siteDt.Append("<LDAPPrefix></LDAPPrefix>");
                    siteDt.Append("<searchfilter></searchfilter>");
                    siteDt.Append("<AuthenticationType></AuthenticationType>");
                    siteDt.Append("<EnableDomain>0</EnableDomain>");
                    siteDt.Append("<TenantOption>-1</TenantOption>");
                    siteDt.Append("<EnableSSOMode>0</EnableSSOMode>");
                    siteDt.Append("<scheduler>");
                    siteDt.Append("<Time></Time>");
                    siteDt.Append("<Days></Days>");
                    siteDt.Append("</scheduler>");
                }
                //Empty DB issue FB 2164 - End
                else if (ldapSettings.serveraddress.Trim().Length > 1)
                {
                    crypto = new cryptography.Crypto();
                    siteDt.Append("<serverAddress>" + ldapSettings.serveraddress.Trim() + "</serverAddress>");
                    siteDt.Append("<loginName>" + ldapSettings.login.Trim() + "</loginName>");
                    //siteDt.Append("<loginPassword>" + crypto.decrypt(ldapSettings.password.Trim()) + "</loginPassword>");
                    siteDt.Append("<loginPassword>" + ldapSettings.password.Trim() + "</loginPassword>"); //FB 3054
                    siteDt.Append("<portNo>" + ldapSettings.port + "</portNo>");
                    siteDt.Append("<connectionTimeout>" + ldapSettings.timeout + "</connectionTimeout>");
                    siteDt.Append("<loginkey>" + ldapSettings.LoginKey + "</loginkey>");
                    siteDt.Append("<synctime>" + ldapSettings.SyncTime.ToString("hh:mm tt") + "</synctime>");
                    siteDt.Append("<LDAPPrefix>" + ldapSettings.domainPrefix + "</LDAPPrefix>");
                    siteDt.Append("<searchfilter>" + ldapSettings.SearchFilter + "</searchfilter>");
                    siteDt.Append("<scheduler>");
                    siteDt.Append("<Time>" + ldapSettings.scheduleTime.ToString("hh:mm tt") + "</Time>");
                    siteDt.Append("<Days>" + ldapSettings.scheduleDays + "</Days>");
                    siteDt.Append("</scheduler>");
                    siteDt.Append("<AuthenticationType>" + ldapSettings.AuthType + "</AuthenticationType>"); //FB 2993 LDAP
                    //ZD 101443-LDAP Starts
                    siteDt.Append("<EnableDomain>" + ldapSettings.EnableDomain + "</EnableDomain>");
                    siteDt.Append("<TenantOption>" + ldapSettings.TenantOption + "</TenantOption>");
                    siteDt.Append("<EnableSSOMode>" + ldapSettings.EnableSSOMode + "</EnableSSOMode>");
                    //ZD 101443-LDAP End
                }
                siteDt.Append("</LDAP>");

                //FB 2501 EM7 Starts
                siteDt.Append("<EM7Connectivity>");
                if (sysSettings.EM7URI != null)
                    siteDt.Append("<EM7URI>" + sysSettings.EM7URI + "</EM7URI>");
                if (sysSettings.EM7Username != null)
                    siteDt.Append("<EM7Username>" + sysSettings.EM7Username + "</EM7Username>");
                if (sysSettings.EM7Password != null)
                    siteDt.Append("<EM7Password>" +sysSettings.EM7Password + "</EM7Password>");
                 if (sysSettings.EM7Port != null)
                    siteDt.Append("<EM7Port>" +sysSettings.EM7Port + "</EM7Port>");
                siteDt.Append("</EM7Connectivity>");
                //FB 2501 EM7 Ends

                //FB 2363 - Start
                List<ICriterion> criteria = new List<ICriterion>();
                criteria.Add(Expression.Eq("UId", 1));
                IList<sysExternalScheduling> sysES = m_ESDAO.GetByCriteria(criteria);

                siteDt.Append("<External>");
                if (sysES != null && sysES.Count > 0)
                {
                    crypto = new cryptography.Crypto();
                    siteDt.Append("<PartnerName>" + sysES[0].PartnerName + "</PartnerName>");
                    siteDt.Append("<PartnerEmail>" + sysES[0].PartnerEmail + "</PartnerEmail>");
                    siteDt.Append("<PartnerURL>" + sysES[0].PartnerURL + "</PartnerURL>");
                    siteDt.Append("<UserName>" + sysES[0].UserName + "</UserName>");
                    siteDt.Append("<Password>" + sysES[0].Password + "</Password>"); //FB  3054
                    siteDt.Append("<ModifiedDate>" + sysES[0].ModifiedDate.ToString() + "</ModifiedDate>");
                    siteDt.Append("<TimeoutValue>" + sysES[0].TimeoutValue + "</TimeoutValue>");//FB 2363
                }

                List<ESMailUsrRptSettings> esMailUsrRpts = m_ESMailUsrRptSetDAO.GetByCriteria(criteria);
                if (esMailUsrRpts != null && esMailUsrRpts.Count > 0)
                {
                    siteDt.Append("<MailusrRptSetting>");
                    siteDt.Append("<CustomerName>" + esMailUsrRpts[0].CustomerName + "</CustomerName>");
                    siteDt.Append("<DeliveryType>" + esMailUsrRpts[0].DeliveryType + "</DeliveryType>");
                    siteDt.Append("<RptDestination>" + esMailUsrRpts[0].RptDestination + "</RptDestination>");
                    siteDt.Append("<StartTime>" + esMailUsrRpts[0].StartTime.ToString("MM/dd/yyyy") + "</StartTime>");
                    siteDt.Append("<FrequencyType>" + esMailUsrRpts[0].FrequencyType + "</FrequencyType>");
                    siteDt.Append("<FrequencyCount>" + esMailUsrRpts[0].FrequencyCount + "</FrequencyCount>");
                    siteDt.Append("<Sent>" + esMailUsrRpts[0].Sent + "</Sent>");
                    if (esMailUsrRpts[0].Sent == 1)
                        siteDt.Append("<SentTime>" + esMailUsrRpts[0].SentTime + "</SentTime>");
                    else
                        siteDt.Append("<SentTime></SentTime>");

                    siteDt.Append("</MailusrRptSetting>");
                }
                siteDt.Append("</External>");
                //FB 2363 - End

                //FB 2392
                siteDt.Append("<WhyGo>");
                criteriaWhygo = new List<ICriterion>();
                criteriaWhygo.Add(Expression.Eq("UId", 1));
                IList<sysWhygoSettings> sysWhygoList = m_WhygoSettingsDAO.GetByCriteria(criteriaWhygo);
                if (sysWhygoList != null && sysWhygoList.Count > 0)
                {
                    whygoURL = sysWhygoList[0].WhyGoURL;
                    whygoUser = sysWhygoList[0].WhyGoUserName;
                    whygoPassword = sysWhygoList[0].WhyGoPassword;
                    whyGoAdminEmail = sysWhygoList[0].WhyGoAdminEmail;//ZD 100694
                }

                siteDt.Append("<WhyGoURL>" + whygoURL + "</WhyGoURL>");//FB 2392 start
                siteDt.Append("<WhyGoUserName>" + whygoUser + "</WhyGoUserName>");
                siteDt.Append("<WhyGoPassword>" + whygoPassword + "</WhyGoPassword>");//FB 2392 end
                siteDt.Append("<WhyGoAdminEmail>" + whyGoAdminEmail + "</WhyGoAdminEmail>");//ZD 100694
                siteDt.Append("</WhyGo>");
                //FB 2392
                siteDt.Append("<ConciergeSupport>" + sysSettings.ConciergeSupport + "</ConciergeSupport>");//FB 2670
                siteDt.Append("<ViewPublicConf>" + sysSettings.ViewPublicConf + "</ViewPublicConf>");//FB 2858
                siteDt.Append("<ReqUsrAcc>" + sysSettings.ReqUsrAcc + "</ReqUsrAcc>");//ZD 101846

				//ZD 100152 Starts
                siteDt.Append("<GoogleConfig>");
                if (sysSettings.PushtoGoogle != null)
                    siteDt.Append("<PushtoGoogle>" + sysSettings.PushtoGoogle + "</PushtoGoogle>");
                siteDt.Append("</GoogleConfig>");
				//ZD 100152 Ends
                
                siteDt.Append("</preference>");

                obj.outXml = siteDt.ToString();
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        //FB 2027 end


        //FB 2363
        #region GeneratESUserReport
        /// <summary>
        /// GeneratESUserReport
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GenerateESUserReport(ref vrmDataObject obj)
        {
            try
            {
                int noofday = 0, days = 0;
                Double total =0;
                DateTime currdate = DateTime.Now;
                List<ICriterion> criteria = new List<ICriterion>();
                criteria.Add(Expression.Eq("UId", 1));
                criteria.Add(Expression.Eq("Type", "S"));
                List<ESMailUsrRptSettings> esMailUsrRpts = m_ESMailUsrRptSetDAO.GetByCriteria(criteria,true);
                if (esMailUsrRpts != null && esMailUsrRpts.Count > 0)
                {
                    noofday = esMailUsrRpts[0].FrequencyCount;
                    days = esMailUsrRpts[0].FrequencyType; //dafault 1

                    if (esMailUsrRpts[0].FrequencyType == 2)
                        days = 7;
                    else if (esMailUsrRpts[0].FrequencyType == 3)
                        days = 30;

                    if (esMailUsrRpts[0].Sent == 0)
                        total = (currdate - esMailUsrRpts[0].StartTime).TotalDays;
                    else
                        total = (currdate - esMailUsrRpts[0].SentTime).TotalDays;

                    if (total >= (Double)(noofday * days))
                    {
                        m_vrmReportFact = new ReportFactory(ref obj);
                        m_vrmEmailFact = new emailFactory(ref obj);

                        m_vrmReportFact.ConferenceReports(ref obj);
                        if (obj.outXml.IndexOf("<error>") < 0)
                        {
                            if (esMailUsrRpts[0].RptDestination != null && esMailUsrRpts[0].RptDestination != "")//ZD 100350
                            {
                                m_vrmEmailFact.SendESReport(esMailUsrRpts[0].RptDestination, ref m_vrmReportFact.destDirectory, "U"); //FB 2363
                                {
                                    esMailUsrRpts[0].Sent = 1;
                                    esMailUsrRpts[0].SentTime = new DateTime(currdate.Year, currdate.Month, currdate.Day, 0, 0, 0);
                                    m_ESMailUsrRptSetDAO.Update(esMailUsrRpts[0]);
                                }
                            }
                        }
                    }
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }

            return true;
        }

        #endregion

        #region GenerateESErrorReport
        /// <summary>
        /// GenerateESErrorReport
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GenerateESErrorReport(ref vrmDataObject obj)
        {
            try
            {
                int min = 0, days = 0;
                Double total =0;
                DateTime currdate = DateTime.Now;
                List<ICriterion> criteria = new List<ICriterion>();
                criteria.Add(Expression.Eq("Type", "O"));
                List<ESMailUsrRptSettings> esMailUsrRpts = m_ESMailUsrRptSetDAO.GetByCriteria(criteria,true);
                ESMailUsrRptSettings esMailUsrRptSettings = null;

                if (esMailUsrRpts != null && esMailUsrRpts.Count > 0)
                {
                    for (int cnt = 0; cnt < esMailUsrRpts.Count; cnt++)
                    {
                        total = 0;
                        min = 0;

                        esMailUsrRptSettings = esMailUsrRpts[cnt];

                        min = esMailUsrRptSettings.FrequencyCount;
                        
                        if (esMailUsrRptSettings.Sent == 0)
                            total = (currdate - esMailUsrRptSettings.StartTime).TotalMinutes;
                        else
                            total = (currdate - esMailUsrRptSettings.SentTime).TotalMinutes;

                        if (total >= (Double)(min))
                        {
                            obj.inXml = obj.inXml.Replace("</report>", "<organizationID>" + esMailUsrRptSettings.orgId + "</organizationID></report>");

                            m_vrmReportFact = new ReportFactory(ref obj);
                            m_vrmEmailFact = new emailFactory(ref obj);

                            m_vrmReportFact.ConferenceReports(ref obj);
                            if (obj.outXml.IndexOf("<error>") < 0)
                            {
                                if (esMailUsrRptSettings.RptDestination != null && esMailUsrRptSettings.RptDestination != "")//ZD 100350
                                {
                                    if (m_vrmEmailFact.SendESReport(esMailUsrRptSettings.RptDestination, ref m_vrmReportFact.destDirectory, "E"))
                                    {
                                        esMailUsrRptSettings.Sent = 1;
                                        esMailUsrRptSettings.SentTime = DateTime.Now;
                                        m_ESMailUsrRptSetDAO.Update(esMailUsrRptSettings);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }

            return true;
        }

        #endregion

        //FB 2594 Starts
        #region PublicRoomLicenseCheck
        /// <summary>
        /// PublicRoomLicenseCheck
        /// </summary>
        /// <param name="errID"></param>
        /// <returns></returns>
        private bool PublicRoomLicenseCheck(int mode, ref int errID)
        {
            try
            {
                string hql;
                DataSet ds = null;
                
                //Mode  1 - License Checking ,  2 -Deleting Public Rooms and EP
                
                if (m_sysLayer == null)
                    m_sysLayer = new ns_SqlHelper.SqlHelper(m_configPath); 

                m_sysLayer.OpenConnection();
                //FB 3069 Starts
                DateTime confEnd = DateTime.Now;
                DateTime confFrom = DateTime.Now;

                if (m_Search.getSearchDateRange(1, ref confEnd, ref confFrom, 0))
                {
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref confFrom);
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref confEnd);
                }


                hql = " Select a.status, a.confid, a.InstanceId from Conf_Conference_D a, Conf_Room_D b, Loc_Room_D  c "
                    + " where a.confid = b.confid and  a.InstanceId = b.InstanceId and b.roomid = c.roomid and c.isPublic = 1 and "
                + "a.status in ( 0, 1, 5) and a.deleted = 0 and ((a.confdate <= '" + confEnd.ToString() + "' and dateadd(minute,a.duration,a.confdate) >= '" + confFrom.ToString() + "') ";
                hql += " or a.confdate >= '" + confFrom.ToString() + "')"
                    + " group by a.status, a.confid, a.InstanceId ";
                //FB 3069 Starts

                if (hql != "")
                    ds = m_sysLayer.ExecuteDataSet(hql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (mode == 2)
                        errID = 642;
                    else
                        errID = 640;
                    return false;
                }

                m_sysLayer.OpenConnection();
                hql = " Select Count(*) from Loc_Room_D a, Ept_List_D b where a.isPublic = 0 and a.endpointid = b.endpointId and b.PublicEndPoint = 1 ";
                if (hql != "")
                    ds = m_sysLayer.ExecuteDataSet(hql);

                int count = 0;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Int32.TryParse(ds.Tables[0].Rows[0][0].ToString().Trim(), out count);
                    if (count > 0)
                    {
                        if (mode == 2)
                            errID = 643;
                        else
                            errID = 641;

                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("PublicRoomLicenseCheck", e);
                throw e;
            }
        }
        #endregion

        #region DeltePublicRoomEP
        /// <summary>
        /// DeltePublicRoomEP
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeltePublicRoomEP(ref vrmDataObject obj)
        {
            try
            {
                string hql;
                int userID = 0, ErrNo = 0;

                m_vrmFactory = new vrmFactory(ref obj);
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;


                node = xd.SelectSingleNode("//DeltePublicRoomEP/userid");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userID);

                if (!PublicRoomLicenseCheck(2, ref ErrNo))
                {
                    myvrmEx = new myVRMException(ErrNo);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                if (m_sysLayer == null)
                    m_sysLayer = new ns_SqlHelper.SqlHelper(m_configPath); 

                m_sysLayer.OpenConnection();

                m_sysLayer.OpenTransaction();

                hql = " Delete from Loc_Room_D where isPublic = 1; Delete from Ept_List_D where PublicEndPoint =1; truncate table ES_PublicRoom_d;";

                int strExec = m_sysLayer.ExecuteNonQuery(hql);

                m_sysLayer.CommitTransaction();
                m_sysLayer.CloseConnection();
                m_sysLayer = null;

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
        }
        #endregion
        //FB 2594 Ends
        //FB 2616 Start

        #region KeepAlive
        /// <summary>
        /// KeepAlive
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool KeepAlive(ref vrmDataObject obj)
        {
            string sLogin = "", sEmail = "", sPwd = "", sEnPwd = "", sHomeurl = "", sAuthenticated = "NO", templicense = "";
            bool bRet = true;
            DateTime currentLoginDate = DateTime.Now;
            DateTime currentdate = new DateTime();
            int lockcntrns = 0;
            vrmSystemFactory validateLicense = null;
            XmlDocument xd = new XmlDocument();
            XmlNode node;
            List<ICriterion> criterionList = new List<ICriterion>();
            List<vrmUser> vrmUsr = null;
            cryptography.Crypto crypto = null;
            DateTime nowDate = DateTime.Now;
            DateTime ExpiryDate;
            XmlNode node1;
            string UserDomain = "";
            try
            {
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref currentLoginDate);
                currentdate = new DateTime(currentLoginDate.Year, currentLoginDate.Month, currentLoginDate.Day, 0, 0, 0);
                xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                node = xd.SelectSingleNode("//login/homeURL");
                if (node != null)
                {
                    if (node.InnerText != "")
                        sHomeurl = node.InnerXml.Trim();
                }

                node = xd.SelectSingleNode("//login/userName");
                if (node != null)
                {
                    if (node.InnerText != "")
                        sLogin = node.InnerXml.Trim();
                    else
                    {
                        myvrmEx = new myVRMException(529);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                else
                {
                    node = xd.SelectSingleNode("//login/emailID");
                    if (node != null)
                    {
                        if (node.InnerText != "")
                            sEmail = node.InnerXml.Trim();
                        else
                        {
                            myvrmEx = new myVRMException(529);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;

                        }
                    }

                }

                node = xd.SelectSingleNode("//login/userAuthenticated");
                if (node != null)
                    if (node.InnerText != "")
                        sAuthenticated = node.InnerXml.Trim();
                //ZD 103784 starts
                node = xd.SelectSingleNode("//login/userDomain");
                if (node != null)
                    if (node.InnerText != "")
                        UserDomain = node.InnerXml.Trim();
                //ZD 103784 Ends
                if (sAuthenticated.ToUpper() == "NO")
                {

                    node = xd.SelectSingleNode("//login/userPassword");
                    if (node.InnerText != "")
                        sPwd = node.InnerXml.Trim();
                    else
                    {
                        myvrmEx = new myVRMException(530);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    crypto = new cryptography.Crypto();
                    if (sPwd != "")
                        sEnPwd = crypto.encrypt(sPwd);
                }

                criterionList = new List<ICriterion>();

                if (sLogin.Trim() != "")
                {
                    criterionList.Add(Expression.Eq("Login", sLogin));
                    criterionList.Add(Expression.Eq("UserDomain", UserDomain));//ZD 103784
                }
                else if (sEmail.Trim() != "")
                    criterionList.Add(Expression.Eq("Email", sEmail));

                vrmUsr = m_IuserDao.GetByCriteria(criterionList, true);

                if ((vrmUsr.Count <= 0))
                {
                    myvrmEx = new myVRMException(205);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                if (vrmUsr[0] == null)
                {
                    myvrmEx = new myVRMException(205);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                lockcntrns = vrmUsr[0].lockCntTrns;

                if (vrmUsr[0].userid != 11)
                {
                    if (validateLicense == null)
                        validateLicense = new vrmSystemFactory(ref obj);

                    xd = new XmlDocument();
                    crypto = new cryptography.Crypto();
                    templicense = crypto.decrypt(sysSettings.license);

                    xd.LoadXml(templicense);

                    node1 = xd.SelectSingleNode("//myVRMSiteLicense/Site/ExpirationDate");
                    DateTime.TryParse(node1.InnerXml.Trim(), out ExpiryDate);

                    if (nowDate >= ExpiryDate)
                    {
                        myvrmEx = new myVRMException(515);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    if (!(currentLoginDate <= sysSettings.ExpiryDate && vrmUsr[0].accountexpiry >= currentLoginDate))
                    {
                        vrmUsr[0].LockStatus = vrmUserStatus.MAN_LOCKED;
                        m_IuserDao.Update(vrmUsr[0]);
                        myvrmEx = new myVRMException(263);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    if (vrmUsr[0].LockStatus == vrmUserStatus.MAN_LOCKED)
                    {
                        if (vrmUsr[0].lockCntTrns == 10)
                        {
                            myvrmEx = new myVRMException(425);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        else
                        {
                            myvrmEx = new myVRMException(254);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    else if (vrmUsr[0].LockStatus == vrmUserStatus.AUTO_LOCKED)
                    {
                        if (currentLoginDate.Subtract(vrmUsr[0].LastLogin).Minutes < 1)
                        {
                            myvrmEx = new myVRMException(208);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        //FB 2616 End

        //FB 2678 - Start    
        #region SetOrgExpiryLicense
        /// <summary>
        /// SetOrgExpiryLicense
        /// </summary>
        /// <returns></returns>
        private bool SetOrgExpiryLicense(ref int IndividualOrgExpiry, string expirydate)
        {
            string orglicense = "", License = "";
            cryptography.Crypto crypto = new cryptography.Crypto();
            int licenseorgId = 0, status = 0;
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("deleted", 0));
                List<vrmOrganization> orgList = m_IOrgDAO.GetByCriteria(criterionList);

                for (int i = 0; i < orgList.Count; i++)
                {
                    if (IndividualOrgExpiry == 0)
                    {                       
                        licenseorgId = orgList[i].orgId;
                        License = "<Organization><ID>" + licenseorgId + "</ID><ExpiryDate>" + expirydate + "</ExpiryDate></Organization>";
                        orglicense = crypto.encrypt(License);

                        if (licenseorgId == orgList[i].orgId)
                        {
                            if (DateTime.Now < sysSettings.ExpiryDate)
                                status = 1;
                        }
                        orgList[i].orgExpiryKey = orglicense;
                        orgList[i].orgStatus = status;
                    }
                    else
                    {
                        orgList[i].orgExpiryKey = "";
                        orgList[i].orgStatus = status;
                    }

                    m_IOrgDAO.Update(orgList[i]);
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("Issue in Encrypting License Details", e);
                return false;
            }
        }
        #endregion
        //FB 2678 - End
        //FB 2659 - Starts

        #region SetDefaulLicense
        /// <summary>
        /// SetDefaulLicense
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetDefaultLicense(ref vrmDataObject obj)
        {
            XmlDocument xd = new XmlDocument();
            XmlNode node;
            try
            {
                xd.LoadXml(obj.inXml);

                vrmDefaultLicense DefaultLicense = new vrmDefaultLicense();
                List<ICriterion> criterionList = new List<ICriterion>();
                
                DefaultLicense.UserId = 11;
                criterionList.Add(Expression.Eq("UserId", 11));//FB 3002
                List<vrmDefaultLicense> License = m_IDefaultLicense.GetByCriteria(criterionList);

                for (int i = 0; i < License.Count; i++)
                {
                    DefaultLicense = License[0];
                }

                //FB 3002 START
                int LastModifiedUser = 11;
                node = xd.SelectSingleNode("//SetDefaultLicense/LastModifiedUser");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out LastModifiedUser);

                DefaultLicense.LastModifiedUser = LastModifiedUser;
                //FB 3002 END

                int roomlimit = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/roomlimit");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out roomlimit);


                int vroomlimit = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/videoroomlimit");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out vroomlimit);
                DefaultLicense.MaxVideoRooms = vroomlimit;

                int nvroomlimit = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/nonvideoroomlimit");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out nvroomlimit);
                DefaultLicense.MaxNonVideoRooms = nvroomlimit;

                int mculimit = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/mculimit");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out mculimit);
                DefaultLicense.MCULimit = mculimit;

                int userlimit = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/userlimit");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out userlimit);
                DefaultLicense.UserLimit = userlimit;

                int exUserlimit = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/exchangeusers");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out exUserlimit);
                DefaultLicense.ExchangeUserLimit = exUserlimit;

                int extRoomlimit = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/GuestRooms");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out extRoomlimit);
                DefaultLicense.GuestRoomLimit = extRoomlimit;

                int extGuestRoomPerUser = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/GuestRoomPerUser");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out extGuestRoomPerUser);
                DefaultLicense.GuestRoomPerUser = extGuestRoomPerUser;

                int domUserlimit = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/dominousers");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out domUserlimit);
                DefaultLicense.DominoUserLimit = domUserlimit;

                int mobUserlimit = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/mobileusers");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out mobUserlimit);
                DefaultLicense.MobileUserLimit = mobUserlimit;

                //ZD 104021 start
                int BlueJeansUsers = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/BlueJeansUsers");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out BlueJeansUsers);
                DefaultLicense.BlueJeansUserLimit = BlueJeansUsers;
                //ZD 104021 end

                int eplimit = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/endpointlimit");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out eplimit);
                DefaultLicense.MaxEndpoints = eplimit;

                int enableAVMod = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/enablefacilities");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enableAVMod);
                DefaultLicense.EnableFacilities = enableAVMod;

                int enableCat = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/enablecatering");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enableCat);
                DefaultLicense.EnableCatering = enableCat;

                int enableHKG = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/enablehousekeeping");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enableHKG);
                DefaultLicense.EnableHousekeeping = enableHKG;

                int enableAPI = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/enableAPI");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enableAPI);
                DefaultLicense.EnableAPI = enableAPI;

                //int enablePC = 0;
                //node = xd.SelectSingleNode("//SetDefaultLicense/enablePC");
                //if (node != null)
                //    int.TryParse(node.InnerText.Trim(), out enablePC);
                //DefaultLicense.EnablePC = enablePC;

                int mcuenchancedlimit = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/mcuenchancedlimit");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out mcuenchancedlimit);
                DefaultLicense.MCUEnhancedLimit = mcuenchancedlimit;

                int vmrroomlimit = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/vmrroomlimit");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out vmrroomlimit);
                DefaultLicense.MaxVMRRooms = vmrroomlimit;

                //ZD 101098 START
                int iControlroomlimit = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/icotrolroomlimit");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out iControlroomlimit);
                DefaultLicense.MaxiControlRooms = iControlroomlimit;
                //ZD 101098 END

                
                int pcusers = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/pcusers");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out pcusers);
                DefaultLicense.MaxPCUsers = pcusers;

                int enableJabber = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/enableJabber");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enableJabber);
                DefaultLicense.MaxenableJabber = enableJabber;

                int enableLync = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/enableLync");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enableLync);
                DefaultLicense.MaxenableLync = enableLync;

                int enableVidtel = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/enableVidtel");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enableVidtel);
                DefaultLicense.MaxenableVidtel = enableVidtel;

                int VCHotRooms = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/VCHotRooms");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out VCHotRooms);
                DefaultLicense.MaxVCHotRooms = VCHotRooms;

                int ROHotRooms = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/ROHotRooms");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out ROHotRooms);
                DefaultLicense.MaxROHotRooms = ROHotRooms;

                int enableadvancedreport = 0;
                node = xd.SelectSingleNode("//SetDefaultLicense/AdvancedReports");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enableadvancedreport);
                DefaultLicense.EnableAdvancedReport = enableadvancedreport;


                
                m_IDefaultLicense.SaveOrUpdate(DefaultLicense);

            }
            catch (Exception e)
            {
                m_log.Error("SetDefaultLicense Exception", e);
                obj.outXml = "";
                return false;
            }

            return true;
        }
        #endregion

        #region GetDefaultLicense
        /// <summary>
        /// GetDefaultLicense
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetDefaultLicense(ref vrmDataObject obj)
        {
            try
            {
                vrmDefaultLicense DefaultLicense = new vrmDefaultLicense();
                List<ICriterion> criterionList = new List<ICriterion>();

                StringBuilder outXML = new StringBuilder();

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                int userId = 0;
                node = xd.SelectSingleNode("//GetDefaultLicense/userid");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out userId);

                DefaultLicense.UserId = 11;//FB 3002
                criterionList.Add(Expression.Eq("UserId", 11));
                List<vrmDefaultLicense> License = m_IDefaultLicense.GetByCriteria(criterionList);


                for (int i = 0; i < License.Count; i++)
                {
                    DefaultLicense = License[0];
                }

                outXML.Append("<GetDefaultLicense>");
                outXML.Append("<Rooms></Rooms>");
                outXML.Append("<Users>" + DefaultLicense.UserLimit + "</Users>");
                outXML.Append("<LastModifiedUser>" + DefaultLicense.LastModifiedUser + "</LastModifiedUser>");//FB 3002
                outXML.Append("<ExchangeUsers>" + DefaultLicense.ExchangeUserLimit + "</ExchangeUsers>");
                outXML.Append("<DominoUsers>" + DefaultLicense.DominoUserLimit + "</DominoUsers>");
                outXML.Append("<MobileUsers>" + DefaultLicense.MobileUserLimit + "</MobileUsers>");
                outXML.Append("<MaxWebexUsers>" + DefaultLicense.MaxWebexUsers + "</MaxWebexUsers>");
                outXML.Append("<BlueJeansUsers>" + DefaultLicense.BlueJeansUserLimit + "</BlueJeansUsers>"); //ZD 104021
                outXML.Append("<MCU>" + DefaultLicense.MCULimit + "</MCU>");
                outXML.Append("<MCUEnchanced>" + DefaultLicense.MCUEnhancedLimit + "</MCUEnchanced>");
                outXML.Append("<EndPoints>" + DefaultLicense.MaxEndpoints + "</EndPoints>");
                outXML.Append("<VideoRooms>" + DefaultLicense.MaxVideoRooms + "</VideoRooms>");
                outXML.Append("<NonVideoRooms>" + DefaultLicense.MaxNonVideoRooms + "</NonVideoRooms>");
                outXML.Append("<VMRRooms>" + DefaultLicense.MaxVMRRooms + "</VMRRooms>");
                outXML.Append("<iControlRooms>" + DefaultLicense.MaxiControlRooms + "</iControlRooms>");//ZD 101098
                outXML.Append("<GuestRooms>" + DefaultLicense.GuestRoomLimit + "</GuestRooms>");
                outXML.Append("<GuestRoomPerUser>" + DefaultLicense.GuestRoomPerUser + "</GuestRoomPerUser>");
                outXML.Append("<EnableFacilites>" + DefaultLicense.EnableFacilities + "</EnableFacilites>");
                outXML.Append("<EnableCatering>" + DefaultLicense.EnableCatering + "</EnableCatering>");
                outXML.Append("<EnableHouseKeeping>" + DefaultLicense.EnableHousekeeping + "</EnableHouseKeeping>");
                outXML.Append("<EnableAPIs>" + DefaultLicense.EnableAPI + "</EnableAPIs>");
                
                //outXML.Append("<EnablePC>" + DefaultLicense.EnablePC + "</EnablePC>");

                outXML.Append("<PCUsers>" + DefaultLicense.MaxPCUsers + "</PCUsers>");
                outXML.Append("<EnableJabber>" + DefaultLicense.MaxenableJabber + "</EnableJabber>");
                outXML.Append("<EnableLync>" + DefaultLicense.MaxenableLync + "</EnableLync>");
                outXML.Append("<EnableVidtel>" + DefaultLicense.MaxenableVidtel + "</EnableVidtel>");
                outXML.Append("<VCHotRooms>" + DefaultLicense.MaxVCHotRooms + "</VCHotRooms>");
                outXML.Append("<ROHotRooms>" + DefaultLicense.MaxROHotRooms + "</ROHotRooms>");
                outXML.Append("<AdvancedReports>" + DefaultLicense.EnableAdvancedReport + "</AdvancedReports>");

                outXML.Append("</GetDefaultLicense>");


                obj.outXml = outXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = "";
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
            }
            return true;
        }
        #endregion

        //FB 2659 - End

        //ZD 100230 Start

        #region SendMailtoSiteAdmin
        /// <summary>
        /// SendMailtoSiteAdmin
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SendMailtoAdmin(ref vrmDataObject obj)
        {
            try
            {
                m_vrmEmailFact = new emailFactory(ref obj);
                m_vrmEmailFact.SendMailtoSiteAdmin(ref obj);
                m_vrmEmailFact.SendMailtomyVRMAdmin(ref obj);
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";

            }

            return true;
        }
        #endregion

        //ZD 100230 End

        //ZD 100781 Starts
        
        #region SendPasswordRequest
        /// <summary>
        /// SendPasswordRequest
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SendPasswordRequest(ref vrmDataObject obj)
        {
            DateTime currentDate = DateTime.Now;
            emailFactory m_Email = null;
            List<ICriterion> criterionList = null;
            List<vrmOrganization> vrmOrg = null;
            List<ICriterion> criUsrList = null;
            List<vrmUser> vrmUsr = null;
            OrgData usrOrg = null;
            DateTime PwdDateTime = DateTime.UtcNow;
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("deleted", 0));
                vrmOrg = m_IOrgDAO.GetByCriteria(criterionList);

                for (int i = 0; i < vrmOrg.Count; i++)
                {
                    m_Email = new emailFactory(ref obj);
                    usrOrg = m_IOrgSettingsDAO.GetByOrgId(vrmOrg[i].orgId);
                    if (usrOrg.EnablePasswordExp == 1)
                    {
                        criUsrList = new List<ICriterion>();
                        criUsrList.Add(Expression.Eq("companyId", vrmOrg[i].orgId));
                        criUsrList.Add(Expression.Eq("Deleted", 0));

                        vrmUsr = m_IuserDao.GetByCriteria(criUsrList);

                        for (int j = 0; j < vrmUsr.Count; j++)
                        {
                            if (vrmUsr[j].userid != 11)
                            {
                                if (vrmUsr[j].IsPasswordReset == 0)
                                {
                                    PwdDateTime = vrmUsr[j].PasswordTime.AddMonths(usrOrg.PasswordMonths);
                                    if (PwdDateTime.Date < DateTime.UtcNow.Date)
                                    {
                                        vrmUsr[j].LockStatus = vrmUserStatus.MAN_LOCKED;
                                        m_IuserDao.Update(vrmUsr[j]);

                                        //m_Email.ChangePasswordRequest(vrmUsr[j]); // comment for passwordreset new method
                                    }
                                }
                            }
                        }
                    }

                }

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();

            }
            catch (Exception e)
            {
                m_log.Error("SendPasswordRequest", e);
                obj.outXml = "";

            }
            return true;

        }
        #endregion

        //ZD 100781 Ends

        #region SetConfIDStartValue

        private Boolean SetConfIDStartValue(int ConfIDStartValue,ref vrmDataObject obj, ref int errNo)
        {
            try
            {
                string stmt = "";
                sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);

                int confnumname = 0;
                sqlCon.OpenConnection();
                sqlCon.OpenTransaction();
                stmt = "select MAX(confnumname) from Conf_Conference_D";
                object retVal = sqlCon.ExecuteScalar(stmt);
                if (retVal != null)
                    confnumname = (int)retVal;

                if (confnumname > 0)
                {
                    if (confnumname >= ConfIDStartValue)
                    {
                        errNo = 726;
                        sqlCon.RollBackTransaction();
                        sqlCon.CloseConnection();
                        return false;
                    }
                    else
                    {
                        stmt = "INSERT [dbo].[Conf_Conference_D]";
                        stmt += " ([userid], [confid], [externalname], [internalname], [password], [owner], [confdate]";
                        stmt += " , [conftime], [timezone], [immediate], [audio], [videoprotocol], [videosession], [linerate], [recuring], [duration]";
                        stmt += " , [description], [public], [deleted], [continous], [transcoding], [deletereason], [instanceid], [advanced], [totalpoints]";
                        stmt += " , [connect2], [settingtime], [videolayout], [manualvideolayout], [conftype], [confnumname], [status], [lecturemode]";
                        stmt += " , [lecturer], [dynamicinvite], [CreateType], [ConfDeptID], [ConfOrigin], [SetupTime], [TearDownTime], [orgId]";
                        stmt += " , [LastRunDateTime], [IcalID], [confMode], [isDedicatedEngineer], [isLiveAssistant], [isVIP], [isReminder], [ServiceType]";
                        stmt += " , [ConceirgeSupport], [sentSurvey], [isVMR], [ESId], [ESType], [PushedToExternal], [isTextMsg], [startmode], [loginUser]";
                        stmt += " , [GUID], [OnSiteAVSupport], [MeetandGreet], [ConciergeMonitoring], [DedicatedVNOCOperator], [E164Dialing], [H323Dialing]";
                        stmt += " , [Secured], [NetworkSwitch], [DialString], [Etag], [HostName], [isPCconference], [pcVendorId], [CloudConferencing]";
                        stmt += " , [ProcessStatus], [Uniqueid], [Seats], [EnableNumericID], [McuSetupTime], [MCUTeardonwnTime], [SetupTimeinMin]";
                        stmt += " , [TearDownTimeinMin], [WebExConf], [WebExPW], [WebExMeetingKey], [WebExHostURL], [MeetingId], [EnableStaticID])";
                        stmt += " VALUES (11, (select MAX(confid)+1 from Conf_Conference_D), N'Dummy Conference', N'11Dummy Conference', N'', 11, CAST(GETDATE() AS DateTime)";
                        stmt += " , CAST(GETDATE() AS DateTime), 26, 0, 0, 0, 0, 1024, 0, 60";
                        stmt += " , N'N/A', 0, 1, 0, 0, NULL, 1, 0, 0";
                        stmt += " , 0, CAST(GETDATE() AS DateTime), 1, 0, 2, " + ConfIDStartValue.ToString() + ", 0, 0";
                        stmt += " , NULL, 0, 1, 0, 0, CAST(GETDATE() AS DateTime), CAST(GETDATE() AS DateTime), 11";
                        stmt += " , CAST(GETDATE() AS DateTime), N'', 1, 0, 0, 0, 0, -1";
                        stmt += " , NULL, 0, 0, NULL, NULL, 0, 0, 0, 11, N'', 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, N'VRM Administrator', 0, 0, 0, 0, NULL, 0, 0, 0, 0";
                        stmt += " , 0, 0, 0, NULL, NULL, NULL, NULL, NULL)";

                        sqlCon.ExecuteNonQuery(stmt);
                        sqlCon.CommitTransaction();
                        sqlCon.CloseConnection();
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                sqlCon.RollBackTransaction();
                sqlCon.CloseConnection();
                return false;
            }
        }
        #endregion
    }
}
