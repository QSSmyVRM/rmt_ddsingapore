/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100866 End*/
var MINUTE = 60 * 1000;
var HOUR = 60 * MINUTE;
var DAY = 24 * HOUR;
var WEEK = 7 * DAY;

var pgmod = "";
pgmod = ( (document.location.href).toLowerCase().indexOf("customly") != -1 ) ? 1 : pgmod;
pgmod = ( (document.location.href).toLowerCase().indexOf("recurnet") != -1 ) ? 6 : pgmod; // code added for FB 412
pgmod = ( (document.location.href).toLowerCase().indexOf("conferencesetup") != -1 ) ? 6 : pgmod; // Merging Recurrence
pgmod = ( (document.location.href).toLowerCase().indexOf("expressconference") != -1 ) ? 6 : pgmod; // FB 1779
pgmod = ( (document.location.href).toLowerCase().indexOf("editholidaydetails") != -1 ) ? 6 : pgmod; 
pgmod = ( (document.location.href).toLowerCase().indexOf("managebatchreport") != -1 ) ? 6 : pgmod;//FB 2410
pgmod = ( (document.location.href).toLowerCase().indexOf("weekly") != -1 ) ? 2 : pgmod;
pgmod = ( (document.location.href).toLowerCase().indexOf("monthly") != -1 ) ? 3 : pgmod;
pgmod = ( (document.location.href).toLowerCase().indexOf("calendaryearly") != -1 ) ? 4 : pgmod;
pgmod = ( (document.location.href).toLowerCase().indexOf("daily") != -1 ) ? 5 : pgmod;
pgmod = ( (document.location.href).toLowerCase().indexOf("workorder.aspx") != -1 ) ? 5 : pgmod;
pgmod = ( (document.location.href).toLowerCase().indexOf("calendar.aspx") != -1 ) ? 5 : pgmod;

function isBeforeDisabled(date) {
	var existed = false;

	dateEnabled = !(caldatecompare(date, servertoday) == -1);

	if 	(dateEnabled) {
		cb = document.getElementById("CustomDate");
		if (cb) {
			for (i=0; i<cb.length; i++)
				existed = (caldatecompare(date, cb.options[i].value) == 0) ? true : existed;
		}
		dateEnabled = existed ? 2 : 1;
	};

	return (dateEnabled);
}

function isAfterDisabled(date) {
	var existed = false;

	dateEnabled = !(caldatecompare(date, servertoday) == 1);
	
	if 	(dateEnabled) {
		cb = document.getElementById("CustomDate");
		if (cb) {
			for (i=0; i<cb.length; i++)
				existed = (caldatecompare(date, cb.options[i].value) == 0) ? true : existed;
		}
		dateEnabled = existed ? 2 : 1;
	};

	return (dateEnabled);
}





// for flat claendar in recur page
function chgOption(cb, txt, val, defsel, sel)
{
	var existed = false;
	var idx = -1;
	
    var defaultSelected = defsel;
    var selected = sel;
    var option = new Option(txt, val, defaultSelected, selected);
    var length = cb.length;
    
    for (var i=0; ((i<length) && !existed); i++) {
		existed =  (cb.options[i].value == val) ? true : existed;
		idx = (cb.options[i].value == val) ? i : idx;
	}
	
	if (existed) {
		if (idx != -1)
			cb.options[idx] = null;
	} else {
		cb.options[length] = option;
	}
	
//	sortdate (cb, val);
}


function sortdate(datecb, pickedval)
{
	var dateary = new Array();

	for (var i=0; i<datecb.length; i++) {
		dateary[i] = datecb.options[i].value;
		
		dateary[i] = ( (parseInt(dateary[i].split("/")[0], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[0], 10) : parseInt(dateary[i].split("/")[0], 10) ) + "/" + ( (parseInt(dateary[i].split("/")[1], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[1], 10) : parseInt(dateary[i].split("/")[1], 10) ) + "/" + ( parseInt(dateary[i].split("/")[2], 10) );
	}

	dateary.sort (mydatesort);

	for (var i=0; i<datecb.length; i++) {
		cb.options[i].text = dateary[i];
		cb.options[i].value = dateary[i];
		cb.options[i].selected = (cb.options[i].value == pickedval) ? true : false;
	}
}


function mydatesort(a, b)
{
	var datereg = /^(\d{2})[\/\- ](\d{2})[\/\- ](\d{4})/;

    /* *** Code added by offshore for FB Issue 1073 -- start  *** */
   if(dFormat == "dd/MM/yyyy")
   {
        a = a.replace(datereg,"$3$2$1");
	    b = b.replace(datereg,"$3$2$1");
   }
   else
   {
        a = a.replace(datereg,"$3$1$2");
	    b = b.replace(datereg,"$3$1$2");
   }  
   /* *** Code added by offshore for FB Issue 1073 -- end *** */
    
	if (a > b) return 1;
	if (a < b) return -1;

	return 0;
}



function selDaily(val)
{
	confdate = val;
    
	if (window.location.href.toLowerCase().indexOf("calendarroomdaily.asp") > 0 || window.location.href.toLowerCase().indexOf("workorder.aspx") > 0 || window.location.href.toLowerCase().indexOf("calendar.aspx") > 0)
	{
		datechg();
	} else {
		drawRoomConf();
		initsubtitle ();
	}
}


function selWeek(val)
{
//	alert("Weekly");
	var seldate = new Date(val);
	confweekmon = addDates(seldate.getFullYear(), seldate.getMonth() + 1, seldate.getDate(), (seldate.getDay() == 0) ? -6 : (1-seldate.getDay()) );
	selnewwk(confweekmon);
	inityaxle ();
	initsubtitle ();
}


function selMonth(val)
{
//	alert("Monthly");
	var seldate = new Date(val);
	var curmonthfirst = new Date(confmonthfirst);
	if ( ! ((seldate.getFullYear() == curmonthfirst.getFullYear()) && (seldate.getMonth()  == curmonthfirst.getMonth())) ) {
		confmonthfirst = (seldate.getMonth() + 1) + "/1/" + seldate.getFullYear();	//new Date( (seldate.getMonth() + 1) + "/1/" + seldate.getFullYear());
		selnewmo(confmonthfirst);
		inityaxle ();
		initsubtitle ();
	}
}

var mouserightmenu = null;
function selYear(val)
{
	if (IE4) {
		if (event.button==1)
			mouse_click_menu_prompt("view Daily Calendar", "selviewmod(1, '" + val + "')", "view Weekly Calendar", "selviewmod(2, '" + val + "')", "view Monthly Calendar", "selviewmod(3, '" + val + "')");
		if (event.button==2)
		    if (closethis) closethis(); 
	} else{
		if (mouserightmenu != val) {
		    if (closethis) closethis(); 
			mouse_click_menu_prompt("view Daily Calendar", "selviewmod(1, '" + val + "')", "view Weekly Calendar", "selviewmod(2, '" + val + "')", "view Monthly Calendar", "selviewmod(3, '" + val + "')");
			mouserightmenu=val;
			return true;
		} else {
		    if (closethis) closethis(); 
		    mouserightmenu=null;
			return false;
		}
	}
}


function flatSelected(cal, date) {
//  var el = document.getElementById("preview");
//  el.innerHTML = date;
	cb = document.getElementById("CustomDate");
	
	switch (pgmod) {
		case 1:
			if (!isOverInstanceLimit (cb)) {
				chgOption(cb, date, date, false, false);
				cal.refresh();
			}
			break;
		case 2:
			selWeek(date);
			break;
		case 3:
			selMonth(date);
			break;
		case 4:
			selYear(date);
			break;
		case 5:
			selDaily(date);
			break;
	// code added for FB 412 - start			
		case 6:
			if (!isOverInstanceLimit (cb)) {
			//if(date.substring(0,1) == '0')
			 //   date = date.substring(1,date.length);
			   
				chgOption(cb, date, date, false, false);
				cal.refresh();
			}
			break;
	// code added for FB 412 - end
	}
	
	//cal.refresh();
}

var cal;
switch (pgmod) {
	case 1:
		cal = new Calendar(0, parent.document.getElementById("StartDate").value, flatSelected);
		break;
	case 2:
	case 3:
	case 4:
		cal = new Calendar(1, null, flatSelected);
		break;
		// code added for FB 412 - start		
	case 6:
	    cal = new Calendar(0, '', flatSelected);
		break;
    // code added for FB 412 - start	
	default:
		cal = new Calendar(0, null, flatSelected);
		break;
}

function showFlatCalendar(limit, format, preseldate, calid) {
//alert(limit + " : " + format + " : " + preseldate + " : " + calid);
	var parent;
	if (calid == null)
		parent = document.getElementById("flatCalendarDisplay");
	else
		parent = document.getElementById("flatCalendarDisplay" + calid);
	
    /* *** Code added by offshore for FB Issue 1073 -- start  *** */
   
   if(format == "dd/MM/yyyy")
   {
        format = '%d/%m/%Y';
        cal.dateStr = "";
   }
   else if(format == "MM/dd/yyyy")
        format = '%m/%d/%Y';
   else
        format = '%m/%d/%Y';
      
   /* *** Code added by offshore for FB Issue 1073 -- end *** */
   
  // construct a calendar giving only the "selected" handler.

  // hide week numbers
//  cal.weekNumbers = false;

  // We want some dates to be disabled; see function isDisabled above
    switch (limit) {
		case -1:
			cal.setDisabledHandler(isAfterDisabled); break;
		case 0:
			break;
		case 1:
			cal.setDisabledHandler(isBeforeDisabled); break;
	}
  cal.setDateFormat(format);
//calendar.sel = el;
//cal.setDate("05/01/2006")

  // this call must be the last as it might use data initialized above; if
  // we specify a parent, as opposite to the "showCalendar" function above,
  // then we create a flat calendar -- not popup.  Hidden, though, but...
  
  if (preseldate)
		cal.create(parent, preseldate);
  else{
		cal.create(parent);  }

  // ... we can show it here.
  cal.show();
}