﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" EnableSessionState="True" Inherits="ns_ManageCustomAttribute.ManageCustomAttribute" %><%--ZD 100170--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <%--FB 2779--%>
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<script type="text/javascript">
//ZD 100604 start
var img = new Image();
img.src = "../en/image/wait1.gif";
//ZD 100604 End
function fnOnComboChange()
{
    var optionTR = document.getElementById('TrOptionEntry');
    var optionTRD = document.getElementById('TrOptionDisplay');
    var drpCA = document.getElementById('<%=DrpCAType.ClientID%>');
    if(drpCA)
    {
        if(optionTR && optionTRD)
        {
            if(drpCA.value=='5' || drpCA.value=='6'|| drpCA.value=='8') //FB 1718
            {
                optionTR.style.display = 'block';
                optionTRD.style.display = 'block';
            }
            else
            {
                optionTR.style.display = 'none';
                optionTRD.style.display = 'none';
            }
        }
        if(drpCA.value == '2')//FB 2377
            document.getElementById('ChkCARequired').disabled = true;
        else
            document.getElementById('ChkCARequired').disabled = false;
    }
}
function FnCancel() {
    DataLoading(1); //ZD 100176
	window.location.replace('ViewCustomAttributes.aspx');
}
function fnValidator()
{
    var txtcaname = document.getElementById('<%=TxtCAName.ClientID%>');
    if(txtcaname.value == "")
    {        
        ReqCAName.style.display = 'block';
        txtcaname.focus();
        return false;
    }//FB 2377
    else if (txtcaname.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
    {        
        regItemName1.style.display = 'block';
        txtcaname.focus();
        return false;
    }    
    
    var txtcadesc = document.getElementById('<%=TxtCADesc.ClientID%>');
    
    if(txtcadesc.value != "")
    {//FB 2377
        if (txtcadesc.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!,\[\]{}\x22;=:@#$%&()~]*$/)==-1) //ZD 101714
        {        
            RegCADesc.style.display = 'block';
            txtcadesc.focus();
            return false;
        }    
    }
    //FB 1767 start
//    if(document.getElementById('errLabel') != null)//FB 1970 //ZD 101176
//        document.getElementById('errLabel').style.display = "none";
    if(document.getElementById('ChkCAEmail').checked == true)
    {
        if(document.getElementById('chkHost').checked == false)
            if(document.getElementById('ChkRoomApp').checked == false)
                if(document.getElementById('chkScheduler').checked == false)
                    if(document.getElementById('ChkMcuApp').checked == false)
                        if(document.getElementById('chkParty').checked == false)
                            if(document.getElementById('ChkSysApp').checked == false)
                                if (document.getElementById('chkRoomAdmin').checked == false)
                                    if(document.getElementById('chkVNOCOperator').checked ==false) //FB 2501
                                        if(document.getElementById('chkMcuAdmin').checked == false)
                                          {
                                             document.getElementById('errLabel').innerHTML = RSselectMailingRights; // ZD 100288
                                             //document.getElementById('errLabel').style.display = "block" ; //ZD 101176
                                             return false;
                                          }
    }
	//FB 1767 end
    return(true);

}
function frmValidator()
{   
    document.getElementById("txtMaxChar").style.display = "none"; //FB 1970
    var txtentityname = document.getElementById('<%=txtEntityName.ClientID%>');
    if(txtentityname.value == "")
    {        
        reqEntityName.style.display = 'block';
        txtentityname.focus();
        return false;
    }//FB 2377
    else if (txtentityname.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
    {        
        regItemName1.style.display = 'block';
        txtentityname.focus();
        return false;
    }    
    
    var txtentitydesc = document.getElementById('<%=txtEntityDesc.ClientID%>');
    
    if(txtentitydesc.value != "")
    {//FB 2377
        if (txtentitydesc.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
        {        
            regItemDesc.style.display = 'block';
            txtentitydesc.focus();
            return false;
        }    
    }
    
    return(true);
}

function fnGridValidation()
{
    
    var args = document.getElementById("hdnValue").value;
    var recCount = 2;	    
    var gridName = document.getElementById("dgEntityCode");
    
    for(var i=2; i<=args + 1; i++)
	{				
		statusValue = "";
		if(i < 10)
		    i = "0" + i;
		 
		var newText = document.getElementById("dgEntityCode_ctl"+ i + "_txtEntityName");
		var rqName = document.getElementById("dgEntityCode_ctl"+ i + "_reqName");
		var rgName = document.getElementById("dgEntityCode_ctl"+ i + "_regItemName1");
		
		if(newText)
		{
			if(newText.value == "")
            {        
                rqName.style.display = 'block';
                newText.focus();
                return false;
            }
            
            else if (newText.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
            {        
                rgName.style.display = 'block';
                newText.focus();
                return false;
            }   
		}	
		
		var newText1 = document.getElementById("dgEntityCode_ctl"+ i + "_txtEntityDesc");		
		var rgName1 = document.getElementById("dgEntityCode_ctl"+ i + "_regItemName");
		
		if(newText1)
		{
			if(newText1.value != "")
            {        
               if (newText1.value.search(/^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$/)==-1)
                {      
                    rgName1.style.display = 'block';
                    newText1.focus();
                    return false;
                }   
		    }	
		}
    }
    
    return true;
}

//ZD 100176 start
function DataLoading(val) {
    if (val == "1")
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
    else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}
//ZD 100176 End
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Custom Options</title>
</head>
<body>
    <form id="frmItemsList" runat="server" method="post">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
        <center>
        <input type="hidden" runat="server" id="hdnValue" />
        <input type="hidden" runat="server" id="hdnCustomAttID" enableviewstate="true" />
        <input type="hidden" runat="server" id="hdnCreateby" /><%--FB 2349--%>
        <div>
            <table width="100%" border="0">
                <tr>
                    <td align="center">
                        <h3><asp:Label ID="lblHeader" runat="server"><asp:Literal ID="Literal1" Text='<%$ Resources:WebResources, ManageCustomOption%>' runat='server' /></asp:Label></h3><%-- FB 2670--%>
                    </td>
                </tr>
				<%--FB 1767 start--%>
                <tr>
                    <td align="center">
                        <asp:Label ID="errLabel" runat="server" EnableViewState="false" Text="" CssClass="lblError" ></asp:Label>
                        <div id="dataLoadingDIV" style="display:none" align="center" >
                            <img border='0' src='image/wait1.gif'  alt="<asp:Literal Text='<%$ Resources:WebResources, Loading%>' runat='server' />" />
                        </div> <%--ZD 100176--%> <%--ZD 100678 End--%>
                    </td>
                </tr>
                 <%--FB 1970 start--%>
                <tr>
                    <td align="center">
                      <table width="100%">
                      <tr>
                        <td width="50%">
                         <table cellspacing="3" border="0" width="100%">
                            <tr>
                                <td align="left" width="40%" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_TitleinEnglis%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td> <%-- FB 2050 --%>
                                <td align="left" width="60%"> <%-- FB 2050 --%>
                                    <asp:TextBox ID="TxtCAName" runat="server" class="altText" Width="200" MaxLength="200"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="ReqCAName" ValidationGroup="Upload" runat="server" ControlToValidate="TxtCAName" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic" ></asp:RequiredFieldValidator>                                            
                                    <asp:RegularExpressionValidator ID="RegCAName" ValidationGroup="CustAdd" ControlToValidate="TxtCAName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters7%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td><%--FB 2377--%>
							<%--FB 1767 end--%>
                            </tr>
                            <tr>
                              <td valign="bottom" colspan="2" align="left">
                                <asp:ImageButton id="imgLangsTitle" runat="server" imageurl="image/loc/nolines_minus.gif" height="20" width="20" vspace="0" hspace="0" tooltip="<%$ Resources:WebResources, ViewOtherLanguage%>" AlternateText="<%$ Resources:WebResources, SettingSelect2_Expand%>"></asp:ImageButton>
                                <a class="blackblodtext" style="vertical-align:top"><asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_OtherLanguages%>" runat="server"></asp:Literal></a> 
                              </td>
                            </tr>
                            <tr id="otherLangRow" runat="server" valign="top">
                              <td colspan="2" valign="top" style="padding:opx"> 
                                <asp:DataGrid ID="dgTitlelist" runat="server" AutoGenerateColumns="false" GridLines="None" CellPadding="2" CellSpacing="0" style="border-collapse:separate; vertical-align:top" ShowHeader="false" Width="100%">
                                  <Columns>
                                    <asp:TemplateColumn ItemStyle-CssClass="blackblodtext" ItemStyle-Width="40%" ItemStyle-HorizontalAlign="Left"> <%-- FB 2050 --%>
                                      <ItemTemplate>
                                         <asp:Label ID="lblLangID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label>
                                         <asp:Label ID="lblLangName" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                      </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn ItemStyle-Height="30px" ItemStyle-HorizontalAlign="Left"> <%-- FB 2050 --%>
                                      <ItemTemplate>
                                         <asp:TextBox ID="txtTitle" CssClass="altText" runat="server" Width="200" MaxLength="200" Text='<%#DataBinder.Eval(Container, "DataItem.Title") %>'></asp:TextBox>
                                         <asp:RegularExpressionValidator ID="reglangName" ValidationGroup="Submit" ControlToValidate="txtTitle" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters8%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                      </ItemTemplate><%--FB 2377--%>
                                    </asp:TemplateColumn>
                                  </Columns>
                                </asp:DataGrid>
                              </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_Description%>" runat="server"></asp:Literal></td>
                                <td align="left">
                                    <asp:TextBox ID="TxtCADesc" runat="server" class="altText" Width="260" MaxLength="250"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegCADesc" ControlToValidate="TxtCADesc" Display="dynamic" runat="server" ValidationGroup="CustAdd" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters7%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!,\[\]{}\x22;=:@#$%&()~]*$"></asp:RegularExpressionValidator>
                                </td><%--FB 2377--%>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_IsMandatory%>" runat="server"></asp:Literal></td>
                                <td align="left">
                                    <asp:CheckBox ID="ChkCARequired" runat="server" EnableViewState="true" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_EnableStatus%>" runat="server"></asp:Literal></td>
                                <td align="left">
                                    <asp:CheckBox ID="ChkCAStatus" runat="server" EnableViewState="true" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_IncludeinEmai%>" runat="server"></asp:Literal></td>
                                <td align="left">
                                    <asp:CheckBox ID="ChkCAEmail" runat="server" EnableViewState="true" onClick="javascript:Incmailselectall()" /><%--FB 1767--%>
                                </td>
                            </tr>
                            <tr><%--FB 2013--%>
                                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_IncludeinCale%>" runat="server"></asp:Literal></td>
                                <td align="left">
                                    <asp:CheckBox ID="ChkCAinCalendar" runat="server"/>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_FieldType%>" runat="server"></asp:Literal></td>
                                <%-- FB 1718 --%>
                                <td align="left">
                                    <asp:DropDownList ID="DrpCAType" runat="server" class="altText">
                                        <%--<asp:ListItem Value="3" Text="Radio Button"></asp:ListItem>--%> <%--Search Fixes - FB 1787 --%>
                                        <asp:ListItem Value="8" Text="<%$ Resources:WebResources, RadioButtonList%>"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="<%$ Resources:WebResources, Text%>"></asp:ListItem>
                                        <asp:ListItem Value="10" Text="<%$ Resources:WebResources, TextMultiline%>"></asp:ListItem>
                                        <asp:ListItem Value="5" Text="<%$ Resources:WebResources, ListBox%>"></asp:ListItem>
                                        <asp:ListItem Value="6" Text="<%$ Resources:WebResources, DropDownList%>"></asp:ListItem>
                                        <asp:ListItem Value="7" Text="<%$ Resources:WebResources, SuperAdministrator_URL%>"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="<%$ Resources:WebResources, CheckBox%>"></asp:ListItem> <%--FB 2377--%>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                          </table>
						<%--FB 1767 start--%>
                        </td>
                        <td  width="50%" valign="top">
                         <table width="100%">
                          <tr>
                           <td style="width:20%" >
                             <table width="100%">
                              <tr>
                               <td width="55%">
                                  <span id="mailListHead" runat="server" class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_mailListHead%>" runat="server"></asp:Literal></span>
                               </td>
                               <td>
                                  <asp:CheckBox id="ChkAll" runat="server" text="<%$ Resources:WebResources, ManageCustomAttribute_ChkAll%>" onclick="javascript:selectAllMailRight()"></asp:CheckBox>
                               </td>
                              </tr>
                             </table>
                           </td>
                          </tr>
                          <tr>
                           <td width="80%">
                            <div id="InputParamDiv"  style="border-color:Blue;border-width:1px;border-style:Solid;width:85%;Height:auto;overflow:auto;text-align:left;">
                              <table width="100%">
                                 <tr><td style="height:3px" colspan="3"></td></tr>
                                 <tr>
                                 <td width="2%"></td>
                                  <td width="55%">
                                     <asp:CheckBox id="chkHost" runat="server" text="<%$ Resources:WebResources, ManageCustomAttribute_chkHost%>" onclick="javascript:clearErrLabel(this)"></asp:CheckBox>
                                  </td>
                                  <td>
                                     <asp:CheckBox id="ChkRoomApp" runat="server" text="<%$ Resources:WebResources, ManageCustomAttribute_ChkRoomApp%>" onclick="javascript:clearErrLabel(this)"></asp:CheckBox></td>
                                 </tr>
                                 <tr>
                                  <td width="2%"></td>
                                  <td>
                                     <asp:CheckBox id="chkScheduler" runat="server" text="<%$ Resources:WebResources, ManageCustomAttribute_chkScheduler%>" onclick="javascript:clearErrLabel(this)"></asp:CheckBox>
                                  </td>
                                  <td>
                                     <asp:CheckBox id="ChkMcuApp" runat="server" text="<%$ Resources:WebResources, ManageCustomAttribute_ChkMcuApp%>" onclick="javascript:clearErrLabel(this)"></asp:CheckBox>
                                  </td>
                                 </tr>
                                 <tr>
                                  <td width="2%"></td>
                                  <td>
                                     <asp:CheckBox id="chkParty" runat="server" text="<%$ Resources:WebResources, ManageCustomAttribute_chkParty%>" onclick="javascript:clearErrLabel(this)"></asp:CheckBox>
                                  </td>
                                  <td>
                                     <asp:CheckBox id="ChkSysApp" runat="server" text="<%$ Resources:WebResources, ManageCustomAttribute_ChkSysApp%>" onclick="javascript:clearErrLabel(this)"></asp:CheckBox>
                                  </td>
                                 </tr>
                                 <tr>
                                  <td width="2%"></td>
                                  <td>
                                     <asp:CheckBox id="chkRoomAdmin" runat="server" text="<%$ Resources:WebResources, ManageCustomAttribute_chkRoomAdmin%>" onclick="javascript:clearErrLabel(this)"></asp:CheckBox>
                                  </td>
                                  <td>
                                    <asp:CheckBox id="chkVNOCOperator" runat="server" text="<%$ Resources:WebResources, ManageCustomAttribute_chkVNOCOperator%>" onclick="javascript:clearErrLabel(this)"></asp:CheckBox>
                                  </td>
                                 </tr>
                                 <tr>
                                  <td width="2%"></td>
                                  <td>
                                     <asp:CheckBox id="chkMcuAdmin" runat="server" text="<%$ Resources:WebResources, ManageCustomAttribute_chkMcuAdmin%>" onclick="javascript:clearErrLabel(this)"></asp:CheckBox>
                                  </td>
                                 </tr>
                                 <tr><td style="height:3px" colspan="3"></td></tr>
                              </table>
                            </div>
                           </td>
                          </tr> 
                         </table>
                        </td>
                      </tr>
                      </table>
					<%--FB 1767 End--%>	
                    </td>
                </tr>
                <tr id="TrOptionEntry" runat="server" align="center" style="display:block;"> <%-- FB 2050 --%> <%--ZD 100393--%>
                    <td align="center">
                        <table width="755px" cellspacing="0" cellpadding="2" border="0"> <%--Edited for FF & FB 2050--%>
                            <tr>
                                <td align="left" class="subtitleblueblodtext" colspan="4"><asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_AddOptionsto%>" runat="server"></asp:Literal><br /> 
                                </td>
                            </tr>
                            <tr>
                               <td colspan="4"> <%-- FB 2050 --%>
                                  <table class="tableHeader" border="0" width="100%">
                                    <tr>
                                       <td style="width: 135px" align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_LanguageName%>" runat="server"></asp:Literal></td> <%--FB 1970--%><%--FB 2579--%>
                                       <td align="left" valign="top" style="width: 230px" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_OptionName%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                                       <td align="left" valign="top" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_OptionDescript%>" runat="server"></asp:Literal></td>
                                    </tr>
                                  </table>
                               </td>
                            </tr>
                            <tr>
                                <td valign="top" style="width:13px"><br /> <%-- FB 2050 --%>
                                 <asp:ImageButton ID="imgLangsOption" runat="server" ImageUrl="image/loc/nolines_minus.gif" Height="20" Width="20" vspace="0" hspace="0" ToolTip="<%$ Resources:WebResources, ViewOtherLanguages%>" AlternateText="<%$ Resources:WebResources, SettingSelect2_Expand%>" /> <%--ZD 100419--%>
                                </td>
                                <td style="width: 130.5px" align="left" valign="top"><br /><a class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_English%>" runat="server"></asp:Literal></a></td>
                                <td align="left" valign="top" style="width: 228px"><br />
                                    <asp:TextBox id="txtEntityID" text="<%$ Resources:WebResources, ManageCustomAttribute_txtEntityID%>" visible="false" runat="server" width="32px"></asp:TextBox>
                                    <asp:TextBox id="txtEntityName" runat="server" width="200" cssclass="altText" maxlength="35"></asp:TextBox>
                                    <asp:RequiredFieldValidator id="reqEntityName" validationgroup="Upload" runat="server" controltovalidate="txtEntityName" errormessage="<%$ Resources:WebResources, Required%>" display="dynamic"></asp:RequiredFieldValidator>  <%--FB 2377--%>                                          
                                    <asp:RegularExpressionValidator id="regItemName1" controltovalidate="txtEntityName" display="dynamic" runat="server" setfocusonerror="true" errormessage="<%$ Resources:WebResources, InvalidCharacters7%>" validationexpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" valign="top"><br />
                                  <asp:TextBox ID="txtEntityDesc" Width="220" runat="server" CssClass="altText" MaxLength="35"></asp:TextBox><%--FB 2377--%>
                                  <asp:RegularExpressionValidator ID="regItemDesc" ControlToValidate="txtEntityDesc" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters9%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="center" valign="top"><br />
                                    <asp:Button id="btnAddEntityCode" onclick="AddEntityCode" validationgroup="Upload" runat="server" cssclass="altLongBlueButtonFormat" text="<%$ Resources:WebResources, ManageCustomAttribute_btnAddEntityCode%>" onclientclick="javascript:return frmValidator()" width="210px"></asp:Button>
                                </td>
                            </tr>
                            <tr id="LangsOptionRow" runat="server"> <%-- FB 1970--%>
                              <td></td>
                              <td colspan="5" style="text-align:left"> <%-- FB 2050--%>
                                 <asp:DataGrid ID="dgLangOptionlist" runat="server" AutoGenerateColumns="false" GridLines="None" CellPadding="1" CellSpacing="0" style="border-collapse:separate" ShowHeader="false">
                                  <ItemStyle Height="2" VerticalAlign="Top" />
                                  <Columns>
                                    <asp:TemplateColumn ItemStyle-CssClass="blackblodtext" ItemStyle-Width="116px"><%--ZD 100425--%>
                                      <ItemTemplate>
                                         <asp:Label ID="lblLangID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label>
                                         <asp:Label ID="lblLangName" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                      </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn ItemStyle-Width="225px" ItemStyle-Height="30px"><%--ZD 100425--%>
                                      <ItemTemplate>
                                            <asp:TextBox id="txtOptionID" text="<%$ Resources:WebResources, ManageCustomAttribute_txtOptionID%>" visible="false" runat="server" width="32px"></asp:TextBox>
                                            <asp:TextBox id="txtOptionName" runat="server" width="200" cssclass="altText" maxlength="35" text=""></asp:TextBox><%--FB 2377--%>
                                            <asp:RegularExpressionValidator id="regItemName1" controltovalidate="txtOptionName" display="dynamic" runat="server" setfocusonerror="true" errormessage="<%$ Resources:WebResources, InvalidCharacters7%>" validationexpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                      </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                      <ItemTemplate>
                                            <asp:TextBox ID="txtOptionDesc" Width="220" runat="server" CssClass="altText" MaxLength="35"></asp:TextBox><%--FB 2377--%>
                                            <asp:RegularExpressionValidator ID="regItemDesc" ControlToValidate="txtOptionDesc" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters10%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                      </ItemTemplate>
                                    </asp:TemplateColumn>
                                  </Columns>
                                </asp:DataGrid>
                              </td>
                            </tr>
                            <tr>
							<td></td><%--ZD 100425--%>	
                              <td id="txtMaxChar" runat="server" colspan="4" style="font-size:xx-small; color:Red; display:none" align="left"><asp:Literal Text="<%$ Resources:WebResources, ManageCustomAttribute_txtMaxChar%>" runat="server"></asp:Literal></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="width:100%" align="center"><td align="center" style="width:100%"> <table border="0" style="width:755px" cellpadding="2" cellspacing="0"><%--Added for FF and FB 2050--%>
                <tr id="TrOptionDisplay" align="left" runat="server" style="width:100%"> <%-- FB 2050 --%>
                    <td align="left" style="width:100%">
                        <asp:DataGrid BorderColor="blue" BorderStyle="solid" BorderWidth="1" ID="dgEntityCode" AutoGenerateColumns="false"
                         OnEditCommand="EditItem" OnDeleteCommand="DeleteItem" OnCancelCommand="CancelItem" OnUpdateCommand="UpdateItem"
                         runat="server" Width="750Px" GridLines="None" style="border-collapse:separate"> <%--Edited for FF--%>
                            <HeaderStyle Height="30" CssClass="tableHeader" HorizontalAlign="Left" /> <%--ZD 100425--%>
                            <AlternatingItemStyle CssClass="tableBody"/>
                            <ItemStyle CssClass="tableBody" />
                            <FooterStyle CssClass="tableBody" />
                            <Columns>
                                <asp:BoundColumn DataField="RowUID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="OptionID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="DisplayValue" Visible="false"></asp:BoundColumn>
                                <asp:TemplateColumn ItemStyle-CssClass="blackblodtext"  HeaderStyle-CssClass="tableHeader" ItemStyle-Width="143" HeaderText="<%$ Resources:WebResources, ManageCustomAttribute_LanguageName%>" HeaderStyle-HorizontalAlign="Left"> <%--FB 1970--%>
                                      <ItemTemplate>
                                         <asp:Label ID="lblLangID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ID") %>' Visible="false"></asp:Label>
                                         <asp:Label ID="lblLangName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.name") %>'></asp:Label>
                                      </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, EditEntityOption_lblOptionName%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEntityName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DisplayCaption") %>' ></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtEntityName" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DisplayCaption") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqName" ValidationGroup="Update" runat="server" ControlToValidate="txtEntityName" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic" ></asp:RequiredFieldValidator><%--FB 2377--%>
                                        <asp:RegularExpressionValidator ID="regItemName1" ControlToValidate="txtEntityName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters11%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Option Description" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEntityDesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.HelpText") %>' ></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtEntityDesc" CssClass="altText" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.HelpText") %>'></asp:TextBox><%--FB 2377--%>
                                        <asp:RegularExpressionValidator ID="regItemName" ControlToValidate="txtEntityDesc" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters11%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`,\[\]{}\x22;=:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Actions%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" >
                                    <ItemTemplate>
                                        <asp:LinkButton id="btnEdit" text="<%$ Resources:WebResources, ManageCustomAttribute_btnEdit%>" commandname="Edit" runat="server"></asp:LinkButton>
                                        <asp:LinkButton id="btnDelete" text="<%$ Resources:WebResources, ManageCustomAttribute_btnDelete%>" commandname="Delete" runat="server"></asp:LinkButton>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:LinkButton id="btnUpdate" text="<%$ Resources:WebResources, ManageCustomAttribute_btnUpdate%>" commandname="Update" runat="server" onclientclick="javascript:return fnGridValidation()" validationgroup="Update"></asp:LinkButton>
                                        <asp:LinkButton id="btnCancel" text="<%$ Resources:WebResources, ManageCustomAttribute_btnCancel%>" commandname="Cancel" runat="server"></asp:LinkButton>
                                    </EditItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:Table runat="server" ID="tblNoOptions" Visible="false" Width="90%">
                        <asp:TableRow CssClass="lblError">
                            <asp:TableCell CssClass="lblError" HorizontalAlign="center">
                                <asp:Literal ID="Literal2" Text='<%$ Resources:WebResources, NoOptionsfound%>' runat='server' />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    </td>
                </tr>
                </table></td></tr><%--Added for FF--%>
                 <%--FB 1970 end--%>
                <tr>
                    <td align="center">
                        <br />
						<%--ZD 100420--%>
                        <%--<Button ID="BtnAddCustom" onserverclick="SetCustomAttribute"  runat="server" style="width:100pt" OnClick="javascript:return fnValidator();" >Submit</Button>--%><%-- FB 2796--%>
                        <input id="txthdnFocus" type="text" onfocus="document.getElementById('BtnAddCustom').setAttribute('onfocus', '');document.getElementById('BtnAddCustom').focus();" style="background-color:Transparent;border:none;" />
                        <asp:Button ID="BtnAddCustom" OnClick="SetCustomAttribute"  runat="server" Width="100pt" Text="<%$ Resources:WebResources, ManageCustomAttribute_BtnAddCustom%>" OnClientClick="javascript:return fnValidator()" ></asp:Button><%-- FB 2796--%>
                        <input type="button" id="btnGoBack" onclick="FnCancel()" value="<%$ Resources:WebResources, Cancel%>" runat="server" class="altMedium0BlueButtonFormat"/>
						<%--ZD 100420--%>
                    </td>
                </tr>
            </table>
        </div>
        </center>
    </form>
</body>
</html>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<script language="javascript">
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };

    fnOnComboChange();
    //FB 1767 start
    //Incmailselectall(); //ZD 101176
    function Incmailselectall()
    {
      var chkall = document.getElementById('ChkAll');
      clearErrLabel(chkall);
      if(document.getElementById('ChkCAEmail').checked == true)
      {
        document.getElementById('chkHost').disabled = false ;
        document.getElementById('ChkRoomApp').disabled = false ;
        document.getElementById('chkScheduler').disabled = false ;
        document.getElementById('ChkMcuApp').disabled = false ;
        document.getElementById('chkParty').disabled = false ;
        document.getElementById('ChkSysApp').disabled = false ;
        document.getElementById('chkRoomAdmin').disabled = false ;
        document.getElementById('chkMcuAdmin').disabled = false ;
        document.getElementById('ChkAll').disabled = false;
        document.getElementById('chkVNOCOperator').disabled = false; //FB 2501
      }
      else
      {
        chkall.checked = false;
        document.getElementById('ChkAll').disabled = true ;
        document.getElementById('chkHost').disabled = true ;
        document.getElementById('ChkRoomApp').disabled =  true ;
        document.getElementById('chkScheduler').disabled =  true ;
        document.getElementById('ChkMcuApp').disabled =  true ;
        document.getElementById('chkParty').disabled =  true ;
        document.getElementById('ChkSysApp').disabled =  true ;
        document.getElementById('chkRoomAdmin').disabled =  true ;
        document.getElementById('chkMcuAdmin').disabled = true;
        document.getElementById('chkVNOCOperator').disabled = true; //FB 2501
        selectAllMailRight()
      }
      
       
    }
    function selectAllMailRight()
    {
        var chkall = document.getElementById('ChkAll');
        if (chkall.checked == true)
            {
              if(document.getElementById('ChkCAEmail').checked == true)
              {
                clearErrLabel(chkall);
                document.getElementById('chkHost').checked = true;
                document.getElementById('ChkRoomApp').checked = true;
                document.getElementById('chkScheduler').checked = true ;
                document.getElementById('ChkMcuApp').checked = true;
                document.getElementById('chkParty').checked = true;
                document.getElementById('ChkSysApp').checked = true;
                document.getElementById('chkRoomAdmin').checked = true;
                document.getElementById('chkMcuAdmin').checked = true;
                document.getElementById('chkVNOCOperator').checked = true; //FB 2501
              }
            }
         else
          {
                document.getElementById('chkHost').checked = false;
                document.getElementById('ChkRoomApp').checked = false;
                document.getElementById('chkScheduler').checked = false ;
                document.getElementById('ChkMcuApp').checked = false;
                document.getElementById('chkParty').checked = false;
                document.getElementById('ChkSysApp').checked = false;
                document.getElementById('chkRoomAdmin').checked = false;
                document.getElementById('chkMcuAdmin').checked = false;
                document.getElementById('chkVNOCOperator').checked = false; //FB 2501                
          }
    }
    function clearErrLabel(obj) 
    {
        if (document.getElementById('errLabel') != null) //FB 1970
        {
            document.getElementById('errLabel').innerHTML = ""; // ZD 100288
            //document.getElementById('errLabel').style.display = "none"; //ZD 101176
        }
      if (obj.checked == false)
          document.getElementById('ChkAll').checked = false;
      else if (obj.checked == true)
      {
          if(document.getElementById('chkHost').checked == true)
           if(document.getElementById('ChkRoomApp').checked == true)
            if(document.getElementById('chkScheduler').checked == true)
             if(document.getElementById('ChkMcuApp').checked == true)
              if(document.getElementById('chkParty').checked == true)
               if(document.getElementById('ChkSysApp').checked == true)
                 if(document.getElementById('chkRoomAdmin').checked == true)
                     if (document.getElementById('chkMcuAdmin').checked == true)
                        if(document.getElementById('chkVNOCOperator').checked ==true) //FB 2501
                            document.getElementById('ChkAll').checked = true;
      }
  }
//  if (document.getElementById('errLabel') != null) //FB 1970 //ZD 101176
//    document.getElementById('errLabel').style.display = "block";
  //FB 1767 end

  //FB 1970 start
  ExpandCollapse(document.getElementById("imgLangsTitle"), "<%=otherLangRow.ClientID %>", true)
  ExpandCollapse(document.getElementById("imgLangsOption"), "<%=LangsOptionRow.ClientID %>", true);
  
  function maxCharRowShow()
  {
      document.getElementById("txtMaxChar").style.display = "block";
  }
  
  function ExpandCollapse(img, str, frmCheck)
   {
     
      obj = document.getElementById(str);
      if (obj != null)
      {
          if (frmCheck == true)
          {
                  img.src = img.src.replace("minus", "plus");
                  obj.style.display = "none";
          }
          if (frmCheck == false)
          {
              if (img.src.indexOf("minus") >= 0)
               {
                  img.src = img.src.replace("minus", "plus");
                  obj.style.display = "none";
              }
              else 
              {
                  img.src = img.src.replace("plus", "minus");
                  obj.style.display = "";
              }
          }
      }
  }
  //FB 1970 end

  //ZD 100420
  if (document.getElementById('txtEntityDesc') != null)
      document.getElementById('txtEntityDesc').setAttribute("onblur", "document.getElementById('btnAddEntityCode').focus(); document.getElementById('btnAddEntityCode').setAttribute('onfocus', '');");

  if (document.getElementById('BtnAddCustom') != null)
      document.getElementById('BtnAddCustom').setAttribute("onblur", "document.getElementById('btnGoBack').focus(); document.getElementById('btnGoBack').setAttribute('onfocus', '');");

</script>