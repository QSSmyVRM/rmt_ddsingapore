<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true"  Inherits="en_EmailSearch"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>myVRM</title>
     <%--FB 2790 Starts--%>
    <script type="text/javascript">
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
   </script>   
<%--FB 2790 Ends--%>
</head>

<body>
   <form name="frmEmailsearch" autocomplete="off" id="frmEmailsearch" runat="server"><%--ZD 101190--%>
    <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
  <input type="hidden" name="frm" value="<% =Request.QueryString["frm"].ToString() %>" />
  <input type="hidden" name="t" value="<% =Request.QueryString["t"].ToString() %>" />
  <script language="JavaScript" src="inc/functions.js"></script>
 <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055--%>
  <center>
    <h3><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, outlookemailsearch_AddressBookSe%>" runat="server"></asp:Literal></h3>
         <%--Window Dressing--%>
    <br /><font  class="blackblodtext"><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, EmailSearch_Pleaseuseany%>" runat="server"></asp:Literal></font> <br /><br /><br />
  
    <table cellpadding="6" cellspacing="5">
<%
if (Request.QueryString ["t"] != "g" ){
%>
      <tr>
         <%--Window Dressing--%>
        <td class="blackblodtext"> 
          <div align="center"><asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, EmailSearch_LoginName%>" runat="server"></asp:Literal></div>
        </td>
        <td> 
          <input type="text" tabindex="1" name="LoginName" size="15" class="altText" maxlength="256"  onkeyup="javascript:chkLimit(this,'2');" />
        </td>
      </tr>

<%	
}
%>
      <tr>
      <%--Window Dressing--%>
        <td class="blackblodtext"> 
          <div align="center"><asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, EmailSearch_FirstName%>" runat="server"></asp:Literal></div>
        </td>
        <td>           
            <%--FB 1888--%>
          <asp:TextBox ID="FirstName" runat="server" size="15" class="altText" maxlength="256" ></asp:TextBox>
          <asp:RegularExpressionValidator ID="RegularExpressionValidator20" ControlToValidate="FirstName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters16%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> 
        </td>
        <td>&nbsp;</td>
      <%--Window Dressing--%>
        <td class="blackblodtext"> 
          <div align="center"><asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, EmailSearch_LastName%>" runat="server"></asp:Literal></div>
        </td>
        <td>        
            <%--FB 1888--%>   
          <asp:TextBox ID="LastName" runat="server" size="15" class="altText" maxlength="256" ></asp:TextBox>
          <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="LastName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters16%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|!`\[\]{}\=@#$%&~]*$"></asp:RegularExpressionValidator> 
        </td>
      </tr>
    </table>

   <br /><br />

    <table cellpadding="4" cellspacing="6">
      <tr>
        <td>
         <input type="button" runat="server" name="EmailSearchSubmit" value="<%$ Resources:WebResources, CloseWindow%>" class="altMedium0BlueButtonFormat" onclick="JavaScript: window.close();" style="width:150px" />
        </td>
        <td>
        <%--Code Changed for SoftEdge Corner--%>
         <%--<input type="submit" onfocus="this.blur()" name="EmailSearchSubmit" value="Search" class="altShortBlueButtonFormat" />--%>
       <button ID="btnSearch" runat="server" onserverclick="SearchUsers" class="altMedium0BlueButtonFormat"><asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, EmailSearch_btnSearch%>" runat="server"></asp:Literal></button>
</td>
      </tr>
    </table>
    
  </center>

  <input type="hidden" name="fn" value="<% =Request.QueryString["fn"].ToString() %>" />
  <input type="hidden" name="n" value="<% =Request.QueryString["n"].ToString() %>" /> 
  <input type="hidden" name="cmd" value="RetrieveUsers" />
  </form>	 
</body>
</html>
<%--ZD 100428 START- Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">

    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {
            window.close();
        }
		//ZD 102963
        if (e.keyCode == 13) {
            if (document.getElementById("btnSearch") != null)
                document.getElementById("btnSearch").click();
        }
    }
</script>
<%--ZD 100428 END--%>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>