﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_MyVRM.SelRoomResources" ValidateRequest="false" %> <%--ZD 100170--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <%--ZD 101028 --%>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>myVRM</title>
  <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment.">
  <meta name="Keywords" content="VRM, myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <script type="text/javascript" src="script/wincheck.js"></script> <%--FB 3055-URL--%>
  <%--FB 2790 Starts--%>
<script type="text/javascript">
    var path = '<%=Session["OrgCSSPath"]%>';
    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
    document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
   </script>   
<%--FB 2790 Ends--%>

  <style type="text/css">.hdiv{position:absolute; top: 100; left: 200; width:200; visibility:hidden}</style>
  <script type="text/javascript" src="script/errorList.js"></script>
 
  
      <script language="javascript">
      <!--
    	
	    function errorHandler( e, f, l ){
		    alert("An error has ocurred in the JavaScript on this page.\nFile: " + f + "\nLine: " + l + "\nError:" + e);
		    return true;
	    }
    	
      //-->
      </script>
    <script language="JavaScript">

        function viewEndpoint(endpointId)
        {
//	        url = "dispatcher/admindispatcher.asp?eid=" + endpointId + "&cmd=GetEndpoint&ed=1&wintype=pop"; //Login Management
	        url = "endpointdetails.aspx?f=ed&eid=" + endpointId;//Login Management

	        if (!window.winrtc) {	// has not yet been defined
		        winrtc = window.open(url, "", "width=450,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
		        winrtc.focus();
	        } else { // has been defined
	            if (!winrtc.closed) {     // still open
	    	        winrtc.close();
	                winrtc = window.open(url, "", "width=450,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
			        winrtc.focus();
		        } else {
	                winrtc = window.open(url, "", "width=450,height=310,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
	                winrtc.focus();
		        }
	        }
        }	
        
        //Maps --Start
        function GetDirection()
	    {
		    var args=GetDirection.arguments;
		    
		    if (args[0] != "N")
		    {
			    window.open(args[0],"",'menubar=no,resizable=1,scrollbars=yes,width=770,height=430,left=200,top=400');
		    }
		    else
		    {
		        alert(LocationAddress);
		    }
        }		
       //Maps --End
      
        function toggleDiv(id,flagit) 
        {
            if (flagit=="1")
            {
                if (document.layers) document.layers[''+id+''].visibility = "show"
                else if (document.all[''+id+''].style != null) document.all[''+id+''].style.visibility = "visible" //FB 2164
                else if (document.getElementById) document.getElementById(''+id+'').style.visibility = "visible"
            }
            else
                if (flagit=="0")
                {
                    if (document.layers) document.layers[''+id+''].visibility = "hide"
                    else if (document.all[''+id+''].style != null) document.all[''+id+''].style.visibility = "hidden" //FB 2164
                    else if (document.getElementById) document.getElementById(''+id+'').style.visibility = "hidden"
                }
        }
        //FB 2400
        function ShowHide(id, flagit)
        {
            if (flagit == "1")
                document.getElementById("multiCodecPopUp").style.display = 'block';
            else if (flagit == "0")
                document.getElementById("multiCodecPopUp").style.display = 'none';
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <%--FB 2400 start--%>
        <div id="multiCodecPopUp"  runat="server" align="center" style="top: 150px;left:365px; POSITION: absolute; WIDTH:30%; HEIGHT: 350px;VISIBILITY: visible; Z-INDEX: 3; display:none; background-color:#E0E0E0"> <%--ZD 100426--%> 
          <table cellpadding="2" cellspacing="1"  width="70%" class="tableBody" align="center">
             <tr>
                <td class="subtitleblueblodtext" align="center" colspan="2">
                     <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, Address%>" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="multicodec" runat="server"></asp:Label>               
                </td>
            </tr>
          </table>
    </div>
    <%--FB 2400 end--%>
    <div>
        <center>
            <H3><asp:Label ID="lblHeading" runat="server" /></H3>
        </center>    
        <center>
                <%--Window Dressing Removed BGColor--%>
        <TABLE width='98%' cellspacing='0' cellpadding='0'>
            <TR>
                <%--Window Dressing Removed BGColor--%>
                <TD >
                    <table border='0' cellpadding='0' cellspacing='1' width='100%' bordercolor='#0000CC'>
                        <tr id="trRoomName" runat="server"></tr>
                        <tr id="trTier"  runat="server"></tr>
                        <tr id="trFloorRoom" runat="server"></tr>
                        <tr id="trAddress" runat="server"></tr>
                        <tr id="trPDir" runat="server"></tr>
                        <tr id="trMapLink" runat="server"></tr>
                        <tr id="trTz" runat="server"></tr>
                        <tr id="trPhoneNo" runat="server"></tr>
                        <tr id="trCapacity" runat="server"></tr>
                        <%--ZD 101563 Start--%>
                        <%--<tr id="trSTB"  runat="server"></tr>
                        <tr id="trTTB" runat="server"></tr>--%>
                        <%--ZD 101563 End--%>
                        <tr id="trProjector" runat="server"></tr>
                        <tr id="trRImg" runat="server"></tr>
                        <tr id="trCF" runat="server"></tr>
                        <tr id="trAIC" runat="server"></tr>
                        <tr id="trMap1"  runat="server"></tr>
                        <tr id="trSecurity1" runat="server" style="display:none"></tr>
                        <tr id="trMisc1" runat="server"></tr>
                        <tr id="trPriAppr" runat="server"></tr>
                        <tr id="trEPT" runat="server"></tr>
                        <tr id="trDept" runat="server"></tr>
                        <tr id="trExternalNumber" runat="server"></tr> <%--FB 2448 --%>
                        <tr id="trInternalNumber" runat="server"></tr> <%--FB 2448 --%>
                    </table>
                </TD>
           </TR>
        </TABLE>         
        
        <br><br>

      <table width="60%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
          <td align="center">
            <input type="submit" runat="server" name="Submit" value="<%$ Resources:WebResources, CloseWindow%>" class="altMedium0BlueButtonFormat" onClick="window.close()" style="width:150px"/>
          </td>
        </tr>
      </table>
      </center>
    </div>
    </form>
</body>
</html>
<script language="JavaScript">
<!--
    window.resizeTo(900, 700);
    //ZD 101291 - Start
    if (navigator.userAgent.indexOf('Chrome') > -1 && window.opener.location.href.toLowerCase().indexOf("manageconference.aspx") > -1) {
        setTimeout("window.opener.scrollTo(0, 1);window.opener.scrollTo(0, 0);", 100);
    }
    //ZD 101291 - End
//-->
</script>

<%--ZD 100428 START- Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">

    document.onkeydown = EscClosePopup;
    function EscClosePopup(e) {
        if (e == null)
            var e = window.event;
        if (e.keyCode == 27) {
            window.close();
        }
    }
</script>
<%--ZD 100428 END--%>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>