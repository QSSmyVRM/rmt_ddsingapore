/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
/** FB 1850 & FB 1861 Both aspx and cs were restructured for performance **/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Text;
using System.Threading;
using System.ComponentModel;
using System.Linq;
using System.Xml.Linq;
using DevExpress.Web.ASPxTabControl;
using System.Text.RegularExpressions;

public partial class RoomCalendar : System.Web.UI.Page
{

    protected System.Web.UI.HtmlControls.HtmlGenericControl Html1;
    protected System.Web.UI.HtmlControls.HtmlHead Head1;
    protected System.Web.UI.HtmlControls.HtmlForm frmCalendarviewroom;
    protected System.Web.UI.ScriptManager CalendarScriptManager;
    protected System.Web.UI.HtmlControls.HtmlInputHidden IsMonthChanged;
    protected System.Web.UI.HtmlControls.HtmlInputHidden IsSettingsChange;
    protected System.Web.UI.HtmlControls.HtmlInputHidden IsWeekOverLap;
    protected System.Web.UI.HtmlControls.HtmlInputHidden HdnMonthlyXml;
    protected System.Web.UI.WebControls.Label errLabel;
    protected System.Web.UI.WebControls.Label CalenMenu;
    protected System.Web.UI.UpdatePanel PanelTab;
    protected System.Web.UI.HtmlControls.HtmlInputHidden HdnXml;
    protected System.Web.UI.WebControls.View Daily;
    protected System.Web.UI.WebControls.CheckBox officehrDaily;
    protected System.Web.UI.WebControls.View Weekly;
    protected System.Web.UI.WebControls.CheckBox officehrWeek;
    protected System.Web.UI.WebControls.View Monthly;
    protected System.Web.UI.WebControls.CheckBox officehrMonth;
    protected System.Web.UI.WebControls.Button btnDate;
    protected System.Web.UI.WebControls.TreeView treeRoomSelection;
    protected System.Web.UI.WebControls.TextBox txtType;
    protected System.Web.UI.HtmlControls.HtmlInputHidden txtSelectedDate;
    protected System.Web.UI.HtmlControls.HtmlInputHidden txtSelectedDate1;
    protected System.Web.UI.HtmlControls.HtmlTableCell Tdbuffer1;
    protected System.Web.UI.HtmlControls.HtmlTableCell Tdbuffer2;
    protected System.Web.UI.HtmlControls.HtmlTableCell Tdbuffer3;
    protected System.Web.UI.HtmlControls.HtmlTableCell Tdbuffer4;
    protected AjaxControlToolkit.CollapsiblePanelExtender Dateextender;
    protected System.Web.UI.WebControls.Panel TableCalendar;
    protected System.Web.UI.WebControls.Image Collapse;
    protected System.Web.UI.HtmlControls.HtmlInputButton btnCompare;
    protected System.Web.UI.WebControls.RadioButtonList rdSelView;
    protected System.Web.UI.WebControls.Panel pnlLevelView;
    protected System.Web.UI.WebControls.Panel pnlListView;
    protected System.Web.UI.HtmlControls.HtmlInputCheckBox selectAllCheckBox;
    protected System.Web.UI.WebControls.CheckBoxList lstRoomSelection;
    protected System.Web.UI.WebControls.Panel pnlNoData;
    protected System.Web.UI.WebControls.TextBox txtTemp;
    protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator2;
    protected System.Web.UI.WebControls.Label Label4;
    protected System.Web.UI.WebControls.Label Label3;
    protected System.Web.UI.WebControls.Label NRoom;
    protected System.Web.UI.HtmlControls.HtmlInputHidden selectedloc;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdAV1;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdAV;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdA;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdA1;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdP2p1;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdP2p;
    //protected System.Web.UI.HtmlControls.HtmlSelect lstCalendar;//FB 2585
    //protected System.Web.UI.WebControls.DropDownList lstCalendar; //ZD 101022 //ZD 101388
    
    protected DayPilot.Web.Ui.DayPilotScheduler schDaypilot;
    protected DayPilot.Web.Ui.DayPilotScheduler schDaypilotMonth;
    protected DayPilot.Web.Ui.DayPilotScheduler schDaypilotweek;
    protected DayPilot.Web.Ui.DayPilotBubble Details;
    protected DayPilot.Web.Ui.DayPilotBubble DetailsMonthly;
    protected DayPilot.Web.Ui.DayPilotBubble DetailsWeekly;
    protected System.Web.UI.HtmlControls.HtmlInputHidden locstrname;
    protected System.Web.UI.HtmlControls.HtmlInputHidden Sellocstrname;
    protected System.Web.UI.HtmlControls.HtmlInputHidden selectedList;
    protected System.Web.UI.HtmlControls.HtmlInputHidden roomstrnames; //ZD 102086    
    protected DevExpress.Web.ASPxTreeList.ASPxTreeList treeRoomSelector; // ZD 102358

    protected DevExpress.Web.ASPxTabControl.ASPxPageControl CalendarContainer;

    protected Int32 hasApproal;
    protected String dtFormatType = "MM/dd/yyyy";
    protected String CalendarType = "D";
    protected String timeFormat = "1";
    protected String tFormats = "hh:mm tt";
    protected String isPublic = "D", isFuture = "D", isPending = "D", isApproval = "D", isOngoing = "D";
    protected Boolean bypass = false, isAdminRole = false;
    protected DateTime conf = DateTime.Now;
    
    protected System.Web.UI.HtmlControls.HtmlInputHidden selStatus; //FB 2804
	//FB 2450
    //protected msxml4_Net.DOMDocument40Class XmlDoc = null;

    protected string paramHF;
    protected string paramF;
    protected string selRooms;
    protected String upTZ;
    protected String xmlstr = "";
    protected String enableBufferZone = "";

    myVRMNet.NETFunctions obj = null;
    ns_Logger.Logger log = null;   //Location Issues
    DateTime dtCell = DateTime.Now;//FB 1861
    String cellColor = "";//FB 1861
    String[] strSplitat = new String[1] { "@@" }; //FB 2012
    protected int dayCounter = 0;//1851

    protected HtmlImage ImgPrinter;

    //Organization/CSS Module -- Start
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAV;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnRmHrg;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAudCon;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnPpConf;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAVW;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnRmHrgW;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAudConW;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnPpConfW;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAVM;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnRmHrgM;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnAudConM;
    protected System.Web.UI.HtmlControls.HtmlGenericControl spnPpConfM;
    CustomizationUtil.CSSReplacementUtility cssUtil;
    //Organization/CSS Module -- End

    protected System.Web.UI.WebControls.CheckBox showDeletedConf; //FB 1800
    protected System.Web.UI.HtmlControls.HtmlTableRow trlegend1; //FB 1985
    protected System.Web.UI.HtmlControls.HtmlTableRow trlegend2; //FB 1985
    protected System.Web.UI.HtmlControls.HtmlTableRow trlegend3; //FB 1985
    protected System.Web.UI.HtmlControls.HtmlTableCell tdSelectRooms;//FB 2802 //ZD 102358
    protected System.Web.UI.HtmlControls.HtmlTableCell tdClosed;//FB 2804
    //ZD 100157 Starts
    protected MetaBuilders.WebControls.ComboBox lstStartHrs;
    protected MetaBuilders.WebControls.ComboBox lstEndHrs;
    protected System.Web.UI.WebControls.RegularExpressionValidator reglstStartHrs;
    protected System.Web.UI.WebControls.RegularExpressionValidator reglstEndHrs;
    StringBuilder inXML = new StringBuilder();
    int isShowHrs = 0;
    public int calwidth;
    //ZD 100157 Ends
    
    //ZD 100963 START
    protected System.Web.UI.HtmlControls.HtmlTableRow trprivateview;
    protected System.Web.UI.HtmlControls.HtmlTableRow trprivateconfview;
    protected System.Web.UI.HtmlControls.HtmlTableRow trReserved;
    protected System.Web.UI.HtmlControls.HtmlTableRow trsp1;
    protected System.Web.UI.HtmlControls.HtmlTableCell TdReservedbgcolor;

    protected System.Web.UI.HtmlControls.HtmlTableRow trprivateweeklyview;
    protected System.Web.UI.HtmlControls.HtmlTableRow trprivateweeklyconf;
    protected System.Web.UI.HtmlControls.HtmlTableRow trReservedweekly;
    protected System.Web.UI.HtmlControls.HtmlTableRow trsp2;

    protected System.Web.UI.HtmlControls.HtmlTableRow trprivatemonthlyview;
    protected System.Web.UI.HtmlControls.HtmlTableRow trprivatemonthlyconf;
    protected System.Web.UI.HtmlControls.HtmlTableRow trReservedmonthly;
    protected System.Web.UI.HtmlControls.HtmlTableRow trsp3;

    protected System.Web.UI.HtmlControls.HtmlTableRow trDeleteConf;
    //ZD 100963 END

    protected System.Web.UI.WebControls.Panel PopupRoomPanel; //ZD 101175
    protected System.Web.UI.HtmlControls.HtmlInputHidden selectedListRoomName; //ZD 101175
    public bool Enabletreeview = false; //ZD 101175
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTopTier;//ZD 102358
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMiddleTier;//ZD 102358
    protected System.Web.UI.HtmlControls.HtmlInputHidden selectedTierList; //ZD 102358
    protected System.Web.UI.WebControls.Label DailyViewDate;
    
	//ZD 101022
    #region InitializeCulture
    protected override void InitializeCulture()
    {
        if (Session["UserCulture"] != null)
        {
            UICulture = Session["UserCulture"].ToString();
            Culture = Session["UserCulture"].ToString();
            base.InitializeCulture();
        }
    }
    #endregion

    #region Page Load Event
    /// <summary>
    /// Page Load Event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        String favRooms = "";
        ListItem itemToRemove; //ZD 100284
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("RoomCalendar.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            // ZD 100170                
            string path = Request.Url.AbsoluteUri.ToLower();
            if (path.IndexOf("%3c") > -1 || path.IndexOf("%3e") > -1)
                Response.Redirect("ShowError.aspx");

            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();   //Location Issues

            //ZD 100335 start
            if (Request.Cookies["hdnscreenres"] != null)
                Session["hdnscreenresoul"] = Request.Cookies["hdnscreenres"].Value;
            //ZD 100335 End

            //ZD 100157 start
            calwidth = 1024;
            if (Session["hdnscreenresoul"] != null && Session["hdnscreenresoul"].ToString() != "")
            {
                if (Session["language"] != null && Session["language"].ToString() != "en") // ZD 100288                                     
                    calwidth = Int32.Parse(Session["hdnscreenresoul"].ToString()) - 225; //ZD 100806               
                else
                    calwidth = Int32.Parse(Session["hdnscreenresoul"].ToString()) - 265; //ZD 100806

                schDaypilot.Width = calwidth;
                schDaypilotweek.Width = calwidth;
                schDaypilotMonth.Width = calwidth;
            }
            //ZD 100157 End

            //ZD 100288 Starts
            //Organization/CSS Module - Create folder for UI Settings --- Strat
            /*String fieldText = "";
            cssUtil = new CustomizationUtil.CSSReplacementUtility();
            if (Session["isMultiLingual"] != null)
            {
                if (Session["isMultiLingual"].ToString() != "1")
                {

                    fieldText = cssUtil.GetUITextForControl("RoomCalendar.aspx", "spnAV");
                    spnAV.InnerText = fieldText;
                    spnAVW.InnerText = fieldText;
                    spnAVM.InnerText = fieldText;

                    fieldText = cssUtil.GetUITextForControl("RoomCalendar.aspx", "spnRmHrg");
                    spnRmHrg.InnerText = fieldText;
                    spnRmHrgW.InnerText = fieldText;
                    spnRmHrgM.InnerText = fieldText;

                    if (Application["client"] != null)
                        if (Application["client"].ToString().ToUpper() != "MOJ")
                        {
                            fieldText = cssUtil.GetUITextForControl("RoomCalendar.aspx", "spnAudCon");
                            spnAudCon.InnerText = fieldText;
                            spnAudConW.InnerText = fieldText;
                            spnAudConM.InnerText = fieldText;

                            fieldText = cssUtil.GetUITextForControl("RoomCalendar.aspx", "spnPpConf");
                            spnPpConf.InnerText = fieldText;
                            spnPpConfW.InnerText = fieldText;
                            spnPpConfM.InnerText = fieldText;
                        }
                    //Organization/CSS Module - Create folder for UI Settings --- End
                }
            }*/
            //ZD 100288 Starts

            //ZD 100157 Starts
            lstStartHrs.Items.Clear();
            lstEndHrs.Items.Clear();
            obj.BindTimeToListBox(lstStartHrs, true, false);
            obj.BindTimeToListBox(lstEndHrs, true, false);
            //ZD 100284 - Start
            if (Session["IsOpen24Hours"].ToString() == "0" && Session["SystemEndTime"].ToString() == "11:00 PM")
            {
                itemToRemove = lstEndHrs.Items[lstEndHrs.Items.Count - 1];
                lstEndHrs.Items.Remove(itemToRemove);
            }
            //ZD 100284 - End //ZD 102782 - Start
            if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("TRIDENT"))
                ScriptManager.RegisterStartupScript(this, this.GetType(), "updtCalScroll", "setTimeout('fnTimeScroll();',500);", true);
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(), "updtCalScroll", "setTimeout('fnTimeScroll();',250);", true);
            //ZD 100157 Ends //ZD 102782 - End

            if (Session["EnableBufferZone"] == null)//Organization Module Fixes
            {
                Session["EnableBufferZone"] = "0";//Organization Module Fixes
            }

            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() == "")
                    Session["FormatDateType"] = "MM/dd/yyyy";
                else
                    dtFormatType = Session["FormatDateType"].ToString();
            }

            enableBufferZone = Session["EnableBufferZone"].ToString();//Organization Module Fixes

            if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))//Code added for Phase2
            {
                Tdbuffer1.Attributes.Add("style", "display:none");
                Tdbuffer2.Attributes.Add("style", "display:none");
                Tdbuffer3.Attributes.Add("style", "display:none");
                Tdbuffer4.Attributes.Add("style", "display:none");
                TdA.Attributes.Add("style", "display:none");
                TdA1.Attributes.Add("style", "display:none");
                TdP2p.Attributes.Add("style", "display:none");
                TdP2p1.Attributes.Add("style", "display:none");
                TdAV.Attributes.Add("style", "display:none");
                TdAV1.Attributes.Add("style", "display:none");
                enableBufferZone = "0";
            }

            if (Session["admin"] != null)
            {
                if (Session["admin"].ToString() == "1" || Session["admin"].ToString() == "2")   //buffer zone - Super & conf admin
                    isAdminRole = true;
            }
            //ZD 100085-Commented
            //if (enableBufferZone == "0")
            //{
            //    schDaypilot.UseEventBoxes = DayPilot.Web.Ui.Enums.UseBoxesEnum.ShortEventsOnly;
            //    schDaypilotweek.UseEventBoxes = DayPilot.Web.Ui.Enums.UseBoxesEnum.ShortEventsOnly;
            //    schDaypilotMonth.UseEventBoxes = DayPilot.Web.Ui.Enums.UseBoxesEnum.ShortEventsOnly;
            //}

            selRooms = " ,";
			//ZD 103171 Starts
            string selecteddate = txtSelectedDate.Value;
            if (dtFormatType == "dd/MM/yyyy") //ZD 103080
                selecteddate = myVRMNet.NETFunctions.GetDefaultDate(selecteddate);
            if (!DateTime.TryParse(selecteddate, out conf)) //ZD 103171
                conf = DateTime.Today;

            timeFormat = ((Session["timeFormat"] != null) ? Session["timeFormat"].ToString() : timeFormat);

            if (Session["timeFormat"].ToString() == "0")
            {
                tFormats = "HH:mm";
                schDaypilot.TimeFormat = DayPilot.Web.Ui.Enums.TimeFormat.Clock24Hours;
                schDaypilotweek.TimeFormat = DayPilot.Web.Ui.Enums.TimeFormat.Clock24Hours;
            }
            else if (Session["timeFormat"].ToString() == "2")//FB 2588
            {
                tFormats = "HHmmZ";
            }
            //ZD 100157 Starts
            if (Session["timeFormat"].ToString().Equals("0"))
            {
                reglstStartHrs.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                reglstStartHrs.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                reglstEndHrs.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                reglstEndHrs.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
            }
            else if (Session["timeFormat"].ToString().Equals("2"))
            {
                reglstStartHrs.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                reglstStartHrs.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                reglstEndHrs.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                reglstEndHrs.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
            }
            //ZD 100157 Starts
            if (Session["uptz"] != null)
            {
                upTZ = Session["uptz"].ToString();
            }

            if (Request.QueryString["hf"] != null)
            {
                paramHF = Request.QueryString["hf"].ToString();
            }

            if (Request.QueryString["f"] != null)
            {
                paramF = Request.QueryString["f"].ToString();
            }

            if (Session["Holidays"] != null)
            {
                if (Session["Holidays"].ToString().Trim() != "")
                {
                    schDaypilotweek.BeforeCellRender += new DayPilot.Web.Ui.Events.BeforeCellRenderEventHandler(BeforeCellRenderhandler);

                    schDaypilotMonth.BeforeCellRender += new DayPilot.Web.Ui.Events.BeforeCellRenderEventHandler(BeforeCellRenderhandler);

                }
            }

            //FB 1985 
            if (Application["Client"].ToString().ToUpper() == "DISNEY")
            {
                trlegend1.Style.Add("display", "none");
                trlegend2.Style.Add("display", "none");
                trlegend3.Style.Add("display", "none");
            }
            //IsMonthChanged.Value = "Y";

            //ZD 100963 START
            if (Session["EnableRoomCalendarView"] != null)
            {
                string EnableRoomCalendarView = Session["EnableRoomCalendarView"].ToString();
                if (EnableRoomCalendarView != "1")//Not equal to-> Private Room Calendar View
                {
                    trprivateconfview.Attributes.Add("style", "display:block"); // Daily
                    trprivateview.Attributes.Add("style", "display:block");
                    trReserved.Attributes.Add("style", "display:none");
                    if (trsp1 != null)
                        trsp1.Attributes.Add("style", "display:block");

                    trprivateweeklyview.Attributes.Add("style", "display:block");//Weekly
                    trprivateweeklyconf.Attributes.Add("style", "display:block");
                    trReservedweekly.Attributes.Add("style", "display:none");
                    if (trsp2 != null)
                        trsp2.Attributes.Add("style", "display:block");

                    trprivatemonthlyview.Attributes.Add("style", "display:block");//Monthly
                    trprivatemonthlyconf.Attributes.Add("style", "display:block");
                    trReservedmonthly.Attributes.Add("style", "display:none");
                    if (trsp3 != null)
                        trsp3.Attributes.Add("style", "display:block");

                    trDeleteConf.Attributes.Add("style", "display:block");

                }
                else
                {
                    trprivateconfview.Attributes.Add("style", "display:none");//Daily
                    trprivateview.Attributes.Add("style", "display:none");
                    trReserved.Attributes.Add("style", "display:block");
                    if (trsp1 != null)
                        trsp1.Attributes.Add("style", "display:none");

                    trprivateweeklyview.Attributes.Add("style", "display:none");//Weekly
                    trprivateweeklyconf.Attributes.Add("style", "display:none");
                    trReservedweekly.Attributes.Add("style", "display:block");
                    if (trsp2 != null)
                        trsp2.Attributes.Add("style", "display:none");

                    trprivatemonthlyview.Attributes.Add("style", "display:none");//Monthly
                    trprivatemonthlyconf.Attributes.Add("style", "display:none");
                    trReservedmonthly.Attributes.Add("style", "display:block");
                    if (trsp3 != null)
                        trsp3.Attributes.Add("style", "display:none");

                    trDeleteConf.Attributes.Add("style", "display:none");
                }

                if (EnableRoomCalendarView != "3" && CalendarContainer.ActiveTabIndex == 0)//Private & Partial
                    schDaypilot.BubbleID = "";
                else
                    schDaypilot.BubbleID = "Details";

                if (EnableRoomCalendarView != "3" && CalendarContainer.ActiveTabIndex == 1)//Private & Partial
                    schDaypilotweek.BubbleID = "";
                else
                    schDaypilotweek.BubbleID = "Details";

                if (EnableRoomCalendarView != "3" && CalendarContainer.ActiveTabIndex == 2)//Private & Partial
                    schDaypilotMonth.BubbleID = "";
                else
                    schDaypilotMonth.BubbleID = "Details";
            }
            //ZD 100963 END

            bool VisibleRoom = false;

            if (!IsPostBack)
            {
                GetUserCalendarHrs();//ZD 100157
                //officehrDaily.Checked = true;
                officehrWeek.Checked = true;
                officehrMonth.Checked = true;

                if (Session["DefaultCalendarToOfficeHours"] != null)//Organization Module Fixes
                {
                    if (Session["DefaultCalendarToOfficeHours"].ToString() == "0")
                    {
                        officehrDaily.Checked = false;
                        officehrWeek.Checked = false;
                        officehrMonth.Checked = false;
                    }

                }

                if (Request.QueryString["hf"] != null)
                {
                    if (Request.QueryString["hf"].ToString() == "1")
                    {
                        if (Request.QueryString["m"] != null)
                        {
                            selectedList.Value = Request.QueryString["m"].ToString();

                        }
                        //ZD 102650
                        if (Request.QueryString["m1"] != null)
                        {
                            selectedTierList.Value = Request.QueryString["m1"].ToString();
                        }

                        if (Request.QueryString["d"] != null)
                        {
                            if (!DateTime.TryParse(myVRMNet.NETFunctions.GetDefaultDate(Request.QueryString["d"].ToString()), out conf))
                                conf = DateTime.Today;
                        }

                    }

                }

                //ZD 101175 Starts

                BindData(ref VisibleRoom);
            }
            BindTreeList(); // ZD 102358
            //            selectRoomByValue("3:3:32;1:1:30;5:5:31;5:5:29");

            if (!IsPostBack)
            {
                string favRooms1 = "";
                if (VisibleRoom)
                {
                    IsMonthChanged.Value = "Y";
                    String outXMLDept = "";
                    String inXMLDept = "<login>";
                    inXMLDept += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                    inXMLDept += "</login>";
                    if (Request.QueryString["hf"] != null)
                    {
                        if (Request.QueryString["hf"].ToString() != "1")
                        {

                            if (Session["UserdeptXML"] == null)
                            {
                                outXMLDept = obj.CallMyVRMServer("GetRoomsDepts", inXMLDept, Application["MyVRMServer_ConfigPath"].ToString());
                                Session.Add("UserdeptXML", outXMLDept);

                            }
                            else
                                outXMLDept = Session["UserdeptXML"].ToString();

                            if (outXMLDept != "")
                            {
                                XmlDocument deptdoc = new XmlDocument();
                                deptdoc.LoadXml(outXMLDept);

                                XmlNode favlist = deptdoc.SelectSingleNode("Rooms/favourite");
                                if (favlist != null)
                                    favRooms = favlist.InnerText;
								//ZD 102358
                                XmlNode favlist1 = deptdoc.SelectSingleNode("Rooms/favouriteRooms");
                                if (favlist1 != null)
                                    favRooms1 = favlist1.InnerText;

                                selectedList.Value = favRooms;

                            }
                        }
                    }

                    if (selectedList.Value != "")
                    {
						//ZD 102358
                        if (favRooms1 != "" || selectedTierList.Value != "")
                        {
                            string rooms = "";
                            if (favRooms1 != "")
                                rooms = favRooms1;
                            else if (selectedTierList.Value != "")
                                rooms = selectedTierList.Value;

                            selectRoomByValue(rooms);
                        }
                        rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                        ChangeCalendarDate(null, null);
                    }

                    //ZD 103918
                   /* if (Session["isExpressUser"] != null)//FB 1779
                    {
                        if (Session["isExpressUser"].ToString() == "1")
                            schDaypilot.CellSelectColor = System.Drawing.Color.Empty;
                    }*/
                    IsMonthChanged.Value = "";
                }

            }

            //ZD 101175 Ends

        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace(ex.StackTrace + " : " + ex.Message);  //Location Issues
        }
    }
    #endregion   

    protected void BindTreeList() // //ZD 102358
    {
        treeRoomSelector.DataSource = (DataTable)Session["dtTiers"];
        treeRoomSelector.DataBind();
        treeRoomSelector.ExpandToLevel(1);


        DevExpress.Web.ASPxTreeList.TreeListNodeIterator iterator = treeRoomSelector.CreateNodeIterator();
        //string[] leafNodes = new string[treeRoomSelector.Nodes.Count];
        List<String> leafNodes = new List<String>();
        while (iterator.Current != null)
        {
            if (iterator.Current.Key.IndexOf("NT") > -1 || iterator.Current.Key.IndexOf("NR") > -1)
                iterator.Current.AllowSelect = false;

            if (iterator.Current.ChildNodes.Count == 0 && iterator.Current.Level < 4)
            {
                if (iterator.Current.Key.IndexOf("NT") == -1)
                    leafNodes.Add(iterator.Current.Key);
            }

            if (iterator.Current.ParentNode != null && iterator.Current.ParentNode.Selected == true) // PRABU NEW
                iterator.Current.Selected = true;

            iterator.GetNext();
        }

        for (int i = 0; i < leafNodes.Count; i++) // Set expand button for Leaf node
        {
            DevExpress.Web.ASPxTreeList.TreeListNode node = treeRoomSelector.AppendNode(("L" + i.ToString()), treeRoomSelector.FindNodeByKeyValue(leafNodes[i]));
            node["Name"] = "";
        }
    }

    #region rdSelView_SelectedIndexChanged

    protected void rdSelView_SelectedIndexChanged(object sender, EventArgs e)
    {
        selRooms = "";
        // code changed for FB 1319 -- start
        Int32 cnt = 0;
        Int32 mCnt = 0;
        Int32 tCnt = 0;
        bool isSelectRooms = false;//FB 2802
        bool isClosed = false; //FB 2804

		//ZD 102358
        //FB 2804 Start
        DateTime dd = DateTime.Now;
        string DateNew;
        DateNew = Convert.ToString((int)dd.DayOfWeek + 1);

        string str = "";
        if (Session["DaysClosed"] != null)
            str = Session["DaysClosed"].ToString();

        string[] selDays = str.Split(',');

        for (var i = 0; i < selDays.Length; i++)
        {
            if (selDays[i] == DateNew)
                isClosed = true;
        }

        //FB 2804 - Start
        if (isClosed)
        {
            tdClosed.Attributes.Add("style", "display:block");
            selStatus.Value = "0";
        }
        else
        {
            tdClosed.Attributes.Add("style", "display:none");
            selStatus.Value = "1";
        }

        //FB 2804 - End
        /*

        if (rdSelView.SelectedValue.Equals("2"))
        {
            pnlListView.Visible = true;
            pnlLevelView.Visible = false;
            lstRoomSelection.ClearSelection();
            HtmlInputCheckBox selectAll = (HtmlInputCheckBox)FindControl("selectAllCheckBox");

            if (selectedList.Value.Trim() != "")
            {
                foreach (ListItem lstItem in lstRoomSelection.Items)
                {
                    for (int i = 0; i < selectedList.Value.Trim().Split(',').Length; i++)
                        if (lstItem.Value.Equals(selectedList.Value.Split(',')[i].Trim()))
                        {
                            lstItem.Selected = true;
                            cnt = cnt + 1;
                            isSelectRooms = true;//FB 2802
                        }
                }

                if (selectAll != null)
                {
                    if (cnt == lstRoomSelection.Items.Count)
                        selectAll.Checked = true;
                    else
                        selectAll.Checked = false;
                }
            }
            else
            {
                if (selectAll != null)
                    selectAll.Checked = false;
            }
        }
        else
        {
            pnlLevelView.Visible = true;
            pnlListView.Visible = false;
           //FB 2804 Start
            DateTime dd = DateTime.Now;
            string DateNew;


            DateNew = Convert.ToString((int)dd.DayOfWeek + 1);

            string str = "";
            if (Session["DaysClosed"] != null)
                str = Session["DaysClosed"].ToString();

            string[] selDays = str.Split(',');

            for (var i = 0; i < selDays.Length; i++)
            {
                if (selDays[i] == DateNew)
                    isClosed = true;
            }
            //FB 2804 End

            //ZD 102358 - for Advance Search Selection
            if (selectedList.Value.Trim() != "" || selectedTierList.Value.Trim() != "")
            {
                String[] strList = selectedTierList.Value.Split(',');

                for (Int32 i = 0; i < strList.Length; i++)
                {
                    String[] strTier = strList[i].Split(':');

                    BindTiersANDRooms(strTier[0] + "_T", strTier[0], "", strTier[3]); // Top Tier _ T

                    BindTiersANDRooms(strTier[1] + "_" + strTier[0] + "_M", strTier[1], strTier[2], strTier[3]);  //Middele Tier _ Top Tier _ M
                }
            }

            //ZD 102358
            if (IsPostBack)
            {
                foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                {
                    Enabletreeview = true;
                    tCnt = 0;
                    foreach (TreeNode tnMid in tnTop.ChildNodes)
                    {
                        mCnt = 0;
                        foreach (TreeNode tn in tnMid.ChildNodes)
                        {
                            tn.Checked = false;
                            if (selectedList.Value.Trim() != "")
                            {
                                for (int i = 0; i < selectedList.Value.Trim().Split(',').Length; i++)
                                    if (tn.Depth.Equals(3) && tn.Value.Equals(selectedList.Value.Split(',')[i].Trim()))
                                    {
                                        tn.Checked = true;
                                        isSelectRooms = true;
                                        mCnt++;
                                    }
                            }
                        }

                        if (mCnt == tnMid.ChildNodes.Count)
                        {
                            tnMid.Checked = true;
                            tCnt++;
                        }
                        else
                            tnMid.Checked = false;

                    }

                    if (tCnt == tnTop.ChildNodes.Count)
                    {
                        tnTop.Checked = true;
                        treeRoomSelection.Nodes[0].Checked = true;
                    }
                    else
                    {
                        tnTop.Checked = false;
                        treeRoomSelection.Nodes[0].Checked = false;
                    }
                }
            }
            //FB 2804 - Start
            if (isClosed)
            {
                tdClosed.Attributes.Add("style", "display:block");
                selStatus.Value = "0";
            }
            else
            {
                tdClosed.Attributes.Add("style", "display:none");
                selStatus.Value = "1";
            }
            //FB 2804 - End
        }
        */
        //if (lstRoomSelection != null)
        //{
        //    if (lstRoomSelection.Items.Count == 0)
        //    {
        //        rdSelView.Enabled = false;
        //        pnlListView.Visible = false;
        //        pnlLevelView.Visible = false;
        //        btnCompare.Disabled = true;
        //        pnlNoData.Visible = true;
        //    }
           
        //}

        // code changed for FB 1319 -- end
    }


    #endregion

   #region Bind Data

    private void BindData(ref bool visibletree) //ZD 101175
    {
        int ttlROom = 0; //ZD 102044
        obj = new myVRMNet.NETFunctions();

        //obj.GetManageConfRoomData(paramF, paramHF, "", ""); //ZD 102358

        GetCalendarXML();
		//ZD 102358
        String inXML = "<GetTopTiers>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID></GetTopTiers>";
        String xmlstr = obj.CallMyVRMServer("GetTopTiers", inXML, Application["MyVRMServer_ConfigPath"].ToString());

        if (xmlstr.IndexOf("<error>") < 0)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xmlstr);
            //ZD 101175 Starts
            if (xmldoc.SelectSingleNode("//GetTopTiers/TopTier") != null)
            {
                //ZD 102358 START
                try
                {
                    XmlNodeList xmlnodes = null;
                    DataTable dtTiers = new DataTable();
                    ArrayList colNames = new ArrayList();
                    colNames.Add("ID");
                    colNames.Add("ParentID");
                    colNames.Add("Name");
                    xmlnodes = xmldoc.SelectNodes("//GetTopTiers/TopTier");
                    dtTiers = obj.LoadDataTable(xmlnodes, colNames);
                    dtTiers.Columns.Add("ParentID", typeof(string));

                    //DataTable dtTiers2 = new DataTable();
                    //dtTiers2.Columns.Add("ID", typeof(string));
                    //dtTiers2.Columns.Add("ParentID", typeof(string));
                    //dtTiers2.Columns.Add("Name", typeof(string));
                    //DataRow newRow2;

                    for (int i = 0; i < dtTiers.Rows.Count; i++)
                    {
                        dtTiers.Rows[i]["ParentID"] = "0";

                        //newRow2 = dtTiers2.NewRow();
                        //newRow2["ID"] = "A" + i.ToString();
                        //newRow2["ParentID"] = dtTiers.Rows[i]["ID"];
                        //newRow2["Name"] = "";
                        //dtTiers2.Rows.Add(newRow2);
                    }

                    //dtTiers.Merge(dtTiers2);

                    DataRow newRow = dtTiers.NewRow();
                    newRow["ID"] = "0";
                    newRow["ParentID"] = "-1";
                    newRow["Name"] = obj.GetTranslatedText("All");
                    dtTiers.Rows.Add(newRow);

                    PopupRoomPanel.Style.Add("display", "block");
                    pnlLevelView.Visible = true;
                    pnlListView.Visible = false;
                    visibletree = true;
                    Session["dtTiers"] = dtTiers;
                }
                catch (Exception ex)
                {

                }
                //ZD 102358 END

                //int.TryParse(xmldoc.SelectSingleNode("//locationList/ttlRoomCnt").InnerText.Trim(), out ttlROom); //ZD 102044
                //if (ttlROom <= 150)
                //{
                    //PopupRoomPanel.Style.Add("display", "block");
                    //XmlNodeList nodes = xmldoc.DocumentElement.SelectNodes("//GetTopTiers/TopTier");
                    //lstRoomSelection.Items.Clear();
                    //treeRoomSelection.Nodes.Clear();
                    //TreeNode tn = new TreeNode(obj.GetTranslatedText("All"));
                    //tn.SelectAction = TreeNodeSelectAction.None;
                    //tn.Expanded = true;
                    //treeRoomSelection.Nodes.Add(tn);
                    //GetLocationList(nodes);
                    //visibletree = true;
                    //Enabletreeview = true;
                //}
                //else //ZD 102044 Starts
                //{
                //    PopupRoomPanel.Style.Add("display", "none");
                //    XmlNodeList nodes = xmldoc.DocumentElement.SelectNodes("//locationList/level3List/level3");
                //    lstRoomSelection.Items.Clear();
                //    treeRoomSelection.Nodes.Clear();
                //    TreeNode tn = new TreeNode(obj.GetTranslatedText("All"));
                //    tn.SelectAction = TreeNodeSelectAction.None;
                //    tn.Expanded = true;
                //    treeRoomSelection.Nodes.Add(tn);
                //    GetLocationList(nodes);
                //    visibletree = true;
                //    visibletree = false;
                //} //ZD 102044 Ends
            }
        }

        rdSelView_SelectedIndexChanged(new object(), new EventArgs());
        pnlListView.Visible = false;

        //if (treeRoomSelection != null)
        //{
        //    if (treeRoomSelection.Nodes.Count == 0)
        //    {
        //        rdSelView.Enabled = false;
        //        pnlListView.Visible = false;
        //        pnlLevelView.Visible = false;
        //        btnCompare.Disabled = true;
        //        pnlNoData.Visible = true;
        //    }
        //    else if (treeRoomSelection.Nodes.Count > 0)
        //    {
        //        rdSelView.Enabled = true;
        //        rdSelView_SelectedIndexChanged(new object(), new EventArgs());
        //        btnCompare.Disabled = false;
        //        pnlNoData.Visible = false;
        //    }
        //}

        /*
        if (visibletree)
        {
            int countLeaf = 0;
            int countMid = 0;
            int countTop = 0;
            if (!Session["roomExpandLevel"].ToString().Equals("list"))//Organization Module Fixes
            {
                foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                {
                    countMid = 0;
                    if (Session["roomExpandLevel"] != null)//Organization Module Fixes
                    {
                        if (Session["roomExpandLevel"].ToString() != "")//Organization Module Fixes
                        {
                            if (Int32.Parse(Session["roomExpandLevel"].ToString()) >= 2)//Organization Module Fixes
                                tnTop.Expanded = true;
                        }
                    }
                    else
                        tnTop.Expanded = false;

                    foreach (TreeNode tnMid in tnTop.ChildNodes)
                    {
                        if (Session["roomExpandLevel"] != null)//Organization Module Fixes
                        {
                            if (Session["roomExpandLevel"].ToString() != "")//Organization Module Fixes
                            {
                                if (Int32.Parse(Session["roomExpandLevel"].ToString()) >= 3)//Organization Module Fixes
                                    tnMid.Expanded = true; // fogbugz case 277
                            }
                        }
                        else
                            tnMid.Expanded = false;
                        countLeaf = 0;
                        foreach (TreeNode tn in tnMid.ChildNodes)
                        {
                            for (int i = 0; i < selectedList.Value.Trim().Split(',').Length - 1; i++)
                                if (tn.Depth.Equals(3) && tn.Value.Equals(selectedList.Value.Split(',')[i].Trim()))
                                {
                                    tn.Checked = true;
                                    countLeaf++;
                                }
                        }
                        if (countLeaf.Equals(tnMid.ChildNodes.Count))
                        {
                            tnMid.Checked = true;
                            countMid++;
                        }
                    }
                    if (countMid.Equals(tnTop.ChildNodes.Count))
                    {
                        tnTop.Checked = true;
                        countTop++;
                    }
                }
                if (countTop.Equals(treeRoomSelection.Nodes[0].ChildNodes.Count))
                    treeRoomSelection.Nodes[0].Checked = true;

                //Code added for Search Room Error - start
                if (lstRoomSelection != null)
                {
                    if (lstRoomSelection.Items.Count == 0)
                    {
                        rdSelView.Enabled = false;
                        pnlListView.Visible = false;
                        pnlLevelView.Visible = false;
                        btnCompare.Disabled = true;
                        pnlNoData.Visible = true;
                    }
                    else if (lstRoomSelection.Items.Count > 0)//FB 1481 - Start
                    {
                        rdSelView.Enabled = true;
                        rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                        btnCompare.Disabled = false;
                        pnlNoData.Visible = false;
                    }//FB 1481 - End
                }
            }
        }
        //ZD 101175 Ends
        */
//        XmlDoc = null; //FB 2450
    }
    #endregion

    #region Get calendar XML from the Session

    private void GetCalendarXML()
    {
        int index;
        index = 1;

        string errorMessage = "";

        if (Session["outXML"] != null)
            xmlstr = Session["outXML"].ToString();

        if (paramHF == "1")
        {
            if (Session["calXML"] != null)
                xmlstr = Session["calXML"].ToString();
        }

        if (Session["errMsg"] != null)
        {
            if (Session["errMsg"].ToString() != "")
            {
                if (xmlstr.IndexOf("<error>") > 0)
                {
                    Response.Write("<br><br><p align='center'><font size=4><b>" + xmlstr + "<b></font></p>");
                    Response.End();
                }
            }
        }
    }
    #endregion

    #region GetLocationList

    protected void GetLocationList(XmlNodeList nodes)
    {
        try
        {
            ListItem li = new ListItem(obj.GetTranslatedText("Please select...."), "0");//FB 2272
            int nodes2Count = 0;
            int length = nodes.Count;
            for (int i = 0; i < length; i++)
            {
                TreeNode tn3 = new TreeNode(nodes.Item(i).SelectSingleNode("Name").InnerText, nodes.Item(i).SelectSingleNode("ID").InnerText + "_T");
                tn3.SelectAction = TreeNodeSelectAction.None;
                treeRoomSelection.Nodes[0].ChildNodes.Add(tn3);
                tn3.Expanded = false;

                TreeNode tn2 = new TreeNode("", "");
                tn2.SelectAction = TreeNodeSelectAction.None;
                treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes.Add(tn2);
                tn2.Expanded = false; 
            }
                /*
                XmlNodeList nodes2 = nodes.Item(i).SelectNodes("level2List/level2");
                int length2 = nodes2.Count;
                if (!Session["roomExpandLevel"].ToString().ToLower().Equals("list"))//Organization Module Fixes
                    if (Session["roomExpandLevel"] != null)//Location Issues//Organization Module Fixes
                    {
                        if (Session["roomExpandLevel"].ToString() != "")//Organization Module Fixes
                        {
                            if (Int32.Parse(Session["roomExpandLevel"].ToString()) >= 2)//Organization Module Fixes
                                tn3.Expanded = true;
                        }
                    }
                    else
                        tn3.Expanded = false;
               

                int mid = 0; //FB 1802
                for (int j = 0; j < length2; j++)
                {
                    TreeNode tn2 = new TreeNode(nodes2.Item(j).SelectSingleNode("level2Name").InnerText, nodes2.Item(j).SelectSingleNode("level2ID").InnerText);

                    tn2.SelectAction = TreeNodeSelectAction.None;
                    treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes.Add(tn2);
                    XmlNodeList nodes1 = nodes2.Item(j).SelectNodes("level1List/level1");
                    int length1 = nodes1.Count;
                    tn2.Expanded = true; // fogbugz case 277
                    nodes2Count = 0;
                    for (int k = 0; k < length1; k++)
                    {
                        if (nodes1.Item(k).SelectSingleNode("deleted").InnerText.Trim() == "1") //Room search issue - Deactivated rooms get displayed in Treeview
                            continue;

                        if (nodes1.Item(k).SelectSingleNode("ExternalRoom") != null) //FB 2426
                            if (nodes1.Item(k).SelectSingleNode("ExternalRoom").InnerText.Trim() == "1")
                                continue;

                        TreeNode tn = new TreeNode(nodes1.Item(k).SelectSingleNode("level1Name").InnerText, nodes1.Item(k).SelectSingleNode("level1ID").InnerText);
                        tn.SelectAction = TreeNodeSelectAction.None;
                        tn.ToolTip = nodes1.Item(k).SelectSingleNode("level1Name").InnerText + " " + obj.GetTranslatedText("Click to show room resources");
                        tn.Value = nodes1.Item(k).SelectSingleNode("level1ID").InnerText;
                        treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes[mid].ChildNodes.Add(tn);//FB 1802
                        //FB 1911
                        //tn.NavigateUrl = @"javascript:chkresource('" + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + "');";
                        //string l1Name = "<a class=\"tabtreeLeafNode\"  title='" + nodes1.Item(k).SelectSingleNode("level1Name").InnerText + " Click to show room Resource'  onclick='javascript:chkresource(\"" + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + "\");'>" + nodes1.Item(k).SelectSingleNode("level1Name").InnerText + "</a>";
                        string l1Name = "<a class=\"tabtreeLeafNode\"  title='" + nodes1.Item(k).SelectSingleNode("level1Name").InnerText + "'>" + nodes1.Item(k).SelectSingleNode("level1Name").InnerText + "</a>";
                        li = new ListItem(l1Name, nodes1.Item(k).SelectSingleNode("level1ID").InnerText);
                        lstRoomSelection.ToolTip = obj.GetTranslatedText("Click to show room resources");
                        lstRoomSelection.Items.Add(li);
                        if (selRooms.IndexOf(" " + nodes1.Item(k).SelectSingleNode("level1ID").InnerText + ",") >= 0)
                        {
                            nodes2Count++;
                            tn.Checked = true;
                            li = new ListItem(tn.Text, tn.Value);
                        }
                    }

                    if (treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes[mid].ChildNodes.Count == 0) //FB 1802
                        treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes.Remove(tn2);

                    mid = treeRoomSelection.Nodes[0].ChildNodes[i].ChildNodes.Count; //FB 1802

                    //* Fogbugz case 156 to check the middle tier if all rooms are selected 
                    if (nodes1.Count.Equals(nodes2Count))
                        tn2.Checked = true; // fogbugz case 277
                    if (!Session["roomExpandLevel"].ToString().ToLower().Equals("list"))//Organization Module Fixes
                        if (Session["roomExpandLevel"] != null)//Organization Module Fixes
                        {
                            if (Session["roomExpandLevel"].ToString() != "")//Organization Module Fixes
                            {
                                if (Int32.Parse(Session["roomExpandLevel"].ToString()) >= 3)
                                    tn2.Expanded = true;
                            }
                        }
                        else
                            tn2.Expanded = false;
                }
            }
            //FB Case 1056 - Saima starts here 
            for (int i = 0; i < lstRoomSelection.Items.Count - 1; i++)
                for (int j = i + 1; j < lstRoomSelection.Items.Count; j++)
                    if (String.Compare(lstRoomSelection.Items[i].Text, lstRoomSelection.Items[i].Text.IndexOf(">"), lstRoomSelection.Items[j].Text, lstRoomSelection.Items[j].Text.IndexOf(">"), lstRoomSelection.Items[i].Text.Length, true) > 0)
                    {
                        ListItem liTemp = new ListItem(lstRoomSelection.Items[i].Text, lstRoomSelection.Items[i].Value);
                        lstRoomSelection.Items[i].Value = lstRoomSelection.Items[j].Value;
                        lstRoomSelection.Items[i].Text = lstRoomSelection.Items[j].Text;
                        log.Trace(i + " : " + lstRoomSelection.Items[i].Text.Substring(lstRoomSelection.Items[i].Text.IndexOf(">")) + " : " + lstRoomSelection.Items[i].Value);
                        lstRoomSelection.Items[j].Value = liTemp.Value;
                        lstRoomSelection.Items[j].Text = liTemp.Text;
                    }
            //FB Case 1056 - Saima ends here 

            //FB 1149 --Start
            foreach (ListItem listItem in lstRoomSelection.Items)
            {
                for (int r = 0; r < selRooms.Split(',').Length - 1; r++)
                {

                    if (listItem.Value.Equals(selRooms.Split(',')[r].Trim()))
                    {
                        listItem.Selected = true;
                    }
                }
            }
        */
        
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace(ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region ChangeCalendarDate

    protected void ChangeCalendarDate(Object sender, EventArgs e)
    {
        try
        {
			//ZD 100151
            int tabindex = 0;
            tabindex = CalendarContainer.ActiveTabIndex;
            SetCalendarTImes(); //ZD 100157
            schDaypilot.Visible = false;
            schDaypilotweek.Visible = false;
            schDaypilotMonth.Visible = false;
			//ZD 102358
            //foreach (ListItem lstItem in lstRoomSelection.Items)
            //{
            //    if (lstItem.Selected)
            //    {
            //        if (lstItem.Value != "")
            //        {
            //            if(selectedList.Value == "")
            //                selectedList.Value += lstItem.Text + ",";                            
            //        }
            //    }
            //}

            List<DevExpress.Web.ASPxTreeList.TreeListNode> lst = treeRoomSelector.GetSelectedNodes();
            string str = "";
            //selectedList.Value = "";
            for (int i = 0; i < lst.Count; i++)
            {
                if (lst[i].Level == 4)
                {
                    String strValue = lst[i].Key.Replace("R", "") + ",";
                    if (selectedList.Value.IndexOf(strValue) < 0)
                        selectedList.Value += lst[i].Key.Replace("R", "") + ",";// +lst[i].GetValue("Name").ToString() + ",,";
                }
            }

          
            //if (selectedList.Value != "")
                Enabletreeview = true;

			//ZD 100151
            if (tabindex == 0 || tabindex == 2)
            {
                String Roomsxml = HdnMonthlyXml.Value;
                if (IsMonthChanged.Value == "Y" || IsSettingsChange.Value == "Y")
                {
                    Roomsxml = GetCalendarOutXml(tabindex);
                    schDaypilotMonth.ViewType = DayPilot.Web.Ui.Enums.ViewTypeEnum.Resources;
                }

                if (Roomsxml != "")
                    DatatablefromXML(Roomsxml);

                SetCalOfficeHours(GetUserCalendarHrs());//ZD 100157
				//ZD 100151
                if (CalendarContainer.ActiveTabIndex == 2)
                    BindMonthly();
                if (CalendarContainer.ActiveTabIndex == 0)
                {
                    if(Roomsxml == "")
                        schDaypilot.Resources.Clear();
                    BindDaily();
                }
            }
            else if (CalendarContainer.ActiveTabIndex == 1)
            {
                if (IsWeekOverLap.Value == "Y" || IsSettingsChange.Value == "Y") //ZD 100151
                    BindWeeklyXml();
            }
			//ZD 102358

            //commented for lastest issue
            //if (selectedList.Value != "")
            //    selStatus.Value = "1";
            //else
            //    selStatus.Value = "0";

        
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowHideRow", "ShowHideRow();", true);               

        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region WeeklyBind

    protected void WeeklyBind(Object sender, EventArgs e)
    {


        try
        {
            BindWeeklyXml();
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region GetCalendarOutXml

    public string GetCalendarOutXml(int tabindex)//ZD 100151
    {
        String inXML = "";
        String outXML = "";
        String outXMLMonthly = "";
        String roomsselected = "";
        
        try
        {
            roomsselected = selectedList.Value;
            selectedList.Value = "";
            roomstrnames.Value = ""; //ZD 102086
            int isdeletedconf = 0; //FB 1800
            if (showDeletedConf.Checked)
                isdeletedconf = 1;
			//ZD 102358
            List<DevExpress.Web.ASPxTreeList.TreeListNode> lst = treeRoomSelector.GetSelectedNodes();

            if (Enabletreeview)
            {
                for (int i = 0; i < lst.Count; i++)
                {
                    if (lst[i].Level == 4)
                    {

                        selectedList.Value += lst[i].Key.Replace("R", "") + ",";
                        //ZD 102086
                        if (roomstrnames.Value == "")
                            roomstrnames.Value = lst[i].Key.Replace("R", "") + "|" + lst[i].GetValue("Name").ToString();
                        else
                            roomstrnames.Value += "++" + lst[i].Key.Replace("R", "") + "|" + lst[i].GetValue("Name").ToString();

                        inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room>" + lst[i].Key.Replace("R", "") + "</room><isDeletedConf>" + isdeletedconf + "</isDeletedConf></calendarView>"; //FB 1800
                        //outXMLDaliy += "!" + node.Value + "&" + node.Text + "&" + obj.CallCOM("GetRoomDailyView", inXML, Application["COM_ConfigPath"].ToString());
                        //outXMLWeekly += "!" + node.Value + "&" + node.Text + "&" + obj.CallCOM("GetRoomWeeklyView", inXML, Application["COM_ConfigPath"].ToString());

                        if (IsMonthChanged.Value != "Y" && IsSettingsChange.Value == "Y")
                        {
                            //if (CehckifStringContains(roomsselected.Trim(), ",", lst[i].Key.Replace("R", "")))
                            //    continue;
                            //if (CehckifStringContains(HdnMonthlyXml.Value, lst[i].Key.Replace("R", ""))) ZD 103169
                            //    continue;
                        }


                        if (IsMonthChanged.Value == "Y" || IsSettingsChange.Value == "Y")
                        {
                            //ZD 100151
                            if (tabindex == 2)
                                outXMLMonthly += "~~" + lst[i].Key.Replace("R", "") + "@@" + lst[i].GetValue("Name").ToString() + "@@"//ZD 100263
                                + obj.CallMyVRMServer("GetRoomMonthlyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027
                            else
                                outXMLMonthly += "~~" + lst[i].Key.Replace("R", "") + "@@" + lst[i].GetValue("Name").ToString() + "@@"
                                + obj.CallMyVRMServer("GetRoomDailyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027
                        }

                    }
                }
            }
            else
            {
                HdnMonthlyXml.Value = "";

                if (selectedListRoomName.Value != "")
                {
                    for (int i = 0; i < selectedListRoomName.Value.Split('�').Count(); i++)
                    {
                        inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room>" + selectedListRoomName.Value.Split('�')[i].Split('|')[0] + "</room><isDeletedConf>" + isdeletedconf + "</isDeletedConf></calendarView>"; //FB 1800
                        if (IsMonthChanged.Value == "Y" || IsSettingsChange.Value == "Y")
                        {
                            //ZD 100151
                            if (tabindex == 2)
                                outXMLMonthly += "~~" + selectedListRoomName.Value.Split('�')[i].Split('|')[0] + "@@" + selectedListRoomName.Value.Split('�')[i].Split('|')[1] + "@@"//ZD 100263
                                + obj.CallMyVRMServer("GetRoomMonthlyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027
                            else
                                outXMLMonthly += "~~" + selectedListRoomName.Value.Split('�')[i].Split('|')[0] + "@@" + selectedListRoomName.Value.Split('�')[i].Split('|')[1] + "@@"
                                + obj.CallMyVRMServer("GetRoomDailyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027
                        }

                    }
                }
            }

            /*
            if (rdSelView.SelectedValue.Equals("1"))
            {
                if (Enabletreeview)
                {
                    foreach (TreeNode node in treeRoomSelection.CheckedNodes)
                    {
                        if (node.Depth == 3)
                        {
                            if (node.Value != "")
                            {
                                selectedList.Value += node.Value + ",";
                                //ZD 102086
                                if (roomstrnames.Value == "")
                                    roomstrnames.Value = node.Value + "|" + node.Text;
                                else
                                    roomstrnames.Value += "++" + node.Value + "|" + node.Text;

                                inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room>" + node.Value + "</room><isDeletedConf>" + isdeletedconf + "</isDeletedConf></calendarView>"; //FB 1800
                                //outXMLDaliy += "!" + node.Value + "&" + node.Text + "&" + obj.CallCOM("GetRoomDailyView", inXML, Application["COM_ConfigPath"].ToString());
                                //outXMLWeekly += "!" + node.Value + "&" + node.Text + "&" + obj.CallCOM("GetRoomWeeklyView", inXML, Application["COM_ConfigPath"].ToString());

                                if (IsMonthChanged.Value != "Y" && IsSettingsChange.Value == "Y")
                                {
                                    if (CehckifStringContains(roomsselected.Trim(), ",", node.Value))
                                        continue;
                                    if (CehckifStringContains(HdnMonthlyXml.Value, node.Value))
                                        continue;
                                }


                                if (IsMonthChanged.Value == "Y" || IsSettingsChange.Value == "Y")
                                {
                                    //ZD 100151
                                    if (tabindex == 2)
                                        outXMLMonthly += "~~" + node.Value + "@@" + node.Text + "@@"//ZD 100263
                                        + obj.CallMyVRMServer("GetRoomMonthlyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027
                                    else
                                        outXMLMonthly += "~~" + node.Value + "@@" + node.Text + "@@"
                                        + obj.CallMyVRMServer("GetRoomDailyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027
                                }

                            }
                        }
                    }
                }
                else
                {
                    HdnMonthlyXml.Value = "";

                    if (selectedListRoomName.Value != "")
                    {
                        for (int i = 0; i < selectedListRoomName.Value.Split('�').Count(); i++)
                        {
                            inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room>" + selectedListRoomName.Value.Split('�')[i].Split('|')[0] + "</room><isDeletedConf>" + isdeletedconf + "</isDeletedConf></calendarView>"; //FB 1800
                            if (IsMonthChanged.Value == "Y" || IsSettingsChange.Value == "Y")
                            {
                                //ZD 100151
                                if (tabindex == 2)
                                    outXMLMonthly += "~~" + selectedListRoomName.Value.Split('�')[i].Split('|')[0] + "@@" + selectedListRoomName.Value.Split('�')[i].Split('|')[1] + "@@"//ZD 100263
                                    + obj.CallMyVRMServer("GetRoomMonthlyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027
                                else
                                    outXMLMonthly += "~~" + selectedListRoomName.Value.Split('�')[i].Split('|')[0] + "@@" + selectedListRoomName.Value.Split('�')[i].Split('|')[1] + "@@"
                                    + obj.CallMyVRMServer("GetRoomDailyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027
                            }

                        }
                    }
                }

            }
            else if (rdSelView.SelectedValue.Equals("2"))
            {
                if (Enabletreeview)
                {
                    foreach (ListItem lstItem in lstRoomSelection.Items)
                    {
                        if (lstItem.Selected)
                        {
                            if (lstItem.Value != "")
                            {
                                selectedList.Value += lstItem.Value + ",";

                                String txtList = Regex.Replace(lstItem.Text, "</?(a|A).*?>", ""); //ZD 102357

                                //ZD 102086
                                if (roomstrnames.Value == "")
                                    roomstrnames.Value = lstItem.Value + "|" + txtList;
                                else
                                    roomstrnames.Value += "++" + lstItem.Value + "|" + txtList;

                                inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room>" + lstItem.Value + "</room><isDeletedConf>" + isdeletedconf + "</isDeletedConf></calendarView>"; //FB 1800
                                //outXMLDaliy += "!" + lstItem.Value + "&" + lstItem.Text.Trim().Split('>')[1].Split('<')[0] + "&" + obj.CallCOM("GetRoomDailyView", inXML, Application["COM_ConfigPath"].ToString());
                                //outXMLWeekly += "!" + lstItem.Value + "&" + lstItem.Text.Trim().Split('>')[1].Split('<')[0] + "&" + obj.CallCOM("GetRoomWeeklyView", inXML, Application["COM_ConfigPath"].ToString());

                                if (IsMonthChanged.Value != "Y" && IsSettingsChange.Value == "Y")
                                {
                                    if (CehckifStringContains(roomsselected.Trim(), ",", lstItem.Value))
                                        continue;

                                    if (CehckifStringContains(HdnMonthlyXml.Value, lstItem.Value))
                                        continue;
                                }

                                if (IsMonthChanged.Value == "Y" || IsSettingsChange.Value == "Y")
                                {
                                    //ZD 100151
                                    if (tabindex == 2)
                                        outXMLMonthly += "~~" + lstItem.Value + "@@" + lstItem.Text.Trim().Split('>')[1].Split('<')[0] + "@@"
                                        + obj.CallMyVRMServer("GetRoomMonthlyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027
                                    else
                                        outXMLMonthly += "~~" + lstItem.Value + "@@" + lstItem.Text.Trim().Split('>')[1].Split('<')[0] + "@@"
                                            + obj.CallMyVRMServer("GetRoomDailyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027
                                }

                            }
                        }

                    }
                }
                else
                {
                    HdnMonthlyXml.Value = "";
                }

            }
            */

            Session["RoomStrValue"] = roomstrnames.Value; //ZD 102086

            //if (IsMonthChanged.Value.Trim() == "" && IsSettingsChange.Value == "Y") ZD 103169
            //    outXMLMonthly += HdnMonthlyXml.Value;

            if (outXMLMonthly != "")
                HdnMonthlyXml.Value = outXMLMonthly;

            outXML = HdnMonthlyXml.Value;//FB 1779
            IsSettingsChange.Value = "";

            return outXML;
        }
        catch (Exception ex)
        {
            //errLabel.Text = ex.StackTrace;
            errLabel.Text  = obj.ShowSystemMessage();
            errLabel.Visible = true;
            log.Trace(ex.StackTrace + "GetCalendarOutXml: Error Retrieving Data" + ex.Message);
            return obj.ShowSystemMessage();//FB 1881
            //return "<error><level>E</level><message>Error Retrieving Data</message></error>";
        }
    }

    #endregion

    #region BeforeEventRenderhandler

    protected void BeforeEventRenderhandler(object sender, DayPilot.Web.Ui.Events.BeforeEventRenderEventArgs e)
    {
        try
        {            
            string EnableRoomCalendarView = Session["EnableRoomCalendarView"].ToString();
            if (EnableRoomCalendarView != "1" || e.Tag["owner"] == Session["userID"].ToString() || e.Tag["requestor"] == Session["userID"].ToString())//Partial / Public Room Calendar View //ZD 100963 //ZD 102832
            {
                e.InnerHTML = "";
                switch (e.Tag[0])
                {
                    case ns_MyVRMNet.vrmConfType.AudioOnly: e.BackgroundColor = "#EAA2D4"; ; break;
                    case ns_MyVRMNet.vrmConfType.AudioVideo: e.BackgroundColor = "#BBB4FF"; ; break;
                    case ns_MyVRMNet.vrmConfType.P2P: e.BackgroundColor = "#85EE99"; ; break;
                    case ns_MyVRMNet.vrmConfType.RoomOnly: e.BackgroundColor = "#F16855"; ; break;
                    case ns_MyVRMNet.vrmConfType.HotDesking: e.BackgroundColor = "#cc0033"; ; break;//FB 2694
                    case "S": e.BackgroundColor = "#FFCC99"; break;
                    case "T": e.BackgroundColor = "#CCCC99"; break;
                    case "PreStartEnd": e.BackgroundColor = "#FFFF00"; break; //ZD 100085
                    case "9": e.BackgroundColor = "#01DFD7"; ; break;
                    case "10": e.BackgroundColor = "#82CAFF"; ; break; //FB 2448

                }
            }
            else//Private Room Calendar View- ZD 100963
            {
                e.InnerHTML = "";
                e.BackgroundColor = "#00FFFF";

                switch (e.Tag[0])
                {
                    
                    case "S": e.InnerHTML = ""; break;
                    case "T": e.InnerHTML = ""; break;
                    case "PreStartEnd": e.InnerHTML = ""; break; 
                    case "9": e.InnerHTML = ""; ; break;
                    case "10": e.InnerHTML = ""; ; break; 

                }
            }

        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("BeforeEventRenderhandler" + ex.Message);

        }
    }

    #endregion
    //FB 2588 START
    #region BeforeTimeHeaderRender
    protected void BeforeTimeHeaderRenderDaily(Object sender, DayPilot.Web.Ui.Events.BeforeTimeHeaderRenderEventArgs e)
    {
        try
        {
            
            //ZD 100284 - Start
            DayPilot.Web.Ui.Events.BeforeTimeHeaderRenderEventArgs DateEvent = (DayPilot.Web.Ui.Events.BeforeTimeHeaderRenderEventArgs)e;
            
            string str0 = schDaypilot.BusinessEndsHour.ToString();
            string[] str1 = DateEvent.Start.ToString().Split(' ');
            string[] str2 = str1[1].Split(':');
            if (str2[0] == "11" && str1[2] == "PM" && str0 == "24")
                DateEvent.Visible = true;
            //ZD 100284 - End
            
            if (Session["timeFormat"].ToString().Equals("2"))
            {
                string dt;
                string[] date_arr;
                //DayPilot.Web.Ui.Events.BeforeTimeHeaderRenderEventArgs DateEvent = (DayPilot.Web.Ui.Events.BeforeTimeHeaderRenderEventArgs)e;

                if (e.IsColGroup)
                {
                    dt = DateEvent.InnerHTML;
                    date_arr = dt.Split(' ');
                    if (date_arr[1] == "AM")
                    {

                        if (date_arr[0].Length == 1)
                            date_arr[0] = "0" + date_arr[0];
                        
                        if (date_arr[0] == "12")
                            date_arr[0] = "0000Z";
                        else
                            date_arr[0] = date_arr[0] + "00Z";

                    }
                    else if (date_arr[1] == "PM")
                    {
                        if (date_arr[0].Trim() == "12")
                            date_arr[0] = "1200Z";
                        else
                            date_arr[0] = (Int32.Parse(date_arr[0]) + 12).ToString() + "00Z";
                    }
                    DateEvent.InnerHTML = date_arr[0];
                    
                }
            }
            
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("BeforeTimeHeaderRenderDaily" + ex.StackTrace);
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();
        }
    }
    #endregion
    //FB 2588 END
    //FB 1851 - Start
    #region BeforeTimeHeaderRender
    protected void BeforeTimeHeaderRender(Object sender, DayPilot.Web.Ui.Events.BeforeTimeHeaderRenderEventArgs e)
    {
        try
        {
            string dt, wk, mon;
            string[] date_arr;
            DayPilot.Web.Ui.Events.BeforeTimeHeaderRenderEventArgs DateEvent = (DayPilot.Web.Ui.Events.BeforeTimeHeaderRenderEventArgs)e;

            if (!e.IsColGroup)
            {
                if (dayCounter < 7)
                    DateEvent.InnerHTML = obj.GetTranslatedText(String.Format(e.Start.DayOfWeek.ToString())) + "<br />" + String.Format(myVRMNet.NETFunctions.GetFormattedDate(e.Start.ToShortDateString()));//ZD 100995
                else
                    DateEvent.InnerHTML = obj.GetTranslatedText(String.Format(e.Start.DayOfWeek.ToString().Substring(0, 3).ToString())) + "<br />" + String.Format(e.Start.Day.ToString());

                dayCounter += 1;
            }
            else
            {
                dt = DateEvent.InnerHTML;
                date_arr = dt.Split(' ');
                if (date_arr.Length > 2)
                {
                    wk = dt.Split(' ')[0];
                    mon = dt.Split('(')[1].Split(' ')[0];
                    dt = dt.Replace(wk, obj.GetTranslatedText(wk));
                    dt = dt.Replace(mon, obj.GetTranslatedText(mon));
                    DateEvent.InnerHTML = dt;
                    DateEvent.ToolTip = dt;
                }
                else
                {
                    mon = dt.Split(' ')[0];
                    dt = dt.Replace(mon, obj.GetTranslatedText(mon));
                    DateEvent.InnerHTML = dt;
                    DateEvent.ToolTip = dt;
                }
            }
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("BeforeTimeHeaderRender" + ex.StackTrace);
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();
        }
    }
    #endregion
    //FB 1851 - End

    #region BubbleRenderhandler
    protected void BubbleRenderhandler(object sender, DayPilot.Web.Ui.Events.Bubble.RenderEventArgs e)
    {
        try
        {
            DayPilot.Web.Ui.Events.Bubble.RenderEventBubbleEventArgs re = (DayPilot.Web.Ui.Events.Bubble.RenderEventBubbleEventArgs)e;
            // re.InnerHTML = re.Text;
            //re.InnerHTML = re.Tag["CustomDescription"];

            //ZD 100151
            StringBuilder m = new StringBuilder();
            
            //ZD 100963 START
            string EnableRoomCalendarView = Session["EnableRoomCalendarView"].ToString();
            if (EnableRoomCalendarView == "3" || re.Tag["owner"] == Session["userID"].ToString() || re.Tag["requestor"] == Session["userID"].ToString()) // Public //ZD 102832
            {
                m.Append("<table cellspacing='0' cellpadding='0' border='0' width='300px' class='promptbox' bgColor = '#ccccff'>");
                m.Append("<tr valign='middle'>");
                m.Append("<td width='100%' height='22' style='text-indent:2;' class='titlebar' align='left' colspan='2' bgColor = '#9999ff'>");
                m.Append("<img src='image/pen.gif' height='18' width='18'>&nbsp;&nbsp;" + re.Tag["Heading"]);
                m.Append("</td>");
                m.Append("</tr>");
                m.Append("</table>");
                m.Append("<div style='width: 300px; height: 140px; overflow: auto;'>");
                m.Append("<table cellspacing='0' cellpadding='0' border='0' class='promptbox' bgColor = '#ccccff'>");
                m.Append("<tr>");
                //ZD 100085- Starts
                m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Name") + ": </span></td>");
                m.Append("<td width='200px'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["confName"] + "</span></td>");
                m.Append("</tr>");
                m.Append("<tr>");
                m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Unique ID") + ": </span></td>"); // FB 2002
                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["ID"] + "</span></td>");
                m.Append("</tr>");
                //ZD 100421
                m.Append("<tr>");
                m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'> " + obj.GetTranslatedText("Conf.Type") + ": </span></td>"); // FB 2002
                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText(re.Tag["ConferenceTypeName"]) + "</span></td>");
                m.Append("</tr>");
                //ZD 100421
                if (re.Tag["Setup"] != "")
                {
                    m.Append("  <tr>");
                    m.Append("    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Setup") + ": </span></td>");//ZD 100421
                    m.Append("    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["Setup"] + "</span></td>");
                    m.Append("  </tr>");
                }
                if (re.Tag["MCUPreStart"] != "")
                {
                    m.Append("<tr>");
                    m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Pre Start") + ": </span></td>");
                    m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["MCUPreStart"] + " </span></td>");
                    m.Append("</tr>");
                }
                m.Append("<tr>");
                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Start - End") + ": </span></td>");
                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["ConfTime"] + "</span></td>");
                m.Append("</tr>");
                if (re.Tag["MCUPreEnd"] != "")
                {
                    m.Append("  <tr>");
                    m.Append("    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Pre End") + ": </span></td>");
                    m.Append("    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["MCUPreEnd"] + "</span></td>");
                    m.Append("  </tr>");
                }
                if (re.Tag["Tear"] != "")
                {
                    m.Append("  <tr>");
                    m.Append("    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("TearDown") + ": </span></td>");//ZD 100421
                    m.Append("    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["Tear"] + "</span></td>");
                    m.Append("  </tr>");
                }
                m.Append("<tr>");
                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Duration") + ": </span></td>");
                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["Hours"] + " hrs " + re.Tag["Minutes"] + obj.GetTranslatedText(" mins") + "</span></td>"); //ZD 100528
                m.Append("</tr>");
                m.Append("<tr>");
                m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Location") + ": </span></td>");
                m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["Location"] + "</span></td>");
                m.Append("</tr>");
                if (re.Tag["ConfSupport"] == "" && re.Tag["CustomOptions"] == "")
                {
                    m.Append("<tr>");//ZD 100433
                    m.Append("<td valign='top' style='width:90px;'><span style='font-size: 8pt; font-weight: bold; color: #990033'></span></td>");
                    m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'></span></td>");
                    m.Append("</tr>");
                }
                if (re.Tag["ConfSupport"] != "")
                {
                    m.Append("<tr>");
                    m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Conference Support") + ": </span></td>"); //FB 3023
                    m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["ConfSupport"] + "</span></td>");
                    m.Append("</tr>");
                }

                if (re.Tag["CustomOptions"] != "")
                {
                    m.Append("<tr>");
                    m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Custom Options") + ": </span></td>");
                    m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + re.Tag["CustomOptions"] + "</span></td>");
                    m.Append("</tr>");
                }
                m.Append("</table>");
                m.Append("</div>");

                re.InnerHTML = m.ToString();
                //ZD 100085- End
            }
        }
        //ZD 100963 END
        
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("BubbleRenderhandler" + ex.StackTrace);
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();
        }
    }
    #endregion

    #region ChangeBusinessHour

    protected void ChangeBusinessHour(Object sender, EventArgs e)
    {


        try
        {

            SetCalendarTImes(); //ZD 100157
            SetCalOfficeHours(GetUserCalendarHrs());//ZD 100157
            BindMonthly();
            BindDaily();

        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region ChangeSettings

    protected void ChangeSettings(Object sender, EventArgs e)
    {
        try
        {
            ChangeCalendarDate(null, null);

        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region FromRoomSearch

    protected void FromRoomSearch(Object sender, EventArgs e)
    {
        try
        {
           
            IsSettingsChange.Value = "Y";
            IsMonthChanged.Value = "";
            if (treeRoomSelection.Nodes.Count > 0)
            {
                Enabletreeview = true;
                rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                PanelTab.Update();
                selectedList.Value = "";
                //HdnMonthlyXml.Value = "";
                ChangeCalendarDate(null, null);
                
            }
            else
            {
                if (selectedList.Value != "")
                {
                    selStatus.Value = "1";
                    selectedList.Value = selectedList.Value + ",";
                }
                else
                    selStatus.Value = "0";
                Enabletreeview = false;
                //ZD 102358
                if (selectedList.Value.Trim() != "" || selectedTierList.Value.Trim() != "")
                {
                    selectRoomByValue(selectedTierList.Value);
                }

                ChangeCalendarDate(null, null);
                PanelTab.Update();
            }
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region Datatable from XML

    protected void DatatablefromXML(String Roomsxml)
    {
        String hdr = obj.GetTranslatedText("Conference Details"), startHour = "00", startMin = "00", startSet = "AM", endHour = "23", endMin = "59", endSet = "PM", open24 = "";
        string CongAtt = "", CongAttStatus = obj.GetTranslatedText("No"), DedicatedVNOCOperator = obj.GetTranslatedText("Dedicated VNOC Operator");//FB 2632
        string OnSiteAVSupport = obj.GetTranslatedText("On-Site A/V Support"), MeetandGreet = obj.GetTranslatedText("Meet and Greet"), ConciergeMonitoring = obj.GetTranslatedText("Call Monitoring");//FB 3023

        int hrs, mins = 0, hasRights = 0; //ZD 101942 


        String locStr = "";
        String setupTxt = "";
        String trdnTxt = "";
        XmlNode node = null;
        XmlNodeList nodes = null;
        XmlDocument xmldoc = null;
        DataTable dt = null;
        DataTable dtDaily = null;
        StringBuilder m = null;
        XmlNodeList subnotes2 = null;
        XmlNode subnode2 = null;
        Boolean Blnsetup = false, BlnTear = false;
        string partyid = "0"; //FB 1659
        string ownerid = "0"; //FB 1659
        bool isParty = false; //FB 1659
        bool isOwner = false; //FB 1659
        isPublic = "0"; //FB 1659
        try
        {

            if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))//Code added for Phase2
                hdr = "Hearing Details";

            String spn = "<span  class=\"eventtext\">";
            if (Roomsxml != "")
                {


                    dt = GetDataTable();
                    dtDaily = GetDataTable();
                    schDaypilot.Resources.Clear();
                    schDaypilotweek.Resources.Clear();
                    schDaypilotMonth.Resources.Clear();

                    //FB 1779
                    String[] strSplit = new String[1] { "~~" }; //FB 2012//ZD 100151//ZD 100263

                    foreach (String xmls in Roomsxml.Split(strSplit, StringSplitOptions.RemoveEmptyEntries))
                    {

                        if (xmls != "")
                        {
                            bypass = true;
                            if (Enabletreeview)
                            {
                                if (!CehckifStringContains(selectedList.Value.Trim(), ",", xmls.Split(strSplitat, StringSplitOptions.RemoveEmptyEntries)[0])) //FB 2012
                                    continue;
                            }

                            xmldoc = new XmlDocument();
                            xmldoc.LoadXml(xmls.Split(strSplitat, StringSplitOptions.RemoveEmptyEntries)[2]); //FB 2012
                            
                            //ZD 100157 Starts
                            if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/Room/ShowRoomHrs") != null)
                                open24 = xmldoc.DocumentElement.SelectSingleNode("systemAvail/Room/ShowRoomHrs").InnerText; //ZD 100157
                           
                            if (open24 == "1")
                            {
                                officehrDaily.Checked = true;
                                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/Room/startTime/startHour") != null)
                                    startHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/Room/startTime/startHour").InnerText;

                                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/Room/startTime/startMin") != null)
                                    startMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/Room/startTime/startMin").InnerText;

                                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/Room/startTime/startSet") != null)
                                    startSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/Room/startTime/startSet").InnerText;

                                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/Room/endTime/endHour") != null)
                                    endHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/Room/endTime/endHour").InnerText;

                                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/Room/endTime/endMin") != null)
                                    endMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/Room/endTime/endMin").InnerText;

                                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/Room/endTime/endSet") != null)
                                    endSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/Room/endTime/endSet").InnerText;

                                schDaypilot.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                                schDaypilot.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                                if (endMin != "00")//FB 2057
                                    schDaypilot.BusinessEndsHour = schDaypilot.BusinessEndsHour + 1;

                                schDaypilotweek.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                                schDaypilotweek.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));


                                if (endMin != "00")//FB 2057
                                    schDaypilotweek.BusinessEndsHour = schDaypilotweek.BusinessEndsHour + 1;

                                if (startHour == "12" && startSet == "AM" && endHour == "11" && endMin != "00" && endSet == "PM") // ZD 100284
                                    officehrDaily.Checked = false;
                            }
                            else
                            {
                                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24") != null)
                                    open24 = xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24").InnerText;
                                if (open24 == "1")
                                {
                                    officehrDaily.Checked = false;

                                    schDaypilot.BusinessBeginsHour = 0;
                                    schDaypilot.BusinessEndsHour = 24;

                                    schDaypilotweek.BusinessBeginsHour = 0;
                                    schDaypilotweek.BusinessEndsHour = 24;
                                }
                                else
                                {

                                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour") != null)
                                        startHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour").InnerText;

                                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin") != null)
                                        startMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin").InnerText;

                                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet") != null)
                                        startSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet").InnerText;

                                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour") != null)
                                        endHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour").InnerText;

                                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin") != null)
                                        endMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin").InnerText;

                                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet") != null)
                                        endSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet").InnerText;

                                    schDaypilot.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                                    schDaypilot.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                                    if (endMin != "00")//FB 2057
                                        schDaypilot.BusinessEndsHour = schDaypilot.BusinessEndsHour + 1;

                                    schDaypilotweek.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                                    schDaypilotweek.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));


                                    if (endMin != "00")//FB 2057
                                        schDaypilotweek.BusinessEndsHour = schDaypilotweek.BusinessEndsHour + 1;

                                    if (startHour == "12" && startSet == "AM" && endHour == "11" && endMin != "00" && endSet == "PM") // ZD 100284
                                        officehrDaily.Checked = false;
                                }
                            }
                            DayPilot.Web.Ui.Resource room = new DayPilot.Web.Ui.Resource(xmls.Split(strSplitat, StringSplitOptions.RemoveEmptyEntries)[1], xmls.Split(strSplitat, StringSplitOptions.RemoveEmptyEntries)[0]); //FB 2012


                            schDaypilot.Resources.Add(room);
                            schDaypilotweek.Resources.Add(room);
                            schDaypilotMonth.Resources.Add(room);

                            nodes = xmldoc.SelectNodes("//days/day/conferences/conference");

                            for (int confnodes = 0; confnodes < nodes.Count; confnodes++)// XmlNode node in nodes)
                            {
                                node = nodes[confnodes];
                                String tempFor = "", confSTime = "", Deleted = "";//ZD 100421
                                String setupSTime = "", teardownSTime = "", confSDate = "", uniqueID=""; // FB 2002
                                String Hostname = "", WorkPhone = "", CellPhone = "";//ZD 100963

                                if (node.SelectSingleNode("durationMin") != null)
                                {

                                    if (node.SelectSingleNode("durationMin").InnerText != "")
                                    {
                                        if (!showDeletedConf.Checked)
                                        {
                                            if (node.SelectSingleNode("deleted") != null)
                                            {
                                                if (node.SelectSingleNode("deleted").InnerText == "1")
                                                    continue;
                                            }
                                        }

                                       

                                        if (bypass)
                                        {
                                            DataRow dr = dt.NewRow();
                                            DataRow drDaily = dtDaily.NewRow();

                                            dr["RoomID"] = room.Value;

                                            if (node.SelectSingleNode("party") != null) //FB 1659
                                            {
                                                partyid = node.SelectSingleNode("party").InnerText.Trim();
                                            }
                                            if (node.SelectSingleNode("owner") != null) //FB 1659
                                            {
                                                ownerid = node.SelectSingleNode("owner").InnerText.Trim();
                                            }

                                            if (node.SelectSingleNode("isPublic") != null) //FB 1659
                                            {
                                                isPublic = node.SelectSingleNode("isPublic").InnerText.Trim();
                                            }

                                            if (node.SelectSingleNode("confName") != null)
                                                dr["confName"] = node.SelectSingleNode("confName").InnerText;

                                            if (Session["isVIP"] != null)
                                            {
                                                if (Session["isVIP"].ToString() == "1")
                                                {
                                                    if (node.SelectSingleNode("isVIP") != null) // FB 1864
                                                        if (node.SelectSingleNode("isVIP").InnerText == "1")
                                                            dr["confName"] = dr["confName"].ToString() + " {VIP}";
                                                }
                                            }
                                            

                                            // FB 1659 code starts ...

                                            if (partyid == Session["userID"].ToString() || node.SelectSingleNode("requestor").InnerText == Session["userID"].ToString()) //ZD 103500
                                                isParty = true;
                                            else
                                                isParty = false;

                                            if (ownerid == Session["userID"].ToString())
                                                isOwner = true;
                                            else
                                                isOwner = false;
                                            hasRights = 0;
                                            if (isAdminRole || isParty || isOwner || (isPublic == "1"))
                                            {
                                                if (isAdminRole || isOwner) //ZD 101942 //ZD103051 STARTS
                                                    hasRights = 1;

                                                int hasView = 0;
                                                int hasEdit = 0;
                                                int hasDelete = 0;
                                                int hasManage = 0;
                                                int hasClone = 0;
                                                int hasMCUInfo = 0;
                                                int hasExtendTime = 0;

                                                if (hasRights == 1)
                                                {
                                                    int confStatus = 0, filterType = 0, hasReservations = 0, hasPublic = 0;

                                                    if (node.SelectSingleNode("ConferenceStatus") != null)//ZD 100421
                                                        int.TryParse(node.SelectSingleNode("ConferenceStatus").InnerText, out confStatus);

                                                    if (Session["hasReservations"] != null && Session["hasReservations"].ToString() == "1")
                                                        hasReservations = 1;

                                                    if (Session["hasPublic"] != null && Session["hasPublic"].ToString() == "1")
                                                        hasPublic = 1;

                                                    if (hasReservations == 1 && confStatus == 0)
                                                        filterType = 3;
                                                    else if (hasPublic == 1 && confStatus == 0 && Convert.ToInt16(isPublic) == 1)
                                                        filterType = 4;
                                                    else if (confStatus == 1) //Pending
                                                        filterType = 5;
                                                    else if (confStatus == 6) //On MCU
                                                        filterType = 8;
                                                    else if (confStatus == 5) //Ongoing
                                                        filterType = 2;
                                                    //else if (confStatus == 2) //Wait List //ZD 102532
                                                    //    filterType = 11;
                                                    else if (confStatus == 3 || confStatus == 9) //Terminated or Deleted
                                                        filterType = 9;
                                                    else if (confStatus == 7) //Completed
                                                        filterType = 10;
                                                    else
                                                        filterType = 3; //if (confStatus == 0 && isPublicConf == 0)//Reservation with Puclic or ConfSupport or without both

                                                    obj.CheckConferenceRights(filterType, ref hasView, ref hasManage, ref hasExtendTime, ref hasMCUInfo, ref hasEdit, ref hasDelete, ref hasClone);
                                                }

                                                dr["confName"] = "<a href='#' onclick='javascript: ViewConfDetails(\"" + node.SelectSingleNode("confID").InnerText.Trim() + "|" + hasRights + "|" + hasEdit + "\");' title='" + obj.GetTranslatedText("Click to see more details") + "'><font style='cursor:hand;'>" + dr["confName"].ToString() + "</font></a>"; //ZD 10028 //ZD 101942 //ZD 103051
                                            }
                                            // FB 1659 code ends ..

                                            dr["durationMin"] = node.SelectSingleNode("durationMin").InnerText;
                                            hrs = Convert.ToInt32(dr["durationMin"].ToString()) / 60;
                                            mins = Convert.ToInt32(dr["durationMin"].ToString()) % 60;
                                            if (node.SelectSingleNode("confID") != null)
                                                dr["ConfID"] = node.SelectSingleNode("confID").InnerText;
                                            if (node.SelectSingleNode("ConferenceType") != null)
                                                dr["ConferenceType"] = node.SelectSingleNode("ConferenceType").InnerText;

                                            if (node.SelectSingleNode("ConferenceTypeName") != null)//ZD 100421
                                                dr["ConferenceTypeName"] = node.SelectSingleNode("ConferenceTypeName").InnerText;

                                            dr["owner"] = ownerid; //ZD 102832
                                            dr["requestor"] = node.SelectSingleNode("requestor").InnerText; //ZD 102832
                                            
                                            //FB 2448 Starts
                                            if (node.SelectSingleNode("isVMR") != null)
                                            {
												// if (node.SelectSingleNode("isVMR").InnerText == "1")
                                                if (Convert.ToInt32(node.SelectSingleNode("isVMR").InnerText) > 0) //FB 2620
                                                {
                                                    dr["ConferenceType"] = "10";
                                                    dr["ConferenceTypeName"] = obj.GetTranslatedText("Virtual Meeting Room Conference"); //ZD 100421 //ZD 100806 ZD 100806
                                                }
                                            }
                                            //FB 2448 Ends
                                            if (showDeletedConf.Checked)
                                            {
                                                if (node.SelectSingleNode("deleted") != null)
                                                {
                                                    if (node.SelectSingleNode("deleted").InnerText == "1")
                                                    {
                                                        dr["ConferenceType"] = "9";
                                                        Deleted = "- Deleted"; //ZD 100421
                                                    }
                                                }
                                            }

                                            // FB 2002 starts
                                            if (node.SelectSingleNode("uniqueID") != null)
                                            {
                                                if (node.SelectSingleNode("uniqueID").InnerText != "")
                                                {
                                                    uniqueID = node.SelectSingleNode("uniqueID").InnerText;
                                                }
                                            }
                                            // FB 2002 ends
                                            dr["ID"] = uniqueID;// dr["ConfID"].ToString(); //ZD 100151

                                            //ZD 100963 START
                                            if (node.SelectSingleNode("Hostname") != null)
                                            {
                                                if (node.SelectSingleNode("Hostname").InnerText != "")
                                                {
                                                    Hostname = node.SelectSingleNode("Hostname").InnerText;
                                                }
                                            }

                                            if (node.SelectSingleNode("WorkPhone") != null)
                                            {
                                                if (node.SelectSingleNode("WorkPhone").InnerText != "")
                                                {
                                                    WorkPhone = node.SelectSingleNode("WorkPhone").InnerText;
                                                }
                                            }

                                            if (node.SelectSingleNode("CellPhone") != null)
                                            {
                                                if (node.SelectSingleNode("CellPhone").InnerText != "")
                                                {
                                                    CellPhone = node.SelectSingleNode("CellPhone").InnerText;
                                                }
                                            }
                                            //ZD 100963 END

                                            if (node.SelectSingleNode("confTime") != null)
                                                confSTime = node.SelectSingleNode("confTime").InnerText;

                                            if (node.SelectSingleNode("setupTime") != null)
                                                setupSTime = node.SelectSingleNode("setupTime").InnerText;

                                            if (node.SelectSingleNode("teardownTime") != null)
                                                teardownSTime = node.SelectSingleNode("teardownTime").InnerText;

                                            //ZD 100085 Starts
                                            
                                            int setupDur = 0;
                                            if (node.SelectSingleNode("setupDur") != null)
                                            {
                                                if (node.SelectSingleNode("setupDur").InnerText.Trim() != "")
                                                    int.TryParse(node.SelectSingleNode("setupDur").InnerText.Trim(), out setupDur);
                                            }

                                            int teardownDur = 0;
                                            if (node.SelectSingleNode("teardownDur") != null)
                                            {
                                                if (node.SelectSingleNode("teardownDur").InnerText.Trim() != "")
                                                    int.TryParse(node.SelectSingleNode("teardownDur").InnerText.Trim(), out teardownDur);
                                            }
                                            

                                            int MCUPreStartDur = 0;
                                            if (node.SelectSingleNode("MCUPreStartDur") != null)
                                            {
                                                if (node.SelectSingleNode("MCUPreStartDur").InnerText.Trim() != "")
                                                    int.TryParse(node.SelectSingleNode("MCUPreStartDur").InnerText.Trim(), out MCUPreStartDur);
                                            }

                                            int MCUPreEndDur = 0;
                                            if (node.SelectSingleNode("MCUPreEndDur") != null)
                                            {
                                                if (node.SelectSingleNode("MCUPreEndDur").InnerText.Trim() != "")
                                                    int.TryParse(node.SelectSingleNode("MCUPreEndDur").InnerText.Trim(), out MCUPreEndDur);
                                            }
											//ZD 100085- End
                                            if (node.SelectSingleNode("confDate") != null)
                                            {
                                                if (node.SelectSingleNode("confDate").InnerText != "")
                                                {
                                                    confSDate = node.SelectSingleNode("confDate").InnerText;
                                                }
                                            }
											//ZD 100085- Starts
                                            /*int adddays = 0;
                                            if (confSTime.Trim() == "00:00 AM")
                                                adddays = 1;

                                            DateTime start = DateTime.Parse(confSDate + " " + confSTime);
                                            DateTime stUp = DateTime.Parse(confSDate + " " + setupSTime);
                                            DateTime end = start.AddMinutes(Convert.ToDouble(dr["durationMin"]));
                                            DateTime trDn = DateTime.Parse(end.ToString("MM/dd/yyyy") + " " + teardownSTime);

                                            start = start.AddDays(adddays);
                                            stUp = stUp.AddDays(adddays);
                                            end = end.AddDays(adddays);
                                            trDn = trDn.AddDays(adddays);

                                            if (adddays < 1 && stUp.ToString("hh:mm tt") == "12:00 AM" && start.ToString("hh:mm tt") != "12:00 AM") //FB 2398
                                                stUp = stUp.AddDays(1);

                                            dr["start"] = start;
                                            dr["end"] = end;

                                            dr["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(start.ToString("MM/dd/yyyy")) + " " + start.ToString(tFormats);
                                            dr["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(end.ToString("MM/dd/yyyy")) + " " + end.ToString(tFormats); ;*/

                                            DateTime confStart = DateTime.Parse(confSDate + " " + confSTime);//ZD 100085
                                            DateTime ConfEnd = confStart.AddMinutes(Convert.ToDouble(dr["durationMin"]));

                                            DateTime PreStart = DateTime.MinValue;
                                            DateTime PreEnd = DateTime.MinValue;
                                            DateTime teardownDateTime = DateTime.MinValue;

                                            PreStart = confStart.AddMinutes(-MCUPreStartDur);
                                            PreEnd = ConfEnd.AddMinutes(-MCUPreEndDur);
                                            teardownDateTime = ConfEnd.AddMinutes(teardownDur);

                                            DateTime start = DateTime.Now;
											//ZD 100433 Starts
                                            if (enableBufferZone == "1" && setupDur > MCUPreStartDur)
                                                start = confStart.AddMinutes(-setupDur);
                                            else 
                                                start = confStart.AddMinutes(-MCUPreStartDur);
                                            
                                            int mcuPreEndPostive = 0 ;
                                            if (MCUPreEndDur < 0)
                                                mcuPreEndPostive = MCUPreEndDur * -1;
											//ZD 100433 End
                                            dr["start"] = confStart;
                                            dr["end"] = ConfEnd;

                                            dr["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(confStart.ToString("MM/dd/yyyy")) + " " + confStart.ToString(tFormats);
                                            dr["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(ConfEnd.ToString("MM/dd/yyyy")) + " " + ConfEnd.ToString(tFormats);
											//ZD 100085- End
                                            subnotes2 = node.SelectNodes("mainLocation/location");

                                            if (subnotes2 != null)
                                            {
                                                for (int subnodescnt = 0; subnodescnt < subnotes2.Count; subnodescnt++)
                                                {
                                                    subnode2 = subnotes2[subnodescnt];
                                                    locStr = locStr + subnode2.SelectSingleNode("locationName").InnerText;
                                                    locStr = locStr + "<br>";
                                                }
                                            }
											//ZD 100085- Starts
                                            if (teardownDur > 0  && enableBufferZone == "1")
                                                BlnTear = true;

                                            if (setupDur > 0 && enableBufferZone == "1")
                                                Blnsetup = true;

                                            //trdnTxt = "";
                                            //if (BlnTear)//&& cnt > 1
                                            //    trdnTxt = "Tear Down :" + ConfEnd.ToString(tFormats) + " - " + teardownDateTime.ToString(tFormats) + "<br>";

                                            //setupTxt = "";
                                            //if (Blnsetup)//&& cnt > 1
                                            //    setupTxt = "Setup :" + start.ToString(tFormats) + " - " + PreStart.ToString(tFormats) + "<br>"; //FB 2961

                                            if (Blnsetup)
                                            {
                                                dr["start"] = start;
                                                dr["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(start.ToString("MM/dd/yyyy")) + " " + start.ToString(tFormats);
                                            }

                                            if (BlnTear)
                                            {
                                                dr["end"] = teardownDateTime;
                                                dr["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(teardownDateTime.ToString("MM/dd/yyyy")) + " " + teardownDateTime.ToString(tFormats);
                                            }

                                            if (enableBufferZone == "0")
                                            {
                                                Blnsetup = false;
                                                BlnTear = false;
                                                //trDn = end;
                                                //stUp = start;
                                                //trdnTxt = "";
                                                //setupTxt = "";
                                            }
											//ZD 100085- End
                                            #region
                                            //FB 2013 start
                                            String customText = "", attriName = "", attriValue = "";
                                            //if (Session["EnableEntity"].ToString() != "0") //FB 2547
                                            //{
                                                XmlNodeList customnodes = node.SelectNodes("CustomAttributesList/CustomAttribute");
                                                XmlNode customnode = null;
                                                for(int i=0;i < customnodes.Count; i++)
                                                {
                                                    customnode = customnodes[i];

                                                    if (customnode.SelectSingleNode("IncludeInCalendar") != null)
                                                        if (customnode.SelectSingleNode("IncludeInCalendar").InnerText.Trim().Equals("0"))
                                                            continue;

                                                    if (customnode.SelectSingleNode("Status") != null)
                                                    {
                                                        if (customnode.SelectSingleNode("Status").InnerText == "0")
                                                        {
                                                            attriName = "";
                                                            attriValue = "";

                                                            if (customnode.SelectSingleNode("Title") != null)
                                                                attriName = customnode.SelectSingleNode("Title").InnerText;

                                                            if (customnode.SelectSingleNode("Type") != null)
                                                            {
                                                                if (customnode.SelectSingleNode("Type").InnerText == "3")
                                                                {
                                                                    if (customnode.SelectSingleNode("SelectedValue") != null)
                                                                    {
                                                                        if (customnode.SelectSingleNode("SelectedValue").InnerText == "1")
                                                                            attriValue = obj.GetTranslatedText("Yes");
                                                                        else
                                                                            attriValue = obj.GetTranslatedText("No");
                                                                    }
                                                                }
                                                                else if (customnode.SelectSingleNode("Type").InnerText == "2")//FB 2377
                                                                {
                                                                    if (customnode.SelectSingleNode("SelectedValue") != null)
                                                                    {
                                                                        if (customnode.SelectSingleNode("SelectedValue").InnerText == "1")
                                                                            attriValue = obj.GetTranslatedText("Yes");
                                                                        else
                                                                            attriValue = obj.GetTranslatedText("No");
                                                                    }
                                                                }
                                                                else
                                                                {

                                                                    if (customnode.SelectSingleNode("SelectedValue") != null)
                                                                        attriValue = customnode.SelectSingleNode("SelectedValue").InnerText;
                                                                }
                                                            }

                                                            XmlNodeList optNodes = customnode.SelectNodes("OptionList/Option");

                                                            if (customnode.SelectSingleNode("Type").InnerText == "5" || customnode.SelectSingleNode("Type").InnerText == "6") //FB 1718
                                                            {
                                                                if (optNodes != null)
                                                                {
                                                                    XmlNode optNode = null;
                                                                    for(int j=0;j <  optNodes.Count; j++)
                                                                    {
                                                                        optNode = optNodes[j];
                                                                        if (optNode.SelectSingleNode("Selected") != null)
                                                                        {
                                                                            if (optNode.SelectSingleNode("Selected").InnerText == "1")
                                                                            {
                                                                                if (optNode.SelectSingleNode("DisplayCaption") != null)
                                                                                {
                                                                                    if (attriValue == "")
                                                                                        attriValue = optNode.SelectSingleNode("DisplayCaption").InnerText;
                                                                                    else
                                                                                        attriValue += "," + optNode.SelectSingleNode("DisplayCaption").InnerText;
                                                                                }

                                                                            }


                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        if (attriValue == "")
                                                            attriValue = obj.GetTranslatedText("N/A");

                                                        customText += attriName + " - " + attriValue + "<br>";
                                                    }
                                                }

                                            //}
                                            //FB 2013 end
                                            // FB 2632 Starts //FB 2670 START //FB 3007 START
                                            string EnableOnsiteAV = Session["EnableOnsiteAV"].ToString();
                                            string EnableMeetandGreet = Session["EnableMeetandGreet"].ToString();
                                            string EnableConciergeMonitoring = Session["EnableConciergeMonitoring"].ToString();
                                            string EnableDedicatedVNOC = Session["EnableDedicatedVNOC"].ToString();
                                            CongAtt = "";
                                            if (EnableOnsiteAV == "1" && dr["ConferenceType"].ToString() != "8")
                                            {
                                               
                                                CongAttStatus = obj.GetTranslatedText("No");
                                                if (node.SelectSingleNode("OnSiteAVSupport") != null)
                                                {
                                                    if (node.SelectSingleNode("OnSiteAVSupport").InnerText.Equals("1"))
                                                        CongAttStatus = obj.GetTranslatedText("Yes");
                                                }
                                                CongAtt = OnSiteAVSupport + " - " + CongAttStatus + "</br>";
                                            }
                                            if (EnableMeetandGreet == "1" && dr["ConferenceType"].ToString() != "8")
                                            {
                                                CongAttStatus = obj.GetTranslatedText("No");
                                                if (node.SelectSingleNode("MeetandGreet") != null)
                                                {
                                                    if (node.SelectSingleNode("MeetandGreet").InnerText.Equals("1"))
                                                        CongAttStatus = obj.GetTranslatedText("Yes");
                                                }
                                                CongAtt += MeetandGreet + " - " + CongAttStatus + "</br>";
                                            }
                                            if (EnableConciergeMonitoring == "1" && dr["ConferenceType"].ToString() != "8")
                                            {

                                                CongAttStatus = obj.GetTranslatedText("No");
                                                if (node.SelectSingleNode("ConciergeMonitoring") != null)
                                                {
                                                    if (node.SelectSingleNode("ConciergeMonitoring").InnerText.Equals("1"))
                                                        CongAttStatus = obj.GetTranslatedText("Yes");
                                                }
                                                CongAtt += ConciergeMonitoring + " - " + CongAttStatus + "</br>";
                                            }

                                            
                                            //FB 2670T Start
                                            if (EnableDedicatedVNOC == "1" && dr["ConferenceType"].ToString() != "8")
                                            {
                                                CongAttStatus = obj.GetTranslatedText("No");
                                                XmlNodeList VNOCNodes = node.SelectNodes("ConfVNOCOperators/VNOCOperator");
                                                if (VNOCNodes != null && VNOCNodes.Count > 0)
                                                {
                                                    CongAttStatus = "";
                                                    for (int VNOCNodescnt = 0; VNOCNodescnt < VNOCNodes.Count; VNOCNodescnt++)
                                                    {
                                                        XmlNode VoptNode = VNOCNodes[VNOCNodescnt];
                                                        if (VoptNode != null)
                                                        {
                                                            if (CongAttStatus == "")
                                                                CongAttStatus = VoptNode.InnerText.Trim();
                                                            else
                                                                CongAttStatus += "<br>" + VoptNode.InnerText.Trim();
                                                        }
                                                    }
                                                }

                                                CongAtt += DedicatedVNOCOperator + " - " + CongAttStatus;
                                            }

                                            //ZD 100151 - start
                                            dr["Heading"] = hdr;
											//ZD 100085- Starts                                            
                                            if (Blnsetup)
                                                dr["Setup"] = setupDur + " mins";
                                            
                                            if (MCUPreStartDur != 0)
                                                dr["MCUPreStart"] = MCUPreStartDur + " mins";
                                            
                                            dr["ConfTime"] = confStart.ToString(tFormats) + " - " + ConfEnd.ToString(tFormats);
                                            
                                            if (MCUPreEndDur != 0)
                                                dr["MCUPreEnd"] = MCUPreEndDur + " mins";

                                            if (BlnTear)
                                                dr["Tear"] = teardownDur + " mins";
											//ZD 100085- End
                                            dr["Hours"] = hrs.ToString();
                                            dr["Minutes"] = mins.ToString();
                                            dr["Location"] = ((locStr == "") ? obj.GetTranslatedText("N/A") : locStr);//ZD 100288
                                            if (Session["EnableOnsiteAV"].ToString() != "1" && Session["EnableMeetandGreet"].ToString() != "1" && Session["EnableConciergeMonitoring"].ToString() != "1" && Session["EnableDedicatedVNOC"].ToString() != "1" || dr["ConferenceType"].ToString() == "8")//FB 3007
                                                dr["ConfSupport"] = "";
                                            else
                                                dr["ConfSupport"] = CongAtt;

                                            if (Session["EnableEntity"].ToString() == "1" && Session["ShowCusAttInCalendar"].ToString() == "1")
                                                dr["CustomOptions"] = ((customText == "") ? obj.GetTranslatedText("N/A") : customText); //100288
                                            else
                                                dr["CustomOptions"] = "";

                                            //FB 2670 End//FB 3007 END
                                            //FB 2632 Ends
                                            #endregion
                                            #region
                                            //m = new StringBuilder(); ;

                                            ////FB 2058 - Start
                                            //m.Append("<table cellspacing='0' cellpadding='0' border='0' width='300px' class='promptbox' bgColor = '#ccccff'>");
                                            //m.Append("<tr valign='middle'>");
                                            //m.Append("<td width='100%' height='22' style='text-indent:2;' class='titlebar' align='left' colspan='2' bgColor = '#9999ff'>");
                                            //m.Append("<img src='image/pen.gif' height='18' width='18'>&nbsp;&nbsp;" + hdr);
                                            //m.Append("</td>");
                                            //m.Append("</tr>");
                                            //m.Append("</table>");
                                            //m.Append("<div style='width: 300px; height: 140px; overflow: auto;'>");
                                            //m.Append("<table cellspacing='0' cellpadding='0' border='0' class='promptbox' bgColor = '#ccccff'>");
                                            //m.Append("<tr>");
                                            //m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Name") + " </style></td>");
                                            //m.Append("<td width='200px'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + dr["confName"].ToString() + "</style></td>");
                                            //m.Append("</tr>");
                                            //m.Append("<tr>");
                                            //m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Unique ID") + ": </style></td>"); // FB 2002
                                            //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + uniqueID.ToString() + "</style></td>");
                                            //m.Append("</tr>");
                                            //m.Append("<tr>");
                                            //if (setupTxt != "")
                                            //{
                                            //    m.Append( "  <tr>");
                                            //    m.Append( "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>SetupTIme: </style></td>");
                                            //    m.Append( "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + start.ToString(tFormats) + " - " + stUp.ToString(tFormats) + "</style></td>");
                                            //    m.Append( "  </tr>");
                                            //}
                                            //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>"+ obj.GetTranslatedText("Start - End") + ": </style></td>");
                                            //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + stUp.ToString(tFormats) + " - " + trDn.ToString(tFormats) + "</style></td>");
                                            //m.Append("</tr>");
                                            //m.Append("<tr>");
                                            //if (trdnTxt != "")
                                            //{
                                            //    m.Append( "  <tr>");
                                            //    m.Append( "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>TearDownTime: </style></td>");
                                            //    m.Append( "    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + trDn.ToString(tFormats) + " - " + end.ToString(tFormats) + "</style></td>");
                                            //    m.Append( "  </tr>");
                                            //}
                                            //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Duration") + ": </style></td>");
                                            //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + hrs.ToString() + " hr(s) " + mins.ToString() + " min(s)</style></td>");
                                            //m.Append("</tr>");
                                            //m.Append("<tr>");
                                            //m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Location") + ": </style></td>");
                                            //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + ((locStr == "") ? "N/A" : locStr) + "</style></td>");
                                            //m.Append("</tr>");
                                            ////FB 2670 START
                                            ////FB 2670 START
                                            //if (Session["EnableOnsiteAV"].ToString() != "1" && Session["EnableMeetandGreet"].ToString() != "1" && Session["EnableConciergeMonitoring"].ToString() != "1" && Session["EnableDedicatedVNOC"].ToString() != "1" || dr["ConferenceType"].ToString() == "8")//FB 3007
                                            //{
                                            //    m.Append("<tr>");
                                            //    m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'></style></td>");
                                            //    m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'></style></td>");
                                            //    m.Append("</tr>");
                                            //}
                                            //else
                                            //{
                                            //    m.Append("<tr>"); //FB 2632
                                            //    m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Conference Support") + ": </style></td>"); //FB 3023
                                            //    m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + CongAtt + "</style></td>");
                                            //    m.Append("</tr>");
                                            //}
                                            ////FB 2670 END
                                            
                                            ////if (Session["EnableEntity"].ToString() != "0")//FB 2013 //FB 2547
                                            ////{
                                            //if (Session["EnableEntity"].ToString() == "1" && Session["ShowCusAttInCalendar"].ToString() == "1") //ZD 100151
                                            //{
                                            //    m.Append("<tr>");
                                            //    m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + obj.GetTranslatedText("Custom Options") + ": </style></td>");
                                            //    m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + ((customText == "") ? "N/A" : customText) + "</style></td>");
                                            //    m.Append("</tr>");
                                            //}
                                            ////}
                                            //m.Append("</table>");
                                            //m.Append("</div>");
                                            #endregion
                                            //FB 2058 - End

                                            //ZD 100963 START
                                            string EnableRoomCalendarView = Session["EnableRoomCalendarView"].ToString();
                                            if (EnableRoomCalendarView == "3" || ownerid == Session["userID"].ToString() || dr["requestor"].ToString() == Session["userID"].ToString()) // Public //ZD 102832
                                                dr["confDetails"] = spn + dr["confName"].ToString() + " - (UID : " + uniqueID + " )" + Deleted + "<br>" + obj.GetTranslatedText("Conf.Type") + " : " + obj.GetTranslatedText(dr["ConferenceTypeName"].ToString()) + "<br>" + hrs.ToString() + " hrs " + mins.ToString() + obj.GetTranslatedText(" mins") + "<br>" + confStart.ToString(tFormats) + " - " + ConfEnd.ToString(tFormats) + "</span><br>"; //ZD 100085 //ZD 100421 //ZD 100777 //ZD 100528
                                            else if (EnableRoomCalendarView == "2")// Private
                                                dr["confDetails"] = spn + obj.GetTranslatedText("Host") + ": " + Hostname + " " + "<br>" + obj.GetTranslatedText("Work") + ": " + WorkPhone + " " + "<br>" + obj.GetTranslatedText("Cell") + ": " + CellPhone + "</span><br>";
                                            //ZD 100963 END

                                            dr["CustomDescription"] = "";// m.ToString();
                                            //ZD 100151 - end

                                            String party = "0";
                                            Boolean isParticipant = false;
                                            if (node.SelectNodes("party") != null)
                                            {
                                                if (node.SelectNodes("party").Count >= 1)
                                                {
                                                    party = node.SelectSingleNode("party").InnerText;
                                                }
                                            }

                                            if (Session["userID"].ToString() == party)
                                                isParticipant = true;

                                            if (isParticipant && !isAdminRole)
                                            {
                                                Blnsetup = false;
                                                BlnTear = false;
                                            }
                                            //ZD 100085 Starts

                                            drDaily["CustomDescription"] = "";
                                            drDaily["confDetails"] = dr["confDetails"];
                                            drDaily["start"] = confStart;
											//ZD 100433 Starts
                                            drDaily["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(confStart.ToString("MM/dd/yyyy")) + " " + confStart.ToString(tFormats);
                                            if (MCUPreEndDur < 0 && mcuPreEndPostive > teardownDur)
                                            {
                                                drDaily["end"] = ConfEnd;
                                                drDaily["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(ConfEnd.ToString("MM/dd/yyyy")) + " " + ConfEnd.ToString(tFormats);
                                            }
                                            else
                                            {
                                                drDaily["end"] = PreEnd;
                                                drDaily["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(PreEnd.ToString("MM/dd/yyyy")) + " " + PreEnd.ToString(tFormats);
                                            }
											//ZD 100433 End
											//ZD 100085 End
                                            drDaily["ID"] = dr["ID"];
                                            drDaily["ConfID"] = dr["ConfID"];
                                            drDaily["ConferenceType"] = dr["ConferenceType"];
                                            drDaily["RoomID"] = dr["RoomID"];
                                            drDaily["durationMin"] = dr["durationMin"];
                                            drDaily["confName"] = dr["confName"];
                                            //ZD 100151
                                            drDaily["Heading"] = dr["Heading"];
                                            drDaily["Setup"] = dr["Setup"];
                                            drDaily["MCUPreStart"] = dr["MCUPreStart"];//ZD 100085
                                            drDaily["ConfTime"] = dr["ConfTime"];
                                            drDaily["MCUPreEnd"] = dr["MCUPreEnd"];//ZD 100085
                                            drDaily["Tear"] = dr["Tear"];
                                            drDaily["Hours"] = dr["Hours"];
                                            drDaily["Minutes"] = dr["Minutes"];
                                            drDaily["Location"] = dr["Location"];
                                            drDaily["ConfSupport"] = dr["ConfSupport"];
                                            drDaily["CustomOptions"] = dr["CustomOptions"];
                                            drDaily["ConferenceTypeName"] = dr["ConferenceTypeName"];//ZD 100421
                                            drDaily["owner"] = dr["owner"];//ZD 102832
                                            drDaily["requestor"] = dr["requestor"];//ZD 102832
                                            dt.Rows.Add(dr);

                                            if (Blnsetup)
                                            {
                                                DataRow setup = dtDaily.NewRow();
                                                setup["RoomID"] = room.Value;
                                                setup["ConfID"] = dr["ConfID"].ToString();
                                                setup["ConferenceType"] = "S";
												//ZD 100085 Starts
                                                setup["ID"] = dr["ID"];
                                                setup["start"] = start;
                                                setup["end"] = PreStart;
                                                setup["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(start.ToString("MM/dd/yyyy")) + " " + start.ToString(tFormats);
                                                setup["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(PreStart.ToString("MM/dd/yyyy")) + " " + PreStart.ToString(tFormats);
                                                setup["confDetails"] = "";
                                                setup["CustomDescription"] = "";
                                                setup["confName"] = dr["confName"];
                                                setup["Heading"] = dr["Heading"];
                                                setup["Setup"] = dr["Setup"];
                                                setup["MCUPreStart"] = dr["MCUPreStart"];
                                                setup["ConfTime"] = dr["ConfTime"];
                                                setup["MCUPreEnd"] = dr["MCUPreEnd"];
                                                setup["Tear"] = dr["Tear"];
                                                setup["Hours"] = dr["Hours"];
                                                setup["Minutes"] = dr["Minutes"];
                                                setup["Location"] = dr["Location"];
                                                setup["ConfSupport"] = dr["ConfSupport"];
                                                setup["CustomOptions"] = dr["CustomOptions"];
												//ZD 100085 End
                                                setup["ConferenceTypeName"] = dr["ConferenceTypeName"]; //ZD 100421
                                                setup["owner"] = dr["owner"];//ZD 102832
                                                setup["requestor"] = dr["requestor"];//ZD 102832
                                                dtDaily.Rows.Add(setup);
                                            }
                                            dtDaily.Rows.Add(drDaily);
											//ZD 100085 Starts
                                            if (MCUPreStartDur != 0)
                                            {
                                                DataRow preStart = dtDaily.NewRow();
                                                preStart["RoomID"] = room.Value;
                                                preStart["ConfID"] = dr["ConfID"].ToString();
                                                preStart["ConferenceType"] = "PreStartEnd";
                                                preStart["ID"] = dr["ID"];
                                                preStart["start"] = PreStart;
                                                preStart["end"] = confStart;
                                                preStart["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(PreStart.ToString("MM/dd/yyyy")) + " " + PreStart.ToString(tFormats);
                                                preStart["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(confStart.ToString("MM/dd/yyyy")) + " " + confStart.ToString(tFormats);
                                                preStart["confDetails"] = "";
                                                preStart["CustomDescription"] = "";
                                                preStart["confName"] = dr["confName"];
                                                preStart["Heading"] = dr["Heading"];
                                                preStart["Setup"] = dr["Setup"];
                                                preStart["MCUPreStart"] = dr["MCUPreStart"];
                                                preStart["ConfTime"] = dr["ConfTime"];
                                                preStart["MCUPreEnd"] = dr["MCUPreEnd"];
                                                preStart["Tear"] = dr["Tear"];
                                                preStart["Hours"] = dr["Hours"];
                                                preStart["Minutes"] = dr["Minutes"];
                                                preStart["Location"] = dr["Location"];
                                                preStart["ConfSupport"] = dr["ConfSupport"];
                                                preStart["CustomOptions"] = dr["CustomOptions"];
                                                preStart["ConferenceTypeName"] = dr["ConferenceTypeName"]; //ZD 100421
                                                preStart["owner"] = dr["owner"];//ZD 102832
                                                preStart["requestor"] = dr["requestor"];//ZD 102832
                                                dtDaily.Rows.Add(preStart);
                                            }
											//ZD 100085 End
											//ZD 100433
                                            if (BlnTear && (teardownDur > 0 && mcuPreEndPostive <= teardownDur))
                                            {
                                                DataRow trDown = dtDaily.NewRow();
                                                trDown["RoomID"] = room.Value;
                                                trDown["ConfID"] = dr["ConfID"].ToString();
                                                trDown["ConferenceType"] = "T";
												//ZD 100085 Starts
                                                trDown["ID"] = dr["ID"];
                                                trDown["start"] = ConfEnd;
                                                trDown["end"] = teardownDateTime;
                                                trDown["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(ConfEnd.ToString("MM/dd/yyyy")) + " " + ConfEnd.ToString(tFormats);
                                                trDown["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(teardownDateTime.ToString("MM/dd/yyyy")) + " " + teardownDateTime.ToString(tFormats);
                                                trDown["confDetails"] = "";

                                                trDown["CustomDescription"] = "";
                                                trDown["confName"] = dr["confName"];
                                                trDown["Heading"] = dr["Heading"];
                                                trDown["Setup"] = dr["Setup"];
                                                trDown["MCUPreStart"] = dr["MCUPreStart"];
                                                trDown["ConfTime"] = dr["ConfTime"];
                                                trDown["MCUPreEnd"] = dr["MCUPreEnd"];
                                                trDown["Tear"] = dr["Tear"];
                                                trDown["Hours"] = dr["Hours"];
                                                trDown["Minutes"] = dr["Minutes"];
                                                trDown["Location"] = dr["Location"];
                                                trDown["ConfSupport"] = dr["ConfSupport"];
                                                trDown["CustomOptions"] = dr["CustomOptions"];
												//ZD 100085 End
                                                trDown["ConferenceTypeName"] = dr["ConferenceTypeName"]; //ZD 100421
                                                trDown["owner"] = dr["owner"];//ZD 102832
                                                trDown["requestor"] = dr["requestor"];//ZD 102832
                                                dtDaily.Rows.Add(trDown);
                                            }
											//ZD 100085 Starts
                                            if (MCUPreEndDur != 0)
                                            {
                                                DataRow preEnd = dtDaily.NewRow();
                                                preEnd["RoomID"] = room.Value;
                                                preEnd["ConfID"] = dr["ConfID"].ToString();
                                                preEnd["ConferenceType"] = "PreStartEnd";
                                                preEnd["ID"] = dr["ID"];
												//ZD 100433 Starts
                                                if (MCUPreEndDur < 0 && mcuPreEndPostive > teardownDur)
                                                {
                                                    preEnd["start"] = ConfEnd;
                                                    preEnd["end"] = PreEnd;
                                                    preEnd["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(ConfEnd.ToString("MM/dd/yyyy")) + " " + ConfEnd.ToString(tFormats);
                                                    preEnd["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(PreEnd.ToString("MM/dd/yyyy")) + " " + PreEnd.ToString(tFormats);
                                                }
                                                else
                                                {
                                                    preEnd["start"] = PreEnd;
                                                    preEnd["end"] = ConfEnd;
                                                    preEnd["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(PreEnd.ToString("MM/dd/yyyy")) + " " + PreEnd.ToString(tFormats);
                                                    preEnd["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(ConfEnd.ToString("MM/dd/yyyy")) + " " + ConfEnd.ToString(tFormats);
                                                }
												//ZD 100433 End
                                                preEnd["confDetails"] = "";
                                                preEnd["CustomDescription"] = "";
                                                preEnd["confName"] = dr["confName"];
                                                preEnd["Heading"] = dr["Heading"];
                                                preEnd["Setup"] = dr["Setup"];
                                                preEnd["MCUPreStart"] = dr["MCUPreStart"];
                                                preEnd["ConfTime"] = dr["ConfTime"];
                                                preEnd["MCUPreEnd"] = dr["MCUPreEnd"];
                                                preEnd["Tear"] = dr["Tear"];
                                                preEnd["Hours"] = dr["Hours"];
                                                preEnd["Minutes"] = dr["Minutes"];
                                                preEnd["Location"] = dr["Location"];
                                                preEnd["ConfSupport"] = dr["ConfSupport"];
                                                preEnd["CustomOptions"] = dr["CustomOptions"];
                                                preEnd["ConferenceTypeName"] = dr["ConferenceTypeName"]; //ZD 100421
                                                preEnd["owner"] = dr["owner"];//ZD 102832
                                                preEnd["requestor"] = dr["requestor"];//ZD 102832
                                                dtDaily.Rows.Add(preEnd);
                                            }
                                            //ZD 100085 End


                                            Blnsetup = false; BlnTear = false;

                                            locStr = "";


                                        }
                                    }
                                }
                            }

                        }
                    }


                    if (Session["RoomCalendar"] != null)
                        Session.Add("RoomCalendar", dt);
                    else
                        Session["RoomCalendar"] = dt;

                    if (Session["RoomDaily"] != null)
                        Session.Add("RoomDaily", dtDaily);
                    else
                        Session["RoomDaily"] = dtDaily;
                    
                       

            }
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message + ":" + ex.InnerException);
        }

    }

    protected DataTable DatatablereturnfromXML(String Roomsxml)
    {
        //ZD 1010122
        String hdr = obj.GetTranslatedText("Conference Details"), startHour = "00", startMin = "00", startSet = "AM", endHour = "23", endMin = "59", endSet = "PM", open24 = "";
        int hrs, mins = 0, cnt = 1, hasRights = 0; //ZD 101942 


        String locStr = "";
        //String setupTxt = "";//ZD 100085-Commented
        //String trdnTxt = "";
        XmlNode node = null;
        XmlNodeList nodes = null;
        XmlDocument xmldoc = null;
        DataTable dt = null;
        StringBuilder m = null;
        XmlNodeList subnotes2 = null;
        XmlNode subnode2 = null;
        Boolean Blnsetup = false, BlnTear = false;
        string partyid = "0"; //FB 1659
        string ownerid = "0"; //FB 1659
        bool isParty = false; //FB 1659
        bool isOwner = false; //FB 1659
        isPublic = "0"; //FB 1659
        try
        {

            if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))//Code added for Phase2
                hdr = "Hearing Details";

            //String roomsselected = selectedList.Value;

            String spn = "<span  class=\"eventtext\">";
            if (Roomsxml != "")
            {


                dt = GetDataTable();
                schDaypilot.Resources.Clear();
                schDaypilotweek.Resources.Clear();
                schDaypilotMonth.Resources.Clear();

                //FB 1779
                String[] strSplit = new String[1] { "~~" }; //FB 2012//ZD 100151//ZD 100263
               
                foreach (String xmls in Roomsxml.Split(strSplit, StringSplitOptions.RemoveEmptyEntries))
                {

                    if (xmls != "")
                    {
                        bypass = true;

                        if (Enabletreeview)
                            {

                                //if (CehckifStringContains(roomsselected.Trim(), ",", node.Value))
                                //    continue;
                                //if (CehckifStringContains(HdnMonthlyXml.Value, node.Value))
                                //    continue;

                                if (!CehckifStringContains(selectedList.Value.Trim(), ",", xmls.Split(strSplitat, StringSplitOptions.RemoveEmptyEntries)[0])) //FB 2012
                                    continue;
                            }

                        xmldoc = new XmlDocument();
                        xmldoc.LoadXml(xmls.Split(strSplitat, StringSplitOptions.RemoveEmptyEntries)[2]); //FB 2012
                        if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24") != null)
                            open24 = xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24").InnerText;

                        if (open24 == "1")
                        {
                           
                            schDaypilotweek.BusinessBeginsHour = 0;
                            schDaypilotweek.BusinessEndsHour = 24;
                        }
                        else
                        {

                            if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour") != null)
                                startHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour").InnerText;

                            if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin") != null)
                                startMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin").InnerText;

                            if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet") != null)
                                startSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet").InnerText;

                            if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour") != null)
                                endHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour").InnerText;

                            if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin") != null)
                                endMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin").InnerText;

                            if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet") != null)
                                endSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet").InnerText;

                           
                            schDaypilotweek.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                            schDaypilotweek.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                            if (endMin != "00")//FB 2057
                                schDaypilotweek.BusinessEndsHour = schDaypilotweek.BusinessEndsHour + 1;

                            if (startHour == "12" && startSet == "AM" && endHour == "11" && endMin != "00" && endSet == "PM") // ZD 100284
                                officehrDaily.Checked = false;

                        }

                        DayPilot.Web.Ui.Resource room = new DayPilot.Web.Ui.Resource(xmls.Split(strSplitat, StringSplitOptions.RemoveEmptyEntries)[1], xmls.Split(strSplitat, StringSplitOptions.RemoveEmptyEntries)[0]); //FB 2012


                        
                        schDaypilotweek.Resources.Add(room);

                        nodes = xmldoc.SelectNodes("//days/day/conferences/conference");

                        for (int confnodes = 0; confnodes < nodes.Count; confnodes++)// XmlNode node in nodes)
                        {
                            node = nodes[confnodes];
                            String tempFor = "", confSTime = "", Deleted = "";//ZD 100421
                            String setupSTime = "", teardownSTime = "", confSDate = "", uniqueID = ""; // FB 2002
                            String Hostname = "", WorkPhone = "", CellPhone="";//ZD 100963

                            if (node.SelectSingleNode("durationMin") != null)
                            {

                                if (node.SelectSingleNode("durationMin").InnerText != "")
                                {
                                    if (!showDeletedConf.Checked)
                                    {
                                        if (node.SelectSingleNode("deleted") != null)
                                        {
                                            if (node.SelectSingleNode("deleted").InnerText == "1")
                                                continue;
                                        }
                                    }



                                    if (bypass)
                                    {
                                        DataRow dr = dt.NewRow();

                                        dr["RoomID"] = room.Value;

                                        if (node.SelectSingleNode("party") != null) //FB 1659
                                        {
                                            partyid = node.SelectSingleNode("party").InnerText.Trim();
                                        }
                                        if (node.SelectSingleNode("owner") != null) //FB 1659
                                        {
                                            ownerid = node.SelectSingleNode("owner").InnerText.Trim();
                                        }

                                        if (node.SelectSingleNode("isPublic") != null) //FB 1659
                                        {
                                            isPublic = node.SelectSingleNode("isPublic").InnerText.Trim();
                                        }

                                        if (node.SelectSingleNode("confName") != null)
                                            dr["confName"] = node.SelectSingleNode("confName").InnerText;

                                        if (Session["isVIP"] != null)
                                        {
                                            if (Session["isVIP"].ToString() == "1")
                                            {
                                                if (node.SelectSingleNode("isVIP") != null) // FB 1864
                                                    if (node.SelectSingleNode("isVIP").InnerText == "1")
                                                        dr["confName"] = dr["confName"].ToString() + " {VIP}";
                                            }
                                        }


                                        // FB 1659 code starts ... //ZD 104126  start
                                         if (partyid == Session["userID"].ToString() || node.SelectSingleNode("requestor").InnerText == Session["userID"].ToString()) //ZD 103500
                                                isParty = true;
                                            else
                                                isParty = false;

                                            if (ownerid == Session["userID"].ToString())
                                                isOwner = true;
                                            else
                                                isOwner = false;
                                            hasRights = 0;
                                            if (isAdminRole || isParty || isOwner || (isPublic == "1"))
                                            {
                                                if (isAdminRole || isOwner) //ZD 101942 //ZD103051 STARTS
                                                    hasRights = 1;

                                                int hasView = 0;
                                                int hasEdit = 0;
                                                int hasDelete = 0;
                                                int hasManage = 0;
                                                int hasClone = 0;
                                                int hasMCUInfo = 0;
                                                int hasExtendTime = 0;

                                                if (hasRights == 1)
                                                {
                                                    int confStatus = 0, filterType = 0, hasReservations = 0, hasPublic = 0;

                                                    if (node.SelectSingleNode("ConferenceStatus") != null)//ZD 100421
                                                        int.TryParse(node.SelectSingleNode("ConferenceStatus").InnerText, out confStatus);

                                                    if (Session["hasReservations"] != null && Session["hasReservations"].ToString() == "1")
                                                        hasReservations = 1;

                                                    if (Session["hasPublic"] != null && Session["hasPublic"].ToString() == "1")
                                                        hasPublic = 1;

                                                    if (hasReservations == 1 && confStatus == 0)
                                                        filterType = 3;
                                                    else if (hasPublic == 1 && confStatus == 0 && Convert.ToInt16(isPublic) == 1)
                                                        filterType = 4;
                                                    else if (confStatus == 1) //Pending
                                                        filterType = 5;
                                                    else if (confStatus == 6) //On MCU
                                                        filterType = 8;
                                                    else if (confStatus == 5) //Ongoing
                                                        filterType = 2;
                                                    //else if (confStatus == 2) //Wait List //ZD 102532
                                                    //    filterType = 11;
                                                    else if (confStatus == 3 || confStatus == 9) //Terminated or Deleted
                                                        filterType = 9;
                                                    else if (confStatus == 7) //Completed
                                                        filterType = 10;
                                                    else
                                                        filterType = 3; //if (confStatus == 0 && isPublicConf == 0)//Reservation with Puclic or ConfSupport or without both

                                                    obj.CheckConferenceRights(filterType, ref hasView, ref hasManage, ref hasExtendTime, ref hasMCUInfo, ref hasEdit, ref hasDelete, ref hasClone);
                                                }

                                                dr["confName"] = "<a href='#' onclick='javascript: ViewConfDetails(\"" + node.SelectSingleNode("confID").InnerText.Trim() + "|" + hasRights + "|" + hasEdit + "\");' title='" + obj.GetTranslatedText("Click to see more details") + "'><font style='cursor:hand;'>" + dr["confName"].ToString() + "</font></a>"; //ZD 10028 //ZD 101942 //ZD 103051
                                            }
                                                //ZD 104126 End
                                        // FB 1659 code ends ..

                                        dr["durationMin"] = node.SelectSingleNode("durationMin").InnerText;
                                        hrs = Convert.ToInt32(dr["durationMin"].ToString()) / 60;
                                        mins = Convert.ToInt32(dr["durationMin"].ToString()) % 60;
                                        if (node.SelectSingleNode("confID") != null)
                                            dr["ConfID"] = node.SelectSingleNode("confID").InnerText;
                                        if (node.SelectSingleNode("ConferenceType") != null)
                                            dr["ConferenceType"] = node.SelectSingleNode("ConferenceType").InnerText;

                                        if (node.SelectSingleNode("ConferenceTypeName") != null)//ZD 100421
                                            dr["ConferenceTypeName"] = node.SelectSingleNode("ConferenceTypeName").InnerText;

                                        dr["owner"] = ownerid; //ZD 102832
                                        dr["requestor"] = node.SelectSingleNode("requestor").InnerText; //ZD 102832

                                       if (showDeletedConf.Checked)
                                        {
                                            if (node.SelectSingleNode("deleted") != null)
                                            {
                                                if (node.SelectSingleNode("deleted").InnerText == "1")
                                                {
                                                    dr["ConferenceType"] = "9";
                                                    Deleted = "- Deleted"; //ZD 100421
                                                }
                                            }
                                        }

                                        dr["ID"] = dr["ConfID"].ToString();
                                        // FB 2002 starts
                                        if (node.SelectSingleNode("uniqueID") != null)
                                        {
                                            if (node.SelectSingleNode("uniqueID").InnerText != "")
                                            {
                                                uniqueID = node.SelectSingleNode("uniqueID").InnerText;
                                            }
                                        }
                                        // FB 2002 ends
                                        dr["ID"] = uniqueID;// dr["ConfID"].ToString(); //ZD 100151

                                        //ZD 100963 START
                                        if (node.SelectSingleNode("Hostname") != null)
                                        {
                                            if (node.SelectSingleNode("Hostname").InnerText != "")
                                            {
                                                Hostname = node.SelectSingleNode("Hostname").InnerText;
                                            }
                                        }

                                        if (node.SelectSingleNode("WorkPhone") != null)
                                        {
                                            if (node.SelectSingleNode("WorkPhone").InnerText != "")
                                            {
                                                WorkPhone = node.SelectSingleNode("WorkPhone").InnerText;
                                            }
                                        }

                                        if (node.SelectSingleNode("CellPhone") != null)
                                        {
                                            if (node.SelectSingleNode("CellPhone").InnerText != "")
                                            {
                                                CellPhone = node.SelectSingleNode("CellPhone").InnerText;
                                            }
                                        }
                                        //ZD 100963 END

                                        if (node.SelectSingleNode("confTime") != null)
                                            confSTime = node.SelectSingleNode("confTime").InnerText;

                                        if (node.SelectSingleNode("setupTime") != null)
                                            setupSTime = node.SelectSingleNode("setupTime").InnerText;

                                        if (node.SelectSingleNode("teardownTime") != null)
                                            teardownSTime = node.SelectSingleNode("teardownTime").InnerText;


                                        //ZD 100085 Starts
                                        int setupDur = 0;
                                        if (node.SelectSingleNode("setupDur") != null)
                                        {
                                            if (node.SelectSingleNode("setupDur").InnerText.Trim() != "")
                                                int.TryParse(node.SelectSingleNode("setupDur").InnerText.Trim(), out setupDur);
                                        }

                                        int teardownDur = 0;
                                        if (node.SelectSingleNode("teardownDur") != null)
                                        {
                                            if (node.SelectSingleNode("teardownDur").InnerText.Trim() != "")
                                                int.TryParse(node.SelectSingleNode("teardownDur").InnerText.Trim(), out teardownDur);
                                        }

                                        
                                        int MCUPreStartDur = 0;
                                        if (node.SelectSingleNode("MCUPreStartDur") != null)
                                        {
                                            if (node.SelectSingleNode("MCUPreStartDur").InnerText.Trim() != "")
                                                int.TryParse(node.SelectSingleNode("MCUPreStartDur").InnerText.Trim(), out MCUPreStartDur);
                                        }

                                        int MCUPreEndDur = 0;
                                        if (node.SelectSingleNode("MCUPreEndDur") != null)
                                        {
                                            if (node.SelectSingleNode("MCUPreEndDur").InnerText.Trim() != "")
                                                int.TryParse(node.SelectSingleNode("MCUPreEndDur").InnerText.Trim(), out MCUPreEndDur);
                                        }
                                        //ZD 100085 End
                                        if (node.SelectSingleNode("confDate") != null)
                                        {
                                            if (node.SelectSingleNode("confDate").InnerText != "")
                                            {
                                                confSDate = node.SelectSingleNode("confDate").InnerText;
                                            }
                                        }
                                        //ZD 100085 Starts
                                        //int adddays = 0;
                                        //if (confSTime.Trim() == "00:00 AM")
                                        //    adddays = 1;

                                        DateTime confStart = DateTime.Parse(confSDate + " " + confSTime);
                                        DateTime ConfEnd = confStart.AddMinutes(Convert.ToDouble(dr["durationMin"]));

                                        DateTime PreStart = DateTime.MinValue;
                                        DateTime PreEnd = DateTime.MinValue;
                                        DateTime teardownDateTime = DateTime.MinValue;

                                        PreStart = confStart.AddMinutes(-MCUPreStartDur);
                                        PreEnd = ConfEnd.AddMinutes(-MCUPreEndDur);
                                        teardownDateTime = ConfEnd.AddMinutes(teardownDur);

                                        DateTime start = DateTime.Now;
										//ZD 100433 Starts
                                        if (enableBufferZone == "1" && setupDur > MCUPreStartDur)
                                            start = confStart.AddMinutes(-setupDur);
                                        else
                                            start = confStart.AddMinutes(-MCUPreStartDur);
										//ZD 100433 End
                                        /*
                                        int adddays = 0;
                                        if (confSTime.Trim() == "00:00 AM")
                                            adddays = 1;

                                        DateTime start = DateTime.Parse(confSDate + " " + confSTime);
                                        DateTime stUp = DateTime.Parse(confSDate + " " + setupSTime);
                                        DateTime end = start.AddMinutes(Convert.ToDouble(dr["durationMin"]));
                                        DateTime trDn = DateTime.Parse(end.ToString("MM/dd/yyyy") + " " + teardownSTime);

                                        start = start.AddDays(adddays);
                                        stUp = stUp.AddDays(adddays);
                                        end = end.AddDays(adddays);
                                        trDn = trDn.AddDays(adddays);
                                        */


                                        dr["start"] = confStart;
                                        dr["end"] = ConfEnd;

                                        dr["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(confStart.ToString("MM/dd/yyyy")) + " " + confStart.ToString(tFormats);
                                        dr["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(ConfEnd.ToString("MM/dd/yyyy")) + " " + ConfEnd.ToString(tFormats); ;
                                        //ZD 100085 End

                                        subnotes2 = node.SelectNodes("mainLocation/location");

                                        if (subnotes2 != null)
                                        {
                                            for (int subnodescnt = 0; subnodescnt < subnotes2.Count; subnodescnt++)
                                            {
                                                subnode2 = subnotes2[subnodescnt];
                                                locStr = locStr + subnode2.SelectSingleNode("locationName").InnerText;
                                                locStr = locStr + "<br>";
                                            }
                                        }
                                        //ZD 100085 Starts
                                        if (teardownDur > 0 && enableBufferZone == "1")
                                            BlnTear = true;

                                        if (setupDur > 0 && enableBufferZone == "1")
                                            Blnsetup = true;

                                        //trdnTxt = "";
                                        //if (end != trDn)//&& cnt > 1
                                        //    trdnTxt = "Tear Down :" + trDn.ToString(tFormats) + " - " + end.ToString(tFormats) + "<br>";

                                        //setupTxt = "";
                                        //if (stUp != start)//&& cnt > 1
                                        //    setupTxt = "Setup :" + start.ToString(tFormats) + " - " + stUp.ToString(tFormats) + "<br>"; //FB 2961

                                        if (Blnsetup)
                                        {
                                            dr["start"] = start;
                                            dr["formatstart"] = myVRMNet.NETFunctions.GetFormattedDate(start.ToString("MM/dd/yyyy")) + " " + start.ToString(tFormats);
                                        }

                                        if (BlnTear)
                                        {
                                            dr["end"] = teardownDateTime;
                                            dr["formatend"] = myVRMNet.NETFunctions.GetFormattedDate(teardownDateTime.ToString("MM/dd/yyyy")) + " " + teardownDateTime.ToString(tFormats);
                                        }

                                        if (enableBufferZone == "0")
                                        {
                                            Blnsetup = false;
                                            BlnTear = false;
                                            //trDn = end;
                                            //stUp = start;
                                            //trdnTxt = "";
                                            //setupTxt = "";
                                        }
                                        //ZD 100151 - start
                                        dr["Heading"] = hdr;
                                        if (Blnsetup)
                                            dr["Setup"] = setupDur + " mins";

                                        if (MCUPreStartDur != 0)
                                            dr["MCUPreStart"] = MCUPreStartDur + " mins";

                                        dr["ConfTime"] = confStart.ToString(tFormats) + " - " + ConfEnd.ToString(tFormats);
                                        if (MCUPreEndDur != 0)
                                            dr["MCUPreEnd"] = MCUPreEndDur + " mins";

                                        if (BlnTear)
                                            dr["Tear"] = teardownDur + " mins";
                                        //ZD 100085 End
                                        dr["Hours"] = hrs.ToString();
                                        dr["Minutes"] = mins.ToString();
                                        dr["Location"] = ((locStr == "") ? obj.GetTranslatedText("N/A") : locStr);//100288
                                        dr["ConfSupport"] = ""; ;
                                        dr["CustomOptions"] = "";

                                        //m = new StringBuilder(); ;
                                        //m.Append("<table cellspacing='0' cellpadding='0' border='0' width='310px' class='promptbox' bgColor = '#ccccff'>");
                                        //m.Append("<tr valign='middle'>");
                                        //m.Append("<td width='100%' height='22' style='text-indent:2;' class='titlebar' align='left' colspan='2' bgColor = '#9999ff'>");
                                        //m.Append("<img src='image/pen.gif' height='18' width='18'>&nbsp;&nbsp;" + hdr);
                                        //m.Append("</td>");
                                        //m.Append("</tr>");
                                        //m.Append("<tr>");
                                        //m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>Name: </style></td>");
                                        //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + dr["confName"].ToString() + "</style></td>");
                                        //m.Append("</tr>");
                                        //m.Append("<tr>");
                                        //m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>Unique ID: </style></td>"); // FB 2002
                                        //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + uniqueID.ToString() + "</style></td>");
                                        //m.Append("</tr>");
                                        //if (setupTxt != "")
                                        //{
                                        //    m.Append("  <tr>");
                                        //    m.Append("    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>SetupTIme: </style></td>");
                                        //    m.Append("    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + start.ToString(tFormats) + " - " + stUp.ToString(tFormats) + "</style></td>");
                                        //    m.Append("  </tr>");
                                        //}
                                        //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>Start - End: </style></td>");
                                        //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + stUp.ToString(tFormats) + " - " + trDn.ToString(tFormats) + "</style></td>");
                                        //m.Append("</tr>");
                                        //m.Append("<tr>");
                                        //if (trdnTxt != "")
                                        //{
                                        //    m.Append("  <tr>");
                                        //    m.Append("    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>TearDownTime: </style></td>");
                                        //    m.Append("    <td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + trDn.ToString(tFormats) + " - " + end.ToString(tFormats) + "</style></td>");
                                        //    m.Append("  </tr>");
                                        //}
                                        //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>Duration: </style></td>");
                                        //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + hrs.ToString() + " hr(s) " + mins.ToString() + " min(s)</style></td>");
                                        //m.Append("</tr>");
                                        //m.Append("<tr>");
                                        //m.Append("<td valign='top'><span style='font-size: 8pt; font-weight: bold; color: #990033'>Location: </style></td>");
                                        //m.Append("<td><span style='font-size: 8pt; font-weight: bold; color: #990033'>" + ((locStr == "") ? "N/A" : locStr) + "</style></td>");
                                        //m.Append("</tr>");
                                        //m.Append("</table>");
                                        //ZD 100085 Starts
                                        
                                        
                                        //ZD 100963 START 
                                        string EnableRoomCalendarView = Session["EnableRoomCalendarView"].ToString();
                                        if (EnableRoomCalendarView == "3" || ownerid == Session["userID"].ToString() || dr["requestor"].ToString() == Session["userID"].ToString()) // Public //ZD 102832
                                            dr["confDetails"] = spn + dr["confName"].ToString() + " - (UID : " + uniqueID + " )" + Deleted + " Conf.Type : " + dr["ConferenceTypeName"].ToString() + "<br>" + hrs.ToString() + " hrs " + mins.ToString() + " mins<br>" + confStart.ToString(tFormats) + " - " + ConfEnd.ToString(tFormats) + "</span><br>";//ZD 100421 ZD 100528

                                        else if (EnableRoomCalendarView == "2")// Private
                                            dr["confDetails"] = spn + obj.GetTranslatedText("Host") + ": " + Hostname + " " + "<br>" + obj.GetTranslatedText("Work") + ": " + WorkPhone + " " + "<br>" + obj.GetTranslatedText("Cell") + ": " + CellPhone + "</span><br>";
                                        //ZD 100963 END

                                        dr["CustomDescription"] = "";

                                        //ZD 100085 End

                                        String party = "0";
                                        Boolean isParticipant = false;
                                        if (node.SelectNodes("party") != null)
                                        {
                                            if (node.SelectNodes("party").Count >= 1)
                                            {
                                                party = node.SelectSingleNode("party").InnerText;
                                            }
                                        }

                                        if (Session["userID"].ToString() == party)
                                            isParticipant = true;

                                        if (isParticipant && !isAdminRole)
                                        {
                                            Blnsetup = false;
                                            BlnTear = false;
                                        }
                                        

                                        dt.Rows.Add(dr);

                                       
                                        Blnsetup = false; BlnTear = false;

                                        locStr = "";


                                    }
                                }
                            }
                        }

                    }
                }

            }
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message + ":" + ex.InnerException);
        }

        return dt;

    }

    #endregion    

    #region Get Data Table

    private DataTable GetDataTable()
    {
        DataTable dt = null;
        try
        {
            dt = new DataTable();

            if (!dt.Columns.Contains("start")) dt.Columns.Add("start");
            if (!dt.Columns.Contains("end")) dt.Columns.Add("end");
            if (!dt.Columns.Contains("formatstart")) dt.Columns.Add("formatstart");
            if (!dt.Columns.Contains("formatend")) dt.Columns.Add("formatend");
            if (!dt.Columns.Contains("confDetails")) dt.Columns.Add("confDetails");
            if (!dt.Columns.Contains("ID")) dt.Columns.Add("ID");
            if (!dt.Columns.Contains("ConfID")) dt.Columns.Add("ConfID");
            if (!dt.Columns.Contains("ConferenceType")) dt.Columns.Add("ConferenceType");
            if (!dt.Columns.Contains("RoomID")) dt.Columns.Add("RoomID");
            if (!dt.Columns.Contains("confName")) dt.Columns.Add("confName");
            if (!dt.Columns.Contains("durationMin")) dt.Columns.Add("durationMin");
            if (!dt.Columns.Contains("CustomDescription")) dt.Columns.Add("CustomDescription");
			//ZD 100151
            if (!dt.Columns.Contains("Heading")) dt.Columns.Add("Heading");
            if (!dt.Columns.Contains("Setup")) dt.Columns.Add("Setup");
            if (!dt.Columns.Contains("MCUPreStart")) dt.Columns.Add("MCUPreStart"); //ZD 100085
            if (!dt.Columns.Contains("ConfTime")) dt.Columns.Add("ConfTime");
            if (!dt.Columns.Contains("MCUPreEnd")) dt.Columns.Add("MCUPreEnd");//ZD 100085
            if (!dt.Columns.Contains("Tear")) dt.Columns.Add("Tear");
            if (!dt.Columns.Contains("Hours")) dt.Columns.Add("Hours");
            if (!dt.Columns.Contains("Minutes")) dt.Columns.Add("Minutes");
            if (!dt.Columns.Contains("Location")) dt.Columns.Add("Location");
            if (!dt.Columns.Contains("ConfSupport")) dt.Columns.Add("ConfSupport");
            if (!dt.Columns.Contains("CustomOptions")) dt.Columns.Add("CustomOptions");
            if (!dt.Columns.Contains("ConferenceTypeName")) dt.Columns.Add("ConferenceTypeName");//ZD 100421
            if (!dt.Columns.Contains("owner")) dt.Columns.Add("owner");//ZD 102832
            if (!dt.Columns.Contains("requestor")) dt.Columns.Add("requestor");//ZD 102832
        }
        catch (Exception ex)
        {

            throw ex;
        }

        return dt;

    }

    #endregion

    #region Cehck if String Contains

    private Boolean CehckifStringContains(String container,String delimiter,String iscontained)
    {
        bool rtrn = false;
        try
        {
            if (container != "" && delimiter != "" && iscontained != "")
            {
                String[] containString = container.Split(delimiter.ToCharArray());

                for(int strcnt = 0;strcnt<containString.Length;strcnt ++)
                {
                    if(containString[strcnt] != "")
                    {
                        if (containString[strcnt] == iscontained.Trim())
                        {
                            rtrn = true; break;
                        }
                    }
                }

            }



        }
        catch (Exception ex)
        {

            throw ex;
        }

        return rtrn;

    }

    #endregion

    #region BindMonthly

    protected void BindMonthly()
    {
        DataTable dt = null;

        try
        {
            if (dt == null)
            {
                if (Session["RoomCalendar"] != null)
                {
                    dt = (DataTable)Session["RoomCalendar"];
                }
            }

            if (dt != null)
            {
                schDaypilotweek.DataSource = dt;
                schDaypilotweek.DataBind();
                if (schDaypilotweek.Resources.Count > 0)
                {
                    schDaypilotweek.Visible = true;
                }
                else
                {
                    schDaypilotweek.Visible = false;
                }
                schDaypilotweek.StartDate = DayPilot.Utils.Week.FirstDayOfWeek(conf);
                schDaypilotweek.Days = 7;


                if (officehrWeek.Checked)
                {
                    schDaypilotweek.ShowNonBusiness = false;
                }
                else
                {
                    schDaypilotweek.ShowNonBusiness = true;
                }

                schDaypilotMonth.DataSource = dt;
                schDaypilotMonth.DataBind();
                if (schDaypilotMonth.Resources.Count > 0)
                {
                    schDaypilotMonth.Visible = true;
                }
                else
                {
                    schDaypilotMonth.Visible = false;
                }
                schDaypilotMonth.StartDate = new DateTime(conf.Year, conf.Month, 1);


                if (officehrMonth.Checked)
                    schDaypilotMonth.ShowNonBusiness = false;
                else
                    schDaypilotMonth.ShowNonBusiness = true; ;

            }
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region BindDaily

    protected void BindDaily()
    {
        DataTable dt = null; ;

        try
        {
            if (dt == null)
            {

                if (Session["RoomDaily"] != null)
                {
                    dt = (DataTable)Session["RoomDaily"];
                }
                else if (Session["RoomCalendar"] != null)
                {
                    dt = (DataTable)Session["RoomCalendar"];
                }
            }

            if (dt != null)
            {
                schDaypilot.DataSource = dt;
                schDaypilot.DataBind();

                if (schDaypilot.Resources.Count > 0)
                {
                    DailyViewDate.Text = conf.ToLongDateString();
                    schDaypilot.Visible = true;
                }
                else
                {
                    DailyViewDate.Text = "";
                    schDaypilot.Visible = false;
                }
                schDaypilot.StartDate = conf;
				//ZD 102358

                if (officehrDaily.Checked)
                    schDaypilot.ShowNonBusiness = false;
                else
                    schDaypilot.ShowNonBusiness = true;
            }


        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
            log.Trace("ChangeCalendarDate: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    #endregion

    #region Cehck if String Contains

    private Boolean CehckifStringContains(String Roomsxml,String container)
    {
        bool rtrn = false;
        try
        {
            String[] strSplit = new String[1] { "~~" }; //FB 2012//ZD 100151//ZD 100263

            foreach (String xmls in Roomsxml.Split(strSplit, StringSplitOptions.RemoveEmptyEntries))
            {

                if (xmls != "")
                {
                    bypass = true;
                    if (xmls.Split(strSplitat, StringSplitOptions.RemoveEmptyEntries)[0] == container) //FB 2012
                    {
                        rtrn = true;
                        break;

                    }
                }
            }




        }
        catch (Exception ex)
        {

            throw ex;
        }

        return rtrn;

    }

    #endregion

    #region BeforeCellRenderhandler
    /// <summary>
    /// // FB 1860
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BeforeCellRenderhandler(object sender, DayPilot.Web.Ui.Events.BeforeCellRenderEventArgs e)
    {
        try
        {
            

            if (Session["Holidays"] != null)
            {
                if (Session["Holidays"].ToString().Trim() != "")
                {
                    dtCell = e.Start;
                    GetdayColour();

                    if (cellColor != "")
                        e.BackgroundColor = cellColor;

                }
            }



            /* DateTime cellTIme = e.Start;
             if (cellTIme.Day % 5 == 0)
                 e.BackgroundColor = "#EAA2D5";
             else if (cellTIme.Day % 7 == 0)
                 e.BackgroundColor = "#85EE98";*/

        }
        catch (Exception ex)
        {
            log.Trace("BeforeCellRenderhandler" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            errLabel.Visible = true;
        }
    }

    #endregion

    //FB 2804 - Start
    #region BeforeCellRenderhandlerClosedDate
    /// <summary>
    /// // FB 2804
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void BeforeCellRenderhandlerClosedDate(object sender, DayPilot.Web.Ui.Events.BeforeCellRenderEventArgs e)
    {
        try
        {
            if (selStatus.Value == "0")
            {
                e.IsBusiness = false;
                e.BackgroundColor = "#FFFFFF";                
            }
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("BeforeCellRenderhandlerClosedDate" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;
        }
    }
    #endregion
    //FB 2804 - End

    #region Get Day Colour
    /// <summary>
    /// FB 1860
    /// </summary>
    private void GetdayColour()
    {
        try
        {
            if (Session["Holidays"] != null)
            {
                if (Session["Holidays"].ToString().Trim() != "")
                {
                    cellColor = "";


                    XElement root = XElement.Parse(Session["Holidays"].ToString());
                    IEnumerable<XElement> hldys =
                        from hldyelmnts in root.Elements("Holiday")
                        where (string)hldyelmnts.Element("Date") == dtCell.ToString("MM/dd/yyyy") //FB 2052
                        select hldyelmnts;

                    foreach (XElement elmnts in hldys)
                    {
                        cellColor = (string)elmnts.Element("Color");
                        break;
                    }
                }
            }

        }
        catch (Exception ex)
        {

            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    # region bind weekly

    public void BindWeeklyXml()
    {
        String inXML = "";
        String outXML = "";
        String outXMLMonthly = "";
        String roomsselected = "";
        try
        {
            roomsselected = selectedList.Value;
            selectedList.Value = "";
           
            int isdeletedconf = 1;
			//ZD 102358
            List<DevExpress.Web.ASPxTreeList.TreeListNode> lst = treeRoomSelector.GetSelectedNodes();
            if (Enabletreeview)
            {
                for (int i = 0; i < lst.Count; i++)
                {
                    if (lst[i].Level == 4)
                    {

                        selectedList.Value += lst[i].Key.Replace("R", "") + ",";

                        //if (CehckifStringContains(HdnMonthlyXml.Value, lst[i].Key.Replace("R", ""))) ZD 103169
                        //    continue;

                        inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room>" + lst[i].Key.Replace("R", "") + "</room><isDeletedConf>" + isdeletedconf + "</isDeletedConf></calendarView>";
                        //ZD 100151
                        outXMLMonthly += "~~" + lst[i].Key.Replace("R", "") + "@@" + lst[i].GetValue("Name").ToString() + "@@" + obj.CallMyVRMServer("GetRoomWeeklyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027//ZD 100263

                    }
                }
            }
            //else
            //{
            //    HdnMonthlyXml.Value = "";
            //    if (selectedListRoomName.Value != null)
            //    {
            //        for (int i = 0; i < selectedListRoomName.Value.Split('�').Count(); i++)
            //        {
            //            inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room>" + selectedListRoomName.Value.Split('�')[i].Split('|')[0] + "</room><isDeletedConf>" + isdeletedconf + "</isDeletedConf></calendarView>";
            //            outXMLMonthly += "~~" + selectedListRoomName.Value.Split('�')[i].Split('|')[0] + "@@" + selectedListRoomName.Value.Split('�')[i].Split('|')[1] + "@@" + obj.CallMyVRMServer("GetRoomWeeklyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027//ZD 100263
            //        }
            //    }
            //}

            /*

            if (rdSelView.SelectedValue.Equals("1"))
            {
                if (Enabletreeview)
                {
                    foreach (TreeNode node in treeRoomSelection.CheckedNodes)
                    {
                        if (node.Depth == 3)
                        {
                            if (node.Value != "")
                            {
                                //if (!CehckifStringContains(selectedList.Value, ",", node.Value))
                                    selectedList.Value += node.Value + ",";

                                if (CehckifStringContains(roomsselected.Trim(), ",", node.Value))
                                    continue;
                                if (CehckifStringContains(HdnMonthlyXml.Value, node.Value))
                                    continue;

                                inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room>" + node.Value + "</room><isDeletedConf>" + isdeletedconf + "</isDeletedConf></calendarView>";
                                //ZD 100151
                                outXMLMonthly += "~~" + node.Value + "@@" + node.Text + "@@" + obj.CallMyVRMServer("GetRoomWeeklyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027//ZD 100263

                            }
                        }
                    }
                }
                else
                {
                    HdnMonthlyXml.Value = "";
                    if (selectedListRoomName.Value != null)
                    {
                        for (int i = 0; i < selectedListRoomName.Value.Split('�').Count(); i++)
                        {

                            inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room>" + selectedListRoomName.Value.Split('�')[i].Split('|')[0] + "</room><isDeletedConf>" + isdeletedconf + "</isDeletedConf></calendarView>";
                            outXMLMonthly += "~~" + selectedListRoomName.Value.Split('�')[i].Split('|')[0] + "@@" + selectedListRoomName.Value.Split('�')[i].Split('|')[1] + "@@" + obj.CallMyVRMServer("GetRoomWeeklyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027//ZD 100263
                        }
                    }
                }
            }
            else if (rdSelView.SelectedValue.Equals("2"))
            {
                if (Enabletreeview)
                {
                    foreach (ListItem lstItem in lstRoomSelection.Items)
                    {
                        if (lstItem.Selected)
                        {
                            if (lstItem.Value != "")
                            {
                                //if(!CehckifStringContains(selectedList.Value,",",lstItem.Value))
                                selectedList.Value += lstItem.Value + ",";

                                if (CehckifStringContains(roomsselected.Trim(), ",", lstItem.Value))
                                    continue;
                                if (CehckifStringContains(HdnMonthlyXml.Value, lstItem.Value))
                                    continue;

                                inXML = "<calendarView>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><date>" + conf.ToString("MM / dd / yyyy") + "</date><room>" + lstItem.Value + "</room><isDeletedConf>" + isdeletedconf + "</isDeletedConf></calendarView>";
                                //ZD 100151
                                outXMLMonthly += "~~" + lstItem.Value + "@@" + lstItem.Text.Trim().Split('>')[1].Split('<')[0] + "@@" + obj.CallMyVRMServer("GetRoomWeeklyView", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2012 //FB 2027//ZD 100263



                            }
                        }

                    }
                }
                else
                {
                    HdnMonthlyXml.Value = "";
                }
            }
             * */
            //outXMLMonthly += HdnMonthlyXml.Value; ZD 103169

            if (outXMLMonthly != "")
                HdnMonthlyXml.Value = outXMLMonthly;

            DataTable dt = DatatablereturnfromXML(HdnMonthlyXml.Value);


            if (dt != null)
            {
                schDaypilotweek.DataSource = dt;
                schDaypilotweek.DataBind();
                if (schDaypilotweek.Resources.Count > 0)
                {
                    schDaypilotweek.Visible = true;
                }
                else
                {
                    schDaypilotweek.Visible = false;
                }
                schDaypilotweek.StartDate = DayPilot.Utils.Week.FirstDayOfWeek(conf);
                schDaypilotweek.Days = 7;


                if (officehrWeek.Checked)
                {
                    schDaypilotweek.ShowNonBusiness = false;
                }
                else
                {
                    schDaypilotweek.ShowNonBusiness = true;
                }
            }

        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("BindWeeklyXml" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;
        }
    }

#endregion

    //ZD 100157 StartS
    private void SetCalOfficeHours(string xmls)
    {
        string hdr = obj.GetTranslatedText("Conference Details"), startHour = "00", startMin = "00", startSet = "AM", endHour = "23", endMin = "59", endSet = "PM", open24 = "";

        XmlDocument xmldoc = null;
        DataTable dt = null;
        DateTime sysSttime = DateTime.Now;

        try
        {
            xmldoc = new XmlDocument();
            xmldoc.LoadXml(xmls);

            dt = GetDataTable();

            if (xmldoc.DocumentElement.SelectSingleNode("RoomCalendar/ShowRoomHrs") != null)
                open24 = xmldoc.DocumentElement.SelectSingleNode("RoomCalendar/ShowRoomHrs").InnerText;

            if (open24 == "1")
            {
                officehrDaily.Checked = true;
                if (xmldoc.DocumentElement.SelectSingleNode("RoomCalendar/StartDateTime") != null)
                    startHour = xmldoc.DocumentElement.SelectSingleNode("RoomCalendar/StartDateTime").InnerText;

                DateTime.TryParse(startHour, out sysSttime);

                startHour = sysSttime.ToString("hh");
                startMin = sysSttime.ToString("mm");
                startSet = sysSttime.ToString("tt");


                if (xmldoc.DocumentElement.SelectSingleNode("RoomCalendar/EndDateTime") != null)
                    endHour = xmldoc.DocumentElement.SelectSingleNode("RoomCalendar/EndDateTime").InnerText;

                DateTime.TryParse(endHour, out sysSttime);

                endHour = sysSttime.ToString("hh");
                endMin = sysSttime.ToString("mm");
                endSet = sysSttime.ToString("tt");

                schDaypilot.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                schDaypilot.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                if (endMin != "00")
                    schDaypilot.BusinessEndsHour = schDaypilot.BusinessEndsHour + 1;

                schDaypilotweek.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                schDaypilotweek.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                if (endMin != "00")
                    schDaypilotweek.BusinessEndsHour = schDaypilotweek.BusinessEndsHour + 1;

                if (startHour == "12" && startSet == "AM" && endHour == "11" && endMin != "00" && endSet == "PM") // ZD 100284
                    officehrDaily.Checked = false;
            }
            else
            {
                if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24") != null)
                    open24 = xmldoc.DocumentElement.SelectSingleNode("systemAvail/open24").InnerText;

                if (open24 == "1")
                {
                    officehrDaily.Checked = false;

                    schDaypilot.BusinessBeginsHour = 0;
                    schDaypilot.BusinessEndsHour = 24;

                    schDaypilotweek.BusinessBeginsHour = 0;
                    schDaypilotweek.BusinessEndsHour = 24;
                }
                else
                {

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour") != null)
                        startHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startHour").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin") != null)
                        startMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startMin").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet") != null)
                        startSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/startTime/startSet").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour") != null)
                        endHour = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endHour").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin") != null)
                        endMin = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endMin").InnerText;

                    if (xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet") != null)
                        endSet = xmldoc.DocumentElement.SelectSingleNode("systemAvail/endTime/endSet").InnerText;

                    schDaypilot.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                    schDaypilot.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                    if (endMin != "00")
                        schDaypilot.BusinessEndsHour = schDaypilot.BusinessEndsHour + 1;

                    schDaypilotweek.BusinessBeginsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + startHour + ":" + startMin + " " + startSet).ToString("HH"));
                    schDaypilotweek.BusinessEndsHour = Convert.ToInt32(DateTime.Parse("06/06/2006 " + endHour + ":" + endMin + " " + endSet).ToString("HH"));

                    if (endMin != "00")
                        schDaypilotweek.BusinessEndsHour = schDaypilotweek.BusinessEndsHour + 1;

                    if (startHour == "12" && startSet == "AM" && endHour == "11" && endMin != "00" && endSet == "PM") // ZD 100284
                        officehrDaily.Checked = false;
                }
            }

            //ZD 100157 Starts
        }
        catch (Exception ex)
        {
            log.Trace("SetCalOfficeHours: " + ex.StackTrace + " : " + ex.Message);
        }
    }
    private string GetUserCalendarHrs()
    {
        try
        {
            string outXML = "";
            int Hour = 0, OffStartHr = 0;
            string EndTime = "";
            inXML = new StringBuilder();
            string PerStTime = DateTime.Now.ToShortDateString() + " 12:00:00 AM";
            string PerEndTime = DateTime.Now.ToShortDateString() + " 12:00:00 AM";
            DateTime sysSttime = DateTime.Now;
            DateTime CalStTime = DateTime.Now;
            inXML.Append("<GetCalendarTimes>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
            inXML.Append("</GetCalendarTimes>");
            outXML = obj.CallMyVRMServer("GetCalendarTimes", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

            if (outXML.IndexOf("<error>") < 0)
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = null;
                XmlNode node = (XmlNode)xmldoc.DocumentElement;
                node = xmldoc.SelectSingleNode("//GetCalendarTimes/RoomCalendar/ShowRoomHrs");
                if (node != null)
                {
                    int.TryParse(node.InnerText.Trim(), out isShowHrs);
                    if (isShowHrs.Equals(1))
                        officehrDaily.Checked = true;
                    else
                        officehrDaily.Checked = false;

                }
                if (isShowHrs == 1)
                {
                    node = xmldoc.SelectSingleNode("//GetCalendarTimes/RoomCalendar/StartTime");
                    if (node != null)
                    {
                        PerStTime = node.InnerText;
                        DateTime.TryParse(PerStTime, out sysSttime);
                        DateTime.TryParse(PerStTime, out CalStTime);
                        if (Session["timeFormat"].ToString() == "2")
                        {
                            myVRMNet.NETFunctions.ChangeTimeFormat(CalStTime.ToShortTimeString());
                            Hour = obj.GetHour(CalStTime.ToShortTimeString());

                            OffStartHr = obj.GetHour(Session["SystemStartTime"].ToString());

                            if (Hour >= OffStartHr)
                                lstStartHrs.Text = sysSttime.ToString(tFormats);
                            else
                                lstStartHrs.Text = lstStartHrs.Items[0].ToString();
                        }
                        else
                        {
                            ListItem tempLI = new ListItem(sysSttime.ToString(tFormats), sysSttime.ToString(tFormats));
                            if (lstStartHrs.Items.Contains(tempLI))
                                lstStartHrs.Text = sysSttime.ToString(tFormats);
                            else
                                lstStartHrs.Text = lstStartHrs.Items[0].ToString();
                        }
                    }
                    node = xmldoc.SelectSingleNode("//GetCalendarTimes/RoomCalendar/EndTime");
                    if (node != null)
                    {
                        PerEndTime = node.InnerText;
                        DateTime.TryParse(PerEndTime, out sysSttime);

                        if (Session["timeFormat"].ToString() == "2")
                        {
                            myVRMNet.NETFunctions.ChangeTimeFormat(PerEndTime);
                            Hour = obj.GetHour(PerEndTime);

                            OffStartHr = obj.GetHour(Session["SystemEndTime"].ToString());

                            if (Hour <= OffStartHr)
                                lstEndHrs.Text = sysSttime.ToString(tFormats);
                            else
                                lstEndHrs.Text = lstEndHrs.Items[lstEndHrs.Items.Count - 1].ToString();
                        }
                        else
                        {
                            ListItem tempLI = new ListItem(sysSttime.ToString(tFormats), sysSttime.ToString(tFormats));
                            if (lstEndHrs.Items.Contains(tempLI))//ZD 100284
                                lstEndHrs.Text = sysSttime.ToString(tFormats);
                            else
                                lstEndHrs.Text = lstEndHrs.Items[lstEndHrs.Items.Count - 1].ToString();
                        }
                    }
                }
                else
                {
                    lstStartHrs.Text = lstStartHrs.Items[0].ToString();
                    EndTime = lstEndHrs.Items[lstEndHrs.Items.Count - 1].ToString();
                    lstEndHrs.Text = lstEndHrs.Items[lstEndHrs.Items.Count - 1].ToString();
                    //lstEndHrs.Text = myVRMNet.NETFunctions.GetEndTime(EndTime, Session["timeFormat"].ToString());
                
                }
                return outXML;
            }
            return outXML;
        }
        catch (Exception ex)
        {
            return "";
        }
    }
    private void SetCalendarTImes()
    {
        try
        {
            string outxml = "";

            inXML = new StringBuilder();

            inXML.Append("<SetCalendarTimes>");
            inXML.Append(obj.OrgXMLElement());
            inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
            inXML.Append("<RoomCalendar>");
            if (officehrDaily.Checked)
                inXML.Append("<ShowRoomHrs>1</ShowRoomHrs>");
            else
                inXML.Append("<ShowRoomHrs>0</ShowRoomHrs>");
            inXML.Append("<StartTime>" + DateTime.Parse("06/06/2006 " + myVRMNet.NETFunctions.ChangeTimeFormat(lstStartHrs.Text)).ToShortTimeString() + "</StartTime>"); //DateTime.Parse("06/06/2006 " + myVRMNet.NETFunctions.ChangeTimeFormat(lstStartHrs.Text)).ToString("HH")
            inXML.Append("<EndTime>" + DateTime.Parse("06/06/2006 " + myVRMNet.NETFunctions.ChangeTimeFormat(lstEndHrs.Text)).ToShortTimeString() + "</EndTime>");
            inXML.Append("</RoomCalendar>");
            inXML.Append("</SetCalendarTimes>");
            outxml = obj.CallMyVRMServer("SetCalendarTimes", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
        }
        catch (Exception ex)
        {

        }
    }
    //ZD 100157 Ends
   
    // ZD 102358
    protected void selectionChanged(object sender, EventArgs e)
    {
        if (treeRoomSelector.FocusedNode.Key == "0")
        {
            //treeRoomSelector.UnselectAll();
            //treeRoomSelector.RefreshVirtualTree();
            //bool VisibleRoom = false;
            //BindData(ref VisibleRoom);
            //BindTreeList();
            treeRoomSelector.SelectAll();
            selectTiers();
            treeRoomSelector.SelectAll();
            selectRooms();
        }
        else
        {
            selectTiers();
            selectRooms();
        }
    }

    private void selectTiers()
    {
        try
        {
            List<DevExpress.Web.ASPxTreeList.TreeListNode> lst = treeRoomSelector.GetSelectedNodes();
            DataTable dtTiers = (DataTable)Session["dtTiers"];
            bool tierAffected = false; // NEWLY ADDED
            for (int j = 0; j < lst.Count; j++)
            {
                if (lst[j].Selected == true && lst[j].Level == 2)
                {
                    if (lst[j].ChildNodes[0].GetValue("Name").ToString() == "")
                    {
                        tierAffected = true; // NEWLY ADDED
                        String inXML = "<GetMiddleTiers>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString()
                            + "</userID><TopTierID>" + lst[j].Key + "</TopTierID></GetMiddleTiers>";
                        String xmlstr = obj.CallMyVRMServer("GetMiddleTiersByTopTier", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                        if (xmlstr.IndexOf("<error>") < 0)
                        {
                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.LoadXml(xmlstr);
                            if (xmldoc.SelectSingleNode("//GetMiddleTiers/MiddleTier") != null)
                            {
                                XmlNodeList xmlnodes = xmldoc.SelectNodes("//GetMiddleTiers/MiddleTier");
                                DataTable dtMiddleTiers = new DataTable();
                                ArrayList colNames = new ArrayList();
                                colNames.Add("ID");
                                colNames.Add("ParentID");
                                colNames.Add("Name");
                                dtMiddleTiers = obj.LoadDataTable(xmlnodes, colNames);
                                dtMiddleTiers.Columns.Add("ParentID", typeof(string));

                                for (int i = 0; i < dtMiddleTiers.Rows.Count; i++)
                                {
                                    dtMiddleTiers.Rows[i]["ParentID"] = lst[j].Key;
                                    dtMiddleTiers.Rows[i]["ID"] = "T" + dtMiddleTiers.Rows[i]["ID"].ToString();
                                }

                                dtTiers.Merge(dtMiddleTiers);

                            }
                            else
                            {
                                DataRow newRow = dtTiers.NewRow();
                                newRow["ID"] = "NT" + lst[j].Key; // NT - No Tier
                                newRow["ParentID"] = lst[j].Key;
                                newRow["Name"] = "No Tier";
                                dtTiers.Rows.Add(newRow);

                            }
                        }
                    }
                }
            }

            if (tierAffected) // NEWLY ADDED
            {
                Session["dtTiers"] = dtTiers;
                BindTreeList();
            }
        }
        catch(Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;
            log.Trace("selectTiers: " + ex.StackTrace + " : " + ex.Message);

        }
    }

    private void selectRooms()
    {
        try
        {
            DataTable dtTiers2 = (DataTable)Session["dtTiers"];
            List<DevExpress.Web.ASPxTreeList.TreeListNode> lst2 = treeRoomSelector.GetSelectedNodes();
            bool roomAffected = false; // NEWLY ADDED
            for (int j = 0; j < lst2.Count; j++)
            {
                if (lst2[j].Selected == true && lst2[j].Level == 3)
                {
                    if (lst2[j].ChildNodes[0].GetValue("Name").ToString() == "")
                    {
                        roomAffected = true; // NEWLY ADDED
                        String inXML = "<GetRoomsByTiers>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString()
                            + "</userID><TopTierID>" + lst2[j].ParentNode.Key + "</TopTierID><MiddleTierID>" + lst2[j].Key.Replace("T", "") + "</MiddleTierID></GetRoomsByTiers>";
                        String xmlstr = obj.CallMyVRMServer("GetRoomsByTiers", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        if (xmlstr.IndexOf("<error>") < 0)
                        {
                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.LoadXml(xmlstr);
                            if (xmldoc.SelectSingleNode("//GetRoomsByTiers/Room") != null)
                            {
                                XmlNodeList xmlnodes = xmldoc.SelectNodes("//GetRoomsByTiers/Room");
                                DataTable dtRooms = new DataTable();
                                ArrayList colNames = new ArrayList();
                                colNames.Add("ID");
                                colNames.Add("ParentID");
                                colNames.Add("Name");
                                dtRooms = obj.LoadDataTable(xmlnodes, colNames);
                                dtRooms.Columns.Add("ParentID", typeof(string));
                                for (int i = 0; i < dtRooms.Rows.Count; i++)
                                {
                                    dtRooms.Rows[i]["ParentID"] = lst2[j].Key;
                                    dtRooms.Rows[i]["ID"] = "R" + dtRooms.Rows[i]["ID"].ToString();
                                }
                                dtTiers2.Merge(dtRooms);
                            }
                            else
                            {
                                DataRow newRow = dtTiers2.NewRow();
                                newRow["ID"] = "NR" + lst2[j].Key; // NR - No Room
                                newRow["ParentID"] = lst2[j].Key;
                                newRow["Name"] = "No Room";
                                dtTiers2.Rows.Add(newRow);
                            }
                        }
                    }
                }
            }

            if (roomAffected) // NEWLY ADDED
            {
                Session["dtTiers"] = dtTiers2;
                BindTreeList();
            }
        }
        catch (Exception ex)
        {
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;
            log.Trace("selectRooms: " + ex.StackTrace + " : " + ex.Message);
        }
    }

    private void selectRoomByValue(string selec)
    {
        bool VisibleRoom = false;
        //selec = "3:3:32;1:1:30";
        //string[] selecArray = selec.Split(';').ToArray();
        string[] selecArray = selec.Split(',').ToArray();
        string node = string.Empty;
        //for (int i = 0; i < selecArray.Length; i++)
        //{
        //    node = selecArray[i].Split(':')[0];
        //    if (treeRoomSelector.FindNodeByKeyValue(node) != null)
        //        treeRoomSelector.FindNodeByKeyValue(node).Selected = true;
        //}
        //selectTiers();

        //for (int i = 0; i < selecArray.Length; i++)
        //{
        //    node = "T" + selecArray[i].Split(':')[1];
        //    if (treeRoomSelector.FindNodeByKeyValue(node) != null)
        //        treeRoomSelector.FindNodeByKeyValue(node).Selected = true;
        //}

        //selectRooms();
        //ZD 103092 START
        treeRoomSelector.SelectAll();
        selectTiers();
        treeRoomSelector.SelectAll();
        selectRooms();
        //ZD 1030920 END
        treeRoomSelector.UnselectAll();        

        //BindData(ref VisibleRoom);

        String[] strRooms = selectedList.Value.Split(',');

        for (int i = 0; i < strRooms.Length; i++)
        {
            node = "R" + strRooms[i];
            if (treeRoomSelector.FindNodeByKeyValue(node) != null)
                treeRoomSelector.FindNodeByKeyValue(node).Selected = true;
        }

    }

    protected void nodeExpanding(object sender, DevExpress.Web.ASPxTreeList.TreeListNodeEventArgs e)
    {
        List<DevExpress.Web.ASPxTreeList.TreeListNode> lst = treeRoomSelector.GetSelectedNodes();
        string str = "";
        //selectedList.Value = "";
        //for (int i = 0; i < lst.Count; i++)
        //{
        //    if (lst[i].Level == 4)
        //    {
        //        String strValue = lst[i].Key.Replace("R", "") + ",";
        //        if (selectedList.Value.IndexOf(strValue) < 0)
        //            selectedList.Value += lst[i].Key.Replace("R", "") + ",";
        //    }
        //}

        if (e.Node.Level == 2 && e.Node.ChildNodes[0].GetValue("Name").ToString() == "")
        {
            String inXML = "<GetMiddleTiers>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString()
                + "</userID><TopTierID>" + e.Node.Key + "</TopTierID></GetMiddleTiers>";
            String xmlstr = obj.CallMyVRMServer("GetMiddleTiersByTopTier", inXML, Application["MyVRMServer_ConfigPath"].ToString());
            
            if (xmlstr.IndexOf("<error>") < 0)
            {
                DataTable dtTiers = (DataTable)Session["dtTiers"];

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlstr);
                if (xmldoc.SelectSingleNode("//GetMiddleTiers/MiddleTier") != null)
                {
                    
                    XmlNodeList xmlnodes = xmldoc.SelectNodes("//GetMiddleTiers/MiddleTier");

                    DataTable dtMiddleTiers = new DataTable();
                    ArrayList colNames = new ArrayList();
                    colNames.Add("ID");
                    colNames.Add("ParentID");
                    colNames.Add("Name");
                    dtMiddleTiers = obj.LoadDataTable(xmlnodes, colNames);
                    dtMiddleTiers.Columns.Add("ParentID", typeof(string));

                    for (int i = 0; i < dtMiddleTiers.Rows.Count; i++)
                    {
                        dtMiddleTiers.Rows[i]["ParentID"] = e.Node.Key;
                        dtMiddleTiers.Rows[i]["ID"] = "T" + dtMiddleTiers.Rows[i]["ID"].ToString();

                    }

                    dtTiers.Merge(dtMiddleTiers);
                }
                else
                {
                    DataRow newRow = dtTiers.NewRow();
                    newRow["ID"] = "NT" + e.Node.Key; // NT - No Tier
                    newRow["ParentID"] = e.Node.Key;
                    newRow["Name"] = "No Tier";
                    dtTiers.Rows.Add(newRow);

                }
                Session["dtTiers"] = dtTiers;

                BindTreeList();
            }
        }
        else if (e.Node.Level == 3 && e.Node.ChildNodes[0].GetValue("Name").ToString() == "")
        {
            String inXML = "<GetRoomsByTiers>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString()
                + "</userID><TopTierID>" + e.Node.ParentNode.Key + "</TopTierID><MiddleTierID>" + e.Node.Key.Replace("T", "") + "</MiddleTierID></GetRoomsByTiers>";
            String xmlstr = obj.CallMyVRMServer("GetRoomsByTiers", inXML, Application["MyVRMServer_ConfigPath"].ToString());

            if (xmlstr.IndexOf("<error>") < 0)
            {
                DataTable dtTiers = (DataTable)Session["dtTiers"];

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlstr);
                if (xmldoc.SelectSingleNode("//GetRoomsByTiers/Room") != null)
                {
                    XmlNodeList xmlnodes = xmldoc.SelectNodes("//GetRoomsByTiers/Room");
                    DataTable dtRooms = new DataTable();
                    ArrayList colNames = new ArrayList();
                    colNames.Add("ID");
                    colNames.Add("ParentID");
                    colNames.Add("Name");
                    dtRooms = obj.LoadDataTable(xmlnodes, colNames);
                    dtRooms.Columns.Add("ParentID", typeof(string));

                    for (int i = 0; i < dtRooms.Rows.Count; i++)
                    {
                        dtRooms.Rows[i]["ParentID"] = e.Node.Key;
                        dtRooms.Rows[i]["ID"] = "R" + dtRooms.Rows[i]["ID"].ToString();
                    }
                    
                    dtTiers.Merge(dtRooms);
                }
                else
                {
                    DataRow newRow = dtTiers.NewRow();
                    newRow["ID"] = "NR" + e.Node.Key; // NR - No Room
                    newRow["ParentID"] = e.Node.Key;
                    newRow["Name"] = "No Room";
                    dtTiers.Rows.Add(newRow);
                }

                Session["dtTiers"] = dtTiers;
                BindTreeList();
            }
        }
    }
}
