/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;
using NHibernate.Cache;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Data Access Object for Users.
    /// Implement IDispose to close session
    /// </summary>
    public class userDAO
    {
        private static Hashtable userDAOFactories = null;
        //
        // TODO: Add constructor logic here
        //
        private IUserDao m_userDAO;
        private IUserRolesDao m_userRolesDAO;
        private IUserLotusDao m_userlotusDAO; //FB 2027
        private IUserTemplateDao m_userTempDAO;
        private IUserSearchTemplateDao m_userSearchTempDAO;
        private IVRMLdapUserDao m_LDAPUserDAO;
        private IUserAccountDao m_UserAccountDAO;
        private IEmailDao m_emailDAO;
        private IAccountGroupListDAO m_GroupAccountDAO; //FB 2027
        private IUserTokenDAO m_userTokenDAO; //ZD 100152
        //  email is a private interface specific to user
        private IUserDeleteListDAO m_userDeleteListDAO; //ZD 103878

        private log4net.ILog m_log;
        private string m_configPath;

        private int m_maxresultsreturned;
        private int m_pageno;
        private int m_pagesize;
        private bool m_pageenabled;

        public int maxResultsReturned
        {
            get { return m_maxresultsreturned; }
            set { m_maxresultsreturned = value; }
        }
        public int pageNo
        {
            get { return m_pageno; }
            set { m_pageno = value; m_userDAO.pageNo(m_pageno); }
        }
        public int pageSize
        {
            get { return m_pagesize; }
            set { m_pagesize = value; m_userDAO.pageSize(m_pagesize); }
        }
        public bool pageEnabled
        {
            get { return m_pageenabled; }
            set { m_pageenabled = value; }
        }

        public userDAO(string config, log4net.ILog log)
        {
            try
            {
                m_log = log;
                m_configPath = config;

                bool valid = Init();
                if (!valid)
                    throw new Exception("Could not initialize ReportDAO");

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        private bool Init()
        {
            try
            {
                if (userDAOFactories != null)
                {

                    if (userDAOFactories["m_userDAO"] != null)
                        m_userDAO = (vrmUserDAO)userDAOFactories["m_userDAO"];
                    else
                        m_userDAO = new vrmUserDAO(m_configPath);

                    if (userDAOFactories["m_userRolesDAO"] != null)
                        m_userRolesDAO = (userRolesDAO)userDAOFactories["m_userRolesDAO"];
                    else
                        m_userRolesDAO = new userRolesDAO(m_configPath);
                    if (userDAOFactories["m_userTempDAO"] != null)
                        m_userTempDAO = (vrmUserTemplateDAO)userDAOFactories["m_userTempDAO"];
                    else
                        m_userTempDAO = new vrmUserTemplateDAO(m_configPath);
                    if (userDAOFactories["m_userSearchTempDAO"] != null)
                        m_userSearchTempDAO = (vrmUserSearchTemplateDAO)userDAOFactories["m_userSearchTempDAO"];
                    else
                        m_userSearchTempDAO = new vrmUserSearchTemplateDAO(m_configPath);

                    if (userDAOFactories["m_LDAPUserDAO"] != null)
                        m_LDAPUserDAO = (vrmLDAPuserDAO)userDAOFactories["m_LDAPUserDAO"];
                    else
                        m_LDAPUserDAO = new vrmLDAPuserDAO(m_configPath);

                    if (userDAOFactories["m_UserAccountDAO"] != null)
                        m_UserAccountDAO = (UserAccountDAO)userDAOFactories["m_UserAccountDAO"];
                    else
                        m_UserAccountDAO = new UserAccountDAO(m_configPath);

                    if (userDAOFactories["m_emailDAO"] != null)
                        m_emailDAO = (vrmUserEmailDAO)userDAOFactories["m_emailDAO"];
                    else
                        m_emailDAO = new vrmUserEmailDAO(m_configPath);
                    if (userDAOFactories["m_GroupAccountDAO"] != null)  //FB 2027
                        m_GroupAccountDAO = (vrmAccountGroupDAO)userDAOFactories["m_GroupAccountDAO"];
                    else
                        m_GroupAccountDAO = new vrmAccountGroupDAO(m_configPath);
					//ZD 100152 Starts
                    if (userDAOFactories["m_userTokenDAO"] != null) 
                        m_userTokenDAO = (UserTokenDAO)userDAOFactories["m_userTokenDAO"];
                    else
                        m_userTokenDAO = new UserTokenDAO(m_configPath);
					//ZD 100152 Ends 

                    //ZD 103878
                    if (userDAOFactories["m_userDeleteListDAO"] != null)
                        m_userDeleteListDAO = (vrmUserDeleteListDAO)userDAOFactories["m_userDeleteListDAO"];
                    else
                        m_userDeleteListDAO = new vrmUserDeleteListDAO(m_configPath);
                }

                if (userDAOFactories == null)
                {

                    //userDAOFactories = new Hashtable(7);//FB 2027
                    userDAOFactories = new Hashtable();
                    m_userDAO = new vrmUserDAO(m_configPath);
                    userDAOFactories.Add("m_userDAO", m_userDAO);
                    m_userRolesDAO = new userRolesDAO(m_configPath);
                    userDAOFactories.Add("m_userRolesDAO", m_userRolesDAO);
                    m_userlotusDAO = new userLotusDAO(m_configPath); //FB 2027
                    userDAOFactories.Add("m_userlotusDAO", m_userlotusDAO);
                    m_userTempDAO = new vrmUserTemplateDAO(m_configPath);
                    userDAOFactories.Add("m_userTempDAO", m_userTempDAO);
                    m_userSearchTempDAO = new vrmUserSearchTemplateDAO(m_configPath);
                    userDAOFactories.Add("m_userSearchTempDAO", m_userSearchTempDAO);
                    m_LDAPUserDAO = new vrmLDAPuserDAO(m_configPath);
                    userDAOFactories.Add("m_LDAPUserDAO", m_LDAPUserDAO);
                    m_UserAccountDAO = new UserAccountDAO(m_configPath);
                    userDAOFactories.Add("m_UserAccountDAO", m_UserAccountDAO);
                    m_emailDAO = new vrmUserEmailDAO(m_configPath);
                    userDAOFactories.Add("m_emailDAO", m_emailDAO);
                    m_GroupAccountDAO = new vrmAccountGroupDAO(m_configPath); //FB 2027
                    userDAOFactories.Add("m_GroupAccountDAO", m_GroupAccountDAO);
                }

                m_pagesize = m_userDAO.getPageSize();

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        public IUserDao GetUserDao() { return new vrmUserDAO(m_configPath); }
        public IUserLotusDao GetUserLotusDao() { return new userLotusDAO(m_configPath); }//FB 2027
        public IGuestUserDao GetGuestUserDao() { return new vrmGuestUserDAO(m_configPath); }
        public IInactiveUserDao GetInactiveUserDao() { return new vrmInactiveUserDAO(m_configPath); }

        public IEmailDao GetUserEmailDao() { return new vrmUserEmailDAO(m_configPath); }

        public class vrmUserEmailDAO :
                AbstractPersistenceDao<vrmEmail, int>, IEmailDao
        { public vrmUserEmailDAO(string ConfigPath) : base(ConfigPath) { } }

        public IGrpDetailDao GetGrpDetailDao() { return new vrmGrpDetailDAO(m_configPath); }
		public ILanguageTextDao GetLanguageTextDao() { return new vrmLanguageTextDAO(m_configPath); } //FB 1830 - Translation

        public class vrmGrpDetailDAO :
                AbstractPersistenceDao<vrmGroupDetails, int>, IGrpDetailDao
        { public vrmGrpDetailDAO(string ConfigPath) : base(ConfigPath) { } }
		//FB 1830 - Translation - Start 
        public class vrmLanguageTextDAO :
                AbstractPersistenceDao<vrmLanguageText, int>, ILanguageTextDao
        { public vrmLanguageTextDAO(string ConfigPath) : base(ConfigPath) { }

        public List<vrmLanguageText>  GetByLanguageId(int id)
        {
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("LanguageID", id));
                List<vrmLanguageText> myVrmLanguageText = GetByCriteria(criterionList);
                if (myVrmLanguageText.Count <= 0)
                    return null;
                return myVrmLanguageText;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        }
		//FB 1830 - Translation - End
        public IGrpParticipantsDao GetGrpParticipantDAO() { return new vrmGrpParticipantDAO(m_configPath); }

        public class vrmGrpParticipantDAO :
                AbstractPersistenceDao<vrmGroupParticipant, int>, IGrpParticipantsDao
        { public vrmGrpParticipantDAO(string ConfigPath) : base(ConfigPath) { } }

        public class vrmUserDAO :
                AbstractPersistenceDao<vrmUser, int>, IUserDao
        {
            public vrmUserDAO(string ConfigPath) : base(ConfigPath) { }
            public vrmUser GetByUserId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userid", id));
                    List<vrmUser> myVrmUser = GetByCriteria(criterionList);
                    //Code Modified For FB 1462 - Start
                    if (myVrmUser.Count <= 0)   
                        return null;    
                        //throw new Exception("User id not found");
                    //Code Modified For FB 1462 - End
                    return myVrmUser[0];
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            //FB 2342 - Start
            public vrmUser GetByUserEmail(String emailID)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("Email", emailID));
                    List<vrmUser> myVrmUser = GetByCriteria(criterionList);
                    if (myVrmUser.Count <= 0)
                        return null;
                    
                    return myVrmUser[0];
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            //FB 2342 - End
            //ZD 100263 - Start
            public vrmUser GetByUserRequestID(String RequestID)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("RequestID", RequestID));
                    List<vrmUser> myVrmUser = GetByCriteria(criterionList);
                    if (myVrmUser.Count <= 0)
                        return null;

                    return myVrmUser[0];
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            //ZD 100263 - End
            //FB 2724 - Start
            public vrmUser GetByRFIDValue(String RFID)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("RFIDValue", RFID));
                    List<vrmUser> myVrmUser = GetByCriteria(criterionList);
                    if (myVrmUser.Count <= 0)
                        return null;

                    return myVrmUser[0];
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            //FB 2724 - End
        }
        public class vrmGuestUserDAO :
             AbstractPersistenceDao<vrmGuestUser, int>, IGuestUserDao
        {
            public vrmGuestUserDAO(string ConfigPath) : base(ConfigPath) { }
            public vrmGuestUser GetByUserId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userid", id));
                    List<vrmGuestUser> myVrmUser = GetByCriteria(criterionList);

                    //FB 2318 - Start
                    if (myVrmUser.Count != 1)
                        return null;
                        //throw new Exception("User id not found");
                    //FB 2318 - End

                    return myVrmUser[0];
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        public class vrmInactiveUserDAO :
          AbstractPersistenceDao<vrmInactiveUser, int>, IInactiveUserDao
        {
            public vrmInactiveUserDAO(string ConfigPath) : base(ConfigPath) { }
            public vrmInactiveUser GetByUserId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userid", id));
                    List<vrmInactiveUser> myVrmUser = GetByCriteria(criterionList);
                    //Code Modified For FB 1462 - Start
                    if (myVrmUser.Count <= 0)   
                        return null;    
                        //throw new Exception("User id not found");
                    //Code Modified For FB 1462 - End
                    return myVrmUser[0];
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }

        public IUserTemplateDao GetUserTemplateDao() { return new vrmUserTemplateDAO(m_configPath); }

        public class vrmUserTemplateDAO :
                AbstractPersistenceDao<vrmUserTemplate, int>, IUserTemplateDao
        {
            public vrmUserTemplateDAO(string ConfigPath) : base(ConfigPath) { }

        }

        public IUserSearchTemplateDao GetUserSearchTemplateDao() { return new vrmUserSearchTemplateDAO(m_configPath); }

        public class vrmUserSearchTemplateDAO :
                AbstractPersistenceDao<vrmUserSearchTemplate, int>, IUserSearchTemplateDao
        {
            public vrmUserSearchTemplateDAO(string ConfigPath) : base(ConfigPath) { }

        }

        public IUserRolesDao GetUserRolesDao() { return new userRolesDAO(m_configPath); }

        public class userRolesDAO :
                AbstractPersistenceDao<vrmUserRoles, int>, IUserRolesDao
        {
            public userRolesDAO(string ConfigPath) : base(ConfigPath) { }

        }
        //FB 2027 Start
        public class userLotusDAO :
                AbstractPersistenceDao<vrmUserLotusNotesPrefs, int>, IUserLotusDao
        {
            public userLotusDAO(string ConfigPath) : base(ConfigPath) { }
        }
        //FB 2027 End
        public IUserAccountDao GetUserAccountDao() { return new UserAccountDAO(m_configPath); }

        public class UserAccountDAO :
                AbstractPersistenceDao<vrmAccount, int>, IUserAccountDao
        {
            public UserAccountDAO(string ConfigPath) : base(ConfigPath) { }
            public vrmAccount GetByUserId(int id) //FB 2027 - Starts
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userId", id));
                    List<vrmAccount> myVrmUser = GetByCriteria(criterionList);
                    if (myVrmUser.Count <= 0)
                        return null;
                    return myVrmUser[0];
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            //FB 2027 - End
        }
        public IVRMLdapUserDao GetLDAPUserDao() { return new vrmLDAPuserDAO(m_configPath); }

        public class vrmLDAPuserDAO :
                AbstractPersistenceDao<vrmLDAPUser, int>, IVRMLdapUserDao
        {
            public vrmLDAPuserDAO(string ConfigPath) : base(ConfigPath) { }

        }
		
		//NewLobby start
        public IUserLobbyDAO GetUserLobbyIconsDAO()
        {
            return new UserLobbyDAO(m_configPath);
        }
        public class UserLobbyDAO : AbstractPersistenceDao<vrmUserLobby, int>, IUserLobbyDAO
        {
            public UserLobbyDAO(string ConfigPath) : base(ConfigPath) { }
        }
        //NewLobby end

        //FB 2027 - Starts
        public IAccountGroupListDAO GetGroupAccountDao() { return new vrmAccountGroupDAO(m_configPath); }

        public class vrmAccountGroupDAO :
            AbstractPersistenceDao<vrmAccountGroupList, int>, IAccountGroupListDAO
        {
            public vrmAccountGroupDAO(string ConfigPath) : base(ConfigPath) { }
        }
        //FB 2027 - End
        public bool GetUserCount(ref int count)
        {
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();

                m_userDAO.addProjection(Projections.RowCount());

                IList list = m_userDAO.GetObjectByCriteria(criterionList);
                count = Int32.Parse(list[0].ToString());
                m_userDAO.clearProjection();
            }
            catch (Exception e)
            {
                m_log.Error(" Error = " + e.Message);
                return (false);
            }
            return true;
        }
        //Added for License Modification START
        public bool GetExcUserCount(ref int count)
        {
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("enableExchange", 1));
                
                m_userDAO.addProjection(Projections.RowCount());

                IList list = m_userDAO.GetObjectByCriteria(criterionList);
                count = Int32.Parse(list[0].ToString());
                m_userDAO.clearProjection();
            }
            catch (Exception e)
            {
                m_log.Error(" Error = " + e.Message);
                return (false);
            }
            return true;
        }
        //Added for License Modification END

        public bool GetMultipleLDAP(ArrayList ldapID, List<vrmLDAPUser> LDAPRecords)
        {
            try
            {
                for (int i = 0; i < ldapID.Count; i++)
                {
                    vrmLDAPUser LDAPUser = m_LDAPUserDAO.GetById((int)ldapID[i]);
                    LDAPRecords.Add(LDAPUser);
                }
            }
            catch (Exception e)
            {
                m_log.Error(" Error = " + e.Message);
                return (false);
            }
            return true;
        }
        public bool SetMutipleUsers(List<vrmUser> UserRecords, List<vrmLDAPUser> LDAPRecords)
        {
            try
            {
                vrmAccount account;
                List<vrmAccount> userAccount = new List<vrmAccount>();

                DateTime today = DateTime.Now;
                // long key = m_userDAO.BeginTransaction();
                // m_userDAO.SaveOrUpdateList(key, UserRecords);
                // m_LDAPUserDAO.SaveOrUpdateList(key, LDAPRecords);
                // m_UserAccountDAO.SaveOrUpdateList(key, userAccount);
                //m_userDAO.Commit(key);
                m_userDAO.SaveOrUpdateList(UserRecords);
                // FB: 528 AG 07-11-08 remove ldap record
                foreach (vrmLDAPUser lr in LDAPRecords)
                    if(lr.newUser == 0)
                        m_LDAPUserDAO.Delete(lr);
                foreach (vrmUser user in UserRecords)
                {
                    account = new vrmAccount();

                    account.userId = user.userid;
                    account.TotalTime = user.initialTime;
                    account.TimeRemaining
                                      = user.initialTime;
                    account.UsedMinutes = 0; //ZD 102043
                    account.ScheduledMinutes = 0; //ZD 102043
                    account.InitTime = today;
                    account.ExpirationTime = today.AddYears(10);
                    m_UserAccountDAO.Save(account);
                }

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
            return true;
        }
        public bool SetMutipleLDAPUsers(List<vrmUser> UserRecords) //LDAP Fixes
        {
            try
            {
                vrmAccount account;
                List<vrmAccount> userAccount = new List<vrmAccount>();

                DateTime today = DateTime.Now;

                m_userDAO.SaveOrUpdateList(UserRecords);

                foreach (vrmUser user in UserRecords)
                {
                    account = new vrmAccount();

                    account.userId = user.userid;
                    account.TotalTime = user.initialTime;
                    account.TimeRemaining
                                      = user.initialTime;
                    account.UsedMinutes = 0; //ZD 102043
                    account.ScheduledMinutes = 0; //ZD 102043
                    account.InitTime = today;
                    account.ExpirationTime = today.AddYears(10);
                    m_UserAccountDAO.Save(account);
                }

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
            return true;
        }

        // this will be obsolete when all 3 user tables are merged and userid is the identity. 
        public int GetID()
        {
            try
            {
                string stmt = "SELECT MAX(usr.userid) FROM vrmGuestUser usr";
                IList result = m_userDAO.execQuery(stmt);

                int id = 0;
                if (result[0] != null)
                    id = (int)result[0];                

                stmt = "SELECT MAX(usr.userid) FROM vrmUser usr";
                result = m_userDAO.execQuery(stmt);

                for (int i = 0; i < result.Count; i++)
                {
                    if (result[i] != null)
                    {
                        if (id < (int)result[i])
                            id = (int)result[i];
                    }
                }
                //Added for License Modification Start
                stmt = "SELECT MAX(usr.userid) FROM vrmInactiveUser usr";
                result = m_userDAO.execQuery(stmt);

                for (int i = 0; i < result.Count; i++)
                {
                    if (result[i] != null)
                    {
                        if (id < (int)result[i])
                            id = (int)result[i];
                    }
                }
                //Added for License Modification END
                return ++id;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        public bool SaveUserEmail(vrmEmail email)
        {
            try
            {
                email.UUID = getEmailUUID();
                m_emailDAO.Save(email);
                
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
            return true;
        }
        public int getEmailUUID()
        {
            try
            {
                IEmailDao emailDao = GetUserEmailDao();
                string qString = "select max(em.UUID) from myVRM.DataLayer.vrmEmail em";

                IList list = emailDao.execQuery(qString);

                string sMax;
                if (list[0] != null)
                    sMax = list[0].ToString();
                else
                    sMax = "0";

                int id = Int32.Parse(sMax);
                return ++id;
            }
            catch (Exception e)
            {
                m_log.Error("Cannot save email", e);
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        public bool checkUser(vrmUser user)
        {
            try
            {
                string hql;

                //FB 1644
                hql = " SELECT  R.RoomID, R.Disabled FROM myVRM.DataLayer.vrmRoom R WHERE Disabled = 0 and assistant = " + user.userid.ToString();
                IList results = m_userDAO.execQuery(hql);
                if (results.Count > 0)
                {
                    return false;
                }
                hql = " SELECT M.approverid FROM myVRM.DataLayer.vrmMCUApprover M WHERE approverid = " + user.userid.ToString();
                results = m_userDAO.execQuery(hql);
                if (results.Count > 0)
                {
                    return false;
                }
                hql = " SELECT D.approverid FROM myVRM.DataLayer.vrmDeptApprover D WHERE approverid = " + user.userid.ToString();
                results = m_userDAO.execQuery(hql);
                if (results.Count > 0)
                {
                    return false;
                }
                hql = " SELECT L.approverid FROM myVRM.DataLayer.vrmLocApprover L WHERE approverid = " + user.userid.ToString();
                results = m_userDAO.execQuery(hql);
                if (results.Count > 0)
                {
                    return false;
                }
                hql = " SELECT W.AdminID FROM myVRM.DataLayer.WorkOrder W WHERE AdminID = " + user.userid.ToString();
                results = m_userDAO.execQuery(hql);
                if (results.Count > 0)
                {
                    return false;
                }
                hql = " SELECT C.AdminID FROM myVRM.DataLayer.InventoryCategory C WHERE AdminID = " + user.userid.ToString();
                results = m_userDAO.execQuery(hql);
                if (results.Count > 0)
                {
                    return false;
                }
                // Code Added For FB 1462 - Start
                hql = " SELECT M.Admin FROM myVRM.DataLayer.vrmMCU M WHERE Admin = " + user.userid.ToString();
                results = m_userDAO.execQuery(hql);
                if (results.Count > 0)
                {
                    return false;
                }
                // Code Added For FB 1462 - End
                // Code Added For FB 1729 - Start
                hql = " SELECT A.approverid FROM  myVRM.DataLayer.sysApprover A WHERE approverid = " + user.userid.ToString();
                results = m_userDAO.execQuery(hql);
                if (results.Count > 0)
                {
                    return false;
                }
                // Code Added For FB 1729 - End
                // Code Added FB 1754
                hql = " SELECT A.UserID FROM  myVRM.DataLayer.vrmGroupParticipant A WHERE UserID = " + user.userid.ToString();
                results = m_userDAO.execQuery(hql);
                if (results.Count > 0)
                {
                    return false;
                }
                //End FB 1754
                return true;
              
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        //FB 2693 Starts
        public IUserPCDAO GetUserPCDAO()
        {
            return new UserPCDAO(m_configPath);
        }

        public class UserPCDAO : AbstractPersistenceDao<vrmUserPC, int>, IUserPCDAO
        {
            public UserPCDAO(string ConfigPath) : base(ConfigPath) { }
        }
        //FB 2693 Ends
        //ZD 100152 Starts
        public IUserTokenDAO GetUserTokenDAO()
        {
            return new UserTokenDAO(m_configPath);
        }

        public class UserTokenDAO : AbstractPersistenceDao<vrmUserToken, int>, IUserTokenDAO
        {
            public UserTokenDAO(string ConfigPath) : base(ConfigPath) { }
            public vrmUserToken GetByUserId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("userid", id));
                    List<vrmUserToken> myVrmUser = GetByCriteria(criterionList);
                    if (myVrmUser.Count <= 0)
                        return null;
                    return myVrmUser[0];
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
        //ZD 100152 Ends

        //ZD 103878
        public IUserDeleteListDAO GetUserDeleteListDAO()
        {
            return new vrmUserDeleteListDAO(m_configPath);
        }

        public class vrmUserDeleteListDAO : AbstractPersistenceDao<vrmUserDeleteList, int>, IUserDeleteListDAO
        {
            public vrmUserDeleteListDAO(string ConfigPath) : base(ConfigPath) { }
            public vrmUserDeleteList GetByuId(int id)
            {
                try
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("id", id));
                    List<vrmUserDeleteList> myVrmUser = GetByCriteria(criterionList);
                    if (myVrmUser.Count <= 0)
                        return null;
                    return myVrmUser[0];
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
        }
       
    }
}
