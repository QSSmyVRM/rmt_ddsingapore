/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;

namespace myVRM.DataLayer
{
    /// <summary>
    /// Data Access Object for Searches .
    /// Implement IDispose to close session
    /// </summary>
    public class LocationDAO  
    {
        private ISession session;
        private log4net.ILog m_log;
        private string m_configPath;

        public LocationDAO(string config, log4net.ILog olog)
        {
            try
            {
                m_log = olog;
                m_configPath = config;

                bool valid = Init();
                if (!valid)
                    throw new Exception("Could not initialize SearchDAO");

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        private bool Init()
        {
            try
            {
                session = SessionManagement.GetSession(m_configPath);
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        public IT3RoomDAO GetT3RoomDAO()
        {
            return new T3RoomDAO(m_configPath);
        }
        public IT2RoomDAO GetT2RoomDAO()
        {
            return new T2RoomDAO(m_configPath);
        }
        public IRoomDAO GetRoomDAO()
        {
            return new RoomDAO(m_configPath);
        }
        public ILocDeptDAO GetLocDeptDAO()
        {
            return new LocDeptDAO(m_configPath);
        }
        public ILocApprovDAO GetLocApprovDAO()
        {
            return new LocApprovDAO(m_configPath);
        }
        public IESPublicRoomDAO GetESPublicRoomDAO() //FB 2392 -WhyGo
        {
            return new ESPublicRoomDAO(m_configPath);
        }
        //ZD 101527 Starts
        public ISyncLocDAO GetSyncLocDAO()
        {
            return new SyncLocDAO(m_configPath);
        }

        //ZD 101527 Ends
        //ZD 102123 Starts
        public IFloorRoomDAO GetFloorRoomDAO()
        {
            return new FloorRoomDAO(m_configPath);
        }

        //ZD 102123 Ends
        public class T3RoomDAO :
          AbstractPersistenceDao<vrmTier3, int>, IT3RoomDAO
        {
            public T3RoomDAO(string ConfigPath) : base(ConfigPath) { }
            public new List<vrmTier3>  GetActive()
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("disabled", 0));
                return GetByCriteria(criterionList);
            }
        }
        public class T2RoomDAO :
            AbstractPersistenceDao<vrmTier2, int>, IT2RoomDAO
        {
            public T2RoomDAO(string ConfigPath) : base(ConfigPath) { }
            public new List<vrmTier2> GetActive()
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("disabled", 0));
                return GetByCriteria(criterionList);
            }
            
        }
        public class RoomDAO :
              AbstractPersistenceDao<vrmRoom, int>, IRoomDAO
        {
            public RoomDAO(string ConfigPath) : base(ConfigPath) { }
            public new List<vrmRoom> GetActive()
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Disabled", 0));//FB 1286 
                return GetByCriteria(criterionList);
            }
            public List<vrmRoom> GetByT3Id(int Id)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("L3LocationId", Id));
                criterionList.Add(Expression.Eq("Disabled", 0));//FB 1286
                return GetByCriteria(criterionList);
            }
            public List<vrmRoom> GetByT2Id(int Id)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Sql("L2LocationId=" + Id));
                //criterionList.Add(Expression.Eq("Disabled", 0)); //Fix for Glowpoint client
                return GetByCriteria(criterionList);
            }
            public vrmRoom GetByRoomId(int Id)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("RoomID",Id));
                List<vrmRoom> rmList = GetByCriteria(criterionList, true); //FB 2717
                if (rmList.Count < 1)
                    throw new Exception("No data matching to this roomID " + Id.ToString());
                return rmList[0];
            }
            //FB 2342 - Start
            public vrmRoom GetByRoomQueue(string RmEmail)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("RoomQueue", RmEmail)); //Need to change Email field
                List<vrmRoom> rmList = GetByCriteria(criterionList);
                if (rmList.Count < 1)
                    return null;
                    
                return rmList[0];
            }
            //FB 2342 - End
            //ZD 100196 - Start
            public vrmRoom GetByRoomUID(string RmUID)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("RoomUID", RmUID)); 
                List<vrmRoom> rmList = GetByCriteria(criterionList);
                if (rmList.Count < 1)
                    return null;

                return rmList[0];
            }
            //ZD 100196 - End
            //ZD 101879
            public vrmRoom GetByRoomName(string RmName)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Name", RmName)); 
                List<vrmRoom> rmList = GetByCriteria(criterionList);
                if (rmList.Count < 1)
                    return null;

                return rmList[0];
            }

           /* public bool isRoomVIP(int Id)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("RoomID", Id));
                criterionList.Add(Expression.Eq("isVIP", 1));
                List<vrmRoom> rmList = GetByCriteria(criterionList);
                if (rmList.Count < 1)
                    return false;
                return true;
            }*/


        }
        public class LocApprovDAO :
        AbstractPersistenceDao<vrmLocApprover, int>, ILocApprovDAO
        {
            public LocApprovDAO(string ConfigPath) : base(ConfigPath) { }


        }            
        public class LocDeptDAO :
         AbstractPersistenceDao<vrmLocDepartment, int>, ILocDeptDAO
        {
            public LocDeptDAO(string ConfigPath) : base(ConfigPath) { }
         
        }
        //FB 2392 -WhyGo
        public class ESPublicRoomDAO :
        AbstractPersistenceDao<ESPublicRoom, int>, IESPublicRoomDAO
        {
            public ESPublicRoomDAO(string ConfigPath) : base(ConfigPath) { }
            public List<ESPublicRoom> GetBymyVRMRoomId(ICollection Id)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Restrictions.In("RoomID", Id));
                List<ESPublicRoom> vrmRoomList = GetByCriteria(criterionList);
                if (vrmRoomList.Count < 1)
                {
                    return null;
                }
                return vrmRoomList;
            }
            public ESPublicRoom GetBymyVRMRoomId(int Id)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("RoomID", Id));
                List<ESPublicRoom> rmList = GetByCriteria(criterionList);
                if (rmList.Count < 1)
                    return null;

                return rmList[0];
            }
            public ESPublicRoom GetByWhygoRoomId(int Id)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("WhygoRoomId", Id));
                List<ESPublicRoom> rmList = GetByCriteria(criterionList);
                if (rmList.Count < 1)
                    return null;

                return rmList[0];
            }
        }    

        //ZD 101527 Starts
        public class SyncLocDAO :
        AbstractPersistenceDao<vrmSyncLoc, int>, ISyncLocDAO
        {
            public SyncLocDAO(string ConfigPath) : base(ConfigPath) { }
            public List<vrmSyncLoc> GetByOrgId(int Id)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("orgId", Id));
                List<vrmSyncLoc> vrmRoomList = GetByCriteria(criterionList);
                if (vrmRoomList.Count < 1)
                {
                    return null;
                }
                return vrmRoomList;
            }
            public vrmSyncLoc GetBymyVRMRoomId(int Id)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("RoomID", Id));
                List<vrmSyncLoc> rmList = GetByCriteria(criterionList);
                if (rmList.Count < 1)
                    return null;

                return rmList[0];
            }
        }    
        //ZD 101527 Ends

        //ZD 102123 Starts
        public class FloorRoomDAO :
        AbstractPersistenceDao<vrmFloorPlan, int>, IFloorRoomDAO
        {
            public FloorRoomDAO(string ConfigPath) : base(ConfigPath) { }
            public List<vrmFloorPlan> GetByOrgId(int Id)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("orgId", Id));
                List<vrmFloorPlan> vrmFloorRoomList = GetByCriteria(criterionList);
                if (vrmFloorRoomList.Count < 1)
                {
                    return null;
                }
                return vrmFloorRoomList;
            }
            public vrmFloorPlan GetByFloorRoomId(int Id)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Floorplanid", Id));
                List<vrmFloorPlan> rmList = GetByCriteria(criterionList);
                if (rmList.Count < 1)
                    return null;

                return rmList[0];
            }
        }
        //ZD 102123 Ends
    }

}