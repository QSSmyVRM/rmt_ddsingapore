﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
using System;
using System.Runtime.InteropServices;
using System.Net;
using System.IO;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Data;
using System.Xml;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Collections.Generic;
using System.ServiceProcess;


namespace myVRMRoomSyncService
{
    public partial class myVRMRoomSyncService : ServiceBase
    {
        System.Timers.Timer timerPollRPRMRoom = new System.Timers.Timer();
        System.Globalization.CultureInfo globalCultureInfo = new System.Globalization.CultureInfo("en-US", true);
        NS_LOGGER.Log log = null;
        VRMRTC.VRMRTC objRTC = null;
        String dirPth = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        String MyVRMServer_ConfigPath = "";
        String COM_ConfigPath = "";
        String RTC_ConfigPath = "";
        NS_CONFIG.Config config = null;
        NS_MESSENGER.ConfigParams configParams = null;
        string configPath = "";
        string errMsg = null;
        bool ret = false;
        //ASPIL.VRMServer myvrmCom = new ASPIL.VRMServer();
        public myVRMRoomSyncService()
        {
            MyVRMServer_ConfigPath = dirPth + "\\VRMSchemas\\";
            COM_ConfigPath = dirPth + "\\VRMSchemas\\COMConfig.xml";
            RTC_ConfigPath = dirPth + "\\VRMSchemas\\VRMRTCConfig.xml";
            configPath = dirPth + "\\VRMMaintServiceConfig.xml";

            InitializeComponent();
        }

        #region OnStart
        protected override void OnStart(string[] args)
        {
            double RPRMRoomPollInterval = 30000;//1800000 30000
            try
            {
                config = new NS_CONFIG.Config();
                configParams = new NS_MESSENGER.ConfigParams();
                ret = config.Initialize(configPath, ref configParams, ref errMsg, MyVRMServer_ConfigPath, RTC_ConfigPath); //FB 2944
                log = new NS_LOGGER.Log(configParams);

                log.Trace("**** Starting Myvrm Service ****" + DateTime.Now.ToLocalTime());
                log.Trace("Various Configs COM:" + COM_ConfigPath + " RTC:" + RTC_ConfigPath + " ASPIL:" + MyVRMServer_ConfigPath);
                log.Trace("Site URL: " + configParams.siteUrl);
                log.Trace("ActivationTimer:" + configParams.activationTimer);

                timerPollRPRMRoom.Elapsed += new System.Timers.ElapsedEventHandler(RPRMRoomPollInterval_Elapsed);
                timerPollRPRMRoom.Interval = RPRMRoomPollInterval;
                timerPollRPRMRoom.AutoReset = true;
                timerPollRPRMRoom.Start();

            }
            catch (Exception)
            {

            }

        }
        #endregion

        #region OnStop
        protected override void OnStop()
        {
            timerPollRPRMRoom.Enabled = false;
            timerPollRPRMRoom.AutoReset = false;
            timerPollRPRMRoom.Stop();
        }
        #endregion

        #region RPRMRoomPollInterval_Elapsed
        private void RPRMRoomPollInterval_Elapsed(object sender, EventArgs e)
        {
            try
            {

                
                System.Threading.Thread.CurrentThread.CurrentCulture = globalCultureInfo;             
                log.Trace("Into RPRM RoomSync :" + DateTime.Now.ToString("F"));
                timerPollRPRMRoom.AutoReset = false;
                RoomSyncThread();
                System.Threading.Thread.Sleep(5000);
                timerPollRPRMRoom.AutoReset = true; ; 

            }
            catch (Exception)
            {
            }

           

        }
        #endregion

        #region RoomSyncThread

        private void RoomSyncThread()
        {
            try
            {
                objRTC = new VRMRTC.VRMRTC();
                String dirPth = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location); 
                String InXML = "<Login><UserID>11</UserID><SeriveFilePath>" + dirPth + "</SeriveFilePath></Login>";
                String OutXML = objRTC.Operations(RTC_ConfigPath, "SyncRoom", InXML);
                if (OutXML.IndexOf("<success>") >= 0)
                    log.Trace("Room Sync command Lauched:" + DateTime.Now.ToString("F"));

                objRTC = null;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());

            }
        }

        #endregion
    }
}
